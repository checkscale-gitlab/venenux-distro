# Words with hyphens and dots which should be here
# but can't because of a limitation in the spellchecker
# They should be copied to languages lists
# and uncommented there
samba-doc
dhcp.conf
log.nmbd
log.smbd
passdb.tdb
smb.conf
