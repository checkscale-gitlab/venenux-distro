<?xml version="1.0" encoding="ISO-8859-1"?>
<!-- original version: 43965 -->
<!-- Revisado Rudy Godoy, 22 feb. 2005 -->
<!-- Revisado por Igor Tamara, enero 2007 -->

 <sect1 id="module-details">
 <title>Uso de componentes individuales</title>
<para>

En esta secci�n describiremos en detalle cada componente del
instalador. Los componentes han sido agrupados en etapas que los usuarios 
podr�n reconocer. �stos se presentan en el orden en
el que aparecen durante la instalaci�n. Note que no se usar�n todos 
los m�dulos en cada instalaci�n; los m�dulos que se usan realmente
dependen del m�todo de instalaci�n que use y de su hardware.

</para>

  <sect2 id="di-setup">
  <title>Configurar el instalador de Debian y configuraci�n de hardware</title>
<para>

Asumamos que el instalador de Debian ha arrancado y est� visualizando
su pantalla inicial. En este momento, las capacidades del &d-i; son
todav�a algo limitadas. �ste no conoce mucho sobre su hardware, idioma
preferido, o incluso la tarea que deber� realizar. No se preocupe.
Porque &d-i; es bastante intuitivo, puede autom�ticamente explorar su
hardware, localizar el resto de sus componentes y autoactualizarse
a un programa moderno y bien construido.

Sin embargo, todav�a deber� ayudar al &d-i; suministr�ndole la informaci�n
que no puede determinar autom�ticamente (como elegir su idioma preferido,
el mapa del teclado o el servidor de r�plica deseado).

</para><para>

Notar� que &d-i; realiza la <firstterm>detecci�n de hardware</firstterm>
varias veces durante esta etapa. La primera vez se enfoca espec�ficamente
en el hardware requerido para cargar los componentes del instalador (como
su CD-ROM o tarjeta de red). En vista de que no todos los controladores
podr�an estar disponibles en esta primera ejecuci�n, la detecci�n de
hardware necesita repetirse despu�s, durante el proceso.

</para>

&module-lowmem.xml;
&module-localechooser.xml;
&module-kbd-chooser.xml;
&module-s390-netdevice.xml;
&module-ddetect.xml;
&module-cdrom-detect.xml;
&module-iso-scan.xml;
&module-anna.xml;
&module-netcfg.xml;
&module-choose-mirror.xml;

  </sect2>

  <sect2 id="di-partition">
  <title>Particionado y elecci�n de punto de montaje</title>
<para>

En este momento, despu�s de que ha sido ejecutada la detecci�n de hardware
por �ltima vez, &d-i; deber� estar en su total capacidad, adaptado para
las necesidades del usuario y listo para realizar el verdadero trabajo.

Como lo indica el t�tulo de esta secci�n, la tarea principal de los
pr�ximos componentes radica en particionar sus discos, crear sistemas
de ficheros, asignar puntos de montaje y opcionalmente configurar
temas estrechamente relacionados como LVM o dispositivos RAID.

</para>

&module-s390-dasd.xml;
&module-partman.xml;
&module-autopartkit.xml;
&module-partitioner.xml;
&module-partconf.xml;
&module-mdcfg.xml;
&module-partman-lvm.xml;
&module-partman-crypto.xml;
  </sect2>


  <sect2 id="di-system-setup">
  <title>Configurar el sistema</title>
<para>

Despu�s de particionar el instalador realizar� unas preguntas adicionales
que se utilizar�n para configurar el sistema que se va a instalar.

</para>

&module-tzsetup.xml;
&module-clock-setup.xml;
&module-user-setup.xml;
</sect2>

  <sect2 id="di-install-base">
  <title>Instalar el sistema base</title>
<para>

Aunque esta etapa es la menos problem�tica, consume una gran parte del
tiempo de instalaci�n debido a que descarga, verifica y desempaqueta
el sistema base completo. Si tiene un ordenador o conexi�n de red
lentos, esto podr�a tomar alg�n tiempo.

</para>

&module-base-installer.xml;
  </sect2>

  <sect2 id="di-install-software">
  <title>Instalar programas adicionales</title>
<para>

Una vez se haya instalado el sistema base tendr� un sistema usable
pero limitado. La mayor�a de los usuarios querr�n instalar programas adicionales
en el sistema para ajustarlo a sus necesidades, y el instalador le permite 
hacer esto. Este paso puede tardar m�s tiempo que la instalaci�n del sistema
base si tiene un ordenador lento o su red es lenta.

</para>
&module-apt-setup.xml;
&module-pkgsel.xml;
  </sect2>


  <sect2 id="di-make-bootable">
  <title>Hacer su sistema arrancable</title>

<para condition="supports-nfsroot">

Si est� instalando una estaci�n de trabajo sin disco, obviamente,
arrancar desde el disco local no es una opci�n significativa, de modo
que esta etapa se saltar�. <phrase arch="sparc">Tal vez quiera
configurar �OpenBoot� para arrancar desde red en forma predeterminada,
vea <xref linkend="boot-dev-select-sun"/>.</phrase>

</para>

&module-os-prober.xml;
&module-alpha-aboot-installer.xml;
&module-hppa-palo-installer.xml;
&module-x86-grub-installer.xml;
&module-x86-lilo-installer.xml;
&module-ia64-elilo-installer.xml;
&module-mips-arcboot-installer.xml;
&module-mipsel-colo-installer.xml;
&module-mipsel-delo-installer.xml;
&module-powerpc-yaboot-installer.xml;
&module-powerpc-quik-installer.xml;
&module-s390-zipl-installer.xml;
&module-sparc-silo-installer.xml;
&module-nobootloader.xml;
  </sect2>

  <sect2 id="di-finish">
  <title>Finalizar la instalaci�n</title>
<para>

Estas son las �ltimas cosas a hacer antes de reiniciar a su nuevo
sistema. En su mayor�a consiste en ordenar despu�s del &d-i;.

</para>

&module-finish-install.xml;
  </sect2>

  <sect2 id="di-miscellaneous">
  <title>Miscel�nea</title>
<para>

Los componentes listados en esta secci�n usualmente no est�n involucrados
en el proceso de instalaci�n, pero est�n esperando en el segundo plano
para ayudar al usuario en caso de que algo falle.

</para>

&module-save-logs.xml;
&module-cdrom-checker.xml;
&module-shell.xml;
&module-network-console.xml;
  </sect2>
 </sect1>
