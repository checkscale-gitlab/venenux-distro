<?xml version="1.0" encoding="ISO-8859-1"?>
<!-- original version: 43789 -->


 <sect2 arch="alpha" id="alpha-firmware">
 <title>Le microprogramme Alpha Console</title>
<para>

Le microprogramme pour la console est stock� dans une ROM flash. Il est
lanc� lorsqu'un syst�me Alpha est allum� ou remis � z�ro. Il y a deux sortes 
de sp�cifications utilis�es sur les syst�mes Alpha et ainsi deux classes du 
microprogramme console disponibles&nbsp;:

</para>

<itemizedlist>
<listitem><para>

La <emphasis>console SRM</emphasis> bas�e sur une sp�cification du
sous-syst�me console Alpha et qui fournit un environnement op�rationnel pour 
OpenVMS, Tru64 UNIX et les syst�mes d'exploitation de type Linux&nbsp;;

</para></listitem>
<listitem><para>

Les <emphasis>consoles ARC, AlphaBIOS ou ARCSBIOS</emphasis>,
bas�es sur les sp�cifications <quote>Advanced RISC Computing</quote>(ARC)
qui fournissent un environnement op�rationnel pour Windows NT.

</para></listitem>
</itemizedlist>

<para>

Du point de vue de l'utilisateur, la plus grosse diff�rence entre SRM et ARC 
est que le choix de la console induit le sch�ma de partitionnement du disque 
sur lequel vous choisirez d'amorcer.

</para><para>

ARC requiert l'utilisation d'une table de partition MS-DOS
(cr��e par <command>cfdisk</command>) pour le disque amor�able. Les tables 
des partitions MS-DOS sont donc les formats de partition <quote>natifs</quote> 
lorsque l'on amorce avec ARC. En effet, depuis qu'AlphaBIOS contient un 
partititionneur de disque, vous pr�f�rerez peut-�tre partitionner vos disques 
depuis le menu du microprogramme avant d'installer Linux.
</para>
<para>

Inversement, SRM est <emphasis>incompatible</emphasis> avec les tables des
partitions MS-DOS
<footnote><para>Pr�cis�ment, le format du secteur d'amor�age requis par la 
sp�cification du sous-syst�me console entre en conflit avec le placement de 
la table des partitions MS-DOS.</para></footnote>. 
Depuis que Unix Tru64 utilise le format des disques BSD, c'est le format 
<quote>natif</quote> des partitions pour les installations � partir de SRM.

</para><para>

GNU/Linux est le seul syst�me d'exploitation sur Alpha qui peut �tre 
amorc� � partir des deux types de console, mais la version &debian; &release;
ne peut s'amorcer qu'� partir d'un syst�me bas� sur SRM. Si vous poss�dez un
Alpha sans SRM, si vous voulez pouvoir lancer aussi Windows NT ou si votre
programme d'amor�age demande une console ARC pour initialiser le BIOS, vous
ne pourrez pas utiliser l'installateur Debian. Vous pourrez toujours lancer
&debian; &release; sur de tels syst�mes en utilisant un autre support
d'installation. Par exemple, vous pouvez installer Debian woody et faire une 
mise � jour.
</para>
<para>
Comme <command>MILO</command> n'est pas disponible sur tous les syst�mes
Alpha actuels (F�vrier 2000) et comme il n'est plus n�cessaire d'acheter
une license OpenVMS ou Tru64 Unix pour avoir un microprogramme SRM  sur les
anciens Alpha, il est recommand� d'utiliser SRM d�s que possible.

</para><para>

Le tableau suivant r�sume les combinaisons type de syst�me / console 
disponibles (cf. <xref linkend="alpha-cpus"/> pour les noms des types de 
syst�me). Le mot <quote>ARC</quote> ci-dessous d�signe toute console compatible
      avec ARC.

</para><para>

<informaltable><tgroup cols="2">
<thead>
<row>
  <entry>Syst�me</entry>
  <entry>Console accept�e</entry>
</row>
</thead>

<tbody>
<row>
  <entry>alcor</entry>
  <entry>ARC ou SRM</entry>
</row><row>
  <entry>avanti</entry>
  <entry>ARC ou SRM</entry>
</row><row>
  <entry>book1</entry>
  <entry>SRM</entry>
</row><row>
  <entry>cabriolet</entry>
  <entry>ARC ou SRM</entry>
</row><row>
  <entry>dp264</entry>
  <entry>SRM</entry>
</row><row>
  <entry>eb164</entry>
  <entry>ARC ou SRM</entry>
</row><row>
  <entry>eb64p</entry>
  <entry>ARC ou SRM</entry>
</row><row>
  <entry>eb66</entry>
  <entry>ARC ou SRM</entry>
</row><row>
  <entry>eb66p</entry>
  <entry>ARC ou SRM</entry>
</row><row>
  <entry>jensen</entry>
  <entry>SRM</entry>
</row><row>
  <entry>lx164</entry>
  <entry>ARC ou SRM</entry>
</row><row>
  <entry>miata</entry>
  <entry>ARC ou SRM</entry>
</row><row>
  <entry>mikasa</entry>
  <entry>ARC ou SRM</entry>
</row><row>
  <entry>mikasa-p</entry>
  <entry>SRM</entry>
</row><row>
  <entry>nautilus</entry>
  <entry>ARC (voyez le manuel de la carte m�re) ou SRM</entry>
</row><row>
  <entry>noname</entry>
  <entry>ARC ou SRM</entry>
</row><row>
  <entry>noritake</entry>
  <entry>SRM</entry>
</row><row>
  <entry>noritake-p</entry>
  <entry>SRM</entry>
</row><row>
  <entry>pc164</entry>
  <entry>ARC ou SRM</entry>
</row><row>
  <entry>rawhide</entry>
  <entry>SRM</entry>
</row><row>
  <entry>ruffian</entry>
  <entry>ARC</entry>
</row><row>
  <entry>sable</entry>
  <entry>SRM</entry>
</row><row>
  <entry>sable-g</entry>
  <entry>SRM</entry>
</row><row>
  <entry>sx164</entry>
  <entry>ARC ou SRM</entry>
</row><row>
  <entry>takara</entry>
  <entry>ARC ou SRM</entry>
</row><row>
  <entry>xl</entry>
  <entry>ARC</entry>
</row><row>
  <entry>xlt</entry>
  <entry>ARC</entry>
</row>

</tbody>
</tgroup>
</informaltable>

</para><para>

D'une fa�on g�n�rale, aucune de ces consoles ne peut amorcer Linux 
directement&nbsp;; il est donc n�cessaire d'avoir recours � un programme 
d'amor�age interm�diaire. Pour la console SRM, on utilise 
<command>aboot</command>, un programme d'amor�age l�ger et ind�pendant de la
plateforme. Voyez l' obsol�te <ulink url="&url-srm-howto;">SRM HOWTO</ulink> 
pour d'autres informations. 
</para>
<para condition="FIXME">
Les paragraphes suivants proviennent du manuel d'installation de la version 
Woody. Ils sont l� pour r�f�rence et seront peut-�tre utiles quand Debian
pourra utiliser � nouveau MILO.
</para>

<para condition="FIXME">
D'une fa�on g�n�rale, aucune de ces consoles ne peut amorcer Linux
directement&nbsp;; il est donc n�cessaire d'avoir recours � un programme
d'amor�age interm�diaire. Il existe deux programmes d'amor�age 
principaux&nbsp;: <command>MILO</command> and <command>aboot</command>.
</para>
<para>
<command>MILO</command> est lui-m�me une console, qui remplace ARC ou
SRM en m�moire. <command>MILO</command> peut �tre amorc� � la fois par ARC et 
SRM et c'est m�me la seule fa�on de d�marrer Linux � partir d'une console 
ARC. <command>MILO</command> est d�pendant de la plateforme (un programme 
diff�rent de <command>MILO</command> est n�cessaire pour chaque syst�me) et 
n'existe, pour le support ARC, que pour les syst�mes list�s ci-dessus. 
Reportez-vous aussi au <ulink url="&url-milo-howto;">MILO HOWTO</ulink>, 
malheureusement plus tr�s � jour.

</para><para>

<command>aboot</command> est un petit programme d'amor�age, ind�pendant
de la plateforme et qui fonctionne uniquement avec SRM. Reportez-vous au 
<ulink url="&url-srm-howto;">SRM HOWTO</ulink> (h�las plus tr�s � jour) pour 
obtenir d'autres informations sur <command>aboot</command>.

</para><para condition="FIXME">

Vous pouvez donc �tre confront� � trois types de sc�nario possibles, selon le 
microprogramme de la console du syst�me et selon que <command>MILO</command> 
est disponible ou non&nbsp;:

<informalexample><screen>
SRM -&gt; aboot
SRM -&gt; MILO
ARC -&gt; MILO
</screen></informalexample>

Comme <command>MILO</command> n'est disponible pour aucun des syst�mes Alpha 
actuellement en production (f�vrier 2000) et comme il n'est plus n�cessaire 
d'acheter une licence OpenVMS ou Unix Tru64 pour avoir le microprogramme sur 
votre vieil Alpha, il est recommand� d'utiliser SRM et 
<command>aboot</command> pour une nouvelle installation de GNU/Linux, � moins 
que vous ne d�siriez pouvoir amorcer aussi Windows NT.

</para><para>

La majorit� des AlphaServer, tous les serveurs et tous les postes de travail 
contiennent � la fois SRM et AlphaBIOS dans leur microprogramme. Pour les 
machines <quote>half-flash</quote> comme les nombreuses cartes d'�valuation, 
il est possible de passer d'une version � l'autre par flashage du 
microprogramme. Ainsi, lorsque SRM est install�, il est possible d'ex�cuter
ARC/AlphaBIOS depuis une disquette (en utilisant la commande
<command>arc</command>). Pour les raisons mentionn�es pr�c�demment, nous 
recommandons de passer � SRM avant d'installer Debian.

</para><para>

Comme pour toutes les autres architectures, vous devez installer la r�vision 
la plus r�cente du microprogramme
<footnote>
<para>� l'exception de Jensen, o� 
les versions du microprogramme plus r�centes que la version 1.7 
ne reconnaissent pas linux &mdash; voyez 
<ulink url="&url-jensen-howto;"></ulink> pour plus d'informations.
</para></footnote>
 avant d'installer Debian. Pour les Alpha, la mise � jour 
du microprogramme peut s'obtenir � partir des 
<ulink url="&url-alpha-firmware;">mises � jour du Firmware Alpha</ulink>.

</para>
 </sect2>


  <sect2 arch="alpha"><title>Amorcer avec TFTP</title>
<para>

Pour SRM, les interfaces Ethernet sont nomm�es
avec le pr�fixe <userinput>ewa</userinput> et seront affich�es en sortie de
la commande <userinput>show dev</userinput>, comme ceci&nbsp;:

<informalexample><screen>
&gt;&gt;&gt; show dev
ewa0.0.0.9.0               EWA0              08-00-2B-86-98-65
ewb0.0.0.11.0              EWB0              08-00-2B-86-98-54
ewc0.0.0.2002.0            EWC0              00-06-2B-01-32-B0
</screen></informalexample>

Vous devez commencer par configurer le protocole d'amor�age&nbsp;:

<informalexample><screen>
&gt;&gt;&gt; set ewa0_protocols bootp
</screen></informalexample>

puis v�rifier que le type du m�dia est correct&nbsp;:

<informalexample><screen>
&gt;&gt;&gt; set ewa0_mode <replaceable>mode</replaceable>
</screen></informalexample>

Vous pouvez obtenir une liste de modes valides avec 
<userinput>&gt;&gt;&gt;set ewa0_mode</userinput>.

</para><para>

Ensuite, pour amorcer sur la premi�re interface Ethernet, vous
        devrez taper&nbsp;:

<informalexample><screen>
&gt;&gt;&gt; boot ewa -flags ""
</screen></informalexample>

L'amor�age se fera avec les param�tres par d�faut du noyau qui sont dans
l'image netboot.
</para>
<para>

Si vous souhaitez utiliser une console s�rie, vous
<emphasis>devez</emphasis> passer le param�tre <userinput>console=</userinput>
au noyau. Cela peut �tre fait avec l'argument 
<userinput>-flags</userinput> de la commande SRM <userinput>boot</userinput>. 
Les ports s�rie portent le m�me nom que leur fichier correspondant dans
<userinput>/dev</userinput>. Et si vous ajoutez des param�tres du noyau, vous
devez r�p�ter certaines options par d�faut n�cessaires aux images de 
l'installateur Debian.
Par exemple, pour amorcer depuis 
<userinput>ewa0</userinput>
        en utilisant la console sur le premier port s�rie, vous devrez
        saisir&nbsp;:

<informalexample><screen>
&gt;&gt;&gt; boot ewa0 -flags &quot;root=/dev/ram ramdisk_size=16384 console=ttyS0&quot;
</screen></informalexample>

</para>
  </sect2>

  <sect2 arch="alpha"><title>Amorcer depuis un c�d�rom avec une console SRM
</title>
<para>

Les c�d�roms d'installation de &debian; comprennent plusieurs options pr�configur�es
pour amorcer � partir de consoles s�rie ou VGA.
Ex�cutez&nbsp;:

<informalexample><screen>
&gt;&gt;&gt; boot xxxx -flags 0
</screen></informalexample>

pour amorcer avec une console VGA,
o� <replaceable>xxxx</replaceable> est votre lecteur de c�d�rom 
en notation SRM.

Pour utiliser une console s�rie sur le premier ph�riph�rique s�rie,
ex�cutez&nbsp;:

<informalexample><screen>
&gt;&gt;&gt; boot xxxx -flags 1
</screen></informalexample>

Pour une console sur le deuxi�me port s�rie, ex�cutez&nbsp;:

<informalexample><screen>
&gt;&gt;&gt; boot xxxx -flags 2
</screen></informalexample>
 
</para>
  </sect2>

  <sect2 arch="alpha" condition="FIXME">
  <title>Amorcer depuis un c�d�rom avec une console ARC ou AlphaBIOS</title>
<para>

Pour amorcer un c�d�rom depuis la console ARC, trouvez le nom de
code de votre sous-architecture (voyez&nbsp;:
<xref linkend="alpha-cpus"/>, puis entrez 
<filename>\milo\linload.exe</filename>
      comme programme d'amor�age et
 <filename>\milo\<replaceable>subarch</replaceable></filename> 
(o� <replaceable>subarch</replaceable>
      est le nom de la sous-architecture) comme chemin vers
      le syst�me d'exploitation dans le
      menu <quote>OS Selection Setup</quote>. Les
      <quote>Ruffians</quote> font exception&nbsp;: vous aurez besoin
      d'utiliser <filename>\milo\ldmilo.exe</filename> comme programme
      d'amor�age.

</para>
  </sect2>


  <sect2 arch="alpha" condition="supports-floppy-boot">
  <title>Amorcer � partir de disquette avec la console SRM</title>
<para>

� l'invite (<prompt>&gt;&gt;&gt;</prompt>) de SRM, ex�cutez la
       commande suivante&nbsp;:

<informalexample><screen>
&gt;&gt;&gt; boot dva0 -flags 0
</screen></informalexample>

en rempla�ant <filename>dva0</filename> avec le p�riph�rique en service
       sur votre machine. D'habitude, <filename>dva0</filename> est le lecteur
       de disquette&nbsp;; tapez

<informalexample><screen>
&gt;&gt;&gt; show dev
</screen></informalexample>

pour afficher la liste des p�riph�riques (si vous d�sirez
amorcer par exemple depuis un c�d�rom). Remarquez que si vous
amorcez via MILO, l'argument <command>-flags</command> est ignor�,
donc vous pouvez simplement saisir <command>boot dva0</command>.
Si tout se d�roule normalement, vous devriez voir l'amor�age
du noyau Linux.

</para><para>

Si vous d�sirez sp�cifier des arguments pour le noyau en amor�ant
via <command>aboot</command>, utilisez la commande suivante&nbsp;:

<informalexample><screen>
&gt;&gt;&gt; boot dva0 -file linux.bin.gz -flags "root=/dev/fd0 load_ramdisk=1 arguments"
</screen></informalexample>

(saisissez sur une seule ligne) en substituant, si n�cessaire, le
nom de p�riph�rique d'amor�age SRM utilis� pour
<filename>dva0</filename>, le nom de p�riph�rique d'amor�age de Linux
pour <filename>fd0</filename> et les param�tres noyau d�sir�s pour
<filename>arguments</filename>.

</para><para>

Si vous d�sirez sp�cifier des param�tres au noyau lors d'un
amor�age via <command>MILO</command>, il vous faudra interrompre le
chargement une fois entr� dans MILO. Voyez <xref linkend="booting-from-milo"/>.

</para>
  </sect2>


  <sect2 arch="alpha">
  <title>Amorcer depuis les disquettes avec une console ARC ou ALphaBIOS</title>

<para>

Dans le menu de <quote>OS selection</quote>, choisissez
<command>linload.exe</command> comme programme d'amor�age et
<command>milo</command> comme chemin vers le syst�me
d'exploitation. L'amor�age se fera depuis cette nouvelle entr�e.

</para>
  </sect2>


 <sect2 arch="alpha" condition="FIXME" id="booting-from-milo">
<title>Amorcer avec MILO</title>
<para>

Le programme MILO, contenu dans le m�dia d'amor�age, est configur�
pour amorcer automatiquement Linux. Si vous d�sirez intervenir, 
il vous suffit d'appuyer sur la touche d'espacement durant le compte � rebours de MILO.

</para><para>

Si vous d�sirez tout sp�cifier (pour fournir par exemple des param�tres 
suppl�mentaires), vous pouvez utiliser une commande de ce type&nbsp;:

<informalexample><screen>
MILO&gt; boot fd0:linux.bin.gz root=/dev/fd0 load_ramdisk=1 <!-- arguments -->
</screen></informalexample>

Si vous amorcez sur un autre support qu'une disquette,
substituez <filename>fd0</filename> dans l'exemple ci-dessus par le nom
de p�riph�rique appropri� dans la nomenclature Linux. La
commande <command>help</command> vous fournira un rapide aper�u des
commandes MILO.

</para>
 </sect2>
