<?xml version="1.0" encoding="ISO-8859-1"?>
<!-- original version: 43693 -->

 <sect1 id="network-cards">
<title>Mat�riel de connexion r�seau</title>
<para>

Toute carte r�seau (<emphasis>NIC, network interface card</emphasis>)
reconnue par le noyau Linux devrait aussi �tre reconnue par l'installateur.
Les pilotes r�seau sont charg�s sous forme de module.

<phrase arch="x86">La plupart des cartes PCI et PCMCIA sont reconnues.</phrase>
<phrase arch="i386">Beaucoup d'anciennes cartes ISA sont aussi reconnues.</phrase>

<phrase arch="m68k">Voyez <ulink url="&url-m68k-faq;"></ulink>
pour des pr�cisions.</phrase>
</para>

<para arch="sparc">
Beaucoup de cartes PCI g�n�riques sont reconnues, ainsi que les cartes SUN suivantes&nbsp;:

<itemizedlist>
<listitem><para>

Sun LANCE

</para></listitem>
<listitem><para>

Sun Happy Meal

</para></listitem>
<listitem><para>

Sun BigMAC

</para></listitem>
<listitem><para>

Sun QuadEthernet

</para></listitem>
<listitem><para>

MyriCOM Gigabit Ethernet

</para></listitem>
</itemizedlist>

</para>

<para arch="mipsel">
� cause des limitations du noyau, seules les interfaces r�seau sur
DECstations sont reconnues, les cartes r�seau optionnelles
TurboChannel ne fonctionnent pas pour le moment.
</para>

<para arch="s390">
La liste des cartes r�seau reconnues est&nbsp;:

<itemizedlist>
 <listitem><para>

Channel to Channel (CTC) et ESCON connection (r�elle ou �mul�e).

</para></listitem>
 <listitem><para>

OSA-2 Token Ring/Ethernet et OSA-Express Fast Ethernet (non-QDIO)

</para></listitem>
<listitem><para>

OSA-Express in QDIO mode, HiperSockets et Guest-LANs

</para></listitem>
</itemizedlist>

</para>

<para arch="arm">

Sur &arch-title;, la plupart des p�riph�riques Ethernet int�gr�s sont reconnus
et des modules pour les p�riph�riques USB et PCI sont fournis. L'exception
est la plateforme IXP4xx (avec par exemple le Linksys NSLU2) qui a besoin d'un microcode
propri�taire pour faire fonctionner le p�riph�rique Ethernet int�gr�.
Des images non officielles pour le Linksys NSLU2 existent sur le site
<ulink url="&url-slug-firmware;">Slug-Firmware</ulink>.

</para>

<para arch="x86">
Pour ce qui est du RNIS, le protocole sur canal D pour l'1TR6
allemand n'est pas reconnu&nbsp;; les cartes Spellcaster BRI
ISDN ne sont pas reconnues non plus par l'installateur Debian.
L'utilisation du RNIS n'est pas accept� pendant l'installation.
</para>

  <sect2 arch="not-s390" id="nics-firmware">
  <title>Pilotes demandant des microcodes (<emphasis>Firmware</emphasis>)</title>
<para>

Le syst�me d'installation ne peut pas pour l'instant rechercher
les microcodes (<emphasis>firmware</emphasis>). Toute carte qui demande de t�l�charger un
microcode n'est pas reconnue par d�faut.
</para>
<para>
Si vous ne pouvez pas utiliser d'autre carte pour l'installation, vous pouvez
toujours installer &debian; avec un c�d�rom. Choisissez de ne pas
configurer le r�seau et installez uniquement les paquets qui sont sur le c�d�rom.
Vous pourrez ensuite t�l�charger le pilote et le microcode
une fois l'installation termin�e (apr�s le red�marrage) et configurer le r�seau.
Il faut noter que le pilote et le microcode peuvent ne pas �tre
dans le m�me paquet ni �tre pr�sents dans l'archive <quote>main</quote> de Debian.
</para>
<para>
Si le pilote lui-m�me est reconnu, vous pouvez utiliser la carte en copiant
le microcode sur <filename>/usr/lib/hotplug/firmware</filename>.
N'oubliez pas de le copier aussi � cet endroit pour le syst�me install�
avant le red�marrage, � la fin du processus d'installation.
</para>
</sect2>

<sect2 condition="supports-wireless" id="nics-wireless">
  <title>Cartes r�seau sans fil</title>
<para>

Les cartes r�seau sans fil sont reconnues, mais il existe une grosse restriction.
Beaucoup d'adaptateurs demandent des pilotes qui ne sont pas libres ou qui ne sont pas
accept�s dans le noyau officiel Linux. On peut faire fonctionner ces cartes sous
&debian;, mais elles ne sont pas g�r�es pendant l'installation.
</para>

<para>
Si vous ne pouvez pas utiliser d'autre carte pour l'installation, vous pouvez
toujours installer &debian; avec un c�d�rom. Utilisez la proc�dure d�crite pr�c�demment
pour les cartes qui demandent un firmware.
</para>
<para>
Parfois, le pilote n�cessaire n'est pas disponible sous forme de paquet Debian.
Il faudra chercher le code source sur internet et le compiler vous-m�me.
Comment proc�der d�passe le cadre de ce manuel.
<phrase arch="x86">S'il n'existe pas de pilote pour Linux, vous pouvez
toujours utiliser le paquet <classname>ndiswrapper</classname> qui permet
d'installer un pilote Windows.</phrase>

</para>
  </sect2>

  <sect2 arch="sparc" id="nics-sparc-trouble">
  <title>Probl�mes connus sur &arch-title;</title>
<para>

Quelques cartes r�seau sp�ciales posent des probl�mes qui m�ritent d'�tre
mentionn�s.

</para>

   <sect3><title>Conflits entre les pilotes tulip et dfme</title>
<!-- BTS: #334104; may also affect other arches, but most common on sparc -->
<para>

<!-- BTS: #334104; may also affect other arches, but most common on sparc -->

Certaines cartes PCI ont la m�me identit� mais sont g�r�es par des pilotes
diff�rents. Certaines sont g�r�es par le pilote <literal>tulip</literal> et d'autres
par le pilote <literal>dfme</literal>. Comme elles ont la m�me identit�, le noyau
ne les distingue pas et peut charger le mauvais pilote.
</para>
<para>

C'est un probl�me connu sur les syst�mes Netra avec une carte Davicom (DEC-Tulip).
Dans ce cas le pilote <literal>tulip</literal> est correct.

Pendant l'installation, la solution est de passer sur un shell et de supprimer
le mauvais pilote (ou les deux, si les deux sont charg�s) avec
<userinput>modprobe -r <replaceable>module</replaceable></userinput>.
Ensuite il suffit de charger le bon pilote avec
<userinput>modprobe <replaceable>module</replaceable></userinput>.
</para>
</sect3>
   <sect3><title>Sun B100 blade</title>
<!-- BTS: #384549; should be checked for kernels >2.6.18 -->
<para>
Le pilote r�seau <literal>cassini</literal> ne fonctionne pas avec les
syst�mes Sun B100 blade.
</para>
</sect3>
</sect2>

 </sect1>
