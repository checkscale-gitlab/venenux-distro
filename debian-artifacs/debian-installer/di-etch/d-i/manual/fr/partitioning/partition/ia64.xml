<?xml version="1.0" encoding="ISO-8859-1"?>
<!-- original version: 43254 -->

  <sect2 arch="ia64"><title>Partitionnement pour &arch-title;</title>
<para>
<command>Partman</command> est l'outil de partitionnement par d�faut de 
l'installateur. Il permet de cr�er les partitions et de positionner les 
points de montage pour s'assurer que les disques et les syst�mes de fichiers 
seront correctement configur�s lors de l'installation. Il utilise le programme 
<command>parted</command> pour cr�er les partitions du disque.

</para>

   <note>
   <title>Formats reconnu par EFI</title>
<para>
Le microprogramme (<emphasis>firmware</emphasis>) EFI pour IA-64 accepte deux 
formats de table de partitions (ou d'�tiquette disque), GPT et MS-DOS.
MS-DOS est le format classique des PC i386, et n'est pas recommand�
pour les syst�mes IA-64. Bien que l'installateur fournisse aussi l'utilitaire 
<command>cfdisk</command>, vous devez utiliser uniquement 
<ulink url="parted.txt"><command>parted</command></ulink>, parce qu'il est le 
seul capable de g�rer correctement � la fois les tables GPT et MS-DOS.

</para></note>

<para>

Les r�gles de partitionnement automatique de <command>partman</command> 
attribuent la premi�re partition du disque � EFI.
Vous pouvez �galement installer la partition depuis le menu principal 
sous <guimenuitem>Partitionnement assist�</guimenuitem> de la m�me mani�re 
que vous avez install� la partition d'�change (<emphasis>swap</emphasis>).

</para><para>

<command>Partman</command> est capable de g�rer la plupart des disques.
Pour les rares cas o� il est n�cessaire d'installer manuellement un disque, 
vous pouvez ex�cuter l'utilitaire <command>parted</command> directement en 
utilisant son interface en ligne de commande. En supposant que vous d�siriez 
effacer tout votre disque, et cr�er une table GPT et des partitions, 
les commandes suivantes peuvent �tre utilis�es&nbsp;:

<informalexample><screen>
      mklabel gpt
      mkpartfs primary fat 0 50
      mkpartfs primary linux-swap 51 1000
      mkpartfs primary ext2 1001 3000
      set 1 boot on
      print
      quit
</screen></informalexample>

Ces commandes cr�ent une nouvelle table de partitions, et trois partitions,
utilis�es en tant que partition d'amor�age EFI, partition d'�change, et
syst�me de fichiers racine. Enfin, on positionne le drapeau de boot
sur la partition EFI.
Les partitions sont sp�cifi�es en Mo, avec les d�calages de d�but et
de fin � partir du d�but du disque. Ainsi, par exemple, nous avons
cr�� ci-dessus un syst�me de fichier ext2 de 1999&nbsp;Mo, d�marrant �
1001&nbsp;Mo du d�but du disque.
Notez que le formatage de l'espace d'�change avec <command>parted</command> 
peut prendre plusieurs minutes, car il cherche � d�tecter les blocs d�fectueux 
de la partition.

</para>
  </sect2>

  <sect2 arch="ia64"><title>Partition n�cessaire pour le programme d'amor�age</title>

<para>

ELILO, le programme d'amor�age pour IA-64, n�cessite une partition
contenant un syst�me FAT avec le drapeau <userinput>boot</userinput>
positionn�. La partition doit �tre assez grande pour contenir le programme
d'amor�age et tous les noyaux et les images des disques virtuels que vous 
voudrez utiliser pour d�marrer. Une taille minimale serait de 20&nbsp;Mo, 
mais si vous souhaitez avoir diff�rents noyaux, 128&nbsp;Mo est s�rement
plus adapt�.

</para><para>

le programme d'amor�age EFI et le shell EFI g�rent tous deux les tables 
GPT, donc la partition de d�marrage ne doit pas n�cessairement �tre la 
premi�re partition ni m�me �tre sur le m�me disque.
C'est pratique si vous avez oubli� d'allouer une partition et
que vous vous en aperceviez seulement apr�s avoir format� les autres 
partitions de vos disques. L'utilitaire <command>partman</command> v�rifie
la partition EFI en m�me temps qu'il v�rifie que l'installation de la partition
racine (<emphasis>root</emphasis>) s'est correctement pass�e.
Cela vous donne une occasion de corriger la disposition du disque avant que 
l'installation des paquets commence.
La mani�re la plus facile de corriger cette omission est de r�tr�cir la 
derni�re partition du disque afin d'obtenir suffisamment d'espace libre et de 
pouvoir rajouter une partition EFI.

</para><para>

Il est fortement recommand� que votre partition d'amor�age EFI soit sur le 
m�me disque que la racine de votre syt�me de fichiers.
</para>
  </sect2>


    <sect2 arch="ia64"><title>Partitions de diagnostic EFI</title>

<para>

Le microprogramme EFI est sensiblement plus sophistiqu� que le BIOS habituel
qu'on trouve sur les PC x86.
Quelques fournisseurs de syst�me tirent profit des capacit�s du microprogramme 
EFI � acc�der aux fichiers et � ex�cuter des programmes depuis un syst�me de
fichiers pour stocker sur le disque des utilitaires de diagnostics et d'administration 
syst�me bas�s sur EFI. Ils sont sur une partition s�par�e de type FAT sur le 
disque syst�me.
Reportez-vous � la documentation et aux accessoires fournis avec votre syst�me
pour plus de d�tails.
Le moment le plus appropri� pour installer une partition de diagnostic est au 
moment de l'installation de la partition de d�marrage EFI.

</para>

   </sect2>
