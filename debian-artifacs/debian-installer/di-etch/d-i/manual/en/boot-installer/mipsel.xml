<!-- retain these comments for translator revision tracking -->
<!-- $Id: mipsel.xml 35612 2006-03-19 03:34:30Z tbm $ -->

  <sect2 arch="mipsel" id="boot-tftp"><title>Booting with TFTP</title>

   <sect3>
   <title>Cobalt TFTP Booting</title>
<para>

Strictly speaking, Cobalt does not use TFTP but NFS to boot.  You need to
install an NFS server and put the installer files in
<filename>/nfsroot</filename>.  When you boot your Cobalt, you have to
press the left and the right cursor buttons at the same time and the
machine will boot via the network from NFS.  It will then display several
options on the display.  There are the following two installation methods:

<itemizedlist>
<listitem><para>

Via SSH (default): In this case, the installer will configure the network
via DHCP and start an SSH server.  It will then display a random password
and other login information (such as the IP address) on the Cobalt LCD.
When you connect to the machine with an SSH client you can start with
the installation.

</para></listitem>
<listitem><para>

Via serial console: Using a null modem cable, you can connect to the serial
port of your Cobalt machine (using 115200 bps) and perform the installation
this way.  This option is not available on Qube 2700 (Qube1) machines since
they have no serial port.

</para></listitem>
</itemizedlist>

</para>
  </sect3>

   <sect3>
<!-- Note to translators: this is the same section as in mips.xml -->
   <title>Broadcom BCM91250A and BCM91480B TFTP Booting</title>
<para>

On the Broadcom BCM91250A and BCM91480B evaluation boards, you have to load the SiByl boot
loader via TFTP which will then load and start the Debian installer.  In
most cases, you will first obtain an IP address via DHCP but it is also
possible to configure a static address.  In order to use DHCP, you can
enter the following command on the CFE prompt:

<informalexample><screen>
ifconfig eth0 -auto
</screen></informalexample>

Once you have obtained an IP address, you can load SiByl with the following
command:

<informalexample><screen>
boot 192.168.1.1:/boot/sibyl
</screen></informalexample>

You need to substitute the IP address listed in this example with either
the name or the IP address of your TFTP server.  Once you issue this
command, the installer will be loaded automatically.

</para>
</sect3>
  </sect2>

  <sect2 arch="mipsel"><title>Boot Parameters</title>

   <sect3>
   <title>Cobalt TFTP Booting</title>
<para>

You cannot pass any boot parameters directly.  Instead, you have to edit
the <filename>/nfsroot/default.colo</filename> file on the NFS server and
add your parameters to the <replaceable>args</replaceable> variable.

</para>
  </sect3>

   <sect3>
<!-- Note to translators: this is the same section as in mips.xml -->
   <title>Broadcom BCM91250A and BCM91480B TFTP Booting</title>
<para>

You cannot pass any boot parameters directly from the CFE prompt.  Instead,
you have to edit the <filename>/boot/sibyl.conf</filename> file on the TFTP
server and add your parameters to the <replaceable>extra_args</replaceable>
variable.

</para>
  </sect3>

  </sect2>
