<!-- $Id: m68k.xml 43692 2006-12-30 15:59:22Z mck-guest $ -->
<!-- original version: 43576 -->

  <sect2 arch="m68k">
  <title>Dělení disku v AmigaOS</title>
<para>

Jestliže používáte AmigaOS, můžete upravit původní oblasti ještě před
instalací programem <command>HDToolBox</command>.

</para>
  </sect2>

  <sect2 arch="m68k">
  <title>Dělení disku v Atari TOS</title>
<para>

Identifikátory (ID) diskových oblastí v Atari jsou tříznakové položky:
pro datové oblasti se používá <quote>LNX</quote> a pro oblasti
s virtuální pamětí <quote>SWP</quote>. Pokud instalujete systém na
počítač s malou pamětí, je nutno také vytvořit malou Minixovou oblast
(asi 2MB), která bude mít identifikaci <quote>MNX</quote>.
Pozor! Nejen že špatné nastavení ID jednotlivých oblastí zabrání
instalačnímu programu rozpoznat diskové oblasti, ale také způsobí, že
TOS se bude pokoušet použít na linuxové oblasti, což se nebude líbit
ovladači pevného disku, který na oplátku prohlásí celý disk za
nedostupný.

</para><para>

Mezi ataristy existují spousty rozdělovacích programů od třetích
společností (Atari utilita <command>harddisk</command> bohužel
nepovolí změnit ID oblasti) a tento manuál se nemůže detailně zabývat
každým z nich. Následující postup tedy pokrývá program
<command>SCSITool</command> (od Hard+Soft GmBH).

<orderedlist>
<listitem><para>

Spusťte <command>SCSITool</command> a vyberte disk, který chcete
rozdělit (menu <guimenu>Disk</guimenu>, položka
<guimenuitem>select</guimenuitem>).

</para></listitem>
<listitem><para>

Z menu <guimenu>Partition</guimenu> vyberte buď
<guimenuitem>New</guimenuitem> pro přidání nových oblastí nebo změnu
velikostí stávajících, nebo <guimenuitem>Change</guimenuitem> pro
změnu jedné konkrétní oblasti. Pokud ještě nemáte vytvořené oblasti se
správnými velikostmi a nechcete jenom změnit ID oblasti, je asi lepší
použít možnost <guimenuitem>New</guimenuitem>.

</para></listitem>
<listitem><para>

Pokud jste zvolili <guimenuitem>New</guimenuitem>, vyberte
v dialogovém okně položku <guilabel>existing</guilabel>.
V následujícím okně se zobrazí seznam existujících oblastí, které
můžete doladit posuvníky. První sloupec v seznamu obsahuje typ
oblasti, který můžete editovat kliknutím do textového pole. Když
skončíte s editací oblastí, uložte změny kliknutím na tlačítko
<guibutton>OK</guibutton>.

</para></listitem>
<listitem><para>

V případě že jste zvolili <guimenuitem>Change</guimenuitem>, vyberte
ze seznamu oblast, kterou chcete změnit a v dialogovém okně vyberte
<guilabel>other systems</guilabel>. Další okno vypíše detailní
informace o umístění vybrané oblasti a nechá vás změnit ID oblasti.
Změny uložte kliknutím na tlačítko <guibutton>OK</guibutton>.

</para></listitem>
<listitem><para>

Někam si poznamenejte linuxová jména těch oblastí, které jste
vytvořili nebo změnili pro použití Linuxem &mdash; viz
<xref linkend="device-names"/>.

</para></listitem>
<listitem><para>

Ukončete <command>SCSITool</command> přes menu <guimenu>File</guimenu>
a položku <guimenuitem>Quit</guimenuitem>. Počítač se restartuje,
abyste měli jistotu, že TOS používá změněnou tabulku oblastí. Pokud
jste změnili jakoukoliv TOS/GEM oblast, bude neověřená a bude se muset
znovu inicializovat (říkali jsme vám, že si máte zálohovat všechna
data na disku, že?).

</para></listitem>
</orderedlist>

</para><para>

V instalačním programu existuje rozdělovací nástroj pro Linux/m68k
nazvaný <command>atari-fdisk</command>, ale zatím doporučujeme
rozdělovat disk editorem oblastí v TOS, nebo nějakým jiným nástrojem.
Pokud váš editor oblastí nemá možnost editovat typ oblasti, můžete
udělat tento kritický krok později (z dočasného instalačního
RAMdisku). <command>SCSITool</command> je jediný nám známý editor
oblastí, který podporuje výběr libovolného typu oblasti, ale
samozřejmě mohou existovat i jiné nástroje &mdash; vyberte si ten,
který vyhovuje vašim požadavkům.

</para>
</sect2>

  <sect2 arch="m68k">
  <title>Dělení disku v MacOS</title>
<para>

Na Macintoshi byly testovány následující nástroje pro dělení disku:
<command>pdisk</command>, <command>HD SC Setup</command> 7.3.5
(Apple), <command>HDT</command> 1.8 (FWB),
<command>SilverLining</command> (LaCie) a <command>DiskTool</command>
(Tim Endres, GPL).
U programů <command>HDT</command> a <command>SilverLining</command> je
vyžadována plná verze. Aby nástroj od Applu mohl rozpoznat disky
jiných výrobců, musí být <quote>opraven</quote>. (Postup jak opravit
<command>HD SC Setup</command> za pomoci <command>ResEdit</command>u
je k dispozici na
<ulink url="http://www.euronet.nl/users/ernstoud/patch.html"></ulink>.)

</para><para>

Pro Macintoshe používající IDE disky je potřeba k vytvoření prázdného
místa pro linuxové oblasti použít program <command>Apple Drive
Setup</command> a vlastní dělení disku provést až v Linuxu.
Další možností je použít MacOS verzi pdisku, který lze stáhnout ze
stránek <ulink
url="http://homepage.mac.com/alk/downloads/pdisk.sit.hqx">Alsoft</ulink>.

</para>
</sect2>

