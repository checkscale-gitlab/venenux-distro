<?xml version="1.0" encoding="EUC-JP"?>
<!-- retain these comments for translator revision tracking -->
<!-- original version: 43774 -->

 <sect1 id="mail-setup">
<!--
 <title>Setting Up Your System To Use E-Mail</title>
-->
 <title>電子メールを使用するためのシステム設定</title>
<para>

<!--
Today, email is an important part of many people's life. As there are
many options as to how to set it up, and as having it set up correctly is
important for some Debian utilities, we will try to cover the basics in
this section.
-->
今日では、電子メールは多くの人々にとって生活の重要な一部になっています。
電子メールを使えるように設定するまでには、たくさんの選択肢があり、さらに
電子メールが正確に設定されていることが重要になる Debian ユーティリティ
があります。本節では、基本的なことのみ説明します。

</para><para>

<!--
There are three main functions that make up an e-mail system. First there is
the <firstterm>Mail User Agent</firstterm> (MUA) which is the program a user
actually uses to compose and read mails. Then there is the <firstterm>Mail
Transfer Agent</firstterm> (MTA) that takes care of transferring messages
from one computer to another. And last there is the <firstterm>Mail
Delivery Agent</firstterm> (MDA) that takes care of delivering incoming mail
to the user's inbox.
-->
電子メールシステムは、三つの主要な機能で構築されています。
最初に、ユーザがメールを読み書きするために実際に使用するプログラム
である <firstterm>Mail User Agent</firstterm> (MUA) があります。
次に、あるコンピュータから別のコンピュータまでメッセージの転送処理
をする <firstterm>Mail Transfer Agent</firstterm> (MTA) があります。
そして最後に、ユーザの受信箱に受信メールの配送処理
をする <firstterm>Mail Delivery Agent</firstterm> (MDA) があります。

</para><para>

<!--
These three functions can be performed by separate programs, but they can
also be combined in one or two programs. It is also possible to have
different programs handle these functions for different types of mail.
-->
これら三つの機能は個別のプログラムによって実行されますが、一つあるいは
二つのプログラムに組み込むこともできます。また、異なるタイプのメールのために、
これらの機能を処理する異なるプログラムを使用することもできます。

</para><para>

<!--
On Linux and Unix systems <command>mutt</command> is historically a very
popular MUA. Like most traditional Linux programs it is text based. It is
often used in combination with <command>exim</command> or
<command>sendmail</command> as MTA and <command>procmail</command> as MDA.
-->
Linux や Unix システムにおいては、<command>mutt</command> が歴史的に
とてもよく知られている MUA です。従来のほとんどの Linux プログラムが
そうであるようにテキストベースのプログラムで、
MTA として <command>exim</command> または <command>sendmail</command>、
そして MDA として <command>procmail</command> と組み合わせてよく使用されます。

</para><para>

<!--
With the increasing popularity of graphical desktop systems, the use of
graphical e-mail programs like GNOME's <command>evolution</command>,
KDE's <command>kmail</command> or Mozilla's <command>thunderbird</command>
(in Debian available as <command>icedove</command><footnote>
-->
グラフィカルデスクトップシステムの人気の高まりとともに、
GNOME の <command>evolution</command>、KDE の <command>kmail</command>、
あるいは Mozilla の <command>thunderbird</command> 
(Debian では <command>icedove</command><footnote>

<para>
<!--
The reason that <command>thunderbird</command> has been renamed to
<command>icedove</command> in Debian has to do with licencing issues.
Details are outside the scope of this manual.
-->
<command>thunderbird</command> が Debian で <command>icedove</command> に
改名された理由は、ライセンス問題と関係があります。
詳細は本マニュアルでは扱いません。
</para>

<!--
</footnote>) is becoming more popular. These programs combine the function
of a MUA, MTA and MDA, but can &mdash; and often are &mdash; also be used
in combination with the traditional Linux tools.
-->
</footnote> として利用可能) のようなグラフィカルな電子メールプログラムの
使用がより一般的になっています。これらのプログラムは、MUA、MTA および MDA 
の機能が組み合わされていますが、従来の Linux ツールと組み合わせることもでき 
&mdash; そして多くの場合は組み合わせて &mdash; 使用されます。

</para>

  <sect2 id="mail-default">
<!--
  <title>Default E-Mail Configuration</title>
-->
  <title>デフォルトの電子メール設定</title>
<para>

<!--
Even if you are planning to use a graphical mail program, it is important
that a traditional MTA/MDA is also installed and correctly set up on your
Linux system. Reason is that various utilities running on the
system<footnote>
-->
グラフィカルなメールプログラムを使用するつもりでいても、Linux システムに
従来の MTA/MDA もインストールし、正確に設定するのは大切なことです。
システムで起動している様々なユーティリティ<footnote>

<para>
<!--
Examples are: <command>cron</command>, <command>quota</command>,
<command>logcheck</command>, <command>aide</command>, &hellip;
-->
例えば: <command>cron</command>、<command>quota</command>、
<command>logcheck</command>、<command>aide</command>、&hellip;
</para>

<!--
</footnote> can send important notices by e-mail to inform the system
administrator of (potential) problems or changes.
-->
</footnote> が、システム管理者に (潜在的な) 問題や変更を通知するために、
電子メールで重要な通知を送ることができるからです。

</para><para>

<!--
For this reason the packages <classname>exim4</classname> and
<classname>mutt</classname> will be installed by default (provided you
did not unselect the <quote>standard</quote> task during the installation).
<classname>exim4</classname> is a combination MTA/MDA that is relatively
small but very flexible. By default it will be configured to only handle
e-mail local to the system itself and e-mails addressed to the system
administrator (root account) will be delivered to the regular user account
created during the installation<footnote>
-->
そのため、<classname>exim4</classname> と <classname>mutt</classname> 
パッケージは、デフォルトでインストールされます (インストールの際に 
<quote>標準</quote> タスクを非選択にしなかった場合)。
<classname>exim4</classname> は、比較的小さなプログラムですが、
とても柔軟性のある MTA/MDA の組み合わせです。
デフォルトでは、システム内のローカルな電子メールの処理のみのために設定され、
システム管理者 (root アカウント) 宛ての電子メールは、
インストールの際に作成した標準のユーザアカウントに配送されます<footnote>

<para>
<!--
The forwarding of mail for root to the regular user account is configured
in <filename>/etc/aliases</filename>. If no regular user account was created,
the mail will of course be delivered to the root account itself.
-->
標準のユーザアカウントへの root 宛てのメールの転送は、
<filename>/etc/aliases</filename> で設定します。標準のユーザアカウントを
作成しなかった場合、もちろんメールは root アカウント自身に配送されます。
</para>

<!--
</footnote>.
-->
</footnote>。

</para><para>

<!--
When system e-mails are delivered they are added to a file in
<filename>/var/mail/<replaceable>account_name</replaceable></filename>.
The e-mails can be read using <command>mutt</command>.
-->
システムから配送された電子メールは
<filename>/var/mail/<replaceable>account_name</replaceable></filename> 中
のファイルに加えられます。メールは <command>mutt</command> を使って
読むことができます。

</para>
  </sect2>

  <sect2 id="mail-outgoing">
<!--
  <title>Sending E-Mails Outside The System</title>
-->
  <title>システムの外に電子メールを送る</title>
<para>

<!--
As mentioned earlier, the installed Debian system is only set up to handle
e-mail local to the system, not for sending mail to others nor for
receiving mail from others.
-->
先に述べたように、インストールした Debian システムは、システム内のローカルな
電子メールを処理するようにだけ設定され、他人にメールを送ったり、
他人からメールを受け取ったりするようには設定されません。

</para><para>

<!--
If you would like <classname>exim4</classname> to handle external e-mail,
please refer to the next subsection for the basic available configuration
options. Make sure to test that mail can be sent and received correctly.
-->
<classname>exim4</classname> に外部の電子メールを処理させたい場合は、
利用できる基本設定オプションに関して、次節を参照してください。
メールが正しく送受信できることは、テストして確かめるようにしてください。

</para><para>

<!--
If you intend to use a graphical mail program and use a mail server of
your Internet Service Provider (ISP) or your company, there is not really
any need to configure <classname>exim4</classname> for handling external
e-mail. Just configure your favorite graphical mail program to use the
correct servers to send and receive e-mail (how is outside the scope of
this manual).
-->
もしグラフィカルなメールプログラムを使って、インターネットサービス
プロバイダ (ISP) あるいは会社のメールサーバを使用するつもりならば、
外部の電子メールを処理するために <classname>exim4</classname> を設定する
必要は実際にはありません。電子メールを送受信するために、
好みのグラフィカルなメールプログラムが正しいサーバを使用するようにただ
設定するだけです (設定方法は本マニュアルでは扱いません)。

</para><para>

<!--
However, in that case you may need to configure individual utilities to
correctly send e-mails. One such utility is <command>reportbug</command>,
a program that facilitates submitting bug reports against Debian packages.
By default it expects to be able to use <classname>exim4</classname> to
submit bug reports.
-->
しかしその場合には、電子メールを正しく送れるように個々のユーティリティ
を設定する必要があるかもしれません。そのようなユーティリティの一つに、
Debian パッケージに対するバグ報告の提出を容易にするプログラム
である <command>reportbug</command> があります。
デフォルトでは、バグ報告を提出するために <classname>exim4</classname> が
使用可能であることが期待されます。

</para><para>

<!--
To correctly set up <command>reportbug</command> to use an external mail
server, please run the command <command>reportbug -\-configure</command>
and answer <quote>no</quote> to the question if an MTA is available. You
will then be asked for the SMTP server to be used for submitting bug reports.
-->
外部のメールサーバを使用するように <command>reportbug</command> を正しく
設定するため、<command>reportbug --configure</command> コマンドを
実行し、MTA が利用可能かどうかという質問に <quote>no</quote>と答えてください。
その後、バグ報告の提出に使用する SMTP サーバを尋ねられるでしょう。

</para>
  </sect2>

  <sect2 id="config-mta">
<!--
  <title>Configuring the Exim4 Mail Transport Agent</title>
-->
  <title>Exim4 Mail Transport Agent の設定</title>
<para>

<!--
If you would like your system to also handle external e-mail, you will
need to reconfigure the <classname>exim4</classname> package<footnote>
-->
システムで外部の電子メールを処理するようにしたい場合、
<classname>exim4</classname> パッケージを再設定する必要があります<footnote>

<para>
<!--
You can of course also remove <classname>exim4</classname> and replace
it with an alternative MTA/MDA.
-->
もちろん、<classname>exim4</classname> を削除し、他の MTA/MDA を
使用することもできます。

</para>

</footnote>:

<informalexample><screen>
# dpkg-reconfigure exim4-config
</screen></informalexample>

</para><para>

<!--
After entering that command (as root), you will be asked if you want split
the configuration into small files. If you are unsure, select the default
option.
-->
(root で) 上記のコマンドを入力した後に、設定ファイルを小さなファイルに
分割するかどうか質問されます。よく分からない場合は、デフォルトオプションを
選択してください。

</para><para>

<!--
Next you will be presented with several common mail scenarios. Choose the
one that most closely resembles your needs.
-->
次に、一般的な複数のメールシナリオが提示されます。
あなたが必要としていることに最も近いものを一つ選択してください。

</para>

<variablelist>
<varlistentry>
<!--
<term>internet site</term>
-->
<term>インターネットサイト</term>
<listitem><para>

<!--
Your system is connected to a network and your mail is sent and
received directly using SMTP. On the following screens you will be
asked a few basic questions, like your machine's mail name, or a list of
domains for which you accept or relay mail.
-->
システムはネットワークに接続され、SMTP を使用して直接メールを送受信します。
次の画面で、マシンのメール名や受信あるいは中継するメールのドメインリスト
などのような、いくつかの基本的な質問をされるでしょう。

</para></listitem>
</varlistentry>

<varlistentry>
<!--
<term>mail sent by smarthost</term>
-->
<term>スマートホストでメール送信</term>
<listitem><para>

<!--
In this scenario your outgoing mail is forwarded to another machine,
called a <quote>smarthost</quote>, which takes care of sending the message
on to its destination.
The smarthost also usually stores incoming mail addressed to your
computer, so you don't need to be permanently online. That also means
you have to download your mail from the smarthost via programs like
fetchmail.
-->
このシナリオでは、あなたの送信メールは、宛て先へのメッセージ送信処理をする
<quote>スマートホスト</quote> と呼ばれる他のマシンに転送されます。
通常、スマートホストは、あなたのコンピュータ宛てに送信された受信メールを
保管するので、ずっとオンラインである必要はありません。つまりそれは、
fetchmail のようなプログラムによって、スマートホストのメールを
ダウンロードしなければならないことを意味します。

</para><para>

<!--
In a lot of cases the smarthost will be your ISP's mail server, which
makes this option very suitable for dial-up users. It can also be a
company mail server, or even another system on your own network.
-->
多くの場合、スマートホストはあなたの ISP のメールサーバで、このオプションは
ダイヤルアップユーザにとても適しています。またそれは、会社のメールサーバや
あなた自身のネットワーク上の別のシステムとすることもできます。

</para></listitem>
</varlistentry>

<varlistentry>
<!--
<term>mail sent by smarthost; no local mail</term>
-->
<term>スマートホストでメール送信; ローカルメールなし</term>
<listitem><para>

<!--
This option is basically the same as the previous one except that the
system will not be set up to handle mail for a local e-mail domain. Mail
on the system itself (e.g. for the system administrator) will still be
handled.
-->
このオプションは、システムがローカルの電子メールドメインを処理するようには
設定されないという点を除いては、基本的に前のものと同じです。
システム自体 (例えば、システム管理者のため) のメールは処理されます。

</para></listitem>
</varlistentry>

<varlistentry>
<!--
<term>local delivery only</term>
-->
<term>ローカル配信のみ</term>
<listitem><para>

<!--
This is the option your system is configured for by default.
-->
システムがデフォルトで設定されるオプションです。

</para></listitem>
</varlistentry>

<varlistentry>
<!--
<term>no configuration at this time</term>
-->
<term>今は設定しない</term>
<listitem><para>

<!--
Choose this if you are absolutely convinced you know what you are
doing. This will leave you with an unconfigured mail system &mdash;
until you configure it, you won't be able to send or receive any mail
and you may miss some important messages from your system utilities.
-->
内容を理解できていると絶対に確信している場合のみ選択してください。
このシナリオは、メールシステムを未設定のままにします &mdash;
メールシステムが設定されるまで、メールの送受信は一切できず、
システムユーティリティからの重要なメッセージも逃してしまうかもしれません。

</para></listitem>
</varlistentry>
</variablelist>

<para>

<!--
If none of these scenarios suits your needs, or if you need a finer
grained setup, you will need to edit configuration files under the
<filename>/etc/exim4</filename> directory after the installation is
complete. More information about <classname>exim4</classname> may be
found under <filename>/usr/share/doc/exim4</filename>; the file
<filename>README.Debian.gz</filename> has further details about
configuring <classname>exim4</classname> and explains where to find
additional documentation.
-->
以上のどのシナリオもあなたの必要とするものに合っていない場合や、
より精細な設定が必要な場合は、インストール完了後
に <filename>/etc/exim4</filename> ディレクトリの設定ファイルを
編集する必要があります。<classname>exim4</classname> に関する
より多くの情報は、<filename>/usr/share/doc/exim4</filename> ディレクトリ
にあります &mdash; <filename>README.Debian.gz</filename> 
ファイルには、<classname>exim4</classname>の設定に関するその他の情報や、
補足文書がどこで見つかるかなどの説明があります。

</para><para>

<!--
Note that sending mail directly to the Internet when you don't have an
official domain name, can result in your mail being rejected because of
anti-spam measures on receiving servers. Using your ISP's mail server is
preferred. If you still do want to send out mail directly, you may want to
use a different e-mail address than is generated by default. If you use
<classname>exim4</classname> as your MTA, this is possible by adding an
entry in <filename>/etc/email-addresses</filename>.
-->
公式なドメインネームがない場合、インターネットに直接送信されたメールが
受信サーバのスパム対策のために拒絶され、結果として不着メールとなる可能性が
あることに注意してください。ISP のメールサーバの使用が望まれます。
それでもメールを直接送信したい場合には、デフォルトで生成されるものとは
異なる電子メールアドレスを使用した方が良いでしょう。
MTA として <classname>exim4</classname> を使用するなら、
<filename>/etc/email-addresses</filename> にエントリを追加することで可能です。

</para>
  </sect2>
 </sect1>
