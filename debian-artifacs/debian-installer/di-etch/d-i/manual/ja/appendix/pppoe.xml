<?xml version="1.0" encoding="EUC-JP"?>
<!-- retain these comments for translator revision tracking -->
<!-- original version: 45405 -->

 <sect1 id="pppoe" arch="not-s390">
<!--
 <title>Installing &debian; using PPP over Ethernet (PPPoE)</title>
-->
 <title>PPP over Ethernet (PPPoE) を用いた &debian; のインストール</title>

<para>

<!--
In some countries PPP over Ethernet (PPPoE) is a common protocol for
broadband (ADSL or cable) connections to an Internet Service Provider.
Setting up a network connection using PPPoE is not supported by default
in the installer, but can be made to work very simply. This section
explains how.
-->
いくつかの国でインターネットサービスプロバイダに接続するのに、
ブロードバンド接続 (ADSL やケーブル TV) の一般的なプロトコルは、
PPP over Ethernet (PPPoE) です。
インストーラでは、
PPPoE を用いたネットワーク接続のセットアップをサポートしていませんが、
非常に簡単に設定できます。この節ではその方法を説明します。

</para><para>

<!--
The PPPoE connection set up during the installation will also be available
after the reboot into the installed system (see <xref linkend="boot-new"/>).
-->
また、インストール中に PPPoE 接続をセットアップすると、
インストールしたシステムを再起動した後でも有効になります 
(<xref linkend="boot-new"/> 参照)。

</para><para>

<!--
To have the option of setting up and using PPPoE during the installation,
you will need to install using one of the CD-ROM/DVD images that are
available. It is not supported for other installation methods (e.g.
netboot<phrase condition="supports-floppy-boot"> or floppy</phrase>).
-->
インストール中に PPPoE をセットアップし使用するには、
CD-ROM/DVD イメージを使用する必要があります。
その他のインストール方法 (例: netboot<phrase condition="supports-floppy-boot"> 
やフロッピー</phrase>) では、サポートしていません。

</para><para>

<!--
Installing over PPPoE is mostly the same as any other installation. The
following steps explain the differences.
-->
PPPoE でのインストールは、他のインストール方法とほとんど同じです。
以下で説明するステップが異なるだけです。

</para>

<itemizedlist>
<listitem><para>

<!--
Boot the installer with <userinput>modules=ppp-udeb</userinput> as boot
parameter. This means that at the boot prompt you should enter:
-->
ブートパラメータに <userinput>modules=ppp-udeb</userinput> 
を指定してインストーラを起動してください。
ブートプロンプトに以下のように入力するということです。

<informalexample><screen>
install modules=ppp-udeb
</screen></informalexample>

<!--
or, if you prefer using the graphical installer:
-->
または、グラフィカルインストーラを用いる場合、以下のようにします。

<informalexample><screen>
installgui modules=ppp-udeb
</screen></informalexample>

<!--
This will ensure the component responsible for the setup of PPPoE
(<classname>ppp-udeb</classname>) will be loaded and run automatically.
-->
これにより、PPPoE のセットアップに使用するコンポーネント 
(<classname>ppp-udeb</classname>) を確実に読み込み、自動的に起動します。

</para></listitem>
<listitem><para>

<!--
Follow the regular initial steps of the installation (language,
country and keyboard selection; the loading of additional installer
components<footnote>
-->
通常のインストール初期化手順 (言語、国、キーボードの選択、
追加インストーラコンポーネント<footnote>

<para>

<!--
The <classname>ppp-udeb</classname> component is loaded as one of
the additional components in this step. If you want to install at
medium or low priority (expert mode), you can also manually select
the <classname>ppp-udeb</classname> instead of entering the
<quote>modules</quote> parameter at the boot prompt.
-->
<classname>ppp-udeb</classname> コンポーネントは、
このステップの追加コンポーネントの 1 つとして読み込まれます。
優先度を「中」「低」でインストールする場合 (エキスパートモード)、
ブートプロンプトの <quote>modules</quote> パラメータに入力する代わりに、
<classname>ppp-udeb</classname> を選択することもできます。

</para>

<!--
</footnote>).
-->
</footnote>の読み込み) を行います。

</para></listitem>
<listitem><para>

<!--
The next step is the detection of network hardware, in order to identify
any Ethernet cards present in the system.
-->
次のステップでは、システムにあるイーサネットカードを特定するのに、
ネットワークハードウェアを検出します。

</para></listitem>
<listitem><para>

<!--
After this the actual setup of PPPoE is started. The installer will probe
all the detected Ethernet interfaces in an attempt to find a PPPoE
concentrator (a type of server which handles PPPoE connections).
-->
この後、実際の PPPoE のセットアップが始まります。
インストーラは、PPPoE コンセントレータ (PPPoE 接続を扱う一種のサーバ) 
を見つけるのに、検出したすべてのイーサネットインターフェースを調べます。

</para><para>

<!--
It is possible that the concentrator will not to be found at the first
attempt. This can happen occasionally on slow or loaded networks or with
faulty servers. In most cases a second attempt to detect the concentrator
will be successful; to retry, select <guimenuitem>Configure and start a
PPPoE connection</guimenuitem> from the main menu of the installer.
-->
最初の試行では、コンセントレータが見つからない可能性があります。
これはネットワークが遅い・負荷が高い場合や、
サーバ側のエラーで起こる可能性があります。
ほとんどの場合、2 回目の試行でコンセントレータの検出に成功します。
再試行するには、インストーラのメニューにある <guimenuitem>Configure and start a
PPPoE connection</guimenuitem> を選択してください。

</para></listitem>
<listitem><para>

<!--
After a concentrator is found, the user will be prompted to type the login
information (the PPPoE username and password).
-->
コンセントレータを検出した後、ログイン情報 (PPPoE のユーザ名とパスワード) 
を入力してください。

</para></listitem>
<listitem><para>

<!--
At this point the installer will use the provided information to establish
the PPPoE connection. If the correct information was provided, the PPPoE
connection should be configured and the installer should be able to use it
to connect to the Internet and retrieve packages over it (if needed). If
the login information is not correct or some error appears, the installer
will stop, but the configuration can be attempted again by selecting the
menu entry <guimenuitem>Configure and start a PPPoE connection</guimenuitem>.
-->
インストーラは、先ほど入力した情報を用いて PPPoE 接続を確立します。
正しい情報を入力していれば PPPoE 接続の設定を行い、
インストーラはその接続を用いてインターネットに接続し、
(必要なら) パッケージを取得できます。
ログイン情報が正しくない場合や、何かエラーが発生した場合、
インストーラは停止しますが、メニューの
<guimenuitem>Configure and start a PPPoE connection</guimenuitem> を選択して、
設定を再度行えます。

</para></listitem>
</itemizedlist>

 </sect1>
