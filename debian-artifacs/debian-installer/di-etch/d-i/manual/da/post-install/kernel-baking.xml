<!-- retain these comments for translator revision tracking -->
<!-- original version: 16990 untranslated -->

 <sect1 id="kernel-baking"><title>Compiling a New Kernel</title>
<para>

Why would someone want to compile a new kernel? It is often not
necessary since the default kernel shipped with Debian handles most
configurations. However, it is useful to compile a new kernel in order
to:

<itemizedlist>
<listitem><para>

handle special hardware needs, or hardware conflicts with the pre-supplied
kernels

</para></listitem>
<listitem><para>

handle hardware or options not included in the stock kernel, such as
APM or SMP

</para></listitem>

<listitem arch="x86"><para>
 
The compact and idepci flavors don't come with sound. Although
the vanilla kernel does, it might not work for other reasons.  

</para></listitem>
<listitem><para>

optimize the kernel by removing useless drivers to speed up boot
time

</para></listitem>
<listitem><para>

use options of the kernel which are not supported by the default
kernel (such as network firewalling)

</para></listitem>
<listitem><para>

run a updated or development kernel

</para></listitem>
<listitem><para>

impress your friends, try new things

</para></listitem>
</itemizedlist>

</para>

  <sect2><title>Kernel Image Management</title>
<para>

Don't be afraid to try compiling the kernel.  It's fun and profitable. 

</para><para>

To compile a kernel the Debian way, you need some packages:
<classname>kernel-package</classname>,
<classname>kernel-source-&kernelversion;</classname> (the most recent version
at the time of this writing), <classname>fakeroot</classname> and a
few others which are probably already installed (see
<filename>/usr/share/doc/kernel-package/README.gz</filename> for the
complete list).

</para><para>

This method will make a .deb of your kernel source, and, if you have
non-standard modules, make a synchronized dependent .deb of those
too. It's a better way to manage kernel images;
<filename>/boot</filename> will hold the kernel, the System.map, and a
log of the active config file for the build.

</para><para>

Note that you don't <emphasis>have</emphasis> to compile your kernel
the ``Debian way''; but we find that using the packaging system to
manage your kernel is actually safer and easier.  In fact, you can get
your kernel sources right from Linus instead of
<classname>kernel-source-&kernelversion;</classname>, yet still use the
<classname>kernel-package</classname> compilation method.

</para><para>

Note that you'll find complete documentation on using
<classname>kernel-package</classname> under
<filename>/usr/share/doc/kernel-package</filename>.  This section just
contains a brief tutorial.

</para><para arch="sparc">

If you are compiling a kernel for UltraSPARC you will need to be sure
you have installed the <classname>egcs64</classname> package. This is
the preferred compiler for 64bit SPARC kernels. The default
<command>gcc</command> will also compile 64bit kernels, but is not as
stable. Plus, if you do not use <classname>egcs64</classname> and you
encounter kernel problems, you will most likely be asked to recompile
the kernel using <classname>egcs64</classname> in order to verify your
problem still exists. After installing <classname>egcs64</classname>
be sure to run <userinput>update-alternatives --config
sparc64-linux-gcc</userinput> as root, and be sure that
<classname>egcs64</classname> is being used for this program. 

</para><para>

Hereafter, we'll assume your kernel source will be located in
<filename>/usr/local/src</filename> and that your kernel version is
&kernelversion;.  As root, create a directory under
<filename>/usr/local/src</filename> and change the owner of that
directory to your normal non-root account.  As your normal non-root
account, change your directory to where you want to unpack the kernel
sources (<userinput>cd /usr/local/src</userinput>), extract the kernel
sources (<userinput>tar xjf
/usr/src/kernel-source-&kernelversion;.tar.bz2</userinput>), change your
directory to it (<userinput>cd
kernel-source-&kernelversion;/</userinput>). 

</para><para>

Now, you can configure your kernel.  Run <userinput>make
xconfig</userinput> if X11 is installed, configured and being run,
<userinput>make menuconfig</userinput> otherwise (you'll need
<classname>ncurses-dev</classname> installed). Take the time to read
the online help and choose carefully. When in doubt, it is typically
better to include the device driver (the software which manages
hardware peripherals, such as Ethernet cards, SCSI controllers, and so
on) you are unsure about.  Be careful: other options, not related to a
specific hardware, should be left at the default value if you do not
understand them.  Do not forget to select ``Kernel module loader'' in
``Loadable module support''

<phrase arch="alpha"> and ``Enhanced Real Time
Clock Support'' in ``Character devices'' (they are </phrase>
<phrase arch="not-alpha"> (it is </phrase>

not selected by default).  If not included, your Debian installation
will experience problems.

</para><para>

Clean the source tree and reset the <classname>kernel-package</classname>
parameters.  To do that, do <userinput>make-kpkg clean</userinput>.

</para><para>

Now, compile the kernel: 
<userinput>fakeroot make-kpkg --revision=custom.1.0 kernel_image</userinput>. 
The version number of ``1.0'' can be changed at will; this is just a
version number that you will use to track your kernel builds.
Likewise, you can put any word you like in place of ``custom'' (e.g.,
a host name).  Kernel compilation may take quite a while, depending on
the power of your machine.

</para><para condition="supports-pcmcia">

If you require PCMCIA support, you'll also need to install the
<classname>pcmcia-source</classname> package.  Unpack the gzipped tar file
as root in the directory <filename>/usr/src</filename> (it's important that
modules are found where they are expected to be found, namely,
<filename>/usr/src/modules</filename>).  Then, as root, do <userinput>make-kpkg
modules_image</userinput>.  

</para><para>

Once the compilation is complete, you can install your custom kernel
like any package.  As root, do <userinput>dpkg -i
../kernel-image-&kernelversion;-<replaceable>subarchitecture</replaceable>_custom.1.0_&architecture;.deb</userinput>.
The <replaceable>subarchitecture</replaceable> part is an optional
sub-architecture,
<phrase arch="x86"> such as ``i586'', </phrase>
depending on what kernel options you set.
<userinput>dpkg -i kernel-image...</userinput> will install the
kernel, along with some other nice supporting files.  For instance,
the <filename>System.map</filename> will be properly installed
(helpful for debugging kernel problems), and
<filename>/boot/config-&kernelversion;</filename> will be installed,
containing your current configuration set.  Your new
<classname>kernel-image-&kernelversion;</classname> package is also clever
enough to automatically use your platform's boot-loader to run an
update on the booting, allowing you to boot without re-running the
boot loader.  If you have created a modules package, e.g., if you have
PCMCIA, you'll need to install that package as well.

</para><para>

It is time to reboot the system: read carefully any warning that the
above step may have produced, then <userinput>shutdown -r now</userinput>.

</para><para>

For more information on <classname>kernel-package</classname>, read
the fine documentation in <filename>/usr/share/doc/kernel-package</filename>.

</para>
  </sect2>
 </sect1>
