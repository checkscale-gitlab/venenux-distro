# THIS FILE IS AUTOMATICALLY GENERATED FROM THE MASTER FILE
# packages/po/mk.po
#
# DO NOT MODIFY IT DIRECTLY : SUCH CHANGES WILL BE LOST
# 
# translation of mk.po to
# translation of mk.po to Macedonian
# Macedonian strings from the debian-installer.
# Georgi Stanojevski, <glisha@gmail.com>, 2004, 2005, 2006.
# Georgi Stanojevski <georgis@unet.com.mk>, 2005, 2006.
#
msgid ""
msgstr ""
"Project-Id-Version: mk\n"
"Report-Msgid-Bugs-To: debian-boot@lists.debian.org\n"
"POT-Creation-Date: 2007-01-06 22:48+0100\n"
"PO-Revision-Date: 2005-04-19 13:55+0200\n"
"Last-Translator: Georgi Stanojevski <glisha@gmail.com>\n"
"Language-Team: Macedonian <ossm-members@hedona.on.net.mk>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 1.10.2\n"

#. Type: text
#. Description
#. Main menu item
#: ../zipl-installer.templates:4
msgid "Install the ZIPL boot loader on a hard disk"
msgstr "Инсталирај го ZIPL подигнувачот на тврд диск"
