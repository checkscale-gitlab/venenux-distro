userdevfs (0.13) unstable; urgency=low

  [ Stephen R. Marenka ]
  * Add additional /dev/pts/1-7 to avoid out-of-pty errors.

 -- Stephen R. Marenka <smarenka@debian.org>  Thu,  8 Dec 2005 09:41:16 -0600

userdevfs (0.12) unstable; urgency=low

  [ Colin Watson ]
  * Install init-dev, subarch-dev, and update-dev in /lib/userdevfs; this
    frees up the useful 'update-dev' name for a version that handles udev
    too, and lets us transition to that reasonably. Requires di-utils 1.19
    for the partitioner to keep working.

 -- Joey Hess <joeyh@debian.org>  Wed,  7 Dec 2005 22:11:22 -0500

userdevfs (0.11) unstable; urgency=low

  * Remove some hardcoded paths.
  * Don't just support 2.2, instead start up if devfs and udev are not
    mounted. Just might work with 2.4 or 2.6, depending on how the dmesg greps
    work out, and could be added to any initrd to make it work if a monolithic
    kernel were swapped in to replace the d-i kernel.
  * Update description.
  * Include devices that need to be in the initrd into the udeb. Yay for
    policy not applying to udebs!
  * No longer needs userdevfs-devs hacks in the initrd build process.
  * Include /dev/fb/0, /dev/ptmx, and /dev/pts/0 so framebuffer can work.
  * Cleaned up the output.
  * Added a README.

 -- Joey Hess <joeyh@debian.org>  Tue, 16 Aug 2005 10:05:54 -0400

userdevfs (0.10) unstable; urgency=low

  * Use debhelper v4.
  * Use debhelper's udeb support.

 -- Joey Hess <joeyh@debian.org>  Sun, 17 Jul 2005 16:29:36 +0300

userdevfs (0.09) unstable; urgency=low

  * Wouter Verhelst
    - Eliminate references to seq in init-dev, as seq is not on the boot image.

 -- Joey Hess <joeyh@debian.org>  Mon, 27 Sep 2004 20:52:40 -0400

userdevfs (0.08) unstable; urgency=low

  * Stephen R. Marenka
    - Move S10userdevfs from etc/init.d to lib/debian-installer-startup.d
    (Closes: Bug#260813).

 -- Stephen R. Marenka <smarenka@debian.org>  Thu, 22 Jul 2004 07:34:19 -0500

userdevfs (0.07) unstable; urgency=low

  * Wouter Verhelst
    - Removed myself from Uploaders. I'm not really involved in d-i ATM,
      and Stephen is doing great work for the m68k architecture (keep it
      up, BTW :-)
  * Stephen R. Marenka
    - Add subarch-dev and vme scsi floppy detection.
    - Add support for /var/state/userdevfs.
    - Add copyright file.

 -- Stephen R. Marenka <smarenka@debian.org>  Sun, 23 May 2004 21:05:17 -0500

userdevfs (0.06) unstable; urgency=low

  * Stephen R. Marenka
    - Handle multiple cdrom drives.
    - Update standards.
    - Add versioned build-dep on debhelper (should now be lintian-clean).

 -- Stephen R. Marenka <smarenka@debian.org>  Mon, 29 Mar 2004 13:10:49 -0600

userdevfs (0.05) unstable; urgency=low

  * Stephen R. Marenka
    - Set priority to optional.

 -- Joey Hess <joeyh@debian.org>  Sat, 13 Mar 2004 00:08:17 -0500

userdevfs (0.04) unstable; urgency=low

  * Wouter Verhelst
    - we don't build binary-arch packages, so don't make that target do
      anything. I wonder what crack I was on when I did that, but it's fixed
      now; Closes: #223092.
    - removed debian/isinstallable; it's useless for us and replaced by
      code in init-dev and update-dev anyway now.
  * Stephen R. Marenka
    - Create non-devfs nodes and symlink them into /dev/disc/*.
    - Rework init-dev to include cdrom detection and loopback devices.
    - Put checks in place to make sure it only runs with 2.2.x kernel.
    - Add floppy device.
  * Joey Hess added to uploaders.

 -- Joey Hess <joeyh@debian.org>  Fri, 20 Feb 2004 17:22:16 -0500

userdevfs (0.03) unstable; urgency=low

  * mount /proc before we try to do anything, as it isn't done by other
    means

 -- Wouter Verhelst <wouter@debian.org>  Sat, 11 Oct 2003 21:48:01 +0200

userdevfs (0.02) unstable; urgency=low
  
  * major node number is first field in /proc/partitions, not second.
    Likewise, minor node number is second field, not third.
  
 -- Wouter Verhelst <wouter@debian.org>  Sun,  2 Mar 2003 02:40:50 +0100

userdevfs (0.01) never released; urgency=low
  
  * Initial release
  
 -- Wouter Verhelst <wouter@debian.org>  Sun,  2 Mar 2003 02:40:50 +0100

