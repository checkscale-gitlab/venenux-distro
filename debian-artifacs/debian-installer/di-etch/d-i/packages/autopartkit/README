autopartkit
===========

  A tool to automatically partition hard disks.  It is mostly useful as
  part of a bootstrap package.  It is currently used in
  debian-installer.

Table format
------------

  The format are lines with four fields:
    <mount-point> <filesystem> <minsize> <maxsize>'
  The fields are split on whitespace.

  Normal partitions

    /mount/point  fs-type                             min-size-mb  max-size-mb

  Swap partitions

    <unused>      swap                                min-size-mb  max-size-mb

  Dummy partitions (pass on to /etc/fstab).  This is useful for tmpfs
  entries.

    /mount/point  <passed-on>                         0            <unused>

  LVM partitions and volumes

    vg-name       lvm                                 min-size-mb  max-size-mb
    /mount/point  lvm:vg-name:lv-volume-name:fs-type  min-size-mb  max-size-mb

  For the max-size-mb and min-size-mb variables, you can use an arithmetic
  expression instead of a number. Addition, subtraction, multiplication,
  division and parentheses are supported, as well as the variable $RAMSIZE,
  which expands to the total amount of detected RAM (in megabytes). All
  calculations are done in floating point (and you can use floating point
  numbers in your expressions), but the answer is rounded to the nearest
  megabyte.
  
  A max-size-mb of -1 means `unlimited'.

Download
--------

  Debian CVS.

License
-------

  This source is licensed using the GNU Public License.

Reporting bugs
--------------

  Send bug reports to debian-boot@lists.debian.org, or report them to
  BTS, bugs.debian.org.
