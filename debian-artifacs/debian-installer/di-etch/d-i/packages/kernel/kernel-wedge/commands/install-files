#!/usr/bin/perl -w
# Create and populate the package build directories.

sub doit {
	print "\t".join(" ", @_)."\n";
	my $ret=(system(@_) >> 8);
	if ($ret != 0) {
		die "command exited with status $ret\n";
	}
}

my $hostarch=`dpkg-architecture -qDEB_HOST_ARCH`;
chomp $hostarch;

my $sourcedir=$ENV{SOURCEDIR};
if (! defined $sourcedir) {
	$sourcedir="";
}

open(KVERS, "kernel-versions") || die "kernel-versions: $!";
my $version = 0;
while (<KVERS>) {
	chomp;
	next if /^#/ || ! length;

	my ($arch, $kernelversion, $flavour, $installedname, $suffix, $builddep)=split(' ', $_, 6);
	if (! length $arch || ! length $kernelversion ||
	    ! length $installedname || ! length $flavour || 
	    ! length $builddep ) {
		die "parse error: $_";
	}
	next unless $arch eq $hostarch;

	my $extraname="";
	if ($suffix =~ /^[yY]/) {
		$extraname="-$kernelversion-$flavour";
	}
	elsif ($suffix =~ /^-.+/) {
		$extraname="$suffix";
	}
	
	my $modlistdir;
	if (-d "modules/$arch-$flavour") {
		$modlistdir="modules/$arch-$flavour";
	}
	elsif (-d "modules/$flavour") {
		$modlistdir="modules/$flavour";
	}
	else {
		$modlistdir="modules/$arch";
	}

	if (! -e "$modlistdir/kernel-image") {
		# copy no kernel
	}
	elsif (-e "$sourcedir/boot/vmlinux-$installedname") {
		doit("install", "-D", "-m", 644,
			"$sourcedir/boot/vmlinux-$installedname",
			"debian/kernel-image-$kernelversion-$flavour-di/boot/vmlinux$extraname");
	}
	elsif (-e "$sourcedir/boot/vmlinuz-$installedname") {
		doit("install", "-D", "-m", 644,
			"$sourcedir/boot/vmlinuz-$installedname",
			"debian/kernel-image-$kernelversion-$flavour-di/boot/vmlinuz$extraname");
	}
	elsif (-e "$sourcedir/boot/kfreebsd-$installedname.gz") {
		doit("install", "-D", "-m", 644,
			"$sourcedir/boot/kfreebsd-$installedname.gz",
			"debian/kernel-image-$kernelversion-$flavour-di/boot/kfreebsd$extraname.gz");
	}
	else {
		die "could not find kernel image";
	}
	doit("install", "-D", "-m", 644,
		"$sourcedir/boot/System.map-$installedname",
		"debian/kernel-image-$kernelversion-$flavour-di/boot/System.map$extraname")
	      if -e "$sourcedir/boot/System.map-$installedname" &&
	         -e "$modlistdir/kernel-image";

	# Include extra files to help the d-i build process build kernels
	# for certain subarchitectures. This is powerpc-specific at the
	# moment.
	my $libdir;
	if (-d "$sourcedir/usr/lib/kernel-image-$kernelversion-$flavour") {
		$libdir = "kernel-image";
	} elsif (-d "$sourcedir/usr/lib/linux-image-$kernelversion-$flavour") {
		$libdir = "linux-image";
	}

	if (defined $libdir) {
		doit("install", "-d",
			"debian/kernel-image-$kernelversion-$flavour-di/usr/lib");
		doit("cp", "-a",
			"$sourcedir/usr/lib/$libdir-$kernelversion-$flavour",
			"debian/kernel-image-$kernelversion-$flavour-di/usr/lib/$libdir-$installedname");
	}

	doit("kernel-wedge", "copy-modules", $kernelversion, $flavour, $installedname);
	doit("kernel-wedge", "copy-firmware", $kernelversion, $flavour, $installedname);
	doit("kernel-wedge", "find-dups", "$kernelversion-$flavour");
	doit("kernel-wedge", "strip-modules", "$kernelversion-$flavour");
}
close KVERS;
