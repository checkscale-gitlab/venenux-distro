# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: debian-boot@lists.debian.org\n"
"POT-Creation-Date: 2007-01-06 22:47+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: 8bit\n"

#. Type: select
#. Choices
#: ../cdebconf-udeb.templates:9
msgid "critical"
msgstr ""

#. Type: select
#. Choices
#: ../cdebconf-udeb.templates:9
msgid "high"
msgstr ""

#. Type: select
#. Choices
#: ../cdebconf-udeb.templates:9
msgid "medium"
msgstr ""

#. Type: select
#. Choices
#: ../cdebconf-udeb.templates:9
msgid "low"
msgstr ""

#. Type: select
#. Description
#: ../cdebconf-udeb.templates:11
msgid "Ignore questions with a priority less than:"
msgstr ""

#. Type: select
#. Description
#: ../cdebconf-udeb.templates:11
msgid ""
"Packages that use debconf for configuration prioritize the questions they "
"might ask you. Only questions with a certain priority or higher are actually "
"shown to you; all less important questions are skipped."
msgstr ""

#. Type: select
#. Description
#: ../cdebconf-udeb.templates:11
msgid ""
"You can select the lowest priority of question you want to see:\n"
" - 'critical' is for items that will probably break the system\n"
"    without user intervention.\n"
" - 'high' is for items that don't have reasonable defaults.\n"
" - 'medium' is for normal items that have reasonable defaults.\n"
" - 'low' is for trivial items that have defaults that will work in\n"
"   the vast majority of cases."
msgstr ""

#. Type: select
#. Description
#: ../cdebconf-udeb.templates:11
msgid ""
"For example, this question is of medium priority, and if your priority were "
"already 'high' or 'critical', you wouldn't see this question."
msgstr ""

#. Type: text
#. Description
#: ../cdebconf-priority.templates:3
msgid "Change debconf priority"
msgstr ""

#. Type: text
#. Description
#. Translators, this text will appear on a button, so KEEP IT SHORT
#: ../cdebconf-newt-udeb.templates:4 ../cdebconf-gtk-udeb.templates:4
msgid "Continue"
msgstr ""

#. Type: text
#. Description
#. Translators, this text will appear on a button, so KEEP IT SHORT
#: ../cdebconf-newt-udeb.templates:9 ../cdebconf-gtk-udeb.templates:9
msgid "Go Back"
msgstr ""

#. Type: text
#. Description
#. Translators, this text will appear on a button, so KEEP IT SHORT
#. Type: text
#. Description
#: ../cdebconf-newt-udeb.templates:14 ../cdebconf-gtk-udeb.templates:14
#: ../cdebconf-slang-udeb.templates:4 ../cdebconf-text-udeb.templates:23
msgid "Yes"
msgstr ""

#. Type: text
#. Description
#. Translators, this text will appear on a button, so KEEP IT SHORT
#. Type: text
#. Description
#: ../cdebconf-newt-udeb.templates:19 ../cdebconf-gtk-udeb.templates:19
#: ../cdebconf-slang-udeb.templates:9 ../cdebconf-text-udeb.templates:27
msgid "No"
msgstr ""

#. Type: text
#. Description
#. Translators, this text will appear on a button, so KEEP IT SHORT
#: ../cdebconf-newt-udeb.templates:24
msgid "Cancel"
msgstr ""

#. Type: text
#. Description
#. Help line displayed at the bottom of the cdebconf newt interface.
#. Translators: must fit within 80 characters.
#: ../cdebconf-newt-udeb.templates:30
msgid "<Tab> moves between items; <Space> selects; <Enter> activates buttons"
msgstr ""

#. Type: text
#. Description
#. TRANSLATORS: This should be "translated" to "RTL" or "LTR" depending of
#. default text direction of your language
#. LTR: Left to Right (Latin, Cyrillic, CJK, Indic...)
#. RTL: Right To Left (Arabic, Hebrew, Persian...)
#. DO NOT USE ANYTHING ELSE
#: ../cdebconf-gtk-udeb.templates:28
msgid "LTR"
msgstr ""

#. Type: text
#. Description
#. Translators, this text will appear on a button, so KEEP IT SHORT
#. This button will allow users to virtually "shoot a picture"
#. of the screen
#: ../cdebconf-gtk-udeb.templates:35
msgid "Screenshot"
msgstr ""

#. Type: text
#. Description
#. Text that will appear in a dialog box mentioning which file
#. the screenshot has been saved to. "%s" is a file name here
#: ../cdebconf-gtk-udeb.templates:41
#, no-c-format
msgid "Screenshot saved as %s"
msgstr ""

#. Type: text
#. Description
#: ../cdebconf-text-udeb.templates:3
#, no-c-format
msgid "!! ERROR: %s"
msgstr ""

#. Type: text
#. Description
#: ../cdebconf-text-udeb.templates:7
msgid "KEYSTROKES:"
msgstr ""

#. Type: text
#. Description
#: ../cdebconf-text-udeb.templates:11
msgid "Display this help message"
msgstr ""

#. Type: text
#. Description
#: ../cdebconf-text-udeb.templates:15
msgid "Go back to previous question"
msgstr ""

#. Type: text
#. Description
#: ../cdebconf-text-udeb.templates:19
msgid "Select an empty entry"
msgstr ""

#. Type: text
#. Description
#: ../cdebconf-text-udeb.templates:31
#, no-c-format
msgid "Prompt: '%c' for help, default=%d> "
msgstr ""

#. Type: text
#. Description
#: ../cdebconf-text-udeb.templates:35
#, no-c-format
msgid "Prompt: '%c' for help> "
msgstr ""

#. Type: text
#. Description
#: ../cdebconf-text-udeb.templates:39
#, no-c-format
msgid "Prompt: '%c' for help, default=%s> "
msgstr ""

#. Type: text
#. Description
#: ../cdebconf-text-udeb.templates:43
msgid "[Press enter to continue]"
msgstr ""
