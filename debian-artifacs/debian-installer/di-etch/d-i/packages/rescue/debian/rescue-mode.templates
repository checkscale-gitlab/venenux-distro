Template: debian-installer/rescue-mode/title
Type: text
_Description: Enter rescue mode

Template: rescue/root
Type: select
Choices: ${PARTITIONS}
_Description: Device to use as root file system:
 Enter a device you wish to use as your root file system. You will be able
 to choose among various rescue operations to perform on this file system.

Template: rescue/no-such-device
Type: error
_Description: No such device
 The device you entered for your root file system (${DEVICE}) does not
 exist. Please try again.

Template: rescue/mount-failed
Type: error
_Description: Mount failed
 An error occurred while mounting the device you entered for your root
 file system (${DEVICE}) on /target.
 .
 Please check the syslog for more information.

Template: rescue/menu
Type: select
# This is designed to be a pseudo-title for the menu of possible 
# actions that users can choose when running in rescue mode
Choices: ${CHOICES}
_Description: Rescue operations

Template: rescue/menu-error
Type: error
_Description: Rescue operation failed
 The rescue operation '${OPERATION}' failed with exit code ${CODE}.

Template: rescue/menu/shell
# TRANSLATORS: This is a menu entry. Keep in under 55 columns/characters
Type: text
_Description: Execute a shell in ${DEVICE}

Template: rescue/menu/initrd-shell
# TRANSLATORS: This is a menu entry. Keep in under 55 columns/characters
Type: text
_Description: Execute a shell in the installer environment

Template: rescue/menu/change-root
# TRANSLATORS: This is a menu entry. Keep in under 55 columns/characters
Type: text
_Description: Choose a different root file system

Template: rescue/menu/reboot
# TRANSLATORS: This is a menu entry. Keep in under 55 columns/characters
Type: text
_Description: Reboot the system

Template: rescue/shell/intro
Type: text
_Description: Executing a shell
 After this message, you will be given a shell with ${DEVICE} mounted on
 "/". If you need any other file systems (such as a separate "/usr"), you
 will have to mount those yourself.

Template: rescue/shell/run-failed
Type: error
_Description: Error running shell in /target
 A shell (${SHELL}) was found on your root file system (${DEVICE}), but an
 error occurred while running it.

Template: rescue/shell/not-found
Type: error
_Description: No shell found in /target
 No usable shell was found on your root file system (${DEVICE}).

Template: rescue/initrd-shell/intro
Type: text
_Description: Executing a shell
 After this message, you will be given a shell with ${DEVICE} mounted on
 "/target". You may work on it using the tools available in the installer
 environment. If you want to make it your root file system temporarily, run
 "chroot /target". If you need any other file systems (such as a separate
 "/usr"), you will have to mount those yourself.
