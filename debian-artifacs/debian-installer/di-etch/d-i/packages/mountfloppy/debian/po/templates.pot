# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: debian-boot@lists.debian.org\n"
"POT-Creation-Date: 2007-01-06 22:47+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: 8bit\n"

#. Type: select
#. Choices
#: ../mountfloppy.templates:4
msgid "Cancel"
msgstr ""

#. Type: select
#. Description
#: ../mountfloppy.templates:5
msgid "Select your floppy device:"
msgstr ""

#. Type: select
#. Description
#: ../mountfloppy.templates:5
msgid ""
"No standard floppy drive was found. If you have a USB floppy drive or some "
"other unusual type of floppy drive, select it from the list. Note that the "
"list may also include devices that are not floppy drives."
msgstr ""
