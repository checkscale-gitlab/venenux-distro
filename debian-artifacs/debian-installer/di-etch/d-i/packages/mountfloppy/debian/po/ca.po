# THIS FILE IS AUTOMATICALLY GENERATED FROM THE MASTER FILE
# packages/po/ca.po
#
# DO NOT MODIFY IT DIRECTLY : SUCH CHANGES WILL BE LOST
# 
# Catalan messages for debian-installer.
# Copyright 2002, 2003, 2004, 2005, 2006 Software in the Public Interest, Inc.
# This file is distributed under the same license as debian-installer.
# Jordi Mallach <jordi@debian.org>, 2002, 2003, 2004, 2006.
# Guillem Jover <guillem@debian.org>, 2005.
#
msgid ""
msgstr ""
"Project-Id-Version: debian-installer etch\n"
"Report-Msgid-Bugs-To: debian-boot@lists.debian.org\n"
"POT-Creation-Date: 2007-01-06 22:47+0100\n"
"PO-Revision-Date: 2006-11-03 14:57+0100\n"
"Last-Translator: Jordi Mallach <jordi@debian.org>\n"
"Language-Team: Catalan <debian-l10n-catalan@lists.debian.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. Type: select
#. Choices
#: ../mountfloppy.templates:4
msgid "Cancel"
msgstr "Cancel·la"

#. Type: select
#. Description
#: ../mountfloppy.templates:5
msgid "Select your floppy device:"
msgstr "Seleccioneu la vostra disquetera:"

#. Type: select
#. Description
#: ../mountfloppy.templates:5
msgid ""
"No standard floppy drive was found. If you have a USB floppy drive or some "
"other unusual type of floppy drive, select it from the list. Note that the "
"list may also include devices that are not floppy drives."
msgstr ""
"No s'ha trobat cap disquetera estàndard. Si teniu una disquetera USB o "
"alguna altra disquetera inusual, seleccioneu-la a la llista. Teniu en compte "
"que la llista pot incloure unitats que no són disqueteres."
