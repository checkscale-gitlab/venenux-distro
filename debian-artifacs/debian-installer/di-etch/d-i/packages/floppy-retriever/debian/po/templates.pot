# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: debian-boot@lists.debian.org\n"
"POT-Creation-Date: 2007-01-06 22:47+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: 8bit\n"

#. Type: text
#. Description
#: ../floppy-retriever.templates:3
msgid "Scanning the floppy"
msgstr ""

#. Type: text
#. Description
#: ../floppy-retriever.templates:7
msgid "Cannot read floppy, or not a driver floppy."
msgstr ""

#. Type: text
#. Description
#: ../floppy-retriever.templates:7
msgid ""
"There was a problem reading data from the floppy disk. Please make sure that "
"the right floppy is in the drive. If you continue to have trouble, you may "
"have a bad floppy disk."
msgstr ""

#. Type: boolean
#. Description
#: ../load-floppy.templates:4
msgid "Load drivers from floppy now?"
msgstr ""

#. Type: boolean
#. Description
#: ../load-floppy.templates:4
msgid ""
"You probably need to load drivers from a floppy before continuing with the "
"installation. If you know that the install will work without extra drivers, "
"you can skip this step."
msgstr ""

#. Type: boolean
#. Description
#: ../load-floppy.templates:4
msgid ""
"If you do need to load drivers, insert the appropriate driver floppy before "
"continuing."
msgstr ""

#. Type: text
#. Description
#. main-menu
#: ../load-floppy.templates:15
msgid "Load drivers from a floppy"
msgstr ""

#. Type: boolean
#. Description
#: ../load-floppy.templates:20
msgid "Unknown floppy. Try to load anyway?"
msgstr ""

#. Type: boolean
#. Description
#: ../load-floppy.templates:20
msgid ""
"The floppy is not a known driver floppy. Please make sure that the correct "
"floppy is in the drive. You can still continue if you have an unofficial "
"floppy you want to use."
msgstr ""

#. Type: text
#. Description
#: ../load-floppy.templates:27
msgid "Please insert ${DISK_LABEL} ('${DISK_NAME}') first."
msgstr ""

#. Type: text
#. Description
#: ../load-floppy.templates:27
msgid ""
"Due to dependencies between packages, driver floppies must be loaded in the "
"correct order."
msgstr ""

#. Type: boolean
#. Description
#: ../load-floppy.templates:34
msgid "Load drivers from another floppy?"
msgstr ""

#. Type: boolean
#. Description
#: ../load-floppy.templates:34
msgid ""
"To load additional drivers from another floppy, please insert the "
"appropriate driver floppy before continuing."
msgstr ""
