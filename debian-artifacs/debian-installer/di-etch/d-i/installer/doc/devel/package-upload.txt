To upload a package correctly, do the following:

* update from Subversion:
  svn up

* update the translator info in debian/changelog by copying and pasting
  the output (if any) of the following command:
  scripts/l10n/output-l10n-changes -d .
  Take care that you preserve the UTF-8 encoding of the changes!

* change the distribution to unstable:
  dch -r

* build:
  debuild

* check the resulting package:
  debdiff, etc.

* commit and tag the release:
  debcommit --release
