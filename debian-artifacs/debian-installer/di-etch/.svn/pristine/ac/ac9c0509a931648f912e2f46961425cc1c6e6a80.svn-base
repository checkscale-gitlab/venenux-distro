<?xml version="1.0" encoding="ISO-8859-1"?>
<!-- original version: 35612 -->

 <sect2 arch="mipsel" id="boot-tftp">
 <title>Amorcer avec TFTP</title>
   <sect3>
   <title>Amorcer une machine Cobalt avec TFTP</title>
<para>
Cobalt n'utilise pas TFP mais NFS pour s'amorcer. Vous devez installer un serveur NFS
et mettre les fichiers de l'installateur dans <filename>/nfsroot</filename>.
Quand vous amorcez la machine Cobalt, vous devez appuyer en m�me temps
sur les boutons gauche et droit du curseur, ce qui provoquera l'amor�age depuis NFS.
Plusieurs options sont affich�es � l'�cran. Il existe deux m�thodes d'installation&nbsp;:

<itemizedlist>
<listitem><para>

Avec SSH (m�thode par d�faut)&nbsp;: l'installateur configure le r�seau avec DHCP et
lance un serveur SSH. Un mot de passe est affich� � l'�cran ainsi que d'autres informations de
connexion (une adresse IP, par exemple). Quand vous vous connectez � la machine avec un
client SSH, vous pouvez lancer l'installation.

</para></listitem>
<listitem><para>

Avec une console s�rie&nbsp;: avec un cable null modem, vous pouvez vous connecter
au port s�rie de la machine Cobalt (� 115200 bps) et lancer l'installation.
Cette fa�on de faire n'est pas possible sur les Qube 2700 (Qube1) car elles n'ont pas
de port s�rie.

</para></listitem>
</itemizedlist>

 </para>
  </sect3>

   <sect3>

   <title>Amorcer avec TFTP les cartes Broadcom BCM91250A et BCM91480B</title>
<para>

Avec les cartes d'�valuation Broadcom BCM91250A et BCM91480B, vous devez charger via TFTP 
le programme d'amor�age SiByl, lequel chargera et lancera l'installateur
Debian. Dans la plupart des cas vous obtiendrez une adresse IP par DHCP, mais 
il est aussi possible d'indiquer une adresse fixe. Pour utiliser DHCP, il
suffit d'entrer la commande suivante � l'invite CFE&nbsp;:

<informalexample><screen>
ifconfig eth0 -auto
</screen></informalexample>

Une fois l'adresse obtenue, vous pouvez charger SiByl avec la commande 
suivante&nbsp;:

<informalexample><screen>
boot 192.168.1.1:/boot/sibyl
</screen></informalexample>

Vous devez changer l'adresse donn�e dans l'exemple et indiquer soit le nom soit
l'adresse de votre serveur TFTP. Une fois cette commande ex�cut�e, 
l'installateur sera automatiquement charg�.

</para>
</sect3>
  </sect2>

  <sect2 arch="mipsel"><title>Param�tres d'amor�age</title>

   <sect3>
   <title>Cobalt TFTP Booting</title>
<para>

Vous ne pouvez pas passer des param�tres d'amor�age directement. Vous devez
modifier le fichier <filename>/nfsroot/default.colo</filename> sur le serveur NFS
et y ajouter vos param�tres avec la variable <replaceable>args</replaceable>. 

</para>
  </sect3>

   <sect3>

   <title>Amorcer avec TFTP les cartes Broadcom BCM91250A etBCM91480B </title>
<para>

Vous ne pouvez pas indiquer des param�tres d'amor�age � l'invite CFE. 
Vous devez modifier le fichier <filename>/boot/sibyl.conf</filename> sur le
serveur TFTP et mettre vos param�tres dans la variable 
<replaceable>extra_args</replaceable>.

</para>
  </sect3>

  </sect2>
