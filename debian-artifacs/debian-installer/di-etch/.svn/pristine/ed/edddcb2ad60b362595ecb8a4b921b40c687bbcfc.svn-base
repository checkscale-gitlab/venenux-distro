<?xml version="1.0" encoding="ISO-8859-1"?>
<!-- original version 43558 -->


  <sect2 id="dhcpd">
   <title>Configurer un serveur DHCP</title>
<para>

Il existe un serveur DHCP libre, <command>dhcpd</command> ISC. Pour
&debian;, le paquet <classname>dhcp3-server</classname> est recommand�. 
Voici un exemple de fichier de configuration (habituellement
<filename>/etc/dhcp3/dhcpd.conf</filename>)&nbsp;:

<informalexample><screen>
option domain-name "example.com";
option domain-name-servers ns1.example.com;
option subnet-mask 255.255.255.0;
default-lease-time 600;
max-lease-time 7200;
server-name "servername";

subnet 192.168.1.0 netmask 255.255.255.0 {
  range 192.168.1.200 192.168.1.253;
  option routers 192.168.1.1;
}

host clientname {
  filename "/tftpboot/tftpboot.img";
  server-name "servername";
  next-server servername;
  hardware ethernet 01:23:45:67:89:AB; 
  fixed-address 192.168.1.90;
}
</screen></informalexample>

</para><para>

Dans cet exemple, le serveur <replaceable>servername</replaceable> 
joue le r�le de serveur DHCP, serveur TFTP et passerelle r�seau. Vous 
devrez certainement changer les options concernant le nom de domaine
ainsi que le nom du serveur et l'adresse mat�rielle du client. L'option
<replaceable>filename</replaceable> devrait �tre le nom du fichier extrait
par TFTP.

</para><para>

Apr�s avoir modifi� le fichier de configuration de <command>dhcpd</command>,
relancez <command>dhcpd</command> par 
<userinput>/etc/init.d/dhcpd3-server restart</userinput>.

</para>

   <sect3 arch="x86">
   <title>Amor�age PXE et configuration de DHCP</title>
<para>
Voici un autre exemple de fichier <filename>dhcp.conf</filename> utilisant
la m�thode de chargement du syst�me d'exploitation par le r�seau g�r�e par le 
mat�riel (�&nbsp;Pre-boot Execution Environment&nbsp;� ou PXE) de TFTP.

<informalexample><screen>
option domain-name "example.com";

default-lease-time 600;
max-lease-time 7200;

allow booting;
allow bootp;

# Le paragraphe suivant doit �tre modifi� pour correspondre � votre syst�me
subnet 192.168.1.0 netmask 255.255.255.0 {
  range 192.168.1.200 192.168.1.253;
  option broadcast-address 192.168.1.255;
# L'adresse de la passerelle peut �tre diff�rente
# (acc�s � internet par exemple)
  option routers 192.168.1.1;
# Indiquez le DNS que vous voulez utiliser
  option domain-name-servers 192.168.1.3;
}

group {
  next-server 192.168.1.3;
  host tftpclient {
# Adresse mat�rielle du client TFTP
  hardware ethernet  00:10:DC:27:6C:15;
  filename "pxelinux.0";
 }
}
</screen></informalexample>

Pour un d�marrage PXE, le fichier du client 
<filename>pxelinux.0</filename> est un programme d'amor�age et non une image 
du noyau (voir <xref linkend="tftp-images"/> ci-dessous).

</para>
   </sect3>
  </sect2>
