<?xml version="1.0" encoding="ISO-8859-1"?>
<!-- original version: 43558 -->
<!-- revisado por nahoo, 6 noviembre 2004 -->

  <sect2 id="dhcpd">
   <title>Configuraci�n del servidor DHCP</title>
<para>

Un servidor DHCP de software libre es el <command>dhcpd</command> de
ISC.  Se recomienda el uso del paquete
<classname>dhcp3-server</classname> en &debian;.  A continuaci�n se
muestra una configuraci�n de ejemplo para �l (consulte
<filename>/etc/dhcpd3/dhcpd.conf</filename>):

<informalexample><screen>
option domain-name "ejemplo.com";
option domain-name-servers ns1.ejemplo.com;
option subnet-mask 255.255.255.0;
default-lease-time 600;
max-lease-time 7200;
server-name "nombredeservidor";

subnet 192.168.1.0 netmask 255.255.255.0 {
  range 192.168.1.200 192.168.1.253;
  option routers 192.168.1.1;
}

host nombredecliente {
  filename "/tftpboot/tftpboot.img";
  server-name "nombredeservidor";
  next-server nombredeservidor;
  hardware ethernet 01:23:45:67:89:AB;
  fixed-address 192.168.1.90;
}
</screen></informalexample>

</para><para>

En este ejemplo, hay un servidor
<replaceable>nombredeservidor</replaceable> que hace el trabajo de
DCHP, servidor, servidor TFTP y puerta de enlace de la red.
Ciertamente necesitar� cambiar las opciones de nombre de dominio, as�
como la direcci�n hardware del servidor y del cliente. La opci�n
<replaceable>filename</replaceable> debe ser el nombre del fichero que
se recuperar� a trav�s de TFTP.

</para><para>

Despu�s de editar el fichero de configuraci�n de <command>dhcpd</command>,
rein�cielo con <userinput>/etc/init.d/dhcpd3-server restart</userinput>.

</para>

   <sect3 arch="x86">
   <title>Habilitar el arranque PXE en la configuraci�n DHCP</title>
<para>
Aqu� hay otro ejemplo para un <filename>dhcp.conf</filename> usando el
m�todo de Entorno de ejecuci�n de pre-arranque (PXE) de TFTP.

<informalexample><screen>
option domain-name "ejemplo.com";

default-lease-time 6048;
max-lease-time 604800;

allow booting;
allow bootp;

# El siguiente p�rrafo debe modificarse para adaptarlo a su caso
subnet 192.168.1.0 netmask 255.255.255.0 {
  range 192.168.1.200 192.168.1.253;
  option broadcast-address 192.168.1.255;
# la direcci�n de la puerta de enlace puede ser diferente
# (por ejemplo, acceso a Internet)
  option routers 192.168.1.1;
# indique el dns que desea usar
  option domain-name-servers 192.168.1.3;
}

group {
  next-server 192.168.1.3;
  host tftpclient {
# direcci�n hardware del cliente tftp
  hardware ethernet  00:10:DC:27:6C:15;
  filename "pxelinux.0";
 }
}
</screen></informalexample>

Note que para el arranque PXE, el fichero del cliente
<filename>pxelinux.0</filename> es un gestor de arranque, no
una imagen de n�cleo (vea <xref linkend="tftp-images"/> a
continuaci�n).

</para>
   </sect3>
  </sect2>
