<?xml version="1.0" encoding="ISO-8859-1"?>
<!-- original version: 33772 -->

<sect1 id="doc-organization"><title>Organisation de ce document</title>

<para>
Ce document est destin� aux personnes qui utilisent Debian
pour la premi�re fois. Il tente de faire aussi peu appel que
possible � des connaissances sp�ciales de la part du lecteur. Cependant, il 
suppose une compr�hension �l�mentaire du fonctionnement de son mat�riel.
</para>

<para>
Les utilisateurs exp�riment�s pourront aussi trouver dans ce document 
des informations de r�f�rence, comme la place minimale
n�cessaire � une installation, des pr�cisions au sujet du mat�riel reconnu 
par le syst�me d'installation de Debian, etc. Nous encourageons les 
utilisateurs exp�riment�s � naviguer dans ce document.
</para>

<para>
Ce document, organis� de fa�on lin�aire, guide l'utilisateur � travers le 
processus d'installation. Voici les diff�rentes �tapes de l'installation de
&debian;, et les sections de ce document qui s'y rapportent&nbsp;:

<orderedlist>
<listitem><para>
Comment d�terminer si votre mat�riel poss�de la configuration minimale 
n�cessaire au syst�me d'installation est expliqu� dans le  
<xref linkend="hardware-req"/>&nbsp;;
</para></listitem>

<listitem><para>
Comment faire une sauvegarde de votre syst�me, pr�parer et configurer le 
mat�riel avant d'installer Debian, est expliqu� dans le 
<xref linkend="preparing"/>. 
Si vous pr�voyez de pouvoir d�marrer plusieurs syst�mes, vous aurez besoin de 
partitionner votre disque dur&nbsp;;
</para></listitem>

<listitem><para>
Dans le <xref linkend="install-methods"/>, vous trouverez les fichiers 
n�cessaires pour la m�thode d'installation que vous avez choisie&nbsp;;
</para></listitem>

<listitem><para>
Le <xref linkend="boot-installer"/> d�crit comment amorcer le 
syst�me d'installation&nbsp;; ce chapitre contient aussi des proc�dures de 
d�pannage en cas de probl�mes lors du d�marrage&nbsp;;
</para></listitem>

<listitem><para>
L'installation r�elle est d�taill�e dans le <xref linkend="d-i-intro"/>. Elle
comprend le choix d'une langue, la configuration des modules pour les pilotes 
de p�riph�riques, la configuration de la connexion r�seau &mdash; ainsi, quand
on ne fait pas l'installation � partir d'un c�d�rom, les autres fichiers 
d'installation pourront �tre r�cup�r�s directement sur un serveur 
Debian &mdash;, le partitionnement des disques durs, l'installation du
syst�me de base et la s�lection et l'installation des t�ches&nbsp;;
certains �l�ments concernant la mani�re de cr�er des partitions pour un syst�me
Debian sont donn�s dans l'<xref linkend="partitioning"/>.
</para></listitem>

<listitem><para>
Comment amorcer le syst�me de base install� est expliqu� dans le 
<xref linkend="boot-new"/>&nbsp;;
</para></listitem>

</orderedlist>
</para>

<para>
Une fois que vous avez install� votre syst�me, vous pouvez lire le 
<xref linkend="post-install"/>. Ce chapitre explique o� trouver plus 
d'informations sur Unix et Debian et comment remplacer votre noyau. 

<!-- XXX FIXME: If you want to build your own install system
from source, be sure to read <xref linkend="boot-floppy-techinfo"/>. -->

</para>

<para>
Enfin, vous trouverez des informations sur ce document et sur la mani�re d'y
contribuer dans l'<xref linkend="administrivia"/>.
</para>

</sect1>

 <sect1 condition="FIXME">
  <title>Toute aide est la bienvenue</title>

  <para>
Aide et suggestions, en particulier les correctifs, sont grandement
appr�ci�s. Les versions de travail de ce document sont sur
<ulink url="&url-d-i-alioth-manual;"></ulink>. Vous y trouverez les
architectures et les langues pour lesquelles ce document est disponible.
  </para>

<para>
La version source est aussi librement disponible. Pour davantage 
d'informations sur la mani�re de contribuer, voyez l'
<xref linkend="administrivia"/>.
Nous appr�cions les suggestions, les commentaires, les correctifs et les 
rapports de bogue (envoyez le rapport contre le paquet 
<classname>installation-guide</classname>, mais
v�rifiez d'abord que le probl�me ne soit pas d�j� connu).
</para>
</sect1>
