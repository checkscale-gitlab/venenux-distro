<?xml version="1.0" encoding="ISO-8859-1"?>
<!-- original version: 44002 -->

  <sect2 arch="x86"><title>Amorcer depuis un c�d�rom</title>

&boot-installer-intro-cd.xml;

<!-- We'll comment the following section until we know exact layout -->
<!-- 
Le c�d�rom n�&nbsp;1 du jeu officiel de c�d�roms pour
      &arch-title; vous pr�sentera une invite <prompt>boot:</prompt> sur la
      plupart des mat�riels. Appuyez sur <keycap>F4</keycap> pour afficher la
      liste des options du noyau disponibles � partir desquelles
      amorcer. Tapez simplement le nom de la saveur (idepci, vanilla,
      compact, bf24) apr�s l'invite de <prompt>boot:</prompt> et validez par
      &enterkey;.

</para><para>

Si votre mat�riel ne supporte pas l'amor�age d'images multiples,
      ins�rez un des autres c�d�roms dans le lecteur. Il semble que la
      plupart des lecteurs de c�d�roms SCSI soient incapables de g�rer
l'amor�age d'images multiples de <command>isolinux</command>, les
      possesseurs de tels lecteurs de c�d�roms devraient essayer le
      c�d�rom 2 (vanilla), 3 (compact) ou 5 (bf2.4).

</para><para>

Les c�d�roms num�ro 2 � 5 amorceront chacun sur une saveur diff�rente
d�pendant du c�d�rom ins�r�. cf.<xref linkend="kernel-choice"/>
pour une discussion sur les diff�rentes
variantes. Voici comment sont dispos�es les variantes sur les
      diff�rents c�d�roms&nbsp;:

<variablelist>
<varlistentry>
<term>CD 1</term><listitem><para>

           Sur les nouveaux mat�riels, il permet une s�lection
           d'images de noyau � amorcer. Sur les vieux mat�riels, il
           amorcera le noyau en saveur �&nbsp;idepci&nbsp;�&nbsp;;

</para></listitem></varlistentry>
<varlistentry>
<term>CD 2</term><listitem><para>

Amorce la saveur �&nbsp;vanilla&nbsp;�&nbsp;;

</para></listitem></varlistentry>
<varlistentry>
<term>CD 3</term><listitem><para>

Amorce la saveur �&nbsp;compact&nbsp;�&nbsp;;

</para></listitem></varlistentry>
<varlistentry>
<term>CD 4</term><listitem><para>

Amorce la saveur �&nbsp;idepci&nbsp;�&nbsp;;

</para></listitem></varlistentry>
<varlistentry>
<term>CD 5</term><listitem><para>

Amorce la saveur �&nbsp;bf2.4&nbsp;�.

</para></listitem></varlistentry>

 </variablelist>

</para><para>

-->

</sect2>

<!-- FIXME the documented procedure does not exactly work, commented out
      until fixes

  <sect2 arch="x86" id="install-from-dos">
  <title>Amorcer depuis une partition DOS</title>

&boot-installer-intro-hd.xml;

<para>

D�marrez en MS-DOS (pas en Windows) sans charger de pilotes. Pour faire cela, 
il faut presser <keycap>F8</keycap> au bon moment (et �ventuellement 
s�lectionner l'option �&nbsp;safe mode command prompt only&nbsp;�&nbsp;: 
�&nbsp;mode DOS sans �chec&nbsp;�). Puis allez dans le sous-r�pertoire qui
contient la saveur que vous avez choisie, p. ex.

<informalexample><screen>
cd c:\current\compact
</screen></informalexample>.  

Ensuite ex�cutez <command>install.bat</command>. Le noyau se chargera et 
lancera l'installateur.

</para><para>

Veuillez noter qu'il y a actuellement un probl�me avec loadlin 
(bogue n�142421) qui emp�che le fichier <filename>install.bat</filename> 
d'�tre utilis� par la saveur bf2.4. Le sympt�me de ce probl�me est le message 
<computeroutput>invalid compressed format</computeroutput>.

</para>
  </sect2>

END FIXME -->

  <sect2 arch="x86" id="boot-initrd">
  <title>Amorcer � partir de Linux avec <command>LILO</command>
  ou <command>GRUB</command></title>

<para>
Pour amorcer l'installateur sur un disque dur, vous devez d'abord t�l�charger
les fichiers et les installer comme l'explique la section
<xref linkend="boot-drive-files"/>.
</para>

<para>
Si vous voulez utiliser le disque seulement pour l'amor�age et ensuite 
utiliser le r�seau pour t�l�charger tous les paquets, vous devez r�cup�rer 
le fichier <filename>netboot/debian-installer/&architecture;/initrd.gz</filename> et le
noyau qui lui correspond, <filename>netboot/debian-installer/&architecture;/linux</filename>.
Vous pourrez ainsi repartitionner le disque sur lequel se trouve l'installateur.
Toutefois cette op�ration demande un grand soin.
</para>

<para>
Si vous voulez pr�server une partition de ce disque, vous pouvez
t�l�charger le fichier <filename>hd-media/initrd.gz</filename> et le noyau
correspondant&nbsp;; copiez aussi une image CD sur le disque (assurez-vous
que le nom de cette image finit en <literal>.iso</literal>). L'installateur
peut alors se lancer � partir du disque dur et s'installer � partir de cette
image, sans avoir besoin du r�seau.
</para>

<para>

Pour <command>LILO</command>, vous devez configurer deux choses essentielles
dans <filename>/etc/lilo.conf</filename>&nbsp;:
<itemizedlist>
<listitem><para>

le chargement de <filename>initrd.gz</filename> au moment de l'amor�age&nbsp;;

</para></listitem>
<listitem><para>

l'utilisation par le noyau <filename>vmlinuz</filename> d'un disque virtuel
en m�moire comme sa partition racine.

</para></listitem>
</itemizedlist>


Voici un exemple de fichier <filename>/etc/lilo.conf</filename>&nbsp;:


</para><para>

<informalexample><screen>
image=/boot/newinstall/vmlinuz
       label=newinstall
       initrd=/boot/newinstall/initrd.gz
</screen></informalexample>

Pour plus de pr�cisions, r�f�rez-vous aux pages de manuel de
<citerefentry><refentrytitle>initrd</refentrytitle>
<manvolnum>4</manvolnum></citerefentry> et de 
<citerefentry><refentrytitle>lilo.conf</refentrytitle>
<manvolnum>5</manvolnum></citerefentry>. Ex�cutez maintenant
<userinput>lilo</userinput> et relancez la machine.
</para>

<para>
La proc�dure pour <command>GRUB</command> est relativement similaire.
Cherchez le fichier <filename>menu.lst</filename> dans le r�pertoire
<filename>/boot/grub/</filename> (parfois dans
<filename>/boot/boot/grub/</filename>), ajoutez les lignes suivantes&nbsp;:

<informalexample><screen>
title  New Install
kernel (hd0,0)/boot/newinstall/vmlinuz
initrd (hd0,0)/boot/newinstall/initrd.gz
</screen></informalexample>

et red�marrez.
</para>
<para>
Il sera peut-�tre n�cessaire d'augmenter 
le param�tre <userinput>ramdisk_size</userinput>, selon l'image que vous
d�marrez.
� partir de maintenant, il ne devrait plus y avoir de diff�rences
entre <command>GRUB</command> et <command>LILO</command>.
</para>
</sect2>

<sect2 arch="x86" condition="bootable-usb" id="usb-boot">
<title>Amorcer sur une cl� USB</title>

<para>
Supposons que tout a �t� pr�par� comme l'expliquent les sections
<xref linkend="boot-dev-select"/> et <xref linkend="boot-usb-files"/>. 
Branchez maintenant votre cl� USB dans un port libre et relancez la machine.
Le syst�me devrait se lancer et une invite <prompt>boot:</prompt> appara�tre.
Maintenant, vous pouvez saisir des param�tres optionnels ou simplement
appuyer sur la touche &enterkey;.
</para>

</sect2>

  <sect2 arch="x86" condition="supports-floppy-boot" id="floppy-boot">
  <title>Amorcer depuis des disquettes</title>
<para>

Vous devez avoir d�j� t�l�charg� les images de disquettes n�cessaires
� partir des images pr�sentes dans <xref linkend="create-floppy"/>. 
<!-- missing-doc FIXME If you need to, you can also modify the boot floppy; see\
<xref linkend="rescue-replace-kernel"/>. -->
</para>
<para>

Pour amorcer depuis la disquette d'amor�age, placez-la dans le premier 
lecteur de disquette, �teignez la machine comme 
vous le faites habituellement puis rallumez-la.

</para><para>

Pour r�aliser l'installation � partir d'un lecteur LS-120 (version ATAPI) 
avec un jeu de disquettes, vous devrez pr�ciser l'emplacement virtuel du 
lecteur de disquette. On utilise le param�tre d'amor�age
<emphasis>root=</emphasis>, en indiquant le p�riph�rique que le 
pilote ide-floppy aura simul�. Par exemple si votre lecteur est connect� sur 
la premi�re interface IDE en seconde position sur le c�ble, entrez
<userinput>install root=/dev/hdc</userinput> � l'invite de d�marrage.

</para><para>

Remarquez bien que sur certaines machines <keycombo><keycap>Control</keycap>
<keycap>Alt</keycap> <keycap>Delete</keycap></keycombo> ne remet pas 
correctement la machine � z�ro. �teindre la machine est alors recommand�. Si 
vous installez depuis un syst�me d'exploitation existant, (p. ex. � partir 
d'une machine MS-DOS), vous n'aurez pas le choix. Sinon, �teignez la
machine et rallumez-la.

</para><para>

La disquette sera lue et vous devriez apercevoir un �cran 
pr�sentant la disquette d'amor�age et l'invite <prompt>boot:</prompt> au bas de
l'�cran.
</para>

<para>

Une fois que vous avez appuy� sur la touche &enterkey;, vous devez voir le 
message
<computeroutput>Loading...</computeroutput> suivi par
<computeroutput>Uncompressing Linux...</computeroutput> et ensuite un �cran
rempli d'informations sur les �l�ments mat�riels de votre machine. Vous
pouvez trouver un compl�ment d'information sur cette phase dans la
<xref linkend="kernel-msgs"/>.
</para>

<para>
Apr�s avoir d�marr� depuis la disquette d'amor�age, la disquette root est
demand�e. Ins�rez-la puis pressez la touche &enterkey; et le contenu est
charg� en m�moire. L'<command>installateur Debian</command> est 
automatiquement lanc�.
</para>
</sect2>

  <sect2 arch="x86" id="boot-tftp"><title>Amorcer avec TFTP</title>
 
&boot-installer-intro-net.xml;
 
<para>

Il y a plusieurs fa�ons d'amorcer avec TFTP sur une machine i386.

</para>

   <sect3><title>Carte r�seau ou carte m�re avec PXE</title>
<para>

Il est possible que votre interface r�seau ou votre carte m�re permette
l'amorcage PXE. C'est une r�impl�mentation de l'amor�age TFTP par 
<trademark class="trade">Intel</trademark>. Dans ce cas, vous pourrez 
configurer votre bios pour qu'il s'amorce sur le r�seau.

</para>
   </sect3>

   <sect3><title>Carte r�seau avec ROM de d�marrage r�seau</title>
<para>

Une carte d'interface r�seau peut offrir la possibilit� de d�marrer par TFTP.

</para><para condition="FIXME">

Dites-nous (<email>&email-debian-boot-list;</email>) comment vous 
avez fait. Veuillez vous r�f�rer � ce document.

</para>
   </sect3>

   <sect3><title>Etherboot</title>
<para>
Le <ulink url="http://www.etherboot.org">projet etherboot</ulink> offre des
disquettes d'amor�age et m�me des ROM qui permettent l'amor�age par TFTP.
</para>
</sect3>
</sect2>

  <sect2 arch="x86">
<title>L'invite d'amor�age</title>
<para>
Quand l'installateur d�marre, une invite d'amor�age, <prompt>boot:</prompt>
appara�t dans un joli �cran montrant le logo de la distribution. 

<informalexample><screen>
Press F1 for help, or ENTER to boot:
</screen></informalexample>

Vous pouvez maintenant appuyer sur la touche &enterkey; pour
lancer l'installateur avec les options par d�faut, ou vous pouvez
indiquer une m�thode sp�cifique d'installation avec d'autres param�tres d'amor�age.
</para>

<para>
Des informations utiles sur les param�tres d'amor�age peuvent �tre consult�es
en pressant les touches <keycap>F3</keycap> � 
<keycap>F8</keycap>.
Si vous ajoutez des param�tres � la ligne de commande, n'oubliez pas
d'indiquer la m�thode d'amor�age (la valeur par d�faut est
<userinput>install</userinput>) et un espace avant le premier param�tre
(par exemple, <userinput>install fb=false</userinput>). 

<note><para>
Si vous installez le syst�me depuis une machine distante qui offre une
interface texte � la console VGA, il se peut que l'�cran graphique
n'apparaisse pas&nbsp;; vous pouvez aussi ne pas voir l'invite
d'amor�age elle-m�me. Sur ces syst�mes distants comme la console texte
<quote>integrated Lights Out</quote> (iLO) de Compaq ou la 
<quote>Integrated Remote Assistant</quote> (IRA) de HP, vous pouvez
simplement appuyer sur la touche F1 <footnote>
<para>
Ces syst�mes demandent parfois des s�quences sp�ciales pour activer
cette touche. Par exemple, la console IRA utilise
<keycombo><keycap>Ctrl</keycap><keycap>F</keycap> </keycombo>,&nbsp;<keycap>1</keycap>.
</para></footnote> 
pour sauter cet �cran et voir le texte d'aide. Une fois dans l'aide, les
touches produiront le r�sultat attendu. Pour emp�cher l'installateur
d'utiliser le tampon vid�o dans la suite de l'installation, vous pouvez
ajouter le param�tre <userinput>fb=false</userinput> � l'invite d'amor�age.

</para></note>

</para>
  </sect2>
