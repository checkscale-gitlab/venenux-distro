<!-- retain these comments for translator revision tracking -->
<!-- original version: 18642 untranslated -->

 <sect2 id="configure-apt">
 <title>Configuring APT</title>

<para>

The main means that people use to install packages on their system is
via a program called <command>apt-get</command>, from the
<classname>apt</classname> package.<footnote>

<para>

Note that the actual program that installs packages is called
<command>dpkg</command>.  However, this package is more of a low-level
tool.  <command>apt-get</command> will invoke <command>dpkg</command>
as appropriate; it is a higher-level too, however, because it knows to
install other packages which are required for the package you're
trying to install, as well as how to retrieve the package from your
CD, the network, or wherever.

</para>
</footnote>

APT must be configured, however, so that it knows where to retrieve
packages from.  The helper application which assists in this task is
called <command>apt-setup</command>.

</para><para>

The next step in your configuration process is to tell APT where other
Debian packages can be found.  Note that you can re-run this tool at
any point after installation by running <command>apt-setup</command>,
or by manually editing <filename>/etc/apt/sources.list</filename>.

</para><para>

If an official CD-ROM is in the drive at this point, then that CD-ROM
should automatically be configured as an apt source without prompting.
You will notice this because you will see the CD-ROM being scanned.

</para><para>

For users without an official CD-ROM, you will be offered an array of
choices for how Debian packages are accessed: FTP, HTTP, CD-ROM, or
a local file system.

</para><para>

You should know that it's perfectly acceptable to have a number of
different APT sources, even for the same Debian archive.
<command>apt-get</command> will automatically pick the package with
the highest version number given all the available versions.  Or, for
instance, if you have both an HTTP and a CD-ROM APT source,
<command>apt-get</command> should automatically use the local CD-ROM
when possible, and only resort to HTTP if a newer version is available
there.  However, it is not a good idea to add unnecessary APT sources,
since this will tend to slow down the process of checking the network
archives for new versions.

</para>

  <sect3 id="configure-apt-net">
  <title>Configuring Network Package Sources</title>

<para>

If you plan on installing the rest of your system via the network, the
most common option is to select the <userinput>http</userinput>
source.  The <userinput>ftp</userinput> source is also acceptable, but
tends to be a little slower making connections.

</para><para>

The next step during the configuration of network packages sources is
to tell <command>apt-setup</command> which country you live in.  This
configures which of the official Debian Internet mirror network you
connect to.  Depending on which country you select, you will be given
a list of possible machines.  Its generally fine to pick the one on
the top of the list, but any of them should work.

</para><para>

If you are installing via HTTP, you will be asked to configure your
proxy server.  This is sometimes required by people behind firewalls,
on corporate networks, etc.

</para><para>

Finally, your new network package source will be tested.  If all goes
well, you will be prompted whether you want to do it all over again
with another network source.

</para>
  </sect3>
 </sect2>
