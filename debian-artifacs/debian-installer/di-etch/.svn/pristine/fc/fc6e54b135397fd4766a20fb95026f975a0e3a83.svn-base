<!-- $Id: delo-installer.xml 37321 2006-05-14 16:17:19Z mck-guest $ -->
<!-- original version: 24663 -->

  <sect3 arch="mipsel">
  <title>Instalovat zavaděč <command>delo</command> na pevný disk</title>
<para>

Na strojích DECstation se používá zavaděč <command>DELO</command>. Ten
musí být nainstalován na stejném disku jako jádro (o to se postará
instalační program). <command>DELO</command> podporuje několik různých
konfigurací, které se zapisují do souboru
<filename>/etc/delo.conf</filename>.  Každá taková konfigurace musí
mít jedinečný název. Například standardní konfigurace vytvořená při
instalaci se jmenuje <quote>linux</quote>.  Po instalaci můžete systém
zavést z firmwaru příkazem

<informalexample><screen>
<userinput>boot <replaceable>#</replaceable>/rz<replaceable>id</replaceable> <replaceable>cisobl</replaceable>/<replaceable>jmeno</replaceable></userinput>
</screen></informalexample>

</para>

<variablelist>
<varlistentry>
<term> <replaceable>#</replaceable> </term>
<listitem><para>

je zařízení TurboChannel, ze kterého se má zavést systém.  Většina
počítačů DECstation používá hodnotu <userinput>3</userinput>, což je
řadič umístěný na základní desce.

</para></listitem>
</varlistentry>
<varlistentry>
<term> <replaceable>id</replaceable> </term>
<listitem><para>

je SCSI ID pevného disku, na kterém je zavaděč nainstalován.

</para></listitem>
</varlistentry>
<varlistentry>
<term> <replaceable>cisobl</replaceable> </term>
<listitem><para>

je číslo oblasti obsahující <filename>/etc/delo.conf</filename>

</para></listitem>
</varlistentry>
<varlistentry>
<term> <replaceable>jmeno</replaceable> </term>
<listitem><para>

je název konfigurace v <filename>/etc/delo.conf</filename>, což je
standardně <quote>linux</quote>.

</para></listitem>
</varlistentry>
</variablelist>

<para>

V případě, že chcete zavést implicitní konfiguraci
a <filename>/etc/delo.conf</filename> je na první oblasti na disku,
stačí použít příkaz

<informalexample><screen>
<userinput>boot #/rz<replaceable>id</replaceable></userinput>
</screen></informalexample>

</para>
  </sect3>
