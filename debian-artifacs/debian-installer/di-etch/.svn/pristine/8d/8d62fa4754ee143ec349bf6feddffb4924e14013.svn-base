#!/bin/sh

. /usr/share/debconf/confmodule

set -e

log() {
	logger -t finish-install "$@"
}

partsdir="/usr/lib/finish-install.d"
oldpartsdir="/usr/lib/prebaseconfig.d"
scriptcount=`ls "$partsdir"/* "$oldpartsdir"/* | wc -l`

db_progress START 0 $scriptcount finish-install/progress/title

for script in $(ls "$partsdir"/* "$oldpartsdir"/* | sort -n -k5 -t/)
do
	base=$(basename $script | sed 's/[0-9]*//')
	if ! db_progress INFO finish-install/progress/$base &&
	   ! db_progress INFO prebaseconfig/progress/$base; then
		db_subst finish-install/progress/fallback SCRIPT "$base"
		db_progress INFO finish-install/progress/fallback
	fi

	if [ -x "$script" ] ; then
		log "info: Running $script"
		# Else needed so we don't loose the exit code
		if log-output -t finish-install "$script"; then
			:
		else
			code="$?"
			if [ "$code" = 10 ]; then
				log "$script backed up"
				db_progress STOP
				exit 10
			fi
			log "warning: $script returned error code $code"
		fi
	else
		log "error: Unable to execute $script"
	fi

	db_progress STEP 1
done

db_progress STOP
