<?xml version="1.0" encoding="ISO-8859-1"?>
<!-- original version: 43577 -->

<sect3 id="partman-lvm">
   <title>Configuration du <quote>Logical Volume Manager</quote> (LVM)</title>
<para>

Si vous travaillez sur les ordinateurs comme administrateur syst�me ou
si vous �tes un utilisateur <quote>avanc�</quote>, vous avez s�rement
rencontr� le cas suivant&nbsp;: une partition qui manque d'espace libre 
(c'est habituellement la plus importante du syst�me) et une partition
grossi�rement sous-utilis�e, la gestion de cette situation ayant consist�
en d�placement de fichiers, cr�ation de liens symboliques, etc.
</para>
<para>
Pour �viter cette situation, on peut utiliser un gestionnaire de volumes
logiques, <quote>Logical Volume Manager</quote> (LVM). Dit simplement,
avec LVM vous pouvez combiner vos partitions 
(<firstterm>volumes physiques</firstterm> dans le lexique LVM) pour former
un disque virtuel (<firstterm>groupe de volumes</firstterm>) qui peut �tre
divis� en partitions virtuelles (<firstterm>volumes logiques</firstterm>).
L'id�e est que les volumes logiques s'�tendent sur plusieurs disques physiques.
</para>
<para>
D�s lors, quand vous constatez que votre vieille partition 
<filename>/home</filename> de 160&nbsp;Go a besoin d'espace, vous pouvez
simplement ajouter un disque de 300&nbsp;Go � votre machine, l'int�grer au
groupe de volumes existant et redimensionner le volume logique qui comprend
la partition <filename>/home</filename> et vos utilisateurs ont
de nouveau de la place sur la partition de 460&nbsp;Go&nbsp;!
Cet exemple est bien s�r un peu simplifi�. Si vous ne l'avez pas encore lu,
vous devriez consulter le <ulink url="&url-lvm-howto;">HOWTO LVM</ulink>.

</para>
<para>

La configuration de LVM dans l'installateur Debian est tr�s simple. D'abord,
vous devez marquer les partitions � utiliser comme volumes physiques par
LVM. Cela se fait avec <command>partman</command> dans le menu 
<guimenu>Configuration des partitions</guimenu>, o� vous choisissez
<menuchoice><guimenu>Utiliser comme :</guimenu> 
<guimenuitem>volume physique pour LVM</guimenuitem></menuchoice>.

</para>
<para>

Quand vous revenez � l'�cran principal de <command>partman</command>, vous voyez
une nouvelle option 
<guimenuitem>Configurer LVM, le gestionnaire des volumes logiques</guimenuitem>.
Quand vous la s�lectionnez, on vous demande d'abord de confirmer les modifications en
attente pour la table des partitions (s'il y en a) et le menu de configuration de LVM sera
affich�. Le menu n'affiche que les actions valables selon le contexte.
Les actions possibles sont&nbsp;:

<itemizedlist>
  <listitem><para>
    <guimenuitem>Afficher les d�tails de la configuration</guimenuitem> :
    montre la structure des volumes LVM, le nom et la taille des volumes, etc.
  </para></listitem>
  <listitem><para>
    <guimenuitem>Cr�er un groupe de volumes</guimenuitem>
  </para></listitem>
  <listitem><para>
    <guimenuitem>Cr�er un volume logique</guimenuitem>
  </para></listitem>
  <listitem><para>
    <guimenuitem>Supprimer un groupe de volumes</guimenuitem>
  </para></listitem>
  <listitem><para>
    <guimenuitem>Supprimer un volume logique</guimenuitem>
  </para></listitem>
  <listitem><para>
    <guimenuitem>Etendre un volume logique</guimenuitem>
  </para></listitem>
  <listitem><para>
    <guimenuitem>R�duire un volume logique</guimenuitem>
  </para></listitem>
  <listitem><para>
    <guimenuitem>Terminer</guimenuitem>:
    retourne � l'�cran principal de <command>partman</command>.
  </para></listitem>
</itemizedlist>

</para>
<para>
Utilisez les options de ce menu pour cr�er d'abord un groupe de volumes et pour
cr�er ensuite les volumes logiques.
</para>

<para>  
Quand vous revenez � l'�cran principal de <command>partman</command>,
tous les volumes logiques cr��s sont affich�s comme si c'�taient
de simples partitions, et vous devez les traiter ainsi.
</para>
</sect3>


