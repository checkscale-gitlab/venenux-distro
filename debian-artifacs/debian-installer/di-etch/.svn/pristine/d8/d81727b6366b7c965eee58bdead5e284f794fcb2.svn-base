<!-- retain these comments for translator revision tracking -->
<!-- original version: 35613 -->


  <sect2 arch="mips" id="boot-tftp">
  <!-- <title>Booting with TFTP</title> -->
  <title>Avvio con TFTP</title>

   <sect3>
   <!-- <title>SGI TFTP Booting</title> -->
   <title>Avvio di macchine SGI con TFTP</title>
<para>

<!--
After entering the command monitor use
-->

Sulle macchine SGI una volta entrati nel command monitor usare

<informalexample><screen>
bootp(): root=/dev/ram0
</screen></informalexample>

<!--
on SGI machines to boot linux and to begin installation of the Debian
Software.  In order to make this
work you may have to unset the <envar>netaddr</envar> environment
variable. Type
-->

per avviare Linux e iniziare l'installazione di Debian. Per fare questa
operazione potrebbe essere necessario eliminare la variabile d'ambiente
<envar>netaddr</envar>, eseguire

<informalexample><screen>
unsetenv netaddr
</screen></informalexample>

<!--
in the command monitor to do this.
-->

dal command monitor per farlo.

</para>
   </sect3>

   <sect3>
   <!-- <title>Broadcom BCM91250A and BCM91480B TFTP Booting</title> -->
   <title>Avvio di Broadcom BCM91250A e BCM91480B con TFTP</title>
<para>

<!--
On the Broadcom BCM91250A and BCM91480B evaluation board, you have to load the SiByl boot
loader via TFTP which will then load and start the Debian installer.  In
most cases, you will first obtain an IP address via DHCP but it is also
possible to configure a static address.  In order to use DHCP, you can
enter the following command on the CFE prompt:
-->

Sulle schede per prototipazione Broadcom BCM91250A e BCM91480B è necessario
caricare il bootloader SiByl tramite TFTP, poi questo avvierà l'Installatore
Debian. Nella maggior parte dei casi si ottiene un indirizzo IP via DHCP
oppure è anche possibile configurare un indirizzo statico. Per usare DHCP
si può inserire il seguente comando dal prompt di CFE:

<informalexample><screen>
ifconfig eth0 -auto
</screen></informalexample>

<!--
Once you have obtained an IP address, you can load SiByl with the following
command:
-->

Un volta ottenuto un indirizzo IP si può caricare SiByl con usando il
seguente comando:

<informalexample><screen>
boot 192.168.1.1:/boot/sibyl
</screen></informalexample>

<!--
You need to substitute the IP address listed in this example with either
the name or the IP address of your TFTP server.  Once you issue this
command, the installer will be loaded automatically.
-->

Si deve sostituire l'indirizzo IP mostrato nell'esempio con il nome o
con l'indirizzo IP del proprio server TFTP. Una volta corretto questo
comando l'installatore si dovrebbe caricare automaticamente.

</para>
   </sect3>
  </sect2>

  <sect2 arch="mips">
  <!-- <title>Boot Parameters</title> -->
  <title>Parametri di avvio</title>

   <sect3>
   <!-- <title>SGI TFTP Booting</title> -->
   <title>Avvio di macchine SGI con TFTP</title>
<para>

<!--
On SGI machines you can append boot parameters to the
<command>bootp():</command> command in the command monitor.
-->

Sulle macchine SGI è possibile aggiungere dei parametri di avvio al comando
<command>bootp():</command> nel command monitor.

</para><para>

<!--
Following the <command>bootp():</command> command you can give the
path and name of the file to boot if you did not give an explicit name
via your bootp/dhcp server. Example:
-->

Dopo il comando <command>bootp():</command> si può inserire il percorso
e il nome del file da avviare se non si è già specificato un nome
tramite il server bootp/dhcp. Per esempio:

<informalexample><screen>
bootp():/boot/tftpboot.img
</screen></informalexample>

<!--
Further kernel parameters can be passed via <command>append</command>:
-->

Ulteriori parametri possono essere passati al kernel tramite
<command>append</command>:

<informalexample><screen>
bootp(): append="root=/dev/sda1"
</screen></informalexample>

</para>
   </sect3>

   <sect3>
   <!-- <title>Broadcom BCM91250A and BCM91480B TFTP Booting</title> -->
   <title>Avvio di Broadcom BCM91250A e BCM91480B con TFTP</title>
<para>

<!--
You cannot pass any boot parameters directly from the CFE prompt.  Instead,
you have to edit the <filename>/boot/sibyl.conf</filename> file on the TFTP
server and add your parameters to the <replaceable>extra_args</replaceable>
variable.
-->

Non è possibile passare direttamente dei parametri di avvio dal prompt CFE,
si deve modificare il file <filename>/boot/sibyl.conf</filename> sul server
TFTP e aggiungere i parametri nella variabile
<replaceable>extra_args</replaceable>.

</para>
   </sect3>
  </sect2>
