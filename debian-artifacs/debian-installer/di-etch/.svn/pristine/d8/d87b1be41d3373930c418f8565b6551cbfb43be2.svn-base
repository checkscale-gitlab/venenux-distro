linux-kernel-di-amd64-2.6 (1.31) unstable; urgency=low

  * Built against version 2.6.21-6 of linux-2.6.

 -- Frans Pop <fjp@debian.org>  Thu, 12 Jul 2007 14:17:20 +0200

linux-kernel-di-amd64-2.6 (1.30) unstable; urgency=low

  * Update to kernel ABI 2.6.21-2.
  * Built against version 2.6.21-5 of linux-2.6.

 -- Frans Pop <fjp@debian.org>  Thu, 05 Jul 2007 17:21:34 +0200

linux-kernel-di-amd64-2.6 (1.29) unstable; urgency=low

  * Add new nic-wireless-modules udeb split out of nic-extra-modules.

 -- Joey Hess <joeyh@debian.org>  Mon, 04 Jun 2007 15:22:06 -0400

linux-kernel-di-amd64-2.6 (1.28) unstable; urgency=low

  * Update to kernel image version 2.6.21-4.

 -- Joey Hess <joeyh@debian.org>  Sun, 27 May 2007 13:57:48 -0400

linux-kernel-di-amd64-2.6 (1.27) unstable; urgency=low

  * Update to kernel image version 2.6.21-2.

 -- Joey Hess <joeyh@debian.org>  Wed, 23 May 2007 19:10:48 -0400

linux-kernel-di-amd64-2.6 (1.26) unstable; urgency=low

  * Update to kernel image version 2.6.21-1.

 -- Joey Hess <joeyh@debian.org>  Fri, 18 May 2007 01:07:30 -0400

linux-kernel-di-amd64-2.6 (1.25) unstable; urgency=low

  * Rebuilt with kernel-wedge 2.35.

 -- Joey Hess <joeyh@debian.org>  Thu, 10 May 2007 20:07:13 -0400

linux-kernel-di-amd64-2.6 (1.24) unstable; urgency=low

  * Update to kernel image version 2.6.20-3.

 -- Frans Pop <fjp@debian.org>  Wed, 25 Apr 2007 21:13:47 +0200

linux-kernel-di-amd64-2.6 (1.23) unstable; urgency=low

  [ Frans Pop ]
  * Drop obsolete input modules; hid was renamed to usbhid.
  * Drop usbkbd and usbmouse input modules, usbhid is preferred.

  [ Joey Hess ]
  * Update to kernel image version 2.6.20-2.

 -- Joey Hess <joeyh@debian.org>  Mon, 16 Apr 2007 16:22:01 -0400

linux-kernel-di-amd64-2.6 (1.22) unstable; urgency=low

  * Add ext2-modules udeb. The kernel-image had been providing ext2-modules,
    but while that's correct for the i386 (-486) kernel, which has ext2
    built-in, it's wrong for amd64, which has it modular.
  * Use kernel-wedge's pcmcia-storage-modules.
  * 2.6.20
  * Add new pata-modules and ata-modules packages.
  * Use kernel-wedge's firewire-core-modules again, now that it has eth1394
    moved.
  * firmware-modules renamed to core-modules.
  * Add mbcache to core-modules. It's not built into the kernel as it is on
    i386 since ext2 is modular here, and both ext2 and ext3 need it, so it's
    not much of a strech to call it a core module.

 -- Joey Hess <joeyh@debian.org>  Thu, 12 Apr 2007 03:27:01 -0400

linux-kernel-di-amd64-2.6 (1.21) unstable; urgency=low

  * Update to kernel image version 2.6.18.dfsg.1-11.

 -- Frans Pop <fjp@debian.org>  Thu, 22 Feb 2007 23:25:32 +0100

linux-kernel-di-amd64-2.6 (1.20) unstable; urgency=low

  * Rebuild using kernel-wedge 2.31 to include jmicron module.

 -- Frans Pop <fjp@debian.org>  Sun,  4 Feb 2007 15:32:12 +0100

linux-kernel-di-amd64-2.6 (1.19) unstable; urgency=low

  * Update to kernel image version 2.6.18.dfsg.1-10.

 -- Frans Pop <fjp@debian.org>  Sat,  3 Feb 2007 19:30:38 +0100

linux-kernel-di-amd64-2.6 (1.18) unstable; urgency=low

  * Update to kernel image version 2.6.18.dfsg.1-9.

 -- Joey Hess <joeyh@debian.org>  Sun, 28 Jan 2007 22:10:26 -0500

linux-kernel-di-amd64-2.6 (1.17) unstable; urgency=low

  * Update to kernel image version 2.6.18-8.

 -- Frans Pop <fjp@debian.org>  Wed, 13 Dec 2006 08:09:25 +0100

linux-kernel-di-amd64-2.6 (1.16) unstable; urgency=low

  * Update to kernel image version 2.6.18-7.

 -- Frans Pop <fjp@debian.org>  Wed,  6 Dec 2006 07:59:36 +0100

linux-kernel-di-amd64-2.6 (1.15) unstable; urgency=low

  * Update to kernel image version 2.6.18-6.

 -- Frans Pop <fjp@debian.org>  Wed, 29 Nov 2006 14:30:54 +0100

linux-kernel-di-amd64-2.6 (1.14) unstable; urgency=low

  * Add usbmouse module to mouse-modules for graphical installer.

 -- Frans Pop <fjp@debian.org>  Sat, 23 Sep 2006 16:23:07 +0200

linux-kernel-di-amd64-2.6 (1.13) unstable; urgency=low

  * Update to kernel image version 2.6.17-9.

 -- Frans Pop <fjp@debian.org>  Sun, 17 Sep 2006 02:14:52 +0200

linux-kernel-di-amd64-2.6 (1.12) unstable; urgency=low

  * Updated to 2.6.17-6.
  * Mark irda-modules/irport optional.
  * Rename packages from -amd64-generic to -amd64, following the flavour
    renaming in linux-2.6.

 -- Frederik Schüler <fs@debian.org>  Fri, 18 Aug 2006 18:29:37 +0200

linux-kernel-di-amd64-2.6 (1.11) unstable; urgency=low

  * Rebuild with linux-2.6.16 version 2.6.16-17.

 -- Frederik Schüler <fs@debian.org>  Tue, 18 Jul 2006 18:10:59 +0200

linux-kernel-di-amd64-2.6 (1.10) unstable; urgency=low

  [ Frederik Schüler ]
  * Make scsi-modules and scsi-extra-modules use again the shared
    kernel-wedge list.
  * Rebuild against linux-2.6.16 version 2.6.16-15.
  * Do not use eexpress from nic-extra-modules.

  [ Joey Hess ]
  * Remove de4x5 from nic-extra-modules, it's not registered to any pci
    device.
  * Merge nic-modules to kernel-wedge and i386-2.6.
  * kernel-wedge 2.23 nic-pcmcia-modules can be used unchanged
  * Use nic-extra-modules from kernel-wedge 2.24.

 -- Frederik Schüler <fs@debian.org>  Fri, 23 Jun 2006 11:34:46 +0200

linux-kernel-di-amd64-2.6 (1.9) unstable; urgency=low

  * Rebuild against linux-2.6.16-14.
  * Update scsi-modules list: use a modified list from kernel-wedge. 
  * fork scsi-extra-modules: add i2o_block and i2o_scsi.

 -- Frederik Schüler <fs@debian.org>  Wed, 14 Jun 2006 18:50:34 +0200

linux-kernel-di-amd64-2.6 (1.8) unstable; urgency=low

  [ Joey Hess ]
  * Switch nic-usb-modules to use the list in kernel-wedge 2.19, which
    includes asix.
  * Add crypto-modules.
  * Add usb-serial-modules.

  [ Frederik Schüler ]
  * First release targeted at the official Debian archive.
  * Rebuild with 2.6.15-8 from testing.

 -- Frederik Schüler <fs@debian.org>  Sun, 30 Apr 2006 16:23:30 +0200

linux-kernel-di-amd64-2.6 (1.7) unstable; urgency=low

  * Rebuild with 2.6.15-7.

 -- Frederik Schüler <fs@debian.org>  Fri, 24 Feb 2006 17:25:34 +0100

linux-kernel-di-amd64-2.6 (1.6) unstable; urgency=low

  * Rebuild with 2.6.15-4.

 -- Frederik Schüler <fs@debian.org>  Wed,  8 Feb 2006 22:50:25 +0100

linux-kernel-di-amd64-2.6 (1.5) unstable; urgency=low

  * Rebuild with 2.6.15-1.

 -- Frederik Schüler <fs@debian.org>  Wed,  4 Jan 2006 14:39:56 +0100

linux-kernel-di-amd64-2.6 (1.4) unstable; urgency=low

  * Use ABI not version in kernel-versions.

 -- Frederik Schüler <fs@debian.org>  Tue, 13 Dec 2005 08:46:44 +0100

linux-kernel-di-amd64-2.6 (1.3) unstable; urgency=low

  * Remove socket-modules, the modules are builtin now.
  * Use ide-modules list from kernel-wedge.
  * Rebuild with linux-2.6 version 2.6.14-5.

 -- Frederik Schüler <fs@debian.org>  Sun, 11 Dec 2005 16:10:09 +0100

linux-kernel-di-amd64-2.6 (1.2) unstable; urgency=low

  [ Joey Hess ]
  * Sync with i386 2.6 package:
    - Add qnx4-modules.
    - Mark vesafb as optional (removed in 2.6.14).

  [ Frederik Schüler ]
  * Rebuild with linux-image-2.6.14-2-amd64-generic version 2.6.14-3.
  * New modules in 2.6.14: cassini cxgb, sis190, skge, uli526x to 
    nic-extra-modules.
  * Add pcmcia-modules to package-list, depending on firmware-modules.
  * Make pcmcia-modules and scsi-extra-modules inlcude the kernel-wedge 
    lists, and depend on kernel-wedge (>= 2.10) because of this.
  * Mark fbcon optional, it's compiled in now.
  * Add myself to uploaders.

 -- Frederik Schüler <fs@debian.org>  Sat, 19 Nov 2005 15:05:03 +0100

linux-kernel-di-amd64-2.6 (1.1) unstable; urgency=low

  * Rebuild with linux-image-2.6.12-1-amd64-generic version 2.6.12-10.

 -- Frederik Schüler <fschueler@gmx.net>  Thu, 29 Sep 2005 20:38:25 +0200

linux-kernel-di-amd64-2.6 (1.0) unstable; urgency=low

  [ Joey Hess ]
  * Use common usb-modules and serial-modules from kernel-wedge; usbserial
    moves to serial-modules.

  [ Frederik Schüler ]
  * build against linux-image-2.6.12-1-amd64-generic version 2.6.12-6.
  * bump version (should have done this already for the last release).

 -- Frederik Schüler <fschueler@gmx.net>  Sat, 17 Sep 2005 19:33:14 +0200

linux-kernel-di-amd64-2.6 (0.23) unstable; urgency=low

  [ Joey Hess ]
  * Switch to kernel-wedge 2.0.
  * De-fork sata-modules now that kernel-wedge is updates.

  [ Frederik Schüler ]
  * build against linux-image-2.6.12-1-amd64-generic
  * Adapt all module files to newstyle kernel wedge.
  * Add crc-modules and mouse-modules

 -- Frederik Schüler <fschueler@gmx.net>  Sun,  7 Aug 2005 20:45:11 +0200

linux-kernel-di-amd64-2.6 (0.22) unstable; urgency=low

  * Added sata_uli to sata-modules, temporary forking that file until next
    kernel-wedge upload.

 -- Frederik Schüler <fschueler@gmx.net>  Mon, 14 Feb 2005 20:37:22 +0100

linux-kernel-di-amd64-2.6 (0.21) unstable; urgency=low

  * Rebuild with new kernel-image-2.6.8-10-amd64-generic featuring 
    CONFIG_EXT2_FS=y.

 -- Frederik Schüler <fschueler@gmx.net>  Wed,  9 Feb 2005 23:49:45 +0100

linux-kernel-di-amd64-2.6 (0.20) unstable; urgency=low

  * Added kernel-image-2.6.8-10-amd64-generic to list of kernels.
  * Merged changes to modules configurations to work with both 2.6.8 and 
    2.6.10
  * Rebuild with latest kernel-image-2.6.8-10-amd64-generic and
    kernel-image-2.6.10-9-amd64-generic 

 -- Frederik Schüler <fschueler@gmx.net>  Tue,  8 Feb 2005 21:10:40 +0100

linux-kernel-di-amd64-2.6 (0.19) unstable; urgency=low

  * Update to kernel-image-2.6.10-9-amd64-generic.

 -- Frederik Schüler <fschueler@gmx.net>  Tue,  1 Feb 2005 21:03:41 +0100

linux-kernel-di-amd64-2.6 (0.18) unstable; urgency=low

  * Really rebuild with kernel-image-2.6.8-10-amd64-generic.

 -- Frederik Schüler <fschueler@gmx.net>  Mon, 24 Jan 2005 16:30:02 +0100

linux-kernel-di-amd64-2.6 (0.17) unstable; urgency=low

  * Added crc-ccitt.o driver to kernel-image since two udeb packages need it
    now.
  * Patch debian/rules for multiarch support.
  * Rebuild with kernel-image-2.6.8-10-amd64-generic.

 -- Frederik Schüler <fschueler@gmx.net>  Thu, 30 Dec 2004 15:16:29 +0100

linux-kernel-di-amd64-2.6 (0.16) unstable; urgency=low

  * Rebuilt with kernel-image-2.6.8-9-amd64-generic.

 -- Frederik Schüler <fschueler@gmx.net>  Mon,  4 Oct 2004 21:36:42 +0200

linux-kernel-di-amd64-2.6 (0.15) unstable; urgency=low

  * Rebuilt with kernel-image-2.6.8-4-amd64-generic.

 -- Frederik Schüler <fschueler@gmx.net>  Mon, 20 Sep 2004 21:40:56 +0200

linux-kernel-di-amd64-2.6 (0.14) unstable; urgency=low

  * Frederik Schüler
    - Rebuild with kernel-image-2.6.8-3-amd64-generic
    - Updated package.list to match the i386-2.6 one: added nic-extra-modules
      dependencies
    - Removed sx8 module from scsi-extra-modules, it is provided by
      sata-modules now
    - Added acpi-modules udeb
    - Build-depend on kernel-wedge (>= 1.22) to make sure sata_nv is available

 -- Frederik Schüler <fschueler@gmx.net>  Tue, 31 Aug 2004 13:03:24 +0200

linux-kernel-di-amd64-2.6 (0.13) unstable; urgency=low

  * Added 3w-9xxx driver to scsi-extra-modules 

 -- Frederik Schüler <fschueler@gmx.net>  Wed, 18 Aug 2004 11:36:20 +0200

linux-kernel-di-amd64-2.6 (0.12) unstable; urgency=low

  * Joey Hess
    - Move eth1394 to the nic-modules from firewire-core-modules, so we can
      avoid putting it on initrds that lack other nic drivers, which caused
      the firewire interface to be brought up too early and in the wrong order.
      nic-modules now depends on firewire-core-modules. This is a quick fix,
      so it wasn't changed in kernel-wedge.
  * Frederik Schüler
    - Removed lanstreamer and advansys modules
    - New drivers: sata_nv, sx8 and via-velocity
    - Rebuild with kernel-image-2.6.8-1-amd64-generic

 -- Frederik Schüler <fschueler@gmx.net>  Tue, 17 Aug 2004 00:23:06 +0200

linux-kernel-di-amd64-2.6 (0.11) unstable; urgency=low

  * Frederik Schüler
    - rebuild with kernel-image-2.6.7-5-amd64-generic

 -- Frederik Schüler <fschueler@gmx.net>  Thu, 15 Jul 2004 16:00:14 +0200

linux-kernel-di-amd64-2.6 (0.10) unstable; urgency=low

  * Joey Hess
    - Switch to common firewire-core-modules list.
    - Build depend on latest kernel-wedge.

  * Frederik Schüler
    - added firmware-modules.lnk
    - rebuild with kernel-image-2.6.7-4-generic

 -- Frederik Schüler <fschueler@gmx.net>  Wed, 14 Jul 2004 21:10:11 +0200

linux-kernel-di-amd64-2.6 (0.9) unstable; urgency=low

  * added drivers/scsi/scsi_transport_spi.o and fixed sym53c8xx.o path

 -- Frederik Schüler <fschueler@gmx.net>  Wed, 30 Jun 2004 14:58:03 +0200

linux-kernel-di-amd64-2.6 (0.8) unstable; urgency=low

  * rebuild with kernel-image-2.6.7-3-generic

 -- Frederik Schüler <fschueler@gmx.net>  Wed, 30 Jun 2004 14:39:56 +0200

linux-kernel-di-amd64-2.6 (0.7) unstable; urgency=low

  * and again.

 -- Frederik Schüler <fschueler@gmx.net>  Sun,  6 Jun 2004 13:39:28 +0200

linux-kernel-di-amd64-2.6 (0.6) unstable; urgency=low

  * wanna-build strikes again.

 -- Frederik Schüler <fschueler@gmx.net>  Tue, 25 May 2004 22:23:31 +0200

linux-kernel-di-amd64-2.6 (0.5) unstable; urgency=low

  * need to bump version for wanna-build to install the new udebs.

 -- Frederik Schüler <fschueler@gmx.net>  Mon, 24 May 2004 21:27:28 +0200

linux-kernel-di-amd64-2.6 (0.4) unstable; urgency=low

  * adapted to linux-2.6.6

 -- Frederik Schüler <fschueler@gmx.net>  Sat, 22 May 2004 19:35:51 +0200

linux-kernel-di-amd64 (0.3) unstable; urgency=low

  * fixed kernel-wedge build process
  * using kernel-image-2.6.5-generic with built in unix socket support
  * fixed naming sceme

 -- Frederik Schüler <fschueler@gmx.net>  Wed, 14 Apr 2004 22:42:16 +0000

linux-kernel-di-amd64 (0.2) unstable; urgency=low

  * adapted to Linux 2.6.5

 -- Frederik Schüler <fschueler@gmx.net>  Wed,  7 Apr 2004 22:11:04 +0200

linux-kernel-di-amd64 (0.1) unstable; urgency=low

  * first amd64 package.

 -- Frederik Schüler <fschueler@gmx.net>  Thu,  1 Apr 2004 13:35:57 +0200

