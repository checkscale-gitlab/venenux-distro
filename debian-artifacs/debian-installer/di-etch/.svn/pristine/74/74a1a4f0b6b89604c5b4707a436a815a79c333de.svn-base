<?xml version="1.0" encoding="ISO-8859-1"?>
<!-- original version: 45186 -->

<sect1 id="what-is-linux">
<title>Qu'est-ce que GNU/Linux�?</title>

<para>
Linux est un syst�me d'exploitation, c'est-�-dire, un ensemble de programmes 
qui permet d'agir sur la machine et de lancer d'autres programmes.
  </para>

<para>
Un syst�me d'exploitation comprend les programmes fondamentaux dont votre 
ordinateur a besoin pour �changer des instructions avec les utilisateurs�: 
lire et �crire des donn�es sur disque dur, sur bandes ou vers des imprimantes,
contr�ler l'utilisation de la m�moire, faire tourner d'autres programmes, etc. 
La partie la plus importante d'un syst�me d'exploitation est le noyau. Dans 
un syst�me GNU/Linux, c'est le noyau Linux. Le reste du syst�me comprend 
d'autres programmes, dont beaucoup ont �t� �crits par, ou pour, le projet GNU.
Comme le noyau Linux seul ne forme pas un syst�me d'exploitation fonctionnel, 
nous pr�f�rons, pour nous r�f�rer au syst�me que beaucoup de gens appellent 
de fa�on insouciante <quote>Linux</quote>, utiliser le terme 
<quote>GNU/Linux</quote>.
</para>

<para>
Linux est fond� sur le syst�me d'exploitation Unix. D�s le d�but, 
il fut con�u comme un syst�me multit�che et multiutilisateur. Ces 
caract�ristiques suffisent � distinguer Linux d'autres syst�mes bien connus.
Cependant, Linux est encore plus diff�rent que vous ne pouvez l'imaginer.
Personne ne poss�de Linux, contrairement � d'autres syst�mes. L'essentiel de
son d�veloppement est fait par des volontaires non pay�s.
  </para>

<para>
Le d�veloppement de ce qui fut appel� plus tard GNU/Linux commen�a en 1984, 
quand la <ulink url="&url-fsf;"><quote>Free Software Foundation</quote></ulink>
entreprit le d�veloppement d'un syst�me libre de type Unix, appel� GNU.
  </para>

<para>
Le <ulink url="&url-gnu;">projet GNU</ulink> a d�velopp� un ensemble complet
d'outils libres destin�s � Unix&trade; et aux syst�mes d'exploitation de type Unix,
tel que Linux. Ces outils permettent aux utilisateurs d'accomplir aussi bien les t�ches
les plus simples (copier ou effacer un fichier) que les plus complexes (�crire et
compiler des programmes, �diter de fa�on sophistiqu�e dans un grand nombre de formats).
  </para>
<para>
Beaucoup de groupes et d'individus ont contribu� � Linux mais le plus 
important d'entre eux est la <quote>Free Software Foundation</quote> qui
a non seulement cr�� la plupart des outils utilis�s par Linux mais aussi la
philosophie et la communaut� qui ont rendu Linux possible.
  </para>

<para>
Le <ulink url="&url-kernel-org;">noyau Linux</ulink> est apparu pour la 
premi�re 
fois en 1991, quand un �tudiant en informatique finlandais du nom de Linus 
Torvalds annon�a une version de remplacement du noyau Minix dans le groupe de 
discussion Usenet <userinput>comp.os.minix</userinput>. Consultez la 
<ulink url="&url-linux-history;">page d'histoire de Linux</ulink> 
sur Linux International.
</para>

<para>
Linus Torvalds continue � coordonner le travail de centaines de d�veloppeurs, 
aid� par quelques personnes de confiance. Un excellent r�sum� hebdomadaire 
des discussions de la liste de diffusion 
<userinput>linux-kernel</userinput> se trouve sur 
<ulink url="&url-kernel-traffic;">Kernel Traffic</ulink>. Des 
informations suppl�mentaires sur la liste de diffusion 
<userinput>linux-kernel</userinput> se trouvent sur la 
<ulink url="&url-linux-kernel-list-faq;">FAQ de la liste de diffusion de linux-kernel</ulink>.
</para>

<para>
Les utilisateurs de Linux n'ont que l'embarras du choix pour les logiciels.
Ils peuvent par exemple h�siter entre une douzaine d'interpr�teurs de 
commandes, plusieurs interfaces graphiques. Cette possibilit� de choix �tonne 
souvent les utilisateurs d'autres syst�mes d'exploitation, qui ne sont pas 
habitu�s � penser qu'ils peuvent changer leur interpr�teur de commandes ou 
leur interface graphique.
  </para>
<para>
Linux <quote>plante</quote> moins, peut plus facilement ex�cuter plus d'un
programme � la fois, est plus s�r que beaucoup de syst�mes d'exploitation.
Ces avantages font de Linux le syst�me d'exploitation dont la demande a la 
plus forte croissance sur le march� des serveurs. Plus r�cemment, Linux a 
aussi commenc� � gagner en popularit� parmi les utilisateurs, qu'ils soient en 
entreprise ou chez eux.
  </para>

</sect1>
