linux-kernel-di-arm-2.6 (1.10etch1) stable; urgency=low

  * Update to kernel image version 2.6.18.dfsg.1-13 (2.6.18-5).

 -- Frans Pop <fjp@debian.org>  Mon,  4 Jun 2007 18:44:26 +0200

linux-kernel-di-arm-2.6 (1.10) unstable; urgency=low

  * Update to kernel image version 2.6.18.dfsg.1-11.

 -- Frans Pop <fjp@debian.org>  Mon, 26 Feb 2007 17:52:13 +0100

linux-kernel-di-arm-2.6 (1.9) unstable; urgency=low

  * Update to kernel image version 2.6.18.dfsg.1-10.

 -- Frans Pop <fjp@debian.org>  Mon,  5 Feb 2007 14:15:20 +0100

linux-kernel-di-arm-2.6 (1.8) unstable; urgency=low

  * Update to kernel image version 2.6.18.dfsg.1-9.

 -- Joey Hess <joeyh@debian.org>  Sun, 28 Jan 2007 22:15:24 -0500

linux-kernel-di-arm-2.6 (1.7) unstable; urgency=low

  * Update to kernel image version 2.6.18-8.

 -- Frans Pop <fjp@debian.org>  Mon, 18 Dec 2006 16:52:51 +0100

linux-kernel-di-arm-2.6 (1.6) unstable; urgency=low

  * Update to kernel image version 2.6.18-7.

 -- Frans Pop <fjp@debian.org>  Thu,  7 Dec 2006 23:54:21 +0100

linux-kernel-di-arm-2.6 (1.5) unstable; urgency=low

  * Update to 2.6.18-6 with ABI Version 3.
  * ixp4xx: Create a nic-modules package for the new Ethernet driver.

 -- Martin Michlmayr <tbm@cyrius.com>  Wed, 22 Nov 2006 17:39:50 +0100

linux-kernel-di-arm-2.6 (1.4) unstable; urgency=low

  * Update to 2.6.18-5.
  * Remove the nslu2 flavour since it has been merged into the more
    generic ixp4xx flavour.
  * iop32x: ext2/3 is no longer built in.

 -- Martin Michlmayr <tbm@cyrius.com>  Thu, 16 Nov 2006 20:57:27 +0000

linux-kernel-di-arm-2.6 (1.3) unstable; urgency=low

  [ Martin Michlmayr ]
  * iop32x: import nic-usb-modules from kernel-wedge rather than use our
    own list now that all modules are enabled in the kernel.

  [ Frans Pop ]
  * Update to kernel image version 2.6.17-9.

 -- Frans Pop <fjp@debian.org>  Sun, 17 Sep 2006 02:40:25 +0200

linux-kernel-di-arm-2.6 (1.2) unstable; urgency=low

  [ Martin Michlmayr ]
  * Rebuilt with 2.6.17-6.
  * Add images for iop32x.
  * Add images for ixp4xx.

 -- Martin Michlmayr <tbm@cyrius.com>  Sat, 12 Aug 2006 01:25:19 +0200

linux-kernel-di-arm-2.6 (1.1) unstable; urgency=low

  [ Martin Michlmayr ]
  * Remove the s3c2410 flavour since it's not actually used in d-i.
  * Add SCSI modules on rpc.
  * Temporarily drop crypto-modules since partman-crypto breaks lowmem
    level 1 installations.
  * Rebuilt with 2.6.16-17.

 -- Martin Michlmayr <tbm@cyrius.com>  Thu, 20 Jul 2006 09:34:11 +0200

linux-kernel-di-arm-2.6 (1.0) unstable; urgency=low

  [ Martin Michlmayr ]
  * Rebuilt with 2.6.16-16.

 -- Martin Michlmayr <tbm@cyrius.com>  Tue, 11 Jul 2006 01:12:21 +0200

linux-kernel-di-arm-2.6 (0.9) unstable; urgency=low

  [ Martin Michlmayr ]
  * Loop modules are no longer built into the kernel.
  * Rebuilt with 2.6.16-15.
  * Add a RiscPC (rpc) flavour.

 -- Martin Michlmayr <tbm@cyrius.com>  Thu, 29 Jun 2006 15:23:20 +0200

linux-kernel-di-arm-2.6 (0.8) unstable; urgency=low

  [ Martin Michlmayr ]
  * Rebuilt with 2.6.16-14.

 -- Martin Michlmayr <tbm@cyrius.com>  Tue, 13 Jun 2006 17:15:21 +0200

linux-kernel-di-arm-2.6 (0.7) unstable; urgency=low

  [ Joey Hess ]
  * Add crypto-modules.

  [ Martin Michlmayr ]
  * Rebuilt with 2.6.16-9.
  * Add input-modules with evdev (needed for the beeper).

 -- Martin Michlmayr <tbm@cyrius.com>  Sat, 22 Apr 2006 19:52:16 +0200

linux-kernel-di-arm-2.6 (0.6) unstable; urgency=low

  [ Martin Michlmayr ]
  * Rebuilt with 2.6.16-2.

 -- Martin Michlmayr <tbm@cyrius.com>  Fri, 24 Mar 2006 23:55:49 +0000

linux-kernel-di-arm-2.6 (0.5) unstable; urgency=low

  [ Martin Michlmayr ]
  * Rebuilt with 2.6.15-8.

 -- Martin Michlmayr <tbm@cyrius.com>  Fri, 17 Mar 2006 18:32:42 +0000

linux-kernel-di-arm-2.6 (0.4) unstable; urgency=low

  [ Martin Michlmayr ]
  * Rebuilt with 2.6.15-7.

 -- Martin Michlmayr <tbm@cyrius.com>  Fri, 24 Feb 2006 10:02:32 +0000

linux-kernel-di-arm-2.6 (0.3) unstable; urgency=low

  [ Martin Michlmayr ]
  * Rebuilt with 2.6.15-6.

 -- Martin Michlmayr <tbm@cyrius.com>  Sun, 12 Feb 2006 19:14:21 +0000

linux-kernel-di-arm-2.6 (0.2) unstable; urgency=low

  [ Martin Michlmayr ]
  * Rebuilt with 2.6.15-4.

 -- Martin Michlmayr <tbm@cyrius.com>  Sat, 04 Feb 2006 17:53:15 +0000

linux-kernel-di-arm-2.6 (0.1) unstable; urgency=low

  [ Martin Michlmayr ]
  * Create initial 2.6 packages for ARM.
  * Update copyright information.
  * Remove riscpc, riscstation and lart (at least for now) since there's no
    support in d-i for them.
  * Rename netwinder to footbridge and bast to s3c2410 to comform with
    the upstream names.
  * Add support for NSLU2 (Network Storage Link for USB 2.0 Disk Drives).

 -- Martin Michlmayr <tbm@cyrius.com>  Fri, 20 Jan 2006 21:30:37 +0000

