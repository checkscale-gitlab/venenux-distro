<!-- retain these comments for translator revision tracking -->
<!-- original version: 43744 -->

<bookinfo id="debian_installation_guide">
<title>Guia d'instal·lació de &debian;</title>

<abstract>
<para>
Aquest document conté les instruccions d'instal·lació per a la versió
&release; del sistema &debian; (nom en codi <quote>&releasename;</quote>)
per a l'arquitectura &arch-title; (<quote>&architecture;</quote>). També
conté referències per obtenir més informació i informació de com aprofitar
al màxim el vostre nou sistema Debian.
</para>

<para>
<note arch="m68k"><para>

Com que el port &arch-title; no és una arquitectura suportada per
&releasename;, no hi ha una versió oficial d'aquest manual per
&arch-title; per &releasename;. Per altra banda, com que el port
encara està actiu i hi ha esperances de que &arch-title; s'incloga
altra vegada en futurs llançaments oficials, aquesta versió de la
Guia d'instal·lació encara esta disponible.

</para><para>

Com que &arch-title; no es una arquitectura oficial, alguna de la
informació en aquest manual, especialment alguns enllaços, podria
ser incorrecte. Per aconseguir informació addicional, comproveu les
<ulink url="&url-ports;">pàgines web</ulink> del port o contacteu amb la
<ulink url="&url-list-subscribe;"> llista de correu
debian-&arch-listname;</ulink>.

</para></note>

<warning condition="not-checked"><para>
Aquesta guia d'instal·lació està basada en un manual anterior escrit
per al sistema d'instal·lació antic de Debian (els «boot-floppies»),
i ha estat actualitzat per a documentar el nou instal·lador de Debian.
No obstant, per a &architecture;, el manual no ha estat completament
actualitzat i comprovada la seva veracitat per al nou instal·lador. Pot
ser que algunes parts del manual encara estiguin incompletes o
desactualitzades o que encara documentin l'instal·lador «boot-floppies».
Podeu trobar una versió més nova d'aquest manual, possiblement documentant
millor aquesta arquitectura, a internet a la <ulink url="&url-d-i;">pàgina
de &d-i;</ulink>. Podreu trobar-hi també traduccions addicionals.
</para></warning>

<note condition="checked"><para>
Tot i que aquesta guia d'instal·lació per a &architecture; està
majoritariament al dia, planegem fer alguns canvis i reorganitzar
part del manual després del llançament oficial de &releasename;.
Podeu trobar una versió més nova d'aquest manual a internet a la
<ulink url="&url-d-i;">pàgina de &d-i;</ulink>. Podreu trobar-hi
també traduccions addicionals.
</para></note>
</para>

<para condition="translation-status">
Translators can use this paragraph to provide some information about
the status of the translation, for example if the translation is still
being worked on or if review is wanted (don't forget to mention where
comments should be sent!).

See build/lang-options/README on how to enable this paragraph.
Its condition is "translation-status".

</para>
</abstract>

<copyright>
 <year>2004</year>
 <year>2005</year>
 <year>2006</year>
 <year>2007</year>
 <holder>l'equip de l'instal·lador de Debian</holder>
</copyright>

<legalnotice>
<para>

Aquest manual és programari lliure; podeu redistribuir-lo i/o
modificar-lo sota els termes de la Llicència Pública General de GNU
publicada per la Free Software Foundation. Feu un cop d'ull
a la llicència a <xref linkend="appendix-gpl"/>.

</para>
</legalnotice>
</bookinfo>

