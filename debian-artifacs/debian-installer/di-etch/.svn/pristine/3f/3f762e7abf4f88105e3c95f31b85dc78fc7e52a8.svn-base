<?xml version="1.0" encoding="ISO-8859-1"?>
<!-- original version: 44436 -->

 <sect1 id="linuxdevices"><title>P�riph�riques Linux</title>
<para>

Sous Linux, vous disposez de fichiers sp�ciaux dans le r�pertoire
<filename>/dev</filename>. Ces fichiers sont appel�s fichiers de 
p�riph�rique et ils se comportent diff�remment des fichiers ordinaires.
Les types les plus courants de ces fichiers concernent les p�riph�riques
de type bloc et les p�riph�riques de type caract�re. Ces fichiers sont des interfaces
au pilote r�el qui fait partie du noyau et qui acc�de au p�riph�rique.
Un autre type, moins courant, de fichier de p�riph�rique est <firstterm>pipe</firstterm>.
Ci-dessous sont repris les fichiers de p�riph�rique les plus importants.

</para><para>

<informaltable><tgroup cols="2"><tbody>
<row>
  <entry><filename>fd0</filename></entry>
  <entry>1er lecteur de disquette</entry>
</row><row>
  <entry><filename>fd1</filename></entry>
  <entry>2e lecteur de disquette</entry>
</row>
</tbody></tgroup></informaltable>

<informaltable><tgroup cols="2"><tbody>
<row>
  <entry><filename>hda</filename></entry>
  <entry>Disque dur IDE ou c�d�rom sur le premier port IDE (ma�tre)</entry>
</row><row>
  <entry><filename>hdb</filename></entry>
  <entry>Disque dur IDE ou c�d�rom sur le premier port IDE (esclave)</entry>
</row><row>
  <entry><filename>hdc</filename></entry>
  <entry>Disque dur IDE ou c�d�rom sur le second port IDE (ma�tre)</entry>
</row><row>
  <entry><filename>hdd</filename></entry>
  <entry>Disque dur IDE ou c�d�rom sur le second port IDE (esclave)</entry>
</row><row>
  <entry><filename>hda1</filename></entry>
  <entry>1re partition sur le premier disque dur IDE</entry>
</row><row>
  <entry><filename>hdd15</filename></entry>
  <entry>15e partition sur le quatri�me disque dur IDE</entry>
</row>
</tbody></tgroup></informaltable>

<informaltable><tgroup cols="2"><tbody>
<row>
  <entry><filename>sda</filename></entry>
  <entry>Disque SCSI avec l'identificateur SCSI le plus bas (p. ex. 0)</entry>
</row><row>
  <entry><filename>sdb</filename></entry>
  <entry>Disque SCSI avec l'identificateur suivant (p. ex. 1)</entry>
</row><row>
  <entry><filename>sdc</filename></entry>
  <entry>Disque SCSI avec l'identificateur suivant (p. ex. 2)</entry>
</row><row>
  <entry><filename>sda1</filename></entry>
  <entry>1re partition du premier disque SCSI</entry>
</row><row>
  <entry><filename>sdd10</filename></entry>
  <entry>10e partition du quatri�me disque SCSI</entry>
</row>
</tbody></tgroup></informaltable>

<informaltable><tgroup cols="2"><tbody>
<row>
  <entry><filename>sr0</filename></entry>
  <entry>c�d�rom SCSI avec l'identificateur SCSI le plus bas</entry>
</row><row>
  <entry><filename>sr1</filename></entry>
  <entry>c�d�rom SCSI avec l'identificateur SCSI suivant</entry>
</row>
</tbody></tgroup></informaltable>

<informaltable><tgroup cols="2"><tbody>
<row>
  <entry><filename>ttyS0</filename></entry>
  <entry>Port s�rie 0 (COM1: sous DOS)</entry>
</row><row>
  <entry><filename>ttyS1</filename></entry>
  <entry>Port s�rie 1 (COM2: sous DOS)</entry>
</row><row>
  <entry><filename>psaux</filename></entry>
  <entry>port souris PS/2</entry>
</row><row>
  <entry><filename>gpmdata</filename></entry>
  <entry>pseudo p�riph�rique, r�p�titeur du d�mon GPM de souris</entry>
</row>
</tbody></tgroup></informaltable>

<informaltable><tgroup cols="2"><tbody>
<row>
  <entry><filename>cdrom</filename></entry>
  <entry>Lien symbolique vers le lecteur de c�d�rom</entry>
</row><row>
  <entry><filename>mouse</filename></entry>
  <entry>Lien symbolique vers le fichier de p�riph�rique de la souris</entry>
</row>
</tbody></tgroup></informaltable>

<informaltable><tgroup cols="2"><tbody>
<row>
  <entry><filename>null</filename></entry>
  <entry>tout ce qui est �crit vers ce fichier dispara�t</entry>
</row><row>
  <entry><filename>zero</filename></entry>
  <entry>fichier qui fabrique continuellement des z�ros</entry>
</row>
</tbody></tgroup></informaltable>

</para>

  <sect2 arch="not-s390" id="device-mouse">
<title>R�gler la souris</title>

<para>

La souris peut �tre utilis�e aussi bien dans une console Linux (avec gpm) 
qu'en environnement X window. Il suffit normalement d'installer le fichier
<filename>gpm</filename> et le serveur X. Les deux doivent utiliser 
<filename>/dev/input/mice</filename> comme p�riph�rique.
Le protocole correct pour la souris est <userinput>exps2</userinput> pour gpm et
<userinput>ExplorerPS/2</userinput> pour le serveur X. Les fichiers de configuration sont
respectivement <filename>/etc/gpm.conf</filename> et <filename>/etc/X11/xorg.conf</filename>.

</para>

<para>
Pour faire fonctionner la souris, il faut charger certains modules du noyau.
Dans la plupart des cas, les modules corrects sont d�tect�s mais certaines
souris anciennes <footnote>
<para>
Les souris de type s�rie ont un connecteur � 9 trous en forme de D&nbsp;; les souris
bus ont un connecteur rond � 8 tiges, qu'il ne faut pas confondre avec le
connecteur � 6 tiges d'une souris PS/2 ou avec le connecteur � 4 tiges d'une souris ADB.
</para>
</footnote> sur de vieux ordinateurs posent des probl�mes. Voici les modules du noyau
n�cessaires aux diff�rents types de souris&nbsp;:

<informaltable><tgroup cols="2"><thead>
<row>
  <entry>Module</entry>
  <entry>Description</entry>
</row>
</thead><tbody>
<row>
  <entry>psmouse</entry>
  <entry>Souris PS/2 (devrait �tre d�tect�e)</entry>
</row>
<row>
  <entry>usbhid</entry>
  <entry>Souris USB (devrait �tre d�tect�e)</entry>
</row>
<row>
  <entry>sermouse</entry>
  <entry>Most serial mice</entry>
</row>
<row>
  <entry>logibm</entry>
  <entry>Souris Bus connect�e � l'adaptateur Logitech</entry>
</row>
<row>
  <entry>inport</entry>
  <entry>Souris Bus connect�e � des cartes ATI ou Microsoft</entry>
</row>
</tbody></tgroup></informaltable>

</para><para arch="powerpc">
<!-- FJP 20070122: Unsure if this is still valid -->

Les noyaux modernes offrent la possibilit� d'�muler une souris 3 boutons 
quand votre souris n'en a qu'un. Ajoutez simplement les lignes suivantes au 
fichier <filename>/etc/sysctl.conf</filename>&nbsp;:

<informalexample><screen>
# �mulation d'une souris � 3 boutons 
# mettre en &oelig;uvre l'�mulation
/dev/mac_hid/mouse_button_emulation = 1
# Envoyer le signal du bouton du milieu avec la touche F11
/dev/mac_hid/mouse_button2_keycode = 87
# Envoyer le signal du bouton de droite avec la touche F12
/dev/mac_hid/mouse_button3_keycode = 88
# Pour d'autres touches, utilisez showkey pour conna�tre les codes
</screen></informalexample>  
</para>
  </sect2>
 </sect1>

 <sect1 id="tasksel-size-list">
 <title>Ressources d'espace disque pour les t�ches</title>

<para>
Une installation standard pour l'architecture i386, avec le noyau 2.6 par d�faut
et tous les paquets standard, demande &std-system-size;&nbsp;Mo. Une installation minimale,
sans la t�che <emphasis>Standard system</emphasis>, demande &base-system-size;&nbsp;Mo. 
</para>
<important><para>
Dans ces deux cas, l'espace disque r�ellement utilis� est calcul� <emphasis>apr�s</emphasis>
la fin de l'installation et tous les fichiers temporaires �limin�s. Il ne prend pas
en compte l'espace utilis� par le syst�me de fichiers, par exemple par les 
journaux. Il est donc clair qu'il faut plus d'espace disque � la fois
<emphasis>pendant</emphasis> l'installation et en utilisation r�elle du
syst�me.
</para></important>
<para>
Le tableau suivant montre les tailles rapport�es par aptitude pour les t�ches 
affich�es dans tasksel. Certaines t�ches ont des �l�ments
communs, et donc la taille totale pour ces deux t�ches peut �tre moindre que 
la somme des deux tailles.
</para>
<para>
Pour d�terminer la dimension d'une partition, vous devez ajouter les tailles
dans le tableau et la taille de l'installation de base. Ce que repr�sente
<quote>Taille install�e</quote> va dans <filename>/usr</filename> et dans
<filename>/lib</filename>&nbsp;; et ce que repr�sente <quote>Taille � charger</quote>
va (provisoirement) dans <filename>/var</filename>. 
</para>
<para>

<informaltable><tgroup cols="4">
<thead>
<row>
  <entry>T�ches</entry>
  <entry>Taille Install�e (Mo)</entry>
  <entry>Taille � charger (Mo)</entry>
  <entry>Espace � prendre (Mo)</entry>
</row>
</thead>

<tbody>
<row>
  <entry>Desktop</entry>
  <entry>&task-desktop-inst;</entry>
  <entry>&task-desktop-dl;</entry>
  <entry>&task-desktop-tot;</entry>
</row>

<row>
  <entry>Laptop <footnote>

<para> Les t�ches <emphasis>Laptop</emphasis> et <emphasis>Desktop environment</emphasis>
se recoupent en grande partie. Si vous installez les deux, la t�che Laptop demandera
seulement quelques Mo suppl�mentaires d'espace disque.
</para> 
</footnote></entry>
  <entry>&task-laptop-inst;</entry>
  <entry>&task-laptop-dl;</entry>
  <entry>&task-laptop-tot;</entry>
</row>

<row>
  <entry>Serveur web</entry>
  <entry>&task-web-inst;</entry>
  <entry>&task-web-dl;</entry>
  <entry>&task-web-tot;</entry>
</row>

<row>
  <entry>Serveur d'impression</entry>
  <entry>&task-print-inst;</entry>
  <entry>&task-print-dl;</entry>
  <entry>&task-print-tot;</entry>
</row>

<row>
  <entry>Serveur DNS</entry>
  <entry>&task-dns-inst;</entry>
  <entry>&task-dns-dl;</entry>
  <entry>&task-dns-tot;</entry>
</row>

<row>
  <entry>Serveur de fichiers</entry>
  <entry>&task-file-inst;</entry>
  <entry>&task-file-dl;</entry>
  <entry>&task-file-tot;</entry>
</row>

<row>
  <entry>Serveur de courrier</entry>
  <entry>&task-mail-inst;</entry>
  <entry>&task-mail-dl;</entry>
  <entry>&task-mail-tot;</entry>
</row>

<row>
  <entry>Base de donn�es SQL</entry>
  <entry>&task-sql-inst;</entry>
  <entry>&task-sql-dl;</entry>
  <entry>&task-sql-tot;</entry>
</row>

</tbody>
</tgroup></informaltable>

<note><para>
La t�che <emphasis>Desktop</emphasis> installe l'environnement GNOME.
</para></note>
</para>
<para>
Si vous faites une installation dans une langue autre que l'anglais,
<command>tasksel</command> installera d'abord une <firstterm>locale</firstterm>
s'il en existe une pour votre langue. Chaque langue demande un espace disque.
Vous devriez accorder 350&nbsp;Mo pour l'installation et le t�l�chargement.
</para>
 </sect1>

