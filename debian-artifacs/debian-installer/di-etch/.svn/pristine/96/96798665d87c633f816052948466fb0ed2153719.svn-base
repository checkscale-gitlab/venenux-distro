<?xml version="1.0" encoding="EUC-JP"?>
<!-- retain these comments for translator revision tracking -->
<!-- original version: 45239 -->

 <sect1 id="boot-parms"><title>ブートパラメータ</title>
<para>

<!--
Boot parameters are Linux kernel parameters which are generally used
to make sure that peripherals are dealt with properly.  For the most
part, the kernel can auto-detect information about your peripherals.
However, in some cases you'll have to help the kernel a bit.
-->
ブートパラメータとは  Linux カーネルのパラメータのことで、
一般には周辺機器を適切に扱うために用います。
ほとんどの場合、カーネルは周辺機器の情報を自動的に検出します。
しかし、場合によっては少々カーネルを助けてあげないといけないこともあるのです。

</para><para>

<!--
If this is the first time you're booting the system, try the default
boot parameters (i.e., don't try setting parameters) and see if it works
correctly. It probably will.  If not, you can reboot later and look for
any special parameters that inform the system about your hardware.
-->
システムを初めて起動する場合は、デフォルトのブートパラメータを試して
(つまりなにもパラメータを設定せずに)、正確に動作するか観察してください。
たいていはうまくいくと思います。
なにか問題が起こったら、
そのハードウェアに関する情報をシステムに伝えるためのパラメータを調べ、
あとで再起動します。

</para><para>

<!--
Information on many boot parameters can be found in the
<ulink url="http://www.tldp.org/HOWTO/BootPrompt-HOWTO.html"> Linux
BootPrompt HOWTO</ulink>, including tips for obscure hardware.  This
section contains only a sketch of the most salient parameters.  Some
common gotchas are included below in
<xref linkend="boot-troubleshooting"/>.
-->
多くのブートパラメータの情報は (曖昧なハードウェア用の tips 込みで)、
<ulink url="http://www.tldp.org/HOWTO/BootPrompt-HOWTO.html"> Linux
BootPrompt HOWTO</ulink> で見つけられます。
本節は、最も顕著なパラメータの概要だけを含んでいます。
いくつか共通のものは <xref linkend="boot-troubleshooting"/> 以下に含まれています。

</para><para>

<!--
When the kernel boots, a message

<informalexample><screen>
Memory:<replaceable>avail</replaceable>k/<replaceable>total</replaceable>k available
</screen></informalexample>

should be emitted early in the process.
<replaceable>total</replaceable> should match the total amount of RAM,
in kilobytes.  If this doesn't match the actual amount of RAM you have
installed, you need to use the
<userinput>mem=<replaceable>ram</replaceable></userinput> parameter,
where <replaceable>ram</replaceable> is set to the amount of memory,
suffixed with <quote>k</quote> for kilobytes, or <quote>m</quote> for
megabytes.  For example, both <userinput>mem=65536k</userinput> and
<userinput>mem=64m</userinput> mean 64MB of RAM.
-->
カーネルが起動するときには、プロセスの最初のほうで

<informalexample><screen>
Memory:<replaceable>avail</replaceable>k/<replaceable>total</replaceable>k available
</screen></informalexample>

というメッセージが表示されます。
<replaceable>total</replaceable> は利用可能な RAM の総量をキロバイト単位で表しています。
この値が実際に搭載している RAM の量と一致しないときには、
<userinput>mem=<replaceable>ram</replaceable></userinput> というパラメータが必要になります。
<replaceable>ram</replaceable> のところには、実際に搭載しているメモリ量を、
キロバイト単位なら <quote>k</quote>、
メガバイト単位なら <quote>m</quote> を後ろにつけて記入します。例えば、
<userinput>mem=65536k</userinput> も <userinput>mem=64m</userinput> も 64MB の RAM を意味します。

</para><para condition="supports-serial-console">

<!--
If you are booting with a serial console, generally the kernel will
autodetect
this<phrase arch="mipsel"> (although not on DECstations)</phrase>.
If you have a videocard (framebuffer) and a keyboard also attached to
the computer which you wish to boot via serial console, you may have
to pass the
<userinput>console=<replaceable>device</replaceable></userinput>
argument to the kernel, where <replaceable>device</replaceable> is
your serial device, which is usually something like
<filename>ttyS0</filename>.
-->
起動の際にシリアルコンソールを使うと、通常カーネルはこちらを自動検出します
<phrase arch="mipsel">(ただし DECstation を除く)</phrase>。
ただし、シリアルコンソールから起動させたいコンピュータに
ビデオカード (フレームバッファ) とキーボードもついている場合には、カーネルに 
<userinput>console=<replaceable>device</replaceable></userinput> 
というパラメータを渡す必要があるでしょう。
<replaceable>device</replaceable> は利用するシリアルデバイスです。
これは普通 <filename>ttyS0</filename> のようになるでしょう。

</para><para arch="sparc">

<!--
For &arch-title; the serial devices are <filename>ttya</filename> or
<filename>ttyb</filename>.
Alternatively, set the <envar>input-device</envar> and
<envar>output-device</envar> OpenPROM variables to
<filename>ttya</filename>.
-->
&arch-title; のシリアルデバイスは、
<filename>ttya</filename> や <filename>ttyb</filename> になります。
あるいは、OpenPROM 変数の <envar>input-device</envar> と
<envar>output-device</envar> に <filename>ttya</filename> を設定しても結構です。

</para>


  <sect2 id="installer-args"><title>Debian Installer パラメータ</title>
<para>

<!--
The installation system recognizes a few additional boot parameters<footnote>
-->
インストールシステムは、
おそらく便利だと思われる、追加起動パラメータ<footnote>

<para>

<!--
With current kernels (2.6.9 or newer) you can use 32 command line options and
32 environment options. If these numbers are exceeded, the kernel will panic.
-->
現在のカーネル (2.6.9 以降) では、
コマンドラインオプションを 32 個と環境オプションを 32 個使用できます。
それを越えると、カーネルはパニックしてしまいます。

</para>

<!--
</footnote> which may be useful.
-->
</footnote>をいくつか認識します。

</para><para>

<!--
A number of parameters have a <quote>short form</quote> that helps avoid
the limitations of the kernel command line options and makes entering the
parameters easier. If a parameter has a short form, it will be listed in
brackets behind the (normal) long form. Examples in this manual will
normally use the short form too.
-->
多くのパラメータは、カーネルコマンドラインオプションの制限を避けたり、
パラメータの入力を簡単にするため、<quote>短縮形</quote> を持っています。
パラメータに短縮形がある場合、(通常の)長い形式の後にかっこで囲っています。
本マニュアルの例は、通常、短縮形も使用しています。

</para>

<variablelist>
<varlistentry>
<term>debconf/priority (priority)</term>
<listitem><para>

<!--
This parameter sets the lowest priority of messages to be displayed.
-->
このパラメータには、表示するメッセージのもっとも低い優先度を設定します。

</para><para>

<!--
The default installation uses <userinput>priority=high</userinput>.
This means that both high and critical priority messages are shown, but medium
and low priority messages are skipped.
If problems are encountered, the installer adjusts the priority as needed.
-->
デフォルトのインストールでは、
<userinput>priority=high</userinput> を使用します。
優先度が「高」のものと、「重要」のもののメッセージを表示し、
「標準」や、「低」のメッセージはスキップします。
問題にぶつかった場合、インストーラは必要な優先度に調整します。

</para><para>

<!--
If you add <userinput>priority=medium</userinput> as boot parameter, you
will be shown the installation menu and gain more control over the installation.
When <userinput>priority=low</userinput> is used, all messages are shown
(this is equivalent to the <emphasis>expert</emphasis> boot method).
With <userinput>priority=critical</userinput>, the installation system
will display only critical messages and try to do the right thing without fuss.
-->
ブートパラメータに <userinput>priority=medium</userinput>
を追加すると、インストールメニューが表示され、
インストールについて、さらに多くの制御を行うことができます。
<userinput>priority=low</userinput> を使った場合は、
すべてのメッセージを表示します (<emphasis>expert</emphasis> 起動法と等価)。
<userinput>priority=critical</userinput> の場合は、
インストールシステムは重要なメッセージだけを表示し、
大騒ぎせずに正しい設定をしようとします。

</para></listitem>
</varlistentry>


<varlistentry>
<term>DEBIAN_FRONTEND</term>
<listitem><para>

<!--
This boot parameter controls the type of user interface used for the
installer. The current possible parameter settings are:
-->
このブートパラメータはインストーラで使うユーザインターフェースを
制御します。現在有効な設定は以下の通りです。

<itemizedlist>
<listitem>
<para><userinput>DEBIAN_FRONTEND=noninteractive</userinput></para>
</listitem><listitem>
<para><userinput>DEBIAN_FRONTEND=text</userinput></para>
</listitem><listitem>
<para><userinput>DEBIAN_FRONTEND=newt</userinput></para>
</listitem><listitem>
<para><userinput>DEBIAN_FRONTEND=gtk</userinput></para>
</listitem>
</itemizedlist>

<!--
The default frontend is <userinput>DEBIAN_FRONTEND=newt</userinput>.
<userinput>DEBIAN_FRONTEND=text</userinput> may be preferable for
serial console installs. Generally only the
<userinput>newt</userinput> frontend is available on default install
media. On architectures which support it, the graphical installer uses
the <userinput>gtk</userinput> frontend.
-->
デフォルトのフロントエンドは <userinput>DEBCONF_FRONTEND=newt</userinput> 
です。シリアルコンソールでインストールするには、
<userinput>DEBIAN_FRONTEND=text</userinput> とすべきでしょう。
一般的に、デフォルトのインストールメディアでは <userinput>newt</userinput>
フロントエンドのみが利用可能です。
サポートしているアーキテクチャでは、グラフィカルインストーラが 
<userinput>gtk</userinput> フロントエンドを使用します。


</para></listitem>
</varlistentry>


<varlistentry>
<term>BOOT_DEBUG</term>
<listitem><para>

<!--
Setting this boot parameter to 2 will cause the installer's boot process
to be verbosely logged. Setting it to 3 makes debug shells
available at strategic points in the boot process. (Exit the shells to
continue the boot process.)
-->
このブートパラメータに 2 を設定すると、
インストーラの起動プロセス中に詳細なログを出力します。
3 を設定すると、起動プロセスの要所でデバッグ用のシェルが利用できます。
(シェルを終了すると起動プロセスを継続します)

<variablelist>
<varlistentry>
<term><userinput>BOOT_DEBUG=0</userinput></term>
<!--
<listitem><para>This is the default.</para></listitem>
-->
<listitem><para>デフォルトです。</para></listitem>
</varlistentry>

<varlistentry>
<term><userinput>BOOT_DEBUG=1</userinput></term>
<!--
<listitem><para>More verbose than usual.</para></listitem>
-->
<listitem><para>通常よりも詳細です。</para></listitem>
</varlistentry>

<varlistentry>
<term><userinput>BOOT_DEBUG=2</userinput></term>
<!--
<listitem><para>Lots of debugging information.</para></listitem>
-->
<listitem><para>デバッグ情報を大量に表示します。</para></listitem>
</varlistentry>

<varlistentry>
<term><userinput>BOOT_DEBUG=3</userinput></term>
<listitem><para>

<!--
Shells are run at various points in the boot process to allow detailed
debugging. Exit the shell to continue the boot.
-->
詳細なデバッグを行うよう、ブートプロセスの様々な箇所でシェルが実行されます。
起動を続けるにはシェルから抜けてください。

</para></listitem>
</varlistentry>
</variablelist>

</para></listitem>
</varlistentry>


<varlistentry>
<term>INSTALL_MEDIA_DEV</term>
<listitem><para>

<!--
The value of the parameter is the path to the device to load the
Debian installer from. For example,
<userinput>INSTALL_MEDIA_DEV=/dev/floppy/0</userinput>
-->
このパラメータの値には、Debian インストーラを読み込むデバイスのパスを指定します。
例えば、<userinput>INSTALL_MEDIA_DEV=/dev/floppy/0</userinput> となります。

</para><para>

<!--
The boot floppy, which normally scans all floppies it can to find the
root floppy, can be overridden by this parameter to only look at the
one device.
-->
ブートフロッピー は root フロッピーを探すのに、
通常は全フロッピーを検索しますが、
このパラメータで 1 つのデバイスを探すように上書きできます。

</para></listitem>
</varlistentry>

<varlistentry>
<term>debian-installer/framebuffer (fb)</term>
<listitem><para>
<!--
Some architectures use the kernel framebuffer to offer installation in
a number of languages. If framebuffer causes a problem on your system
you can disable the feature by the parameter
<userinput>fb=false</userinput>. Problem symptoms are error messages
about bterm or bogl, a blank screen, or a freeze within a few minutes after
starting the install.
-->
いくつかのアーキテクチャでは、多くの言語でインストールを行うために、
カーネルフレームバッファを使用します。
フレームバッファが問題となるシステムの場合、
<userinput>fb=false</userinput> パラメータによってこの機能を無効にできます。
bterm や bogl に関するエラーメッセージや、真っ暗な画面、
インストールが始まって数分後にフリーズがおきたら問題の兆候です。

</para><para arch="x86">

<!--
The <userinput>video=vga16:off</userinput> argument may also be used
to disable the kernel's use of the framebuffer. Such problems have been
reported on a Dell Inspiron with Mobile Radeon card.
-->
<userinput>video=vga16:off</userinput> 引数は、
カーネルのフレームバッファ利用を無効にする効果もあります。
そのような問題が、Mobile Radeon を搭載した Dell Inspiron について報告されています。

</para><para arch="m68k">

<!--
Such problems have been reported on the Amiga 1200 and SE/30.
-->
そのような問題が、Amiga 1200 や SE/30 について報告されています。

</para><para arch="hppa">

<!--
Such problems have been reported on hppa.
-->
そのような問題が、hppa について報告されています。

</para><note arch="sparc"><para>

<!--
Because of display problems on some systems, framebuffer support is
<emphasis>disabled by default</emphasis> for &arch-title;. This can result
in ugly display on systems that do properly support the framebuffer, like
those with ATI graphical cards.
If you see display problems in the installer, you can try booting with
parameter <userinput>debian-installer/framebuffer=true</userinput> or
<userinput>fb=true</userinput> for short.
-->
いくつかのシステムで問題が発生するため、
&arch-title; ではフレームバッファのサポートが
<emphasis>デフォルトでは無効</emphasis>になっています。
この結果 ATI のグラフィックカードのように、システムの表示が汚くなる場合には、
フレームバッファをサポートするようにしてください。
インストーラで表示の問題に遭遇したら、
<userinput>debian-installer/framebuffer=true</userinput> や省略形の 
<userinput>fb=true</userinput> というパラメータを試してください。

</para></note></listitem>
</varlistentry>

<varlistentry arch="not-s390">
<term>debian-installer/theme (theme)</term>
<listitem><para>

<!--
A theme determines how the user interface of the installer looks (colors,
icons, etc.). What themes are available differs per frontend.  Currently
both the newt and gtk frontends only have a <quote>dark</quote> theme that was
designed for visually impaired users. Set the theme by booting with
<userinput>theme=<replaceable>dark</replaceable></userinput>.
-->
テーマ (theme) はインストーラのユーザインターフェースがどのように見えるか 
(色、アイコンなど) を決定します。現在、newt と gtk のフロントエンドにのみ、
目の不自由な方向けにデザインされた <quote>dark</quote> テーマがあります。
起動時のパラメータに、
<userinput>theme=<replaceable>dark</replaceable></userinput> 
と指定してテーマを設定してください。

</para></listitem>
</varlistentry>

<varlistentry>
<term>debian-installer/probe/usb</term>
<listitem><para>

<!--
Set to <userinput>false</userinput> to prevent probing for USB on
boot, if that causes problems.
-->
起動時の USB の検出で問題が起きる場合は、
これに <userinput>false</userinput> を設定してください。

</para></listitem>
</varlistentry>

<varlistentry>
<term>netcfg/disable_dhcp</term>
<listitem><para>

<!--
By default, the &d-i; automatically probes for network configuration
via DHCP. If the probe succeeds, you won't have a chance to review and
change the obtained settings. You can get to the manual network setup
only in case the DHCP probe fails.
-->
デフォルトでは、&d-i; は DHCP によりネットワークの設定を自動検出します。
検出が成功すると、確認する機会がなく検出値を変更できないでしょう。
DHCP の検出が失敗する場合のみ、手動ネットワーク設定を行えます。

</para><para>

<!--
If you have a DHCP server on your local network, but want to avoid it
because e.g. it gives wrong answers, you can use the parameter
<userinput>netcfg/disable_dhcp=true</userinput> to prevent configuring
the network with DHCP and to enter the information manually.
-->
ローカルネットワークに DHCP サーバがあるのに、それを回避したい場合 
(例: 誤った値を返す等)、
DHCP でのネットワーク設定をせず手動で情報を入力するのに、
<userinput>netcfg/disable_dhcp=true</userinput> パラメータを使用できます。

</para></listitem>
</varlistentry>

<varlistentry>
<term>hw-detect/start_pcmcia</term>
<listitem><para>

<!--
Set to <userinput>false</userinput> to prevent starting PCMCIA
services, if that causes problems. Some laptops are well known for
this misbehavior.
-->
PCMCIA サービスが原因で問題が発生する場合、
<userinput>false</userinput> を設定することで、
起動しないようにすることができます。
いくつかのラップトップコンピュータには、
そういう行儀悪さがあることが知られています。

</para></listitem>
</varlistentry>

<varlistentry>
<term>preseed/url (url)</term>
<listitem><para>

<!--
Specify the url to a preconfiguration file to download and use in
automating the install. See <xref linkend="automatic-install"/>.
-->
preconfiguration ファイルをダウンロードする URL を指定します。
これは自動インストールで使用します。
<xref linkend="automatic-install"/> を参照してください。

</para></listitem>
</varlistentry>

<varlistentry>
<term>preseed/file (file)</term>
<listitem><para>

<!--
Specify the path to a preconfiguration file to load to
automating the install. See <xref linkend="automatic-install"/>.
-->
自動インストールで読み込む preconfiguration ファイルの PATH を指定します。
<xref linkend="automatic-install"/> を参照してください。

</para></listitem>
</varlistentry>

<varlistentry>
<term>preseed/interactive</term>
<listitem><para>

<!--
Set to <userinput>true</userinput> to display questions even if they have
been preseeded. Can be useful for testing or debugging a preconfiguration
file. Note that this will have no effect on parameters that are passed as
boot parameters, but for those a special syntax can be used.
See <xref linkend="preseed-seenflag"/> for details.
-->
preseed 中に質問を表示する場合には、<userinput>true</userinput> を設定します。
事前設定ファイルのテストやデバッグに便利でしょう。
これは、ブートパラメータに渡すパラメータには影響を及ぼしませんが、
特殊な文法が使えるようになります。
詳細は、<xref linkend="preseed-seenflag"/> をご覧ください。

</para></listitem>
</varlistentry>

<varlistentry>
<term>auto-install/enable (auto)</term>
<listitem><para>

<!--
Delay questions that are normally asked before preseeding is possible until
after the network is configured.
See <xref linkend="preseed-auto"/> for details about using this to
automate installs.
-->
通常 preseed の前に行われる質問を、ネットワークの設定が終わるまで遅らせます。
自動インストールでこのパラメータを使用する際には、
<xref linkend="preseed-auto"/> をご覧ください。

</para></listitem>
</varlistentry>

<varlistentry>
<term>cdrom-detect/eject</term>
<listitem><para>

<!--
By default, before rebooting, &d-i; automatically ejects the optical
media used during the installation. This can be unnecessary if the system
does not automatically boot off the CD. In some cases it may even be
undesirable, for example if the optical drive cannot reinsert the media
itself and the user is not there to do it manually. Many slot loading,
slim-line, and caddy style drives cannot reload media automatically.
-->
デフォルトで &d-i; は、再起動の前にインストールに使用した光学メディアを、
自動的に排出します。
自動的に CD から起動しないようなシステムでは、これは必要ありませんし、
特定の状況下では、困ることになる可能性もあります。
例えば、光学ドライブがメディアを再び差し込むことができず、
手で挿入するようユーザがいなければいけないのに、行うユーザがそこにいないなど。
大半のスロットローディング、スリムライン、キャディタイプのドライブは、
自動的にメディアをリロードできません。

</para><para>

<!--
Set to <userinput>false</userinput> to disable automatic ejection, and
be aware that you may need to ensure that the system does not
automatically boot from the optical drive after the initial
installation.
-->
<userinput>false</userinput> に設定すると、自動排出を無効にできます。
また、システムの初期インストール後に、
光学ドライブから自動起動しないことを保証する必要があります。

</para></listitem>
</varlistentry>

<varlistentry>
<term>debian-installer/allow_unauthenticated</term>
<listitem><para>

<!--
By default the installer requires that repositories be authenticated
using a known gpg key. Set to <userinput>true</userinput> to
disable that authentication.
<emphasis role="bold">Warning: insecure, not recommended.</emphasis>
-->
デフォルトでは、既知の gpg キーで認証されたリポジトリが、
インストーラには必要です。
この認証を無効にするのに <userinput>true</userinput> と設定してください。
<emphasis role="bold">警告: 危険です。お奨めしません。</emphasis>

</para></listitem>
</varlistentry>

<varlistentry arch="alpha;m68k;mips;mipsel">
<term>ramdisk_size</term>
<listitem><para>

<!--
This parameter should already be set to a correct value where needed;
set it only it you see errors during the boot that indicate the ramdisk
could not be loaded completely. The value is in kB.
-->
このパラメータが必要な場合は、すでに正しい値が設定されているはずです。
その上で、RAM ディスクが完全にロードされずに起動に失敗する場合のみ、
設定してください。値は kB で指定してください。

</para></listitem>
</varlistentry>

<varlistentry condition="gtk">
<term>mouse/left</term>
<listitem><para>

<!--
For the gtk frontend (graphical installer), users can switch the mouse to
left-handed operation by setting this parameter to <userinput>true</userinput>.
-->
gtk フロントエンド (グラフィカルインストーラ) 用に、
このパラメータを <userinput>true</userinput> とすると、
マウスを左手で操作するよう切り替えられます。

</para></listitem>
</varlistentry>

<varlistentry condition="gtk">
<term>directfb/hw-accel</term>
<listitem><para>

<!--
For the gtk frontend (graphical installer), hardware acceleration in directfb
is disabled by default. To enable it, set this parameter to
<userinput>true</userinput> when booting the installer.
-->
gtk フロントエンド (グラフィカルインストーラ) 用に、
directfb のハードウェアアクセラレーションは、デフォルトで無効となっています。
有効にするには、インストーラの起動の際、
このパラメータに <userinput>true</userinput> を設定してください。

</para></listitem>
</varlistentry>

<varlistentry>
<term>rescue/enable</term>
<listitem><para>

<!--
Set to <userinput>true</userinput> to enter rescue mode rather than
performing a normal installation. See <xref linkend="rescue"/>.
-->
通常のインストールではなく、レスキューモードを実行する場合、
<userinput>true</userinput> にセットしてください。
<xref linkend="rescue"/> をご覧ください。

</para></listitem>
</varlistentry>

</variablelist>

   <sect3 id="preseed-args">
<!--
   <title>Using boot parameters to answer questions</title>
-->
   <title>ブートパラメータで質問に答える</title>
<para>

<!--
With some exceptions, a value can be set at the boot prompt for any question
asked during the installation, though this is only really useful in specific
cases. General instructions how to do this can be found in
<xref linkend="preseed-bootparms"/>. Some specific examples are listed below.
-->
例外的に、インストール中の質問にブートパラメータで答を与えることができます。
これは、特殊な状況でのみ便利です。
この方法の概要は、<xref linkend="preseed-bootparms"/> にあります。
特殊な例を以下に示します。

</para>

<variablelist>

<varlistentry>
<term>debian-installer/locale (locale)</term>
<listitem><para>

<!--
Can be used to set both the language and country for the installation.
This will only work if the locale is supported in Debian.
For example, use <userinput>locale=de_CH</userinput> to select German as
language and Switzerland as country.

-->
インストールする際の言語や国の情報を設定するのに使用します。
これは Debian でロケールをサポートしている場合のみ有効です。
例えば、言語にドイツ語、国にスイスを指定するには、
<userinput>locale=de_CH</userinput> と指定します。

</para></listitem>
</varlistentry>

<varlistentry>
<term>anna/choose_modules (modules)</term>
<listitem><para>

<!--
Can be used to automatically load installer components that are not loaded
by default.
Examples of optional components that may be useful are
<classname>openssh-client-udeb</classname> (so you can use
<command>scp</command> during the installation)<phrase arch="not-s390"> and
<classname>ppp-udeb</classname> (see <xref linkend="pppoe"/>)</phrase>.
-->
デフォルトではロードされないインストーラコンポーネントを、
自動的に読み込むのに使用します。
追加コンポーネントの例として、<classname>openssh-client-udeb</classname> 
(インストール中に <command>scp</command> コマンドを使用できる) 
<phrase arch="not-s390">や、
<classname>ppp-udeb</classname> (<xref linkend="pppoe"/> をご覧ください) 
</phrase>が便利です。

</para></listitem>
</varlistentry>

<varlistentry>
<term>netcfg/disable_dhcp</term>
<listitem><para>

<!--
Set to <userinput>true</userinput> if you want to disable DHCP and instead
force static network configuration.
-->
DHCP を無効にし、静的ネットワーク設定を強制するには、
<userinput>true</userinput> と設定します。

</para></listitem>
</varlistentry>

<varlistentry>
<term>mirror/protocol (protocol)</term>
<listitem><para>

<!--
By default the installer will use the http protocol to download files from
Debian mirrors and changing that to ftp is not possible during installations
at normal priority. By setting this parameter to <userinput>ftp</userinput>,
you can force the installer to use that protocol instead. Note that you
cannot select an ftp mirror from a list, you have to enter the hostname
manually.
-->
デフォルトでインストーラは、
Debian のミラーサイトからファイルをダウンロードするのに http 
プロトコルを使用し、通常の優先度ではインストール中に ftp に変更できません。
このパラメータに <userinput>ftp</userinput> と設定すると、
インストーラに ftp を使用するように強制できます。
一覧から ftp ミラーを選択できず、
ホスト名を手入力しなければならないことに注意してください。

</para></listitem>
</varlistentry>

<varlistentry>
<term>tasksel:tasksel/first (tasks)</term>
<listitem><para>

<!--
Can be used to select tasks that are not available from the interactive task
list, such as the <literal>kde-desktop</literal> task.
See <xref linkend="pkgsel"/> for additional information.
-->
<literal>kde-desktop</literal> タスクのような、
タスク一覧に表示されないタスクを選択するのに使用します。
さらなる情報は <xref linkend="pkgsel"/> をご覧ください。

</para></listitem>
</varlistentry>

</variablelist>

   </sect3>

   <sect3 id="module-parms">
<!--
   <title>Passing parameters to kernel modules</title>
-->
   <title>カーネルモジュールへパラメータを渡す</title>
<para>

<!--
If drivers are compiled into the kernel, you can pass parameters to them
as described in the kernel documentation. However, if drivers are compiled
as modules and because kernel modules are loaded a bit differently during
an installation than when booting an installed system, it is not possible
to pass parameters to modules as you would normally do. Instead, you need
to use a special syntax recognized by the installer which will then make
sure that the parameters are saved in the proper configuration files and
will thus be used when the modules are actually loaded. The parameters
will also be propagated automatically to the configuration for the installed
system.
-->
カーネル内にコンパイルされているドライバの場合、
カーネルのドキュメントに記載されている方法でパラメータを渡せます。
しかし、ドライバがモジュールとしてコンパイルされており、
インストールしたシステムの起動時に、
インストール時と比べてカーネルモジュールの読み込みが若干異なる場合、
通常の方法ではモジュールにパラメータを渡せません。
代わりに、インストーラが認識できる特殊文法を使って
適切な設定ファイルにパラメータを格納しなければなりません。
パラメータは実際にモジュールをロードする際に利用されます。
パラメータは自動的にインストールしたシステムに伝播します。

</para><para>

<!--
Note that it is now quite rare that parameters need to be passed to modules.
In most cases the kernel will be able to probe the hardware present in a
system and set good defaults that way. However, in some situations it may
still be needed to set parameters manually.
-->
モジュールにパラメータを渡さなければならないというのは、
本当にまれな状況だということに注意してください。
ほとんどの場合、カーネルはシステムにあるハードウェアから得られる値を検出し、
適切な値を設定してくれます。
しかしある状況下では、未だにパラメータを手で設定しなければなりません。

</para><para>

<!--
The syntax to use to set parameters for modules is:
-->
モジュールにパラメータを設定する文法は以下のようになります。

<informalexample><screen>
<replaceable>module_name</replaceable>.<replaceable>parameter_name</replaceable>=<replaceable>value</replaceable>
</screen></informalexample>

<!--
If you need to pass multiple parameters to the same or different modules,
just repeat this. For example, to set an old 3Com network interface card
to use the BNC (coax) connector and IRQ 10, you would pass:
-->
1 つないし複数のモジュールに、複数のパラメータを渡す場合は繰り返してください。
例えば、古い 3Com のネットワークインターフェースカードで BNC (coax) を使用し、
IRQ 10 を設定する場合は、以下のようにします。

<informalexample><screen>
3c509.xcvr=3 3c509.irq=10
</screen></informalexample>

</para>
   </sect3>

   <sect3 id="module-blacklist">
<!--
   <title>Blacklisting kernel modules</title>
-->
   <title>カーネルモジュールのブラックリスト化</title>
<para>

<!--
Sometimes it may be necessary to blacklist a module to prevent it from
being loaded automatically by the kernel and udev. One reason could be that
a particular module causes problems with your hardware. The kernel also
sometimes lists two different drivers for the same device. This can cause
the device to not work correctly if the drivers conflict or if the wrong
driver is loaded first.
-->
時には、カーネルや udev が自動的にモジュールを読み込むのを防ぐために、
ブラックリストに載せる必要があるかもしれません。
目的の 1 つには、特定のモジュールが、
あなたのハードウェアで問題を起こす場合が挙げられます。
またカーネルに、同じデバイスに対して複数の異なるドライバがある場合もあります。
ドライバが衝突したり、間違ったドライバを先に読み込んでしまうと、
デバイスが正しく動作しない原因となります。

</para><para>

<!--
You can blacklist a module using the following syntax:
<userinput><replaceable>module_name</replaceable>.blacklist=yes</userinput>.
This will cause the module to be blacklisted in
<filename>/etc/modprobe.d/blacklist.local</filename> both during the
installation and for the installed system.
-->
<userinput><replaceable>module_name</replaceable>.blacklist=yes</userinput> 
といった文法でモジュールをブラックリストに指定できます。
これでそのモジュールが <filename>/etc/modprobe.d/blacklist.local</filename> 
にあるブラックリストに指定され、
インストール中・インストールしたシステムの双方で、
ブラックリストが有効になります。

</para><para>

<!--
Note that a module may still be loaded by the installation system itself.
You can prevent that from happening by running the installation in expert
mode and unselecting the module from the list of modules displayed during
the hardware detection phases.
-->
インストールシステム自体が、
モジュールをまだ読み込んでいる可能性があることに注意してください。
エキスパートモードでインストールを行い、
ハードウェア検出時にモジュールの一覧からモジュールの選択を外すことで、
モジュールの読み込みを防げます。

</para>
   </sect3>
  </sect2>
 </sect1>
