<?xml version="1.0" encoding="ISO-8859-1"?>
<!-- original version: 40980 -->

 <sect1 id="kernel-baking"><title>Compiler un nouveau noyau</title>
<para>

Pourquoi vouloir compiler un nouveau noyau&nbsp;?
Ce n'est en g�n�ral pas n�cessaire, car le noyau par d�faut de
Debian reconna�t la plupart des configurations. En outre diff�rents noyaux sont
disponibles. Vous devriez chercher s'il n'existe pas de paquet contenant un noyau
qui corresponde � votre mat�riel. Cependant, il peut �tre
utile de compiler un nouveau noyau dans les cas suivants&nbsp;:

<itemizedlist>
<listitem><para>

g�rer des p�riph�riques sp�ciaux, ou des conflits de p�riph�riques dans
les noyaux par d�faut&nbsp;;

</para></listitem>
<listitem><para>

activer des options qui ne sont pas incluses dans le noyau par d�faut, 
permettre la gestion de la m�moire haute par exemple&nbsp;;

</para></listitem>

<listitem><para>

optimiser le noyau en enlevant les pilotes inutiles, ce qui peut
acc�l�rer le d�marrage de la machine&nbsp;;

</para></listitem>
<listitem><para>

cr�er un noyau monolithique � la place d'un noyau modulaire&nbsp;;

</para></listitem>
<listitem><para>

utiliser une version de d�veloppement du noyau&nbsp;;

</para></listitem>
<listitem><para>

mieux conna�tre le noyau Linux.

</para></listitem>
</itemizedlist>

</para>

  <sect2><title>Gestion des images du noyau</title>
<para>

N'ayez pas peur de compiler un nouveau noyau. C'est amusant et tr�s instructif.

</para><para>

Pour compiler un noyau <emphasis>fa�on Debian</emphasis>, vous avez besoin des 
paquets suivants&nbsp;:
<classname>fakeroot</classname>, <classname>kernel-package</classname>,
<classname>linux-source-2.6</classname>
et quelques autres qui sont probablement d�j� install�s sur votre machine 
(pour la liste compl�te, voyez
<filename>/usr/share/doc/kernel-package/README.gz</filename>).

</para><para>

Cette m�thode cr�e un .deb � partir des sources du noyau&nbsp;; si
vous utilisez des modules non standard, elle incorpore aussi ces d�pendances
dans le .deb. C'est une bonne solution pour g�rer les images du noyau&nbsp;;
le r�pertoire <filename>/boot</filename> contiendra le noyau, le fichier
<filename>System.map</filename> 
et une sauvegarde du fichier de configuration utilis� pour ce paquet.

</para><para>

Il faut remarquer qu'il n'est pas <emphasis>obligatoire</emphasis> de 
compiler votre noyau <emphasis>fa�on Debian</emphasis> mais vous vous rendrez compte 
qu'utiliser le syst�me de gestion des paquets pour
g�rer les noyaux rend leur installation plus simple et plus s�re.
De m�me, vous pouvez simplement t�l�charger les sources telles qu'elles sont
propos�es par Linus et non pas
<classname>linux-source-2.6</classname>,
tout en utilisant la m�thode de compilation <classname>kernel-package</classname>.

</para><para>

Notez que vous trouverez une documentation compl�te sur l'utilisation de
<classname>kernel-package</classname> dans le r�pertoire
<filename>/usr/share/doc/kernel-package</filename>. Cette section ne 
contient qu'un bref didacticiel.

</para>
<para>

Dans ce qui suit, nous supposerons que vous pouvez tout faire sur votre 
machine et que vous allez extraire les sources du noyau dans votre r�pertoire
personnel <footnote>
<para> D'autres endroits sont possibles pour extraire les sources et construire
le noyau, mais c'est le plus facile car aucun droit sp�cial n'est demand�.
</para></footnote>.
Nous supposerons aussi que la version du noyau est &kernelversion;. Allez 
dans le r�pertoire o� vous voulez d�sarchiver 
les sources et d�sarchivez-les avec
<userinput>tar xjf /usr/src/linux-source-&kernelversion;.tar.bz2</userinput>, 
et d�placez-vous dans le r�pertoire 
<filename>linux-source-&kernelversion;</filename> qui vient d'�tre cr��.

</para><para>

Maintenant, vous pouvez configurer votre noyau. Ex�cutez 
<userinput>make xconfig</userinput> si X11 est install�, configur� et lanc�. 
Ex�cutez <userinput>make menuconfig</userinput> dans le cas contraire 
(vous aurez alors besoin du paquet <classname>libncurses5-dev</classname>). 
Prenez le temps 
de lire l'aide en ligne et de choisir judicieusement les options � activer.
En cas de doute, il est souvent pr�f�rable d'inclure les pilotes de 
p�riph�riques (tels que les contr�leurs SCSI, cartes Ethernet, etc.) que vous 
ne connaissez pas bien.
Faites attention&nbsp;: les autres options, non sp�cifiques au mat�riel, 
doivent �tre laiss�es � leur valeur par d�faut si vous ne les comprenez pas.
N'oubliez pas de s�lectionner �&nbsp;Kernel module loader&nbsp;� dans la 
section �&nbsp;Loadable module support&nbsp;�
(elle n'est pas s�lectionn�e par d�faut), sinon votre syst�me Debian risque 
d'avoir des probl�mes.

</para><para>

Nettoyez l'arborescence des sources et r�initialisez les param�tres
de <classname>kernel-package</classname>. Pour ce faire, tapez
<userinput>make-kpkg clean</userinput>.

</para><para>

Maintenant, compilez votre noyau&nbsp;: 
<userinput>fakeroot make-kpkg --initrd --revision=custom.1.0 kernel_image</userinput>.
Bien s�r, le num�ro de version �&nbsp;1.0&nbsp;� peut �tre chang�&nbsp;; 
il s'agit juste d'un moyen de suivre vos diff�rentes
versions du noyau. De la m�me fa�on, vous pouvez remplacer le mot 
<quote>custom</quote> par ce que vous voulez (par exemple le nom d'une 
machine). La compilation d'un noyau peut �tre plus ou moins longue, selon la puissance 
de votre machine.

</para><para>

Une fois la compilation termin�e, vous pouvez installer votre noyau
personnalis� comme n'importe quel autre paquet. En tant que superutilisateur,
ex�cutez la commande
<userinput>dpkg -i
../linux-image-&kernelversion;-<replaceable>sous-architecture</replaceable>_custom.1.0_&architecture;.deb</userinput>.
La partie <replaceable>sous-architecture</replaceable> est une 
sous-architecture optionnelle,
<phrase arch="x86"> telle que <quote>686</quote>, </phrase>
suivant les options de compilation que vous avez d�finies.
<userinput>dpkg -i</userinput> installera votre noyau ainsi 
que les autres fichiers qui lui seront n�cessaires.
Par exemple, le fichier <filename>System.map</filename> sera install� 
correctement (tr�s utile pour le d�bogage des probl�mes de noyau) et un 
fichier <filename>/boot/config-&kernelversion;</filename> sera install�, qui 
contiendra votre configuration noyau.
Le nouveau paquet est assez �volu� pour utiliser automatiquement le programme d'amor�age de
votre plateforme et mettre � jour l'information sur l'image de noyau utilis�e.
Si vous avez cr�� un paquet pour les modules, vous devrez installer ce paquet �galement.

</para><para>

Vous devez maintenant red�marrer votre syst�me&nbsp;: lisez attentivement les 
�ventuels avertissements produits par les �tapes pr�c�dentes, puis ex�cutez
<userinput>shutdown -r now</userinput>.

</para><para>

D'autres informations sur la compilation des noyaux Debian
se trouvent dans le 
<ulink url="&url-kernel-handbook;"><quote>Debian Linux Kernel Handbook</quote></ulink>.

Pour plus d'informations sur <classname>kernel-package</classname>, lisez
la documentation dans <filename>/usr/share/doc/kernel-package</filename>.

</para>
  </sect2>
 </sect1>
