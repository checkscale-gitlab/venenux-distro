<?xml version="1.0" encoding="EUC-JP"?>
<!-- retain these comments for translator revision tracking -->
<!-- original version: 43741 -->

   <sect3 id="user-setup">
<!--
   <title>Setting Up Users And Passwords</title>
-->
   <title>ユーザとパスワードのセットアップ</title>

    <sect4 id="user-setup-root">
<!--
    <title>Set the Root Password</title>
-->
    <title>root パスワードの設定</title>

<!-- TODO: Document sudo setup (no root password); medium/low prio only -->

<para>

<!--
The <emphasis>root</emphasis> account is also called the
<emphasis>super-user</emphasis>; it is a login that bypasses all
security protection on your system. The root account should only be
used to perform system administration, and only used for as short
a time as possible.
-->
<emphasis>root</emphasis> アカウントは、
ログインするとシステムのすべてのセキュリティ保護をバイパスしてしまうので、
<emphasis>スーパーユーザ</emphasis>とも呼ばれています。
root アカウントはシステム管理のみに使用し、
可能な限り短時間使用するのみにすべきです。

</para><para>

<!--
Any password you create should contain at least 6 characters, and
should contain both upper- and lower-case characters, as well as
punctuation characters.  Take extra care when setting your root
password, since it is such a powerful account.  Avoid dictionary
words or use of any personal information which could be guessed.
-->
作成するパスワードは、少なくとも 6 文字以上で、
大文字小文字、カンマやピリオドを混ぜるべきです。
root パスワードを設定するときには、
強力なアカウント故に特別注意を払ってください。
辞書にある単語や推測される個人情報を使用するのは避けてください。

</para><para>

<!--
If anyone ever tells you they need your root password, be extremely
wary. You should normally never give your root password out, unless you
are administering a machine with more than one system administrator.
-->
誰であっても、root パスワードが必要だと言う人がいる場合には、
殊更に用心してください。
他のシステム管理者と共に機械の管理をしているのでなければ、
root パスワードを教える必要は、通常決してありません。

</para>
   </sect4>

   <sect4 id="make-normal-user">
<!--
   <title>Create an Ordinary User</title>
-->
   <title>一般ユーザの作成</title>

<para>

<!--
The system will ask you whether you wish to create an ordinary user
account at this point.  This account should be your main personal
log-in.  You should <emphasis>not</emphasis> use the root account for
daily use or as your personal login.
-->
システムは、この時点で一般ユーザアカウントを作成するかどうか質問します。
このアカウントは、個人でログインする時のメインとするべきです。
root アカウントを日常的に使用したり、
個人的な用途でログインするべきでは<emphasis>ありません</emphasis>。

<!-- Note: it only _asks_ at medium/low priority -->

</para><para>

<!--
Why not?  Well, one reason to avoid using root's privileges is that it
is very easy to do irreparable damage as root.  Another reason is that
you might be tricked into running a <emphasis>Trojan-horse</emphasis>
program &mdash; that is a program that takes advantage of your
super-user powers to compromise the security of your system behind
your back. Any good book on Unix system administration will cover this
topic in more detail &mdash; consider reading one if it is new to you.
-->
なぜいけないのでしょう? 
root 権限を使用しないようにする理由のひとつは、
root により簡単に取り返しのつかない損害を与えられるということです。
他には、だまされて<emphasis>トロイの木馬</emphasis>
(あなたに隠れ、スーパーユーザ権限を利用してシステムに感染するプログラム)
を動かしてしまうということもあり得ます。
UNIX システム管理に関するいずれの良書でも、この件に関して詳細に扱っています。
今までご存じなければ、ご一読ください。

</para><para>

<!--
You will first be prompted for the user's full name. Then you'll be asked
for a name for the user account; generally your first name or something
similar will suffice and indeed will be the default. Finally, you will be
prompted for a password for this account.
-->
まず初めに、ユーザのフルネームの入力を求められます。
次にユーザアカウントの名前を求められます。
一般的にファーストネームか、必要充分な名前に似た何かがデフォルトになります。
最後にこのアカウントのパスワードを求められます。

</para><para>

<!--
If at any point after installation you would like to create another
account, use the <command>adduser</command> command.
-->
インストール後いつでも、別のアカウントを作成する場合は、
<command>adduser</command> コマンドを使用してください。

</para>
    </sect4>
   </sect3>
