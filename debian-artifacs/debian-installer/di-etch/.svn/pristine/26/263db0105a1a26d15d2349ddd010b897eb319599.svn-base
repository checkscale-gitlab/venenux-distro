<!-- retain these comments for translator revision tracking -->
<!-- original version: 33725 -->


   <sect3 id="localechooser">
   <title>Selecció de les opcions de localització</title>

<para>

En la majoria dels casos, les primeres qüestions que us apareixeran
faran referència a les opcions de localització a utilitzar en la
instal·lació i pel sistema instal·lat. Les opcions de localització
consisteixen en l'idioma, el país i el locales.

</para><para>

L'idioma seleccionat s'utilitzarà en la resta del procés d'instal·lació,
es disposa d'una traducció de les diferents caixes de diàleg.
En cas de no existir una traducció de l'idioma seleccionat,
l'instal·lador utilitzarà la traducció de l'anglès.

</para><para>

El país seleccionat s'utilitzarà posteriorment en el procés d'instal·lació
per seleccionar el fus horari predeterminat i una rèplica apropiada en
funció de la vostra ubicació geogràfica. L'idioma i el país s'utilitzaran
per definir el locale predeterminat del sistema i per ajudar en la 
selecció del teclat.

</para><para>

Primer se us demanarà que seleccioneu el vostre idioma preferit. Els
noms dels idiomes estan llistats en anglès (banda esquerra) i en
el propi idioma (banda dreta); els noms de la banda dreta
es mostren utilitzant la tipografia pròpia de l'idioma. La llista
està ordenada en funció dels noms en anglès. Al capdemunt de la llista
hi ha un opció extra que permet seleccionar el locale <quote>C</quote>
en comptes d'un idioma. Si seleccioneu el locale <quote>C</quote>
la instal·lació es realitzarà en anglès; el sistema instal·lat
no disposarà del suport per a la localització, ja que el paquet
<classname>locales</classname> no estarà instal·lat.

</para><para>

Si heu seleccionant un idioma que està reconegut com a idioma oficial
en més d'un país<footnote>

<para>

En termes tècnics: en cas que hi hagi  múltiples locales per l'idioma amb
diferents codis de país.

</para>

</footnote>, se us demanarà que seleccioneu un país.
Si seleccioneu l'opció <guimenuitem>Altres</guimenuitem> de la part inferior
de la llista, us apareixerà una llista de tot els països, agrupats
per continent. En cas que l'idioma únicament tingui un país associat,
aquest es seleccionarà automàticament.

</para><para>

Se seleccionarà un locale predeterminat en funció de l'idioma i país
seleccionats.
Si esteu en una instal·lació de prioritat mitjana o baixa, podreu
seleccionar un locale predeterminat diferent i seleccionar locales
addicionals a generar pel sistema instal·lat.

</para>
   </sect3>
