<?xml version="1.0" encoding="ISO-8859-1"?>
<!-- original version: 43943 -->
<!-- revisado por jfs, 8 octubre 2004 -->
<!-- revisado por nahoo, 11 octubre 2004 -->

 <sect1 id="hardware-supported">
 <title>Hardware soportado</title>

<para>

Debian no impone requisitos de hardware m�s all� de los que establecen
el n�cleo Linux y el conjunto de herramientas GNU. En cualquier caso,
cualquier arquitectura o plataforma a la que se haya adaptado el
n�cleo Linux, libc, <command>gcc</command>, etc., y para los que exista una
adaptaci�n de Debian, puede ejecutar Debian. Por favor, dir�jase a las
p�ginas de adaptaciones en <ulink url="&url-ports;"></ulink> para m�s
informaci�n sobre sistemas de arquitectura &arch-title; probados con Debian.

</para><para>

En lugar de intentar describir las diferentes configuraciones de hardware
soportadas por &arch-title;, esta secci�n contiene informaci�n general y
referencias adicionales donde puede encontrar m�s informaci�n.

</para>

  <sect2><title>Arquitecturas soportadas</title>

<para>

Debian &release; soporta doce arquitecturas principales y algunas variaciones
de cada arquitectura conocidas como �sabores�.

</para><para>

<informaltable>
<tgroup cols="4">
<thead>
<row>
  <entry>Arquitectura</entry><entry>Designaci�n de Debian</entry>
  <entry>Subarquitectura</entry><entry>Sabor</entry>
</row>
</thead>

<tbody>
<row>
  <entry>Basada en Intel x86</entry>
  <entry>i386</entry>
  <entry></entry>
  <entry></entry>
</row>

<row>
  <entry>AMD64 e Intel EM64T</entry>
  <entry>amd64</entry>
  <entry></entry>
  <entry></entry>
</row>


<row>
  <entry>DEC Alpha</entry>
  <entry>alpha</entry>
  <entry></entry>
  <entry></entry>
</row>

<row>
  <entry morerows="3">ARM y StrongARM</entry>
  <entry morerows="3">arm</entry>
  <entry>Netwinder y CATS</entry>
  <entry>netwinder</entry>
</row><row>
  <entry>Intel IOP32x</entry>
  <entry>iop32x</entry>
</row><row>
  <entry>Intel IXP4xx</entry>
  <entry>ixp4xx</entry>
</row><row>
  <entry>RiscPC</entry>
  <entry>rpc</entry>
</row>

<row>
  <entry morerows="1">HP PA-RISC</entry>
  <entry morerows="1">hppa</entry>
  <entry>PA-RISC 1.1</entry>
  <entry>32</entry>
</row><row>
  <entry>PA-RISC 2.0</entry>
  <entry>64</entry>
</row>

<row>
  <entry>Intel IA-64</entry>
  <entry>ia64</entry>
  <entry></entry>
  <entry></entry>
</row>

<row>
  <entry morerows="3">MIPS (big endian)</entry>
  <entry morerows="3">mips</entry>
  <entry>SGI IP22 (Indy/Indigo 2)</entry>
  <entry>r4k-ip22</entry>
</row><row>
  <entry>SGI IP32 (O2)</entry>
  <entry>r5k-ip32</entry>
</row><row>
  <entry>Broadcom BCM91250A (SWARM)</entry>
  <entry>sb1-bcm91250a</entry>
</row><row>
  <entry>Broadcom BCM91480B (BigSur)</entry>
  <entry>sb1a-bcm91480b</entry>
</row>

<row>
  <entry morerows="4">MIPS (little endian)</entry>
  <entry morerows="4">mipsel</entry>
  <entry>Cobalt</entry>
  <entry>cobalt</entry>
</row><row>
  <entry morerows="1">DECstation</entry>
  <entry>r4k-kn04</entry>
</row><row>
  <entry>r3k-kn02</entry>
</row><row>
  <entry>Broadcom BCM91250A (SWARM)</entry>
  <entry>sb1-bcm91250a</entry>
</row><row>
  <entry>Broadcom BCM91480B (BigSur)</entry>
  <entry>sb1a-bcm91480b</entry>
</row>

<row>
  <entry morerows="5">Motorola 680x0</entry>
  <entry morerows="5">m68k</entry>
  <entry>Atari</entry>
  <entry>atari</entry>
</row><row>
  <entry>Amiga</entry>
  <entry>amiga</entry>
</row><row>
  <entry>68k Macintosh</entry>
  <entry>mac</entry>
</row><row>
  <entry morerows="2">VME</entry>
  <entry>bvme6000</entry>
</row><row>
  <entry>mvme147</entry>
</row><row>
  <entry>mvme16x</entry>
</row>

<row>
  <entry morerows="2">IBM/Motorola PowerPC</entry>
  <entry morerows="2">powerpc</entry>
  <entry>CHRP</entry>
  <entry>chrp</entry>
</row><row>
  <entry>PowerMac</entry>
  <entry>pmac</entry>
</row><row>
  <entry>PReP</entry>
  <entry>prep</entry>
</row>

<row>
  <entry morerows="2">Sun SPARC</entry>
  <entry morerows="2">sparc</entry>
  <entry>sun4m</entry>
  <entry>sparc32</entry>
</row><row>
  <entry>sun4u</entry>
  <entry morerows="1">sparc64</entry>
</row><row>
  <entry>sun4v</entry>
</row>

<row>
  <entry morerows="1">IBM S/390</entry>
  <entry morerows="1">s390</entry>
  <entry>IPL del lector VM y DASD</entry>
  <entry>generic</entry>
</row><row>
  <entry>IPL de cinta</entry>
  <entry>tape</entry>
</row>

</tbody></tgroup></informaltable>

</para><para>

Este documento cubre la instalaci�n para la arquitectura
<emphasis>&arch-title;</emphasis>. Si busca informaci�n sobre cualquiera
de las otras arquitecturas soportadas por Debian consulte las p�ginas de
las <ulink url="http://www.debian.org/ports/">adaptaciones de Debian</ulink>.

</para><para condition="new-arch">

Esta es la primera versi�n oficial de &debian; para la arquitectura
&arch-title;. Consideramos que se ha probado �sta lo suficientemente como
para ser publicada. No obstante, podr�a encontrar algunos fallos dado que
�sta no ha tenido la exposici�n (y por tanto pruebas por usuarios) que han
tenido otras arquitecturas. Utilice nuestro <ulink url="&url-bts;">sistema
de seguimiento de fallos</ulink> para informar cualquier problema; aseg�rese
de indicar que el problema se encuentra en la plataforma &arch-title;.
Tambi�n, podr�a ser necesario utilizar la <ulink
url="&url-list-subscribe;">lista de correo de debian-&arch-listname;</ulink>.

</para>

  </sect2>

<!-- supported cpu docs -->
&supported-alpha.xml;
&supported-amd64.xml;
&supported-arm.xml;
&supported-hppa.xml;
&supported-i386.xml;
&supported-ia64.xml;  <!-- FIXME: currently missing -->
&supported-m68k.xml;
&supported-mips.xml;
&supported-mipsel.xml;
&supported-powerpc.xml;
&supported-s390.xml;
&supported-sparc.xml;

  <sect2 id="gfx" arch="not-s390"><title>Soporte de tarjeta gr�fica</title>

<para arch="x86">

Debe utilizar una interfaz de visualizaci�n compatible con VGA para la
terminal de consola. Pr�cticamente cualquier tarjeta de v�deo moderna es
compatible con VGA. Los est�ndares viejos como CGA, MDA o HGA tambi�n
deber�an funcionar, asumiendo que no necesite soporte de X11. Tenga en cuenta
que no se usa X11 durante el proceso de instalaci�n descrito en este documento.

</para><para>

El soporte de Debian para interfaces gr�ficas est� determinado por el
soporte subyacente encontrado en el sistema X11 de X.Org. Los puertos de
v�deo AGP, PCI y PCIe funcionan bajo X.Org.
Puede encontrar m�s detalles sobre tarjetas, monitores, dispositivos de
se�alamiento y buses en <ulink url="&url-xorg;"></ulink>. Debian &release;
incluye X.Org versi�n &x11ver;.

</para><para arch="mips">

<!-- FIXME: mention explicit graphics chips and not system names -->

El sistema X Window de X.Org se encuentra �nicamente soportado en la SGI Indy y en
la O2. Las placas de evaluaci�n Broadcom BCM91250A y BCM91250B tienen bah�as
PCI est�ndar de 3.3v y soportan la emulaci�n VGA o framebuffer de Linux en un
rango espec�fico de tarjetas gr�ficas. Existe una <ulink
url="&url-bcm91250a-hardware;">lista de compatibilidad</ulink> para las
placas de evaluaci�n Broadcom.

</para><para arch="mipsel">

El sistema X Window de X.Org est� soportado en algunos modelos DECstation. Las placas
de evaluaci�n Broadcom BCM91250A y BCM91250B tienen bah�as PCI est�ndar de 3.3v
y soportan la emulaci�n VGA o framebuffer de Linux en un rango espec�fico de
tarjetas gr�ficas. Existe una <ulink url="&url-bcm91250a-hardware;">lista de
compatibilidad</ulink> para las placas de evaluaci�n Broadcom.

</para><para arch="sparc">

Se soportan la mayor�a de las opciones gr�ficas que se encuentran
habitualmente en equipos basados en procesador Sparc. Existen
controladores de gr�ficos X.org para el framebufer de sunbw2, suncg14, suncg3, suncg6,
sunleo y suntcx, las tarjetas Creator3D y Elite3D (controlador sunffb),
las tarjetas basadas en ATI PGX24/PGX64 (controlador ati), y las
tarjetas basadas en PermediaII (controlador glint).
Si desea utilizar una tarjeta Elite3D con X.org tiene que instalar
adem�s el paquete <classname>afbinit</classname> y leer la
documentaci�n que se incluye en �l para saber c�mo activar la tarjeta.

</para><para arch="sparc">

Tener dos tarjetas gr�ficas no es raro en la configuraci�n de f�brica
de sistemas Sparc. Existe la posibilidad en este caso de que el n�cleo
de Linux no env�e la salida a la tarjeta utilizada inicialmente por el
firmware. A veces se confunde la falta de salida en la consola gr�fica
con un fallo del sistema (generalmente el �ltimo mensaje que se ve en
la consola es �Booting Linux...�). Una soluci�n posible a este
problema es quitar f�sicamente una de las tarjetas de v�deo, otra
opci�n es deshabilitar una de las tarjetas utilizando un par�metro de
arranque del n�cleo. Tambi�n puede utilizar la consola serie como una
alternativa si no necesita (o no desea) la salida gr�fica. En algunos
sistemas puede activar autom�ticamente el uso de la consola serie
desconectando el teclado antes de arrancar el sistema.

</para>
  </sect2>

  <sect2 arch="x86" id="laptops"><title>Ordenadores port�tiles</title>
<para>

Tambi�n se soportan los ordenadores port�tiles. �stos generalmente son
muy especializados o contienen hardware propietario. Para ver si su ordenador
port�til trabaja bien con GNU/Linux, consulte las
<ulink url="&url-x86-laptop;">p�ginas de port�tiles en Linux</ulink>

</para>
   </sect2>


  <sect2 condition="defaults-smp">
<title>M�ltiples procesadores</title>

<para>

El soporte para m�ltiples procesadores (tambi�n llamado
<quote>multi-procesamiento sim�trico</quote> o SMP) est� disponible para esta
arquitectura. La imagen est�ndar del n�cleo de Debian &release; ha sido
compilada con soporte para SMP. Esto no deber�a entorpecer la instalaci�n,
ya que el n�cleo SMP debe ser capaz de arrancar en sistemas no SMP; el
n�cleo simplemente causar� un poco m�s de sobrecarga.

</para><para>

Para optimizar el n�cleo para sistemas de un s�lo procesador, tendr� que
reemplazar el n�cleo est�ndar de Debian. Puede encontrar una discusi�n
a cerca de c�mo hacer esto en <xref linkend="kernel-baking"/>. En este momento
(versi�n del n�cleo &kernelversion;) la forma de deshabilitar SMP es
deseleccionando 
<quote>&smp-config-option;</quote> 
en la secci�n <quote>&smp-config-section;</quote>
de la configuraci�n del n�cleo.

</para>

  </sect2>
 
  <sect2 condition="smp-alternatives">
<title>M�ltiples procesadores</title>

<para>

Existe soporte para multiprocesadores (tambi�n llamado
<quote>multi-procesamiento sim�trico</quote> o SMP) para esta
arquitectura.  La imagen del n�cleo est�ndar de Debian &release; se
compil� con el soporte <firstterm>SMP-alternatives</firstterm>. Esto
significa que el n�cleo detectar� el n�mero de procesadores (o n�cleos
de procesador) y desactivar� autom�ticamente SMP en los sistemas con
un s�lo procesador.

</para><para arch="i386">

Los paquetes con la versi�n 486 del la imagen del n�cleo de Debian 
en &arch-title; no est�n compilados con el soporte SMP.

</para>
  </sect2>


  <sect2 condition="supports-smp">
 <title>M�ltiples procesadores</title>
<para>

Existe soporte para multiprocesadores (tambi�n llamado
<quote>multi-procesamiento sim�trico</quote> o SMP) para esta arquitectura. Sin
embargo, la imagen est�ndar del n�cleo de Debian &release; no incluye SMP.
Esto no deber�a entorpecer la instalaci�n, ya que el n�cleo est�ndar, sin
SMP, deber�a arrancar en sistemas SMP. El n�cleo simplemente utilizar� s�lo
el primer procesador.

</para><para>

Tendr� que sustituir el n�cleo est�ndar de Debian si quiere aprovechar m�ltiples
procesadores. Puede encontrar una discusi�n a cerca de c�mo hacer
esto en <xref linkend="kernel-baking"/>. En este momento (versi�n del n�cleo
&kernelversion;) puede habilitar SMP seleccionando
<quote>&smp-config-option;</quote> 
en la secci�n <quote>&smp-config-section;</quote>
de la configuraci�n del n�cleo.

</para>
  </sect2>
  <sect2 condition="supports-smp-sometimes">
 <title>M�ltiples procesadores</title>
<para>


El soporte para multiprocesadores (tambi�n llamado <quote>multiprocesamiento
sim�trico</quote> o SMP) est� soportado para �sta arquitectura y est�
incluido en una imagen pre-compilada del n�cleo de Debian. Dependiendo
de su medio de instalaci�n, este n�cleo con soporte de SMP podr�a o no
instalarse como el n�cleo predeterminado. Esto no deber�a entorpecer la
instalaci�n, debido a que el n�cleo est�ndar, sin SMP, deber�a arrancar en
sistemas SMP. En este caso, el n�cleo simplemente utilizar� el primer
procesador.

</para><para>

Si desea hacer uso de m�ltiples procesadores deber� verificar que el
paquete del n�cleo que soporta SMP est� instalado, si no lo est�, seleccione
un n�cleo apropiado.

</para><para>

Tambi�n puede construir su propio n�cleo personalizado incluyendo soporte
SMP. Puede encontrar una discusi�n sobre c�mo hacerlo en <xref
linkend="kernel-baking"/>. En este momento (versi�n del n�cleo &kernelversion;)
la forma de habilitar SMP es elegir la opci�n
<quote>&smp-config-option;</quote> 
en la secci�n <quote>&smp-config-section;</quote>
de la configuraci�n del n�cleo.

</para>
  </sect2>
 </sect1>
