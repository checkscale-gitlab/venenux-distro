<?xml version="1.0" encoding="ISO-8859-1"?>
<!--  original version: 45405 -->

 <sect1 id="pppoe" arch="not-s390">
 <title>Installer &debian; avec PPP sur Ethernet (PPPoE)</title>

<para>

PPP sur Ethernet (PPPoE) est un protocole tr�s utilis� dans certains pays pour
�tablir des connexions de type ADSL ou c�ble � un fournisseur d'acc�s � internet.
L'installateur ne permet pas une mise en place automatique de ce type de connexion mais
il est tr�s facile de le faire soi-m�me.

</para><para>

Une connexion PPPoE �tablie pendant l'installation sera aussi disponible
pour le syst�me install� apr�s le r�armor�age. Consultez <xref linkend="boot-new"/>.

</para><para>

Pour pouvoir utiliser une connexion PPPoE pendant l'installation,
vous devez faire une installation avec l'une des images CD-ROM/DVD disponibles.
Il n'est pas possible d'utiliser une autre m�thode d'installation, par exemple
netboot<phrase condition="supports-floppy-boot"> ou floppy</phrase>.

</para><para>

Une installation avec PPPoE ressemble beaucoup � un autre type d'installation.
La suite du texte explique certaines diff�rences.

</para>

<itemizedlist>
<listitem><para>

Il faut passer le param�tre <userinput>modules=ppp-udeb</userinput> � l'installateur.
Saisissez � l'invite de d�marrage :

<informalexample><screen>
install modules=ppp-udeb
</screen></informalexample>

ou, si vous pr�f�rez l'installateur en mode graphique :

<informalexample><screen>
installgui modules=ppp-udeb
</screen></informalexample>

Ainsi le composant charg� de la mise en &oelig;uvre du protocole PPPoE
(<classname>ppp-udeb</classname>) sera ex�cut� automatiquement.

</para></listitem>
<listitem><para>

Ensuite suivez les �tapes habituelles de l'installateur (s�lection de la langue, du pays
et du clavier&nbsp;; chargement de composants suppl�mentaires<footnote>

<para>

Le composant <classname>ppp-udeb</classname> est charg� � ce moment-l�.
Si vous faites une installation en mode <quote>expert</quote>, priorit� <quote>medium</quote>
ou <quote>low</quote>, vous pouvez choisir vous-m�me le composant
au lieu de saisir un param�tre <quote>modules</quote> � l'amor�age.

</para>

</footnote>).

</para></listitem>
<listitem><para>

L'�tape suivante est la d�tection du mat�riel r�seau et des cartes Ethernet pr�sentes.

</para></listitem>
<listitem><para>

Maintenant commence la mise en &oelig;uvre du protocole PPPoE. L'installateur examine
toutes les interfaces Ethernet d�tect�es pour trouver un concentrateur PPPoE
(type de serveur qui g�re les connexions PPPoE).

</para><para>

Il est possible que le concentrateur ne soit pas trouv� du premier coup.
Cela arrive parfois avec des r�seaux lents ou surcharg�s ou avec des serveurs d�fectueux.
La plupart du temps, le deuxi�me essai r�ussira. Pour r�essayer,
s�lectionnez <guimenuitem>Configurer et lancer une connexion PPPoE</guimenuitem> dans
le menu principal de l'installateur.

</para></listitem>
<listitem><para>

Une fois le concentrateur trouv�, l'utilisateur doit indiquer son identifiant et son mot de
passe PPPoE.

</para></listitem>
<listitem><para>

L'installateur tente maintenant d'�tablir la connexion PPPoE.
Si tout est correct, la connexion est �tablie et l'installateur peut r�cup�rer
des paquets sur internet. Si l'information donn�e n'est pas correcte, l'installateur
s'arr�te. On peut retenter la configuration en s�lectionnant
l'entr�e de menu <guimenuitem>Configurer et lancer une connexion PPPoE</guimenuitem>.

</para></listitem>
</itemizedlist>

 </sect1>
