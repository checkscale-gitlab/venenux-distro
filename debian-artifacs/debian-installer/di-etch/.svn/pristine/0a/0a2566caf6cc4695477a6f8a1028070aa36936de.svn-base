<?xml version="1.0" encoding="EUC-JP"?>
<!-- retain these comments for translator revision tracking -->
<!-- original version: 43943 -->

 <sect1 id="hardware-supported">
<!--
 <title>Supported Hardware</title>
-->
 <title>サポートするハードウェア</title>
<para>

<!--
Debian does not impose hardware requirements beyond the requirements
of the Linux kernel and the GNU tool-sets.  Therefore, any
architecture or platform to which the Linux kernel, libc,
<command>gcc</command>, etc. have been ported, and for which a Debian
port exists, can run Debian. Please refer to the Ports pages at
<ulink url="&url-ports;"></ulink> for
more details on &arch-title; architecture systems which have been
tested with Debian.
-->
Debian は、Linux カーネルや GNU ツールセットが必要とする以上のハードウェア
を要求しません。それゆえ、Linux カーネル、libc、<command>gcc</command> などが
移植されていて、Debian の移植版が存在すれば、どんなアーキテクチャや
プラットフォームでも Debian を動作させることができます。
すでに Debian でテストされている &arch-title; アーキテクチャの詳細は、
移植版のページ (<ulink url="&url-ports;"></ulink>) を参照してください。

</para><para>

<!--
Rather than attempting to describe all the different hardware
configurations which are supported for &arch-title;, this section
contains general information and pointers to where additional
information can be found.
-->
この節では、&arch-title; でサポートされるハードウェアの様々な設定の
すべてに触れることは避け、一般的な情報とさらなる情報が見つけられる場所への
ポインタを紹介します。

</para>

<!--
  <sect2><title>Supported Architectures</title>
-->
  <sect2><title>サポートするアーキテクチャ</title>
<para>

<!--
Debian &release; supports twelve major architectures and several
variations of each architecture known as <quote>flavors</quote>.
-->
Debian &release; は 12 の主要なアーキテクチャと、
<quote>フレーバー</quote>と呼ばれる各アーキテクチャのバリエーションを
サポートしています。

</para><para>

<informaltable>
<tgroup cols="4">
<thead>
<row>
  <entry>アーキテクチャ</entry><entry>Debian 名称</entry>
  <entry>サブアーキテクチャ</entry><entry>フレーバー</entry>
</row>
</thead>

<tbody>
<row>
<!--
  <entry>Intel x86-based</entry>
-->
  <entry>Intel x86 ベース</entry>
  <entry>i386</entry>
  <entry></entry>
  <entry></entry>
</row>

<row>
  <entry>AMD64 &amp; Intel EM64T</entry>
  <entry>amd64</entry>
  <entry></entry>
  <entry></entry>
</row>

<row>
  <entry>DEC Alpha</entry>
  <entry>alpha</entry>
  <entry></entry>
  <entry></entry>
</row>

<row>
<!--
  <entry morerows="3">ARM and StrongARM</entry>
  <entry morerows="3">arm</entry>
  <entry>Netwinder and CATS</entry>
  <entry>netwinder</entry>
-->
  <entry morerows="3">ARM・StrongARM</entry>
  <entry morerows="3">arm</entry>
  <entry>Netwinder・CATS</entry>
  <entry>netwinder</entry>
</row><row>
  <entry>Intel IOP32x</entry>
  <entry>iop32x</entry>
</row><row>
  <entry>Intel IXP4xx</entry>
  <entry>ixp4xx</entry>
</row><row>
  <entry>RiscPC</entry>
  <entry>rpc</entry>
</row>

<row>
  <entry morerows="1">HP PA-RISC</entry>
  <entry morerows="1">hppa</entry>
  <entry>PA-RISC 1.1</entry>
  <entry>32</entry>
</row><row>
  <entry>PA-RISC 2.0</entry>
  <entry>64</entry>
</row>

<row>
  <entry>Intel IA-64</entry>
  <entry>ia64</entry>
  <entry></entry>
  <entry></entry>
</row>

<row>
<!--
  <entry morerows="3">MIPS (big endian)</entry>
-->
  <entry morerows="3">MIPS (ビッグエンディアン)</entry>
  <entry morerows="3">mips</entry>
  <entry>SGI IP22 (Indy/Indigo 2)</entry>
  <entry>r4k-ip22</entry>
</row><row>
  <entry>SGI IP32 (O2)</entry>
  <entry>r5k-ip32</entry>
</row><row>
  <entry>Broadcom BCM91250A (SWARM)</entry>
  <entry>sb1-bcm91250a</entry>
</row><row>
  <entry>Broadcom BCM91480B (BigSur)</entry>
  <entry>sb1a-bcm91480b</entry>
</row>

<row>
<!--
  <entry morerows="4">MIPS (little endian)</entry>
-->
  <entry morerows="4">MIPS (リトルエンディアン)</entry>
  <entry morerows="4">mipsel</entry>
  <entry>Cobalt</entry>
  <entry>cobalt</entry>
</row><row>
  <entry morerows="1">DECstation</entry>
  <entry>r4k-kn04</entry>
</row><row>
  <entry>r3k-kn02</entry>
</row><row>
  <entry>Broadcom BCM91250A (SWARM)</entry>
  <entry>sb1-bcm91250a</entry>
</row><row>
  <entry>Broadcom BCM91480B (BigSur)</entry>
  <entry>sb1a-bcm91480b</entry>
</row>

<row>
  <entry morerows="5">Motorola 680x0</entry>
  <entry morerows="5">m68k</entry>
  <entry>Atari</entry>
  <entry>atari</entry>
</row><row>
  <entry>Amiga</entry>
  <entry>amiga</entry>
</row><row>
  <entry>68k Macintosh</entry>
  <entry>mac</entry>
</row><row>
  <entry morerows="2">VME</entry>
  <entry>bvme6000</entry>
</row><row>
  <entry>mvme147</entry>
</row><row>
  <entry>mvme16x</entry>
</row>

<row>
  <entry morerows="2">IBM/Motorola PowerPC</entry>
  <entry morerows="2">powerpc</entry>
  <entry>CHRP</entry>
  <entry>chrp</entry>
</row><row>
  <entry>PowerMac</entry>
  <entry>pmac</entry>
</row><row>
  <entry>PReP</entry>
  <entry>prep</entry>
</row>

<row>
  <entry morerows="2">Sun SPARC</entry>
  <entry morerows="2">sparc</entry>
  <entry>sun4m</entry>
  <entry>sparc32</entry>
</row><row>
  <entry>sun4u</entry>
  <entry morerows="1">sparc64</entry>
</row><row>
  <entry>sun4v</entry>
</row>

<row>
  <entry morerows="1">IBM S/390</entry>
  <entry morerows="1">s390</entry>
  <entry>VM-reader や DASD からの IPL</entry>
  <entry>generic</entry>
</row><row>
  <entry>テープからの IPL</entry>
  <entry>tape</entry>
</row>

</tbody></tgroup></informaltable>

</para><para>

<!--
This document covers installation for the
<emphasis>&arch-title;</emphasis> architecture.  If you are looking
for information on any of the other Debian-supported architectures
take a look at the
<ulink url="http://www.debian.org/ports/">Debian-Ports</ulink> pages.
-->
この文書は <emphasis>&arch-title;</emphasis> 
アーキテクチャへのインストールを扱います。
なお、他のアーキテクチャに関する情報については、
<ulink url="http://www.debian.org/ports/">Debian 移植版</ulink>
ページをご覧ください。

</para><para condition="new-arch">

<!--
This is the first official release of &debian; for the &arch-title;
architecture.  We feel that it has proven itself sufficiently to be
released. However, because it has not had the exposure (and hence
testing by users) that some other architectures have had, you may
encounter a few bugs. Use our
<ulink url="&url-bts;">Bug Tracking System</ulink> to report any
problems; make sure to mention the fact that the bug is on the
&arch-title; platform. It can be necessary to use the
<ulink url="&url-list-subscribe;">debian-&arch-listname; mailing list</ulink>
as well.
-->
これは &arch-title; アーキテクチャ用 &debian; の初公式リリースです。
すでにリリースとするに充分安定していると私たちは考えています。
しかし、まだ他のアーキテクチャ版ほど広く使われていない
(つまりユーザによるテストも多くない) ことから、
いくつかのバグにでくわす可能性もあります。
何か問題が起きたら、<ulink url="&url-bts;">バグ追跡システム</ulink>
を使って報告してください。その際、そのバグが &arch-title;
プラットフォーム上のものであることを必ず書き添えてください。また
<ulink url="&url-list-subscribe;">debian-&arch-listname; メーリングリスト</ulink>
の購読も必要かもしれません。

</para>
  </sect2>

<!-- supported cpu docs -->
&supported-alpha.xml;
&supported-amd64.xml;
&supported-arm.xml;
&supported-hppa.xml;
&supported-i386.xml;
&supported-ia64.xml;  <!-- FIXME: currently missing -->
&supported-m68k.xml;
&supported-mips.xml;
&supported-mipsel.xml;
&supported-powerpc.xml;
&supported-s390.xml;
&supported-sparc.xml;

<!--
  <sect2 id="gfx" arch="not-s390"><title>Graphics Card Support</title>
-->
  <sect2 id="gfx" arch="not-s390"><title>グラフィックカードのサポート</title>
<para arch="x86">

<!--
You should be using a VGA-compatible display interface for the console
terminal. Nearly every modern display card is compatible with
VGA. Ancient standards such CGA, MDA, or HGA should also work,
assuming you do not require X11 support.  Note that X11 is not used
during the installation process described in this document.
-->
コンソール端末を使用するためには、VGA 互換ディスプレイインターフェースが
必要です。最近のビデオカードは、そのほぼすべてが VGA と互換性があります。
かつての標準であった CGA、MDA、HGA なども、X11
のサポートを必要としないなら動作するでしょう。
なお、この文書で扱うインストールの過程では X11 を用いません。

</para><para>

<!--
Debian's support for graphical interfaces is determined by the
underlying support found in X.Org's X11 system.  Most AGP, PCI and
PCIe video cards work under X.Org.  Details on supported graphics
buses, cards, monitors, and pointing devices can be found at
<ulink url="&url-xorg;"></ulink>.  Debian &release; ships
with X.Org version &x11ver;.
-->
Debian がサポートするグラフィックインターフェースは、
X.Org の X11 System のサポートに基づいたものです。
ほとんどの AGP, PCI, PCIe ビデオカードは X.Org の下で動作します。
サポートされているグラフィックバス、カード、モニタ、ポインティングデバイス
に関するより詳細な情報については、
<ulink url="&url-xorg;"></ulink> をご覧ください。
なお Debian &release; は X.Org バージョン &x11ver; を採用しています。

</para><para arch="mips">

<!-- FIXME: mention explicit graphics chips and not system names -->
<!--
The X.Org X Window System is only supported on the SGI Indy and the O2.  The
Broadcom BCM91250A and BCM91480B evaluation boards have standard 3.3v PCI
slots and support VGA emulation or Linux framebuffer on a selected range
of graphics cards.  A <ulink url="&url-bcm91250a-hardware;">compatibility
listing</ulink> for Broadcom evaluation boards is available.
-->
X.Org X Window System は SGI の Indy と O2 をサポートしています。
Broadcom BCM91250A/BCM91480B 評価ボードには標準 3.3v PCI スロットがあり、
VGA エミュレーションや、グラフィックカードの選択した範囲での 
Linux フレームバッファをサポートしています。
Broadcom 評価ボード用の<ulink url="&url-bcm91250a-hardware;">互換性リスト</ulink>を利用できます。

</para><para arch="mipsel">

<!--
The X.Org X Window System is supported on some DECstation models.  The
Broadcom BCM91250A and BCM91480B evaluation boards have standard 3.3v PCI
slots and support VGA emulation or Linux framebuffer on a selected range
of graphics cards.  A <ulink url="&url-bcm91250a-hardware;">compatibility
listing</ulink> for Broadcom evaluation boards is available.
-->
X.Org X Window System は DECstation モデルをいくつかサポートしています。
Broadcom BCM91250A/BCM91480B 評価ボードには標準 3.3v PCI スロットがあり、
VGA エミュレーションや、グラフィックカードの選択した範囲での 
Linux フレームバッファをサポートしています。
Broadcom 評価ボード用の<ulink url="&url-bcm91250a-hardware;">互換性リスト</ulink>を利用できます。

</para><para arch="sparc">

<!--
Most graphics options commonly found on Sparc-based machines are supported.
X.org graphics drivers are available for sunbw2, suncg14, suncg3, suncg6,
sunleo and suntcx framebuffers, Creator3D and Elite3D cards (sunffb driver),
PGX24/PGX64 ATI-based video cards (ati driver), and PermediaII-based cards
(glint driver). To use an Elite3D card with X.org you additionally need to
install the <classname>afbinit</classname> package, and read the documentation
included with it on how to activate the card.
-->
Sparc ベースマシンで共通して存在する、
ほとんどのグラフィックオプションをサポートしています。
X.org グラフィックドライバでは、
sunbw2, suncg14, suncg3, suncg6, sunleo, suntcx の各フレームバッファや、
Creator3D や Elite3D のカード (sunffb ドライバ)、
PGX24/PGX64 ATI ベースビデオカード (ati ドライバ)、
PermediaII ベースカード (glint ドライバ) が利用できます。
Elite3D カードを X.org で使用するには、
<classname>afbinit</classname> パッケージを追加インストールする必要があります。
また、カードを有効にするために、
そのパッケージに付属するドキュメントをお読みください。

</para><para arch="sparc">

<!--
It is not uncommon for a Sparc machine to have two graphics cards in a
default configuration. In such a case there is a possibility that the
Linux kernel will not direct its output to the card initially used by the
firmware. The lack of output on the graphical console may then be mistaken
for a hang (usually the last message seen on console is 'Booting Linux...').
One possible solution is to physically remove one of the video cards;
another option is to disable one of the cards using a kernel boot parameter.
Also, if graphical output is not required or desired, serial console may be
used as an alternative. On some systems use of serial console can be
activated automatically by disconnecting the keyboard before booting the
system.
-->
Sparc マシンのデフォルト構成で、グラフィックカードを 2 つ持っているのは、
珍しいことではありません。
そのような場合、ファームウェアではじめに使用したカードに、
Linux カーネルが出力を向けない可能性があります。
グラフィカルコンソールへの出力が欠けると、ハングに間違われるかもしれません
(通常、コンソールに表示される最後のメッセージは、'Booting Linux...' です)。
解決法の 1 つにビデオカードを物理的に取り除いてしまう、という物があります。
また、カーネルのブートパラメータで、
カードを 1 つ無効にしてしまうという方法もあります。
グラフィック出力が必須でないか必要ないなら、
シリアルコンソールを代わりに使用する方法もあります。
ある種のシステムでは、システムが起動する前にキーボードを接続しないと、
自動的にシリアルコンソールを使用する物があります。

</para>
  </sect2>

<!--
  <sect2 arch="x86" id="laptops"><title>Laptops</title>
-->
  <sect2 arch="x86" id="laptops"><title>ラップトップコンピュータ</title>
<para>

<!--
Laptops are also supported.  Laptops are often specialized or contain
proprietary hardware.  To see if your particular laptop works well
with GNU/Linux, see the
<ulink url="&url-x86-laptop;">Linux Laptop pages</ulink>
-->
ラップトップコンピュータもサポートしています。
ただラップトップコンピュータでは、特殊な設計がされていたり、
閉鎖的なハードウェアが採用されていたりすることがよくあります。
特定のラップトップが GNU/Linux でうまく動作するかどうかを知るためには、
<ulink url="&url-x86-laptop;">Linux ラップトップページ</ulink>
をご覧ください。

</para>
   </sect2>


  <sect2 condition="defaults-smp">
  <title>マルチプロセッサ</title>
<para>

<!--
Multiprocessor support &mdash; also called <quote>symmetric multiprocessing</quote>
or SMP &mdash; is available for this architecture.  The standard Debian
&release; kernel image was compiled with SMP support.  This should not
prevent installation, since the SMP kernel should boot on non-SMP systems;
the kernel will simply cause a bit more overhead.
-->
このアーキテクチャでは、
マルチプロセッササポート (<quote>対称型マルチプロセッシング</quote>や 
SMP と呼ばれている) が利用できます。
Debian &release; の標準カーネルイメージは SMP サポートを有効にして
コンパイルされています。
SMP カーネルは非 SMP システムでも起動できますから、
インストールには問題ありません。
ただカーネルに若干オーバヘッドがかかります。

</para><para>

<!--
In order to optimize the kernel for single CPU systems, you'll have to
replace the standard Debian kernel.  You can find a discussion of how
to do this in <xref linkend="kernel-baking"/>.  At this time
(kernel version &kernelversion;) the way you disable SMP is to deselect
<quote>&smp-config-option;</quote> in the <quote>&smp-config-section;</quote>
section of the kernel config.
-->
シングル CPU システムにカーネルを最適化したい場合は、
Debian の標準カーネルを置き換える必要があります。
その手順に関する議論は <xref linkend="kernel-baking"/> にあります。
現時点 (カーネルバージョン &kernelversion;) で SMP を無効にするためには、
カーネルコンフィグレーションの <quote>&smp-config-section;</quote> 
セクションにある <quote>&smp-config-option;</quote> の選択を解除してください。

</para>
  </sect2>

  <sect2 condition="smp-alternatives">
<!--
<title>Multiple Processors</title>
-->
<title>マルチプロセッサ</title>

<para>

<!--
Multiprocessor support &mdash; also called <quote>symmetric
multiprocessing</quote> or SMP &mdash; is available for this architecture.
The standard Debian &release; kernel image was compiled with
<firstterm>SMP-alternatives</firstterm> support. This means that the kernel
will detect the number of processors (or processor cores) and will
automatically deactivate SMP on uniprocessor systems.
-->
このアーキテクチャでは、
マルチプロセッササポート (<quote>対称型マルチプロセッシング</quote>や 
SMP と呼ばれている) が利用できます。
Debian &release; の標準カーネルイメージは 
<firstterm>SMP-alternatives</firstterm> 
をサポートするようコンパイルされています。
このため、プロセッサの数 (やプロセッサコアの数) を検出し、
単一プロセッサシステムの場合、自動的に SMP を無効にします。

</para><para arch="i386">

<!--
The 486 flavour of the Debian kernel image packages for &arch-title;
is not compiled with SMP support.
-->
&arch-title; 用 Debian カーネルイメージパッケージの 486 フレーバーでは、
SMP をサポートするようコンパイルされていません。

</para>
  </sect2>

  <sect2 condition="supports-smp">
  <title>マルチプロセッサ</title>
<para>

<!--
Multiprocessor support &mdash; also called <quote>symmetric
multiprocessing</quote> or SMP &mdash; is available for this architecture.
However, the standard Debian &release; kernel image does not support
SMP.  This should not prevent installation, since the standard,
non-SMP kernel should boot on SMP systems; the kernel will simply use
the first CPU.
-->
このアーキテクチャでは、
マルチプロセッササポート (<quote>対称型マルチプロセッシング</quote>や 
SMP と呼ばれている) が利用できます。
しかし Debian &release; の標準カーネルイメージは SMP をサポートしていません。
標準の非 SMP カーネルは SMP システムでも起動できますから、
インストールには問題ありません。
標準カーネルは単に 1 番目の CPU を用います。

</para><para>

<!--
In order to take advantage of multiple processors, you'll have to
replace the standard Debian kernel.  You can find a discussion of how
to do this in <xref linkend="kernel-baking"/>.  At this time
(kernel version &kernelversion;) the way you enable SMP is to select
<quote>&smp-config-option;</quote> in the <quote>&smp-config-section;</quote>
section of the kernel config.
-->
マルチプロセッサを利用するためには、Debian の標準カーネルを置き換える必要
があります。その手順に関する話題は <xref linkend="kernel-baking"/> にあります。
現時点 (カーネルバージョン &kernelversion;) で SMP を有効にするためには、
カーネルコンフィグレーションの <quote>&smp-config-section;</quote>
セクションにある <quote>&smp-config-option;</quote> を選択してください。

</para>
  </sect2>

  <sect2 condition="supports-smp-sometimes">
  <title>マルチプロセッサ</title>
<para>

<!--
Multiprocessor support &mdash; also called <quote>symmetric
multiprocessing</quote> or SMP &mdash; is available for this architecture,
and is supported by a precompiled Debian kernel image. Depending on your
install media, this SMP-capable kernel may or may not be installed by
default. This should not prevent installation, since the standard,
non-SMP kernel should boot on SMP systems; the kernel will simply use
the first CPU.
-->
このアーキテクチャでは、
マルチプロセッササポート (<quote>対称型マルチプロセッシング</quote>や 
SMP と呼ばれている) が利用でき、
コンパイル済みの Debian カーネルイメージでサポートされています。
この SMP 対応カーネルが、デフォルトでインストールされるかどうかは、
インストールに使用するメディアに依存します。
標準の非 SMP カーネルは SMP システムでも起動できますから、
インストールには問題ありません。
標準カーネルは単に 1 番目の CPU を用います。

</para><para>

<!--
In order to take advantage of multiple processors, you should check to see
if a kernel package that supports SMP is installed, and if not, choose an
appropriate kernel package.
-->
複数のプロセッサを利用するためには、
SMP をサポートするカーネルパッケージがインストールされることをチェックするか、
そうでなければ、適切なカーネルパッケージを選ぶ必要があります。

</para><para>

<!--
You can also build your own customized kernel to support SMP. You can find
a discussion of how to do this in <xref linkend="kernel-baking"/>.  At this
time (kernel version &kernelversion;) the way you enable SMP is to select
<quote>&smp-config-option;</quote> in the <quote>&smp-config-section;</quote>
section of the kernel config.
-->
SMP をサポートしたカスタムカーネルを自分で作ることもできます。
その手順に関する話題は <xref linkend="kernel-baking"/> にあります。
現時点 (カーネルバージョン &kernelversion;) で SMP を有効にするためには、
カーネルコンフィグレーションの <quote>&smp-config-section;</quote>
セクションにある <quote>&smp-config-option;</quote> を選択してください。

</para>
  </sect2>
 </sect1>
