<?xml version="1.0" encoding="EUC-JP"?>
<!-- retain these comments for translator revision tracking -->
<!-- original version: 39614 -->


 <sect1 id="device-names">
 <title>Linux におけるデバイス名</title>
<para>

<!--
Linux disks and partition names may be different from other operating
systems.  You need to know the names that Linux uses when you create
and mount partitions. Here's the basic naming scheme:
-->
Linux におけるディスクおよびパーティションの命名法は、
他のオペレーティングシステムとは異なっています。
パーティションを作成したりマウントしたりする際には、
Linux がどのようなディスク名を用いるのか知っておく必要があります。
以下は基本的な命名法の仕組みです。

</para>
<itemizedlist arch="not-s390">
<listitem><para>

<!--
The first floppy drive is named <filename>/dev/fd0</filename>.
-->
第 1 フロッピードライブは <filename>/dev/fd0</filename> と名付けられる。

</para></listitem>
<listitem><para>

<!--
The second floppy drive is named <filename>/dev/fd1</filename>.
-->
第 2 フロッピードライブは <filename>/dev/fd1</filename> と名付けられる。

</para></listitem>
<listitem><para>

<!--
The first SCSI disk (SCSI ID address-wise) is named
<filename>/dev/sda</filename>.
-->
第 1 SCSI ディスク (SCSI ID アドレスによる) は
<filename>/dev/sda</filename> と名付けられる。

</para></listitem>
<listitem><para>

<!--
The second SCSI disk (address-wise) is named
<filename>/dev/sdb</filename>, and so on.
-->
第 2 SCSI ディスク (アドレスによる) は
<filename>/dev/sdb</filename> と名付けられ、以下も同様。

</para></listitem>
<listitem><para>

<!--
The first SCSI CD-ROM is named <filename>/dev/scd0</filename>, also
known as <filename>/dev/sr0</filename>.
-->
第 1 SCSI CD-ROM は <filename>/dev/scd0</filename> および
<filename>/dev/sr0</filename> と名付けられる。

</para></listitem>
<listitem><para>

<!--
The master disk on IDE primary controller is named
<filename>/dev/hda</filename>.
-->
IDE プライマリーコントローラのマスターディスクは
<filename>/dev/hda</filename> と名付けられる。

</para></listitem>
<listitem><para>

<!--
The slave disk on IDE primary controller is named
<filename>/dev/hdb</filename>.
-->
IDE プライマリーコントローラのスレーブディスクは
<filename>/dev/hdb</filename> と名付けられる。

</para></listitem>
<listitem><para>

<!--
The master and slave disks of the secondary controller can be called
<filename>/dev/hdc</filename> and <filename>/dev/hdd</filename>,
respectively.  Newer IDE controllers can actually have two channels,
effectively acting like two controllers.
-->
IDE セカンダリーコントローラのマスターディスクおよびスレーブディスクは、
それぞれ <filename>/dev/hdc</filename>、<filename>/dev/hdd</filename>
と名付けられる。最近の IDE コントローラは 2 つのチャンネルを持ち、
事実上 2 つのコントローラがあるかのように動作します。

<phrase arch="m68k">
<!--
The letters may differ from what shows in the mac program pdisk
(i.e. what shows up as <filename>/dev/hdc</filename> on pdisk may show
up as <filename>/dev/hda</filename> in Debian).
-->
これらの文字は、Mac のプログラム pdisk が表示するものとは異なっています
(例えば pdisk 上で <filename>/dev/hdc</filename> と表示されるものが
Debian 上では <filename>/dev/hda</filename> と表示されます)。
</phrase>

</para></listitem>

<listitem arch="x86"><para>

<!--
The first XT disk is named <filename>/dev/xda</filename>.
-->
第 1 XT ディスクは <filename>/dev/xda</filename> と名付けられる。

</para></listitem>
<listitem arch="x86"><para>

<!--
The second XT disk is named <filename>/dev/xdb</filename>.
-->
第 2 XT ディスクは <filename>/dev/xdb</filename> と名付けられる。

</para></listitem>
<listitem arch="m68k"><para>

<!--
The first ACSI device is named <filename>/dev/ada</filename>, the
second is named <filename>/dev/adb</filename>.
-->
第 1 ASCI デバイスは <filename>/dev/ada</filename>、
第 2 ASCI デバイスは <filename>/dev/adb</filename> と名付けられる。

</para></listitem>
</itemizedlist>

<itemizedlist arch="s390">
<listitem><para>

<!--
The first DASD device is named
<filename>/dev/dasda</filename>.
-->
第 1 DASD デバイスは
<filename>/dev/dasda</filename> と名付けられる。

</para></listitem>
<listitem><para>

<!--
The second DASD device is named
<filename>/dev/dasdb</filename>, and so on.
-->
第 2 DASD デバイスは
<filename>/dev/dasdb</filename> と名付けられ、以下も同様。

</para></listitem>
</itemizedlist>

<para arch="not-s390">

<!--
The partitions on each disk are represented by appending a decimal
number to the disk name: <filename>sda1</filename> and
<filename>sda2</filename> represent the first and
second partitions of the first SCSI disk drive in your system.
-->
各ディスクのパーティションは、ディスク名に十進数を付け加えることで表します。
例えば <filename>sda1</filename> と <filename>sda2</filename> は、
それぞれシステムの第 1 SCSI ディスクドライブの第 1、
第 2 パーティションを表します。

</para><para arch="not-s390">

<!--
Here is a real-life example.  Let's assume you have a system with 2
SCSI disks, one at SCSI address 2 and the other at SCSI address 4.
The first disk (at address 2) is then named <filename>sda</filename>,
and the second <filename>sdb</filename>.  If the
<filename>sda</filename> drive has 3 partitions on it, these will be
named <filename>sda1</filename>, <filename>sda2</filename>, and
<filename>sda3</filename>.  The same applies to the
<filename>sdb</filename> disk and its partitions.
-->
実際にありそうな例を挙げてみましょう。2 つの SCSI ディスクを持つシステムで、
一方の SCSI アドレスが 2、もう一方の SCSI アドレスが 4 だとします。
最初のディスク (アドレス 2) は <filename>sda</filename>、 
2 つ目のディスクは <filename>sdb</filename> と名付けられます。もし
<filename>sda</filename> ドライブに 3 つのパーティションがあるなら、
それらは <filename>sda1</filename>、<filename>sda2</filename>、
<filename>sda3</filename> と名付けられます。
<filename>sdb</filename> ディスクとそのパーティションについても同様です。

</para><para arch="not-s390">

<!--
Note that if you have two SCSI host bus adapters (i.e., controllers),
the order of the drives can get confusing.  The best solution in this
case is to watch the boot messages, assuming you know the drive models
and/or capacities.
-->
2 つの SCSI ホストバスアダプタ (コントローラ) があると、
ドライブの順序が混乱するかもしれないので注意してください。
ドライブのモデルや容量を知っているなら、
ブートメッセージに注目するのが最も良い解決策でしょう。

</para><para arch="x86">

<!--
Linux represents the primary partitions as the drive name, plus the
numbers 1 through 4.  For example, the first primary partition on the
first IDE drive is <filename>/dev/hda1</filename>.  The logical partitions are
numbered starting at 5, so the first logical partition on that same
drive is <filename>/dev/hda5</filename>.  Remember that the extended
partition, that is, the primary partition holding the logical
partitions, is not usable by itself.  This applies to SCSI disks as
well as IDE disks.
-->
Linux は基本パーティションを、ドライブ名に 1 から 4 の数字をつけた名前で
表します。例えば、第 1 IDE ドライブの第 1 基本パーティションは
<filename>/dev/hda1</filename> となります。論理パーティションは、
5 から始まる数字で表され、このドライブの第 1 論理パーティションは
<filename>/dev/hda5</filename> になります。また、拡張パーティションは
論理パーティションを含む基本パーティションのことですが、
これ自体は使用できないことも覚えておいてください。
このことは IDE ディスクにも SCSI ディスクにも当てはまります。

</para><para arch="m68k">

<!--
VMEbus systems using the TEAC FC-1 SCSI floppy drive will see it as normal
SCSI disk. To make identification of the drive simpler the installation
software will create a symbolic link to the appropriate device and name
it <filename>/dev/sfd0</filename>.
-->
TEAC FC-1 SCSI フロッピードライブを利用する VMEbus システムは、そのドライブを
通常の SCSI ディスクと見なします。このドライブを区別して用いるために、
インストーラは、単に適切なデバイスにシンボリックリンクを張り、そのファイル名を
<filename>/dev/sfd0</filename> とします。

</para><para arch="sparc">

<!--
Sun disk partitions allow for 8 separate partitions (or slices). The
third partition is usually (and is preferred to have) the <quote>Whole
Disk</quote> partition. This partition references all of the sectors of the
disk, and is used by the boot loader (either SILO, or Sun's).
-->
Sun のディスクパーティションは 8 つまで別々のパーティション (スライス) を持てます。
通常 3 番目のパーティションは「ディスク全体」のパーティションになります
(こうしておく方がいいです)。このパーティションはディスク上のあらゆるセクタを参照し、
ブートローダ (SILO か Sun のもの) から利用されます。

</para><para arch="s390">

<!--
The partitions on each disk are represented by appending a decimal
number to the disk name: <filename>dasda1</filename> and
<filename>dasda2</filename> represent the first and
second partitions of the first DASD device in your system.
-->
各ディスクのパーティションは、ディスク名に十進数を付け加えることで表します。
例えば <filename>dasda1</filename> と <filename>dasda2</filename> は、
それぞれシステムの第 1 DASD デバイスの第 1、
第 2 パーティションを表します。

</para>
 </sect1>
