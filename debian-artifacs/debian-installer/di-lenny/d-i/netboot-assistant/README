Debian-Installer NetBoot Assistant
------------------------------------

Overview:
    di-netboot-assistant is a tool simplify the preparation of
    files for TFTP Net Booting: Download and Extract the files
    in the right location.
    It also create a "top level" menu, to select among installed
    netboot images (i386, amd64 and ia64 only).

QuickStart:

    1. Install the programs "di-netboot-assistant" and a tftp
       server (like "tftpd-hpa").
       if you netboot ia64 clients, also install elilo.
       if you netboot i386 or amd64 clients, also install syslinux.
    2. Run "di-netboot-assistant install lenny" or similar.
    3. Configure the DHCP server, as explained in [1].  Also read the
       "Architectures Notes" below. and see samples [2].


Architectures Notes:

* i386 / amd64
    DebianInstaller uses Syslinux's PXElinux[3] for both platform.
    Syslinux is a bootloader that provide a menu system to select
    the kernel you want to boot.

    di-netboot-assistant creates a "top level" menu, to select among
    the DebianInstaller netboot menu you previously installed (etch,
    lenny, testing, daily...)

    DHCP configuration:
    Most PC capable of netbooting use DHCP+PXE (as opposed to BOOTP).
    You can either configure the DHCP to tell the PC to start the "top
    level" pxelinux menu, or to directly start a given distribution's
    pxelinux menu.

    Case 1: Configure dhcp3 for a single distribution's menu.

        in /etc/dhcp3/dhcpd.conf, add:

        # Declare pxelinux specific options _names_, at the top of the file.
	# Those are compulsory (nothing to change).
        option space pxelinux;
        option pxelinux.magic      code 208 = string;
        option pxelinux.configfile code 209 = text;
        option pxelinux.pathprefix code 210 = text;
        option pxelinux.reboottime code 211 = unsigned integer 32;

	# Configure PXE options for di-netboot-assistant
	# Those are compulsory (nothing to change).
	site-option-space		"pxelinux";
	option pxelinux.magic		f1:00:74:7e;
	option pxelinux.pathprefix	"/";
	if exists dhcp-parameter-request-list {
		# Always send the PXELINUX options (specified in hexadecimal)
		option dhcp-parameter-request-list = concat(option dhcp-parameter-request-list,d0,d1,d2,d3);
	}

	# Those are compulsory (adjust the values):

        # Configure *your* TFTP server IP address here.
        next-server 192.168.0.10;

        # Provide netboot/pxelinux information, for example:
        filename "debian-installer/lenny/i386/pxelinux.0";
        option pxelinux.configfile "debian-installer/lenny/i386/pxelinux.cfg/default";


    Case 2: Configure dhcp3 for "top level" menu.

        If the  package syslinux  was  installed before  you  "installed" the
        netboot image (with "di-netboot-assistant install ..."), then you can
        use  the generated top  menu. (The  top menu is rebuilt each time you
	install a boot image)

        This is identical to the Case #1 above, except the options "filename"
        and "pxelinux.configfile":

          filename "debian-installer/pxelinux.0";
          option pxelinux.configfile "debian-installer/pxelinux.cfg/default";


* ia64
    IA64 uses  EFI  firmware  instead of  PC's BIOS. DebianInstaller uses the
    bootloader elilo[4] (which supports boot-time menu).

    Case 1: Configure dhcp3 for a single distribution menu.

        This  is  identical  to  the  i386, Case #1 above, except the options
        "filename" and "pxelinux.configfile" should look like:

          filename "debian-installer/lenny/ia64/elilo.efi"
          option pxelinux.configfile "debian-installer/lenny/ia64/elilo.conf"


    Case 2: Configure dhcp3 for "top level" menu.

        If  the  package  elilo  was  installed  before  you  "installed" the
        netboot image (with "di-netboot-assistant install ..."), then you can
        use the generated top menu.

        This is identical to the Case #1 above, except the options "filename"
        and "pxelinux.configfile" should look like:

            filename "debian-installer/elilo.efi"
            option pxelinux.configfile "debian-installer/elilo.conf"


* alpha
* hppa
* sparc
    Those  architectures  netboot using  BOOTP  (some  can use RARP or DHCP).
    They don't have a bootloader that would present a menu at boot-time. This
    means that you must  configure the dhcp to  provide one given kernel. (of
    course,  the  DHCP can  provide  different kernel image, based on the MAC
    or IP address).

    In /etc/dhcpd3/dhcpd.conf you can declare a kernel filename for all hosts

        subnet 192.168.0.0 netmask 255.255.255.0 {
            filename "debian-installer/stable/sparc/boot.img"
        }

    Or You can declare a single host:

        host myhost1 {
            hardware ethernet 00:40:63:d8:02:2c;
            filename "debian-installer/stable/sparc/boot.img"
        }


* arm,armel,
* mips,mipsel
    Those architectures can do netbooting, but aren't supported  by this tool
    at the moment (because there is no ".tar.gz" archive).


* m68k
* s390
    Those architectures can't do netbooting.


MIRRORS
    The file di-sources.list contains the canonical URLs for repositories
    of netboot image.
    You can set the variable MIRROR_REGEXPS to rewrite the URLs to your
    prefered mirror (you can specify multiple space-separated regexp)

    For example, you want to search "://ftp.debian.org/" and replace it with
    "://ftp.fr.debian.org/debian/", then you declare 
    MIRROR_REGEXPS="s=://ftp.debian.org/=://ftp.fr.debian.org/debian/="
    in /etc/di-netboot-assistant/di-netboot-assistant.conf


TFTP SERVER (daemon)
    Any tftp server should work. However the packages tftpd-hpa and atftpd
    are convenient, as they use /var/lib/tftpboot by default.
    For other tftp servers, you will have to adjust TFTP_ROOT=
    in /etc/di-netboot-assistant/di-netboot-assistant.conf
    (There's also a "Using dnsmasq" section below).

DHCP SERVER

    There are two typical scenarios:

                                      +--------+
         ___                          | Switch |  +-------------------+
      __/   \__                       +--------+  |     This host     |
     /         \    +--------------+    |  | |    |    is the only    |
    (  Internet )---| (DSL) Modem  +----+  | +----|    DHCP server    |
     \__     __/    |   or router  |       |      +-------------------+
        \___/       +--------------+       |    +-------------------+
                                           +----| A client computer |
                                                +-------------------+

    First case:  This host is already the DHCP server for the local network.
                 This is the best and recommended configuration. You simply
                 have to add some options to enable netbooting (typically,
                 the "filename" to boot, and the tftp server's address).
                 read [1] and example [2] and architectures specific notes.



                                       +--------+
         ___                           | Switch |
      __/   \__     +---------------+  +--------+
     /         \    |  (DSL) Modem  |    |  | |    +-------------------+
    (  Internet )---|       with    +----+  | +----|     This host     |
     \__     __/    | build-in DHCP |       |      +-------------------+
        \___/       +---------------+       |    +-------------------+
                                            +----| A client computer |
                                                 +-------------------+

    Second case: A DHCP already exists, but you can't/don't want to use it
                 for netbooting. This isn't the recommended configuration,
                 but it was successfully tested for netbooting PXE clients
                 (i386, amd64 and ia64) on network where the existing DHCP
                 server didn't provide netboot informations.
                 !!!! Do not setup rogue dhcp servers in your company !!!!

                 In this situation, you have to divide your DHCP subnet in
                 two ranges of IP address. The first range will still be
                 served by the existing DHCP server.
                 The second part will be served by "this host". It will only
                 reply to DHCP request sent during PXE netboot step. This is
                 possible because client computers (actually, their PXE agent
                 in the NIC's ROM) sends an option like this during PXE boot:
                    vendor-class-id= "PXEClient:Arch:00000:UNDI:002001"
                 Both dhcpd and dnsmasq can be instructed to provide answer
                 depending on such strings.

                 Note: For easy _home_ setup, you can choose not to reduce the
                 range of IP served by the existing DHCP server, and just 
                 declare a *small* range of addresses on *both* DHCP (some
                 addresses that are unlikely to be used on the first one)
                 That's ugly, but it can work ! (as long your existing DHCP
                 don't allocate some of those IP ;-)


Using dnsmasq
    dnsmasq is a simple all-in-one (dns, dhcp and tftp) server. QuickStart :

    Install the package dnsmasq
        apt-get install dnsmasq

    Create tftp directory (YMMV)
	mkdir /var/lib/tftpboot

    Configure /etc/dnsmasq.conf:
        #Simple/Tiny example:

        #Enable built-in tftp server
        enable-tftp
        tftp-root=/var/lib/tftpboot

        dhcp-range=vendor:PXEClient,192.168.0.81,192.168.0.91
        
        #Force the address of the router (or DSL modem)
        #dhcp-option=option:router,192.168.0.1

        dhcp-option-force=208,f1:00:74:7e                       #PXE signature
        dhcp-option-force=210,/                                 #tftp prefix
        dhcp-boot=debian-installer/pxelinux.0
        dhcp-option-force=209,debian-installer/pxelinux.cfg/default

    Install the netboot images
        di-netboot-assistant install lenny


TODO:
    - Allow to specify location of extraction.
    - Allow to be invoked from non-root user (or explain dpkg-statoverride).
    - Allow to specify alternate name for repository (save 'daily' as '$date')
    - support non pxelinux platforms (yaboot powerpc, mips
      sb1-bcm91250a/sb1a-bcm91480b)
    - Implement "clean-cache" action to remove cached files.
    - Implement "remove" action (don't purge DL cache).
    - Default to 64bits on amd64 platform (syslinux DEFAULT64, bug 485656)
    - Use vesamenu rather than menu (for Lenny+1 images).
   

See also:
    http://www.klabs.be/~fpiat/linux/debian/di-netboot-assistant/
    http://wiki.debian.org/DebianInstaller/NetbootAssistant
    and
    http://www.debian.org/releases/stable/debian-installer/


    ----------------------------------------------------------------------
    [1] "Installation Guide", especially the sections
        "4.6 Preparing Files for TFTP Net Booting"
        and "5.1.5 Booting with TFTP"
        http://www.debian.org/releases/stable/installmanual

    [2] Sample dhcp configuration files are located in:
        /usr/share/doc/di-netboot-assistant/examples/dhcpd.conf.simple
        /usr/share/doc/di-netboot-assistant/examples/dhcpd.conf.multiarch
        /usr/share/doc/di-netboot-assistant/examples/dnsmasq.conf.multiarch

    [3] PXElinux homepage.
        http://syslinux.zytor.com/wiki/index.php/PXELINUX

    [4] Elilo bootloader
        http://elilo.sourceforge.net/

    [ ] Preboot Execution Environment (PXE) Specification
        http://www.intel.com/design/archives/wfm/downloads/pxespec.htm

    [ ] Extensible Firmware Interface (EFI)
        http://developer.intel.com/technology/efi/index.htm

 -- Franklin PIAT
