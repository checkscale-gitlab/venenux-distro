# THIS FILE IS GENERATED AUTOMATICALLY FROM THE D-I PO MASTER FILES
# The master files can be found under packages/po/
#
# DO NOT MODIFY THIS FILE DIRECTLY: SUCH CHANGES WILL BE LOST
#
# translation of pa.po to Punjabi
#
# Debian Installer master translation file template
# Don't forget to properly fill-in the header of PO files#
# Debian Installer translators, please read the D-I i18n documentation
# in doc/i18n/i18n.txt#
#
# Amanpreet Singh Alam <amanpreetalam@yahoo.com>, 2005.
# Amanpreet Singh Alam <apreet.alam@gmail.com>, 2006.
# A S Alam <apreet.alam@gmail.com>, 2006, 2007.
# A S Alam <aalam@users.sf.net>, 2007.
# Amanpreet Singh Alam <apreet.alam@gmail.com>, 2008.
# Amanpreet Singh Brar <aalam@users.sf.net>, 2008.
msgid ""
msgstr ""
"Project-Id-Version: pa\n"
"Report-Msgid-Bugs-To: debian-boot@lists.debian.org\n"
"POT-Creation-Date: 2008-01-26 07:32+0000\n"
"PO-Revision-Date: 2008-05-02 22:01+0530\n"
"Last-Translator: Amanpreet Singh Alam <aalam@users.sf.net>\n"
"Language-Team: Punjabi/Panjabi <punjabi-users@lists.sf.net>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"

#. Type: select
#. Choices
#. :sl2:
#: ../cdebconf-udeb.templates:2001
msgid "critical"
msgstr "ਨਾਜ਼ੁਕ"

#. Type: select
#. Choices
#. :sl2:
#: ../cdebconf-udeb.templates:2001
msgid "high"
msgstr "ਵੱਧ"

#. Type: select
#. Choices
#. :sl2:
#: ../cdebconf-udeb.templates:2001
msgid "medium"
msgstr "ਮੱਧਮ"

#. Type: select
#. Choices
#. :sl2:
#: ../cdebconf-udeb.templates:2001
msgid "low"
msgstr "ਘੱਟ"

#. Type: select
#. Description
#. :sl2:
#: ../cdebconf-udeb.templates:2002
msgid "Ignore questions with a priority less than:"
msgstr "ਇਸ ਤੋਂ ਘੱਟ ਦਰਜੇ ਵਾਲੇ ਸਵਾਲਾਂ ਨੂੰ ਅਣਡਿੱਠਾ ਕਰ ਦਿਓ:"

#. Type: select
#. Description
#. :sl2:
#: ../cdebconf-udeb.templates:2002
msgid ""
"Packages that use debconf for configuration prioritize the questions they "
"might ask you. Only questions with a certain priority or higher are actually "
"shown to you; all less important questions are skipped."
msgstr ""
"ਪੈਕੇਜ, ਜੋ ਤੁਹਾਨੂੰ ਪੁੱਛੇ ਜਾਣ ਵਾਲੇ ਸਵਾਲਾਂ ਦੀ ਸੰਰਚਨਾ ਤਰਜੀਹ ਲਈ debconf ਵਰਤਦੇ ਹਨ। ਸਿਰਫ ਜ਼ਿਆਦਾ "
"ਤਰਜੀਹ ਵਾਲੇ ਸਵਾਲ ਤੁਹਾਨੂੰ ਵਿਖਾਏ ਜਾਂਦੇ ਹਨ: ਸਾਰੇ ਘੱਟ ਤਰਜੀਹ ਸਵਾਲ ਛੱਡੇ ਜਾਂਦੇ ਹਨ।"

#. Type: select
#. Description
#. :sl2:
#: ../cdebconf-udeb.templates:2002
msgid ""
"You can select the lowest priority of question you want to see:\n"
" - 'critical' is for items that will probably break the system\n"
"    without user intervention.\n"
" - 'high' is for items that don't have reasonable defaults.\n"
" - 'medium' is for normal items that have reasonable defaults.\n"
" - 'low' is for trivial items that have defaults that will work in\n"
"   the vast majority of cases."
msgstr ""
"ਤੁਸੀਂ ਪ੍ਰਸ਼ਨ ਦੀ ਘੱਟ ਤਰਜੀਹ ਚੁਣ ਸਕਦੇ ਹੋ ਜਿਸ ਨੂੰ ਵੇਖਣਾ ਚਾਹੁੰਦੇ ਹੋ:\n"
" - 'ਨਾਜ਼ੁਕ' ਉਹਨਾਂ ਆਈਟਮਾਂ ਲਈ ਹੈ, ਜੋ ਯੂਜ਼ਰ ਨੂੰ ਬਿਨ ਦੱਸਿਆਂ ਸਿਸਟਮ\n"
"    ਨੂੰ ਸੰਭਾਵੀ ਹੀ ਖਰਾਬ ਕਰਦਾ ਹੈ।\n"
" - 'ਉੱਚ' ਉਹਨਾਂ ਆਈਟਮਾਂ ਲਈ ਹੈ, ਜੋ ਬਿਨਾਂ ਡਿਫਾਲਟ ਕਾਰਨ ਹੁੰਦੀਆਂ ਹਨ।\n"
" - 'ਮੱਧਮ' ਆਮ ਆਈਟਮਾਂ ਲਈ ਹੈ, ਜੋ ਕਾਰਨ ਵਾਲਾ ਡਿਫਾਲਟ ਹੁੰਦਾ ਹੈ।\n"
" - 'ਘੱਟ' ਮਾਮੂਲੀ ਆਈਟਮਾਂ ਲਈ ਹੈ, ਜੋ ਡਿਫਾਲਟ ਹੁੰਦਾ ਹੈ ਜਿਸ ਨਾਲ ਬਹੁਤੇ ਮੁੱਦਿਆਂ\n"
"    ਵਿੱਚ ਕੰਮ ਕਰੇਗੀ।"

#. Type: select
#. Description
#. :sl2:
#: ../cdebconf-udeb.templates:2002
msgid ""
"For example, this question is of medium priority, and if your priority were "
"already 'high' or 'critical', you wouldn't see this question."
msgstr ""
"ਉਦਾਹਰਨ ਵਜੋਂ, ਇਹ ਪ੍ਰਸ਼ਨ ਮੱਧਮ ਤਰਜੀਹ ਵਾਲਾ ਹੈ, ਅਤੇ ਜੇ ਤੁਹਾਡੀ ਤਰਜੀਹ ਪਹਿਲਾਂ ਹੀ 'ਉਚ' ਜਾਂ "
"'ਨਾਜ਼ੁਕ' ਹੈ, ਤੁਸੀਂ ਇਹ ਪ੍ਰਸ਼ਨ ਵੇਖ ਨਹੀਂ ਸਕਦੇ।"

#. Type: text
#. Description
#. :sl2:
#: ../cdebconf-priority.templates:1001
msgid "Change debconf priority"
msgstr "debconf ਦਰਜਾ ਬਦਲੋ"

#. Type: text
#. Description
#. Translators, this text will appear on a button, so KEEP IT SHORT
#. :sl1:
#: ../cdebconf-newt-udeb.templates:1001 ../cdebconf-gtk-udeb.templates:1001
msgid "Continue"
msgstr "ਜਾਰੀ ਰੱਖੋ"

#. Type: text
#. Description
#. Translators, this text will appear on a button, so KEEP IT SHORT
#. :sl1:
#: ../cdebconf-newt-udeb.templates:2001 ../cdebconf-gtk-udeb.templates:2001
msgid "Go Back"
msgstr "ਪਿੱਛੇ ਜਾਓ"

#. Type: text
#. Description
#. Translators, this text will appear on a button, so KEEP IT SHORT
#. :sl1:
#. Type: text
#. Description
#. :sl4:
#: ../cdebconf-newt-udeb.templates:3001 ../cdebconf-gtk-udeb.templates:3001
#: ../cdebconf-slang-udeb.templates:1001 ../cdebconf-text-udeb.templates:6001
msgid "Yes"
msgstr "ਹਾਂ"

#. Type: text
#. Description
#. Translators, this text will appear on a button, so KEEP IT SHORT
#. :sl1:
#. Type: text
#. Description
#. :sl4:
#: ../cdebconf-newt-udeb.templates:4001 ../cdebconf-gtk-udeb.templates:4001
#: ../cdebconf-slang-udeb.templates:2001 ../cdebconf-text-udeb.templates:7001
msgid "No"
msgstr "ਨਹੀਂ"

#. Type: text
#. Description
#. Translators, this text will appear on a button, so KEEP IT SHORT
#. :sl1:
#: ../cdebconf-newt-udeb.templates:5001
msgid "Cancel"
msgstr "ਰੱਦ ਕਰੋ"

#. Type: text
#. Description
#. Help line displayed at the bottom of the cdebconf newt interface.
#. Translators: must fit within 80 characters.
#. :sl1:
#: ../cdebconf-newt-udeb.templates:6001
msgid "<Tab> moves between items; <Space> selects; <Enter> activates buttons"
msgstr "<Tab> ਇਕਾਈਆਂ ਵਿੱਚ ਲਿਜਾਂਦਾ ਹੈ; <Space> ਚੁਣਦਾ ਹੈ; <Enter> ਬਟਨ ਸਰਗਰਮ ਕਰਦਾ ਹੈ"

#. Type: text
#. Description
#. TRANSLATORS: This should be "translated" to "RTL" or "LTR" depending of
#. default text direction of your language
#. LTR: Left to Right (Latin, Cyrillic, CJK, Indic...)
#. RTL: Right To Left (Arabic, Hebrew, Persian...)
#. DO NOT USE ANYTHING ELSE
#. :sl1:
#: ../cdebconf-gtk-udeb.templates:5001
msgid "LTR"
msgstr "LTR"

#. Type: text
#. Description
#. Translators, this text will appear on a button, so KEEP IT SHORT
#. This button will allow users to virtually "shoot a picture"
#. of the screen
#. :sl1:
#: ../cdebconf-gtk-udeb.templates:6001
msgid "Screenshot"
msgstr "ਸਕਰੀਨ-ਸ਼ਾਂਟ"

#. Type: text
#. Description
#. Text that will appear in a dialog box mentioning which file
#. the screenshot has been saved to. "%s" is a file name here
#. :sl1:
#: ../cdebconf-gtk-udeb.templates:7001
#, no-c-format
msgid "Screenshot saved as %s"
msgstr "ਸਕਰੀਨ-ਸ਼ਾਟ %s ਤੌਰ ਤੇ ਸੰਭਾਲਿਆ ਗਿਆ"

#. Type: text
#. Description
#. :sl4:
#: ../cdebconf-text-udeb.templates:1001
#, no-c-format
msgid "!! ERROR: %s"
msgstr "!! ਗਲਤੀ: %s"

#. Type: text
#. Description
#. :sl4:
#: ../cdebconf-text-udeb.templates:2001
msgid "KEYSTROKES:"
msgstr "ਸਵਿੱਚ ਸਟੋਕਰ:"

#. Type: text
#. Description
#. :sl4:
#: ../cdebconf-text-udeb.templates:3001
msgid "Display this help message"
msgstr "ਇਹ ਮੱਦਦ ਸੁਨੇਹਾ ਵੇਖੋ"

#. Type: text
#. Description
#. :sl4:
#: ../cdebconf-text-udeb.templates:4001
msgid "Go back to previous question"
msgstr "ਪਿੱਛੇ ਸਵਾਲ ਤੇ ਜਾਓ"

#. Type: text
#. Description
#. :sl4:
#: ../cdebconf-text-udeb.templates:5001
msgid "Select an empty entry"
msgstr "ਖਾਲੀ ਐਂਟਰੀ ਚੁਣੋ"

#. Type: text
#. Description
#. :sl4:
#: ../cdebconf-text-udeb.templates:8001
#, no-c-format
msgid "Prompt: '%c' for help, default=%d> "
msgstr "ਪਰਾਉਟ: '%c' ਮੱਦਦ ਲਈ, ਡਿਫਾਲਟ=%d>"

#. Type: text
#. Description
#. :sl4:
#: ../cdebconf-text-udeb.templates:9001
#, no-c-format
msgid "Prompt: '%c' for help> "
msgstr "ਪਰਾਉਟ: ਮੱਦਦ ਲਈ '%c'>"

#. Type: text
#. Description
#. :sl4:
#: ../cdebconf-text-udeb.templates:10001
#, no-c-format
msgid "Prompt: '%c' for help, default=%s> "
msgstr "ਪਰਾਉਟ: '%c' ਮੱਦਦ ਲਈ, default=%s>"

#. Type: text
#. Description
#. :sl4:
#: ../cdebconf-text-udeb.templates:11001
msgid "[Press enter to continue]"
msgstr "[ਜਾਰੀ ਰੱਖਣ ਲਈ enter ਦਬਾਓ]"
