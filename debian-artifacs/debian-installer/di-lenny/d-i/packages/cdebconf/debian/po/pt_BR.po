# THIS FILE IS GENERATED AUTOMATICALLY FROM THE D-I PO MASTER FILES
# The master files can be found under packages/po/
#
# DO NOT MODIFY THIS FILE DIRECTLY: SUCH CHANGES WILL BE LOST
#
# This file is distributed under the same license as debian-installer.
#
msgid ""
msgstr ""
"Project-Id-Version: debian-installer\n"
"Report-Msgid-Bugs-To: debian-boot@lists.debian.org\n"
"POT-Creation-Date: 2008-01-26 07:32+0000\n"
"PO-Revision-Date: 2008-08-07 19:53-0300\n"
"Last-Translator: Felipe Augusto van de Wiel (faw) <faw@debian.org>\n"
"Language-Team: Brazilian Portuguese <debian-l10n-portuguese@lists.debian."
"org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. Type: select
#. Choices
#. :sl2:
#: ../cdebconf-udeb.templates:2001
msgid "critical"
msgstr "crítica"

#. Type: select
#. Choices
#. :sl2:
#: ../cdebconf-udeb.templates:2001
msgid "high"
msgstr "alta"

#. Type: select
#. Choices
#. :sl2:
#: ../cdebconf-udeb.templates:2001
msgid "medium"
msgstr "média"

#. Type: select
#. Choices
#. :sl2:
#: ../cdebconf-udeb.templates:2001
msgid "low"
msgstr "baixa"

#. Type: select
#. Description
#. :sl2:
#: ../cdebconf-udeb.templates:2002
msgid "Ignore questions with a priority less than:"
msgstr "Ignorar perguntas com uma prioridade menor que:"

#. Type: select
#. Description
#. :sl2:
#: ../cdebconf-udeb.templates:2002
msgid ""
"Packages that use debconf for configuration prioritize the questions they "
"might ask you. Only questions with a certain priority or higher are actually "
"shown to you; all less important questions are skipped."
msgstr ""
"Pacotes que usam o debconf para sua configuração priorizam as perguntas que "
"lhe fazem. Somente perguntas com uma certa prioridade ou superior serão "
"exibidas; todas as outras perguntas de menor prioridade não serão exibidas."

#. Type: select
#. Description
#. :sl2:
#: ../cdebconf-udeb.templates:2002
msgid ""
"You can select the lowest priority of question you want to see:\n"
" - 'critical' is for items that will probably break the system\n"
"    without user intervention.\n"
" - 'high' is for items that don't have reasonable defaults.\n"
" - 'medium' is for normal items that have reasonable defaults.\n"
" - 'low' is for trivial items that have defaults that will work in\n"
"   the vast majority of cases."
msgstr ""
"Você pode selecionar a menor prioridade das perguntas que você deseja que "
"sejam exibidas:\n"
" - 'crítica' é para itens que provavelmente causariam problemas em\n"
"    seu sistema sem sua intervenção.\n"
" - 'alta' é para itens que não com respostas padrão razoáveis.\n"
" - 'média' é para itens comuns que com respostas padrão razoáveis.\n"
" - 'baixa' é para itens triviais que com respostas padrão que\n"
"    funcionarão para a grande maioria dos casos."

#. Type: select
#. Description
#. :sl2:
#: ../cdebconf-udeb.templates:2002
msgid ""
"For example, this question is of medium priority, and if your priority were "
"already 'high' or 'critical', you wouldn't see this question."
msgstr ""
"Por exemplo, esta pergunta é de prioridade média e caso sua configuração de "
"prioridades já estivesse definida para 'alta' ou 'crítica'  você não a "
"estaria vendo."

#. Type: text
#. Description
#. :sl2:
#: ../cdebconf-priority.templates:1001
msgid "Change debconf priority"
msgstr "Mudar prioridade debconf"

#. Type: text
#. Description
#. Translators, this text will appear on a button, so KEEP IT SHORT
#. :sl1:
#: ../cdebconf-newt-udeb.templates:1001 ../cdebconf-gtk-udeb.templates:1001
msgid "Continue"
msgstr "Continuar"

#. Type: text
#. Description
#. Translators, this text will appear on a button, so KEEP IT SHORT
#. :sl1:
#: ../cdebconf-newt-udeb.templates:2001 ../cdebconf-gtk-udeb.templates:2001
msgid "Go Back"
msgstr "Voltar"

#. Type: text
#. Description
#. Translators, this text will appear on a button, so KEEP IT SHORT
#. :sl1:
#. Type: text
#. Description
#. :sl4:
#: ../cdebconf-newt-udeb.templates:3001 ../cdebconf-gtk-udeb.templates:3001
#: ../cdebconf-slang-udeb.templates:1001 ../cdebconf-text-udeb.templates:6001
msgid "Yes"
msgstr "Sim"

#. Type: text
#. Description
#. Translators, this text will appear on a button, so KEEP IT SHORT
#. :sl1:
#. Type: text
#. Description
#. :sl4:
#: ../cdebconf-newt-udeb.templates:4001 ../cdebconf-gtk-udeb.templates:4001
#: ../cdebconf-slang-udeb.templates:2001 ../cdebconf-text-udeb.templates:7001
msgid "No"
msgstr "Não"

#. Type: text
#. Description
#. Translators, this text will appear on a button, so KEEP IT SHORT
#. :sl1:
#: ../cdebconf-newt-udeb.templates:5001
msgid "Cancel"
msgstr "Cancelar"

#. Type: text
#. Description
#. Help line displayed at the bottom of the cdebconf newt interface.
#. Translators: must fit within 80 characters.
#. :sl1:
#: ../cdebconf-newt-udeb.templates:6001
msgid "<Tab> moves between items; <Space> selects; <Enter> activates buttons"
msgstr "<Tab> move entre itens: <Espaço> seleciona; <Enter> ativa botões"

#. Type: text
#. Description
#. TRANSLATORS: This should be "translated" to "RTL" or "LTR" depending of
#. default text direction of your language
#. LTR: Left to Right (Latin, Cyrillic, CJK, Indic...)
#. RTL: Right To Left (Arabic, Hebrew, Persian...)
#. DO NOT USE ANYTHING ELSE
#. :sl1:
#: ../cdebconf-gtk-udeb.templates:5001
msgid "LTR"
msgstr "LTR"

#. Type: text
#. Description
#. Translators, this text will appear on a button, so KEEP IT SHORT
#. This button will allow users to virtually "shoot a picture"
#. of the screen
#. :sl1:
#: ../cdebconf-gtk-udeb.templates:6001
msgid "Screenshot"
msgstr "Capturar tela"

#. Type: text
#. Description
#. Text that will appear in a dialog box mentioning which file
#. the screenshot has been saved to. "%s" is a file name here
#. :sl1:
#: ../cdebconf-gtk-udeb.templates:7001
#, no-c-format
msgid "Screenshot saved as %s"
msgstr "Captura de tela salva como %s"

#. Type: text
#. Description
#. :sl4:
#: ../cdebconf-text-udeb.templates:1001
#, no-c-format
msgid "!! ERROR: %s"
msgstr "!! ERRO: %s"

#. Type: text
#. Description
#. :sl4:
#: ../cdebconf-text-udeb.templates:2001
msgid "KEYSTROKES:"
msgstr "TECLAS DE ATALHO:"

#. Type: text
#. Description
#. :sl4:
#: ../cdebconf-text-udeb.templates:3001
msgid "Display this help message"
msgstr "Exibir esta mensagem de ajuda"

#. Type: text
#. Description
#. :sl4:
#: ../cdebconf-text-udeb.templates:4001
msgid "Go back to previous question"
msgstr "Voltar a pergunta anterior"

#. Type: text
#. Description
#. :sl4:
#: ../cdebconf-text-udeb.templates:5001
msgid "Select an empty entry"
msgstr "Selecione uma entrada vazia"

#. Type: text
#. Description
#. :sl4:
#: ../cdebconf-text-udeb.templates:8001
#, no-c-format
msgid "Prompt: '%c' for help, default=%d> "
msgstr "Aviso: '%c' para ajuda, padrão=%d> "

#. Type: text
#. Description
#. :sl4:
#: ../cdebconf-text-udeb.templates:9001
#, no-c-format
msgid "Prompt: '%c' for help> "
msgstr "Aviso: '%c' para ajuda> "

#. Type: text
#. Description
#. :sl4:
#: ../cdebconf-text-udeb.templates:10001
#, no-c-format
msgid "Prompt: '%c' for help, default=%s> "
msgstr "Aviso: '%c' para ajuda, padrão=%s> "

#. Type: text
#. Description
#. :sl4:
#: ../cdebconf-text-udeb.templates:11001
msgid "[Press enter to continue]"
msgstr "[Pressione enter para continuar]"
