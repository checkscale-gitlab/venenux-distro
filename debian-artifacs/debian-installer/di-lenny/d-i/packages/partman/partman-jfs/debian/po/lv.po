# THIS FILE IS GENERATED AUTOMATICALLY FROM THE D-I PO MASTER FILES
# The master files can be found under packages/po/
#
# DO NOT MODIFY THIS FILE DIRECTLY: SUCH CHANGES WILL BE LOST
#
# translation of debian-installer_packages_po_sublevel1_lv.po to Latvian
# Aigars Mahinovs <aigarius@debian.org>, 2006.
# Viesturs Zarins <viesturs.zarins@mii.lu.lv>, 2008.
# Peteris Krisjanis <pecisk@gmail.com>, 2008.
# Latvian messages for debian-installer.
# Copyright (C) 2003 Software in the Public Interest, Inc.
# This file is distributed under the same license as debian-installer.
msgid ""
msgstr ""
"Project-Id-Version: debian-installer_packages_po_sublevel1_lv\n"
"Report-Msgid-Bugs-To: debian-boot@lists.debian.org\n"
"POT-Creation-Date: 2008-03-26 13:16+0000\n"
"PO-Revision-Date: 2008-09-19 00:00+0300\n"
"Last-Translator: Peteris Krisjanis <pecisk@gmail.com>\n"
"Language-Team: Latvian <locale@laka.lv>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms:  nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n != 0 ? 1 : "
"2);\n"

#. Type: text
#. Description
#. :sl2:
#: ../partman-jfs.templates:1001
msgid "Checking the jfs file system in partition #${PARTITION} of ${DEVICE}..."
msgstr "Pārbauda jfs failu sistēmu ${DEVICE} #${PARTITION} sadaļā..."

#. Type: boolean
#. Description
#. :sl2:
#: ../partman-jfs.templates:2001
msgid "Go back to the menu and correct errors?"
msgstr "Atgriezties izvēlnē un izlabot kļūdas?"

#. Type: boolean
#. Description
#. :sl2:
#: ../partman-jfs.templates:2001
msgid ""
"The test of the file system with type jfs in partition #${PARTITION} of "
"${DEVICE} found uncorrected errors."
msgstr ""
"${DEVICE} #${PARTITION} sadaļas jfs failu sistēmas pārbaude atklāja "
"neizlabotas kļūdas."

#. Type: boolean
#. Description
#. :sl2:
#: ../partman-jfs.templates:2001
msgid ""
"If you do not go back to the partitioning menu and correct these errors, the "
"partition will not be used at all."
msgstr ""
"Ja jūs neatgriezīsieties uz disku dalīšanas izvēlni un neizlabosiet šīs "
"kļūdas, šī sadaļa turpmāk netiks izmantota."

#. Type: error
#. Description
#. :sl2:
#: ../partman-jfs.templates:3001
msgid "Failed to create a file system"
msgstr "Kļūda, veidojot failu sistēmu"

#. Type: error
#. Description
#. :sl2:
#: ../partman-jfs.templates:3001
msgid ""
"The jfs file system creation in partition #${PARTITION} of ${DEVICE} failed."
msgstr "${DEVICE} #${PARTITION} sadaļas jfs failu sistēmas izveide neizdevās."

#. Type: boolean
#. Description
#. :sl2:
#: ../partman-jfs.templates:4001
msgid "Do you want to return to the partitioning menu?"
msgstr "Vai vēlaties atgriezties disku dalīšanas izvēlnē?"

#. Type: boolean
#. Description
#. :sl2:
#: ../partman-jfs.templates:4001
msgid ""
"No mount point is assigned for the jfs file system in partition #"
"${PARTITION} of ${DEVICE}."
msgstr ""
"${DEVICE} #${PARTITION} sadaļas jfs failu sistēmai nav norādīts montēšanas "
"punkts."

#. Type: boolean
#. Description
#. :sl2:
#: ../partman-jfs.templates:4001
msgid ""
"If you do not go back to the partitioning menu and assign a mount point from "
"there, this partition will not be used at all."
msgstr ""
"Ja neatgriezīsieties uz disku dalīšanas izvēlni un nenorādīsiet montēšanas "
"punktu, šī sadaļa vispār netiks izmantota."

#. Type: text
#. Description
#. :sl2:
#. This is an item in the menu "Partition settings"
#: ../partman-jfs.templates:5001
msgid "Mount point:"
msgstr "Montēšanas punkts:"

#. Type: text
#. Description
#. :sl1:
#. File system name (untranslatable in many languages)
#. Type: text
#. Description
#. Short file system name (untranslatable in many languages)
#. :sl1:
#: ../partman-jfs.templates:6001 ../partman-jfs.templates:8001
msgid "jfs"
msgstr "jfs"

#. Type: text
#. Description
#. File system name
#. :sl2:
#: ../partman-jfs.templates:7001
msgid "JFS journaling file system"
msgstr "JFS žurnalējošā failu sistēma"

#. Type: boolean
#. Description
#. :sl2:
#: ../partman-jfs.templates:9001
msgid "Use unrecommended JFS root file system?"
msgstr "Lietot nerekomendēto JFS saknes failu sistēmu?"

#. Type: boolean
#. Description
#. :sl2:
#: ../partman-jfs.templates:9001
msgid ""
"Your root file system is a JFS file system. This can cause problems with the "
"boot loader used by default by this installer."
msgstr ""
"Jūsu saknes failu sistēma ir JFS failu sistēma. Tas var radīt problēmas ar "
"instalatora izmantoto noklusēto palaidēju."

#. Type: boolean
#. Description
#. :sl2:
#: ../partman-jfs.templates:9001
msgid ""
"You should use a small /boot partition with another file system, such as "
"ext3."
msgstr ""
"Jums vajadzētu lietot mazu kādas citas failu sistēmas (piemēram, ext3) /boot "
"sadaļu."

#. Type: boolean
#. Description
#. :sl2:
#: ../partman-jfs.templates:10001
msgid "Use unrecommended JFS /boot file system?"
msgstr "Lietot nerekomendēto JFS /boot failu sistēmu?"

#. Type: boolean
#. Description
#. :sl2:
#: ../partman-jfs.templates:10001
msgid ""
"You have mounted a JFS file system as /boot. This is likely to cause "
"problems with the boot loader used by default by this installer."
msgstr ""
"Jūs kā /boot piemontējāt JFS failu sistēmu. Tas visdrīzāk radīs problēmas ar "
"šī instalatora izmantoto noklusēto palaidēju."

#. Type: boolean
#. Description
#. :sl2:
#: ../partman-jfs.templates:10001
msgid ""
"You should use another file system, such as ext3, for the /boot partition."
msgstr ""
"Jums /boot sadaļai vajadzētu izmantot citu failu sistēmu, piemēram, ext3."
