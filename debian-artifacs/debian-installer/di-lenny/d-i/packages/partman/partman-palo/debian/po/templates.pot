# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: debian-boot@lists.debian.org\n"
"POT-Creation-Date: 2008-01-26 07:32+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: 8bit\n"

#. Type: text
#. Description
#. :sl5:
#. Type: text
#. Description
#. :sl5:
#: ../partman-palo.templates:1001 ../partman-palo.templates:4001
msgid "PALO boot partition"
msgstr ""

#. Type: boolean
#. Description
#. :sl5:
#. Type: boolean
#. Description
#. :sl5:
#: ../partman-palo.templates:2001 ../partman-palo.templates:3001
msgid "Go back to the menu and resume partitioning?"
msgstr ""

#. Type: boolean
#. Description
#. :sl5:
#: ../partman-palo.templates:2001
msgid "No PALO partition was found."
msgstr ""

#. Type: boolean
#. Description
#. :sl5:
#: ../partman-palo.templates:3001
msgid "The PALO partition must be in the first 2GB."
msgstr ""

#. Type: text
#. Description
#. :sl5:
#. short variant of 'PALO boot partition'
#. Up to 10 character positions
#: ../partman-palo.templates:5001
msgid "boot"
msgstr ""
