# THIS FILE IS GENERATED AUTOMATICALLY FROM THE D-I PO MASTER FILES
# The master files can be found under packages/po/
#
# DO NOT MODIFY THIS FILE DIRECTLY: SUCH CHANGES WILL BE LOST
#
# translation of id.po to Bahasa Indonesia
# Indonesian messages for debian-installer.
#
#
# Copyright (C) 2003 Software in the Public Interest, Inc.
# This file is distributed under the same license as debian-installer.
# Translators:
#
# Debian Indonesian L10N Team <debian-l10n-id@gurame.fisika.ui.ac.id>, 2004.
# * Parlin Imanuel Toh (parlin_i@yahoo.com), 2004-2005.
# * I Gede Wijaya S (gwijayas@yahoo.com), 2004.
# * Arief S F (arief@gurame.fisika.ui.ac.id), 2004-2007.
# * Setyo Nugroho (setyo@gmx.net), 2004.
# Arief S Fitrianto <arief@gurame.fisika.ui.ac.id>, 2008.
msgid ""
msgstr ""
"Project-Id-Version: debian-installer (level1)\n"
"Report-Msgid-Bugs-To: debian-boot@lists.debian.org\n"
"POT-Creation-Date: 2008-03-26 13:16+0000\n"
"PO-Revision-Date: 2008-03-08 13:09+0700\n"
"Last-Translator: Arief S Fitrianto <arief@gurame.fisika.ui.ac.id>\n"
"Language-Team: Bahasa Indonesia <debian-l10n-id@gurame.fisika.ui.ac.id>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0\n"

#. Type: text
#. Description
#. :sl4:
#: ../partman-ext2r0.templates:1001
msgid ""
"Checking the ext2 (revision 0) file system in partition #${PARTITION} of "
"${DEVICE}..."
msgstr ""
"Memeriksa sistem berkas ext2 (revisi 0) pada partisi no. ${PARTITION} dari "
"${DEVICE}..."

#. Type: boolean
#. Description
#. :sl4:
#: ../partman-ext2r0.templates:2001
msgid "Go back to the menu and correct errors?"
msgstr "Kembali ke menu dan memperbaiki kesalahan-kesalahan tersebut?"

#. Type: boolean
#. Description
#. :sl4:
#: ../partman-ext2r0.templates:2001
msgid ""
"The test of the file system with type ext2 (revision 0) in partition #"
"${PARTITION} of ${DEVICE} found uncorrected errors."
msgstr ""
"Pengujian sistem berkas ext2 (revisi 0) pada partisi no. ${PARTITION} dari "
"${DEVICE} menemukan adanya kesalahan-kesalahan yang belum diperbaiki."

#. Type: boolean
#. Description
#. :sl4:
#: ../partman-ext2r0.templates:2001
msgid ""
"If you do not go back to the partitioning menu and correct these errors, the "
"partition will not be used at all."
msgstr ""
"Jika Anda tidak kembali ke menu partisi dan memperbaiki kesalahan-kesalahan "
"ini, partisi ini tidak akan digunakan sama sekali."

#. Type: error
#. Description
#. :sl4:
#: ../partman-ext2r0.templates:3001
msgid "Failed to create a file system"
msgstr "Gagal membuat sistem berkas"

#. Type: error
#. Description
#. :sl4:
#: ../partman-ext2r0.templates:3001
msgid ""
"The ext2 (revision 0) file system creation in partition #${PARTITION} of "
"${DEVICE} failed."
msgstr ""
"Pembuatan sistem berkas ext2 (revisi 0) pada partisi no. ${PARTITION} dari "
"${DEVICE} gagal."

#. Type: boolean
#. Description
#. :sl4:
#: ../partman-ext2r0.templates:4001
msgid "Do you want to return to the partitioning menu?"
msgstr "Apakah Anda ingin kembali ke menu partisi?"

#. Type: boolean
#. Description
#. :sl4:
#: ../partman-ext2r0.templates:4001
msgid ""
"No mount point is assigned for the ext2 (revision 0) file system in "
"partition #${PARTITION} of ${DEVICE}."
msgstr ""
"Belum ditentukan, titik kait untuk sistem berkas ext2 (revisi 0) pada "
"partisi no. ${PARTITION} dari ${DEVICE}."

#. Type: boolean
#. Description
#. :sl4:
#: ../partman-ext2r0.templates:4001
msgid ""
"If you do not go back to the partitioning menu and assign a mount point from "
"there, this partition will not be used at all."
msgstr ""
"Jika Anda tidak kembali ke menu partisi untuk menentukan titik kait pada "
"menu tersebut, partisi ini tidak akan digunakan sama sekali."

#. Type: select
#. Choices
#. :sl4:
#. what's to be entered is a mount point
#: ../partman-ext2r0.templates:5001
msgid "Enter manually"
msgstr "Masukkan secara manual"

#. Type: select
#. Choices
#. :sl4:
#. "it" is a partition
#: ../partman-ext2r0.templates:5001
msgid "Do not mount it"
msgstr "Jangan kaitkan"

#. Type: select
#. Description
#. Type: string
#. Description
#. :sl4:
#: ../partman-ext2r0.templates:5002 ../partman-ext2r0.templates:6001
msgid "Mount point for this partition:"
msgstr "Titik kait untuk partisi ini:"

#. Type: error
#. Description
#. :sl4:
#: ../partman-ext2r0.templates:7001
msgid "Invalid mount point"
msgstr "Titik kait tidak sah"

#. Type: error
#. Description
#. :sl4:
#: ../partman-ext2r0.templates:7001
msgid "The mount point you entered is invalid."
msgstr "Titik kait yang Anda tentukan tidak sah."

#. Type: error
#. Description
#. :sl4:
#: ../partman-ext2r0.templates:7001
msgid "Mount points must start with \"/\". They cannot contain spaces."
msgstr "Titik kait harus diawali dengan \"/\" dan tidak boleh berisi spasi."

#. Type: text
#. Description
#. :sl4:
#. File system name (untranslatable in many languages)
#. Type: text
#. Description
#. :sl4:
#. Short file system name (untranslatable in many languages)
#: ../partman-ext2r0.templates:8001 ../partman-ext2r0.templates:10001
msgid "ext2r0"
msgstr "ext2r0"

#. Type: text
#. Description
#. :sl4:
#. File system name
#: ../partman-ext2r0.templates:9001
msgid "old Ext2 (revision 0) file system"
msgstr "sistem berkas ext2 (revisi 0) lama"

#. Type: boolean
#. Description
#. :sl4:
#. Type: boolean
#. Description
#. :sl4:
#. Type: boolean
#. Description
#. :sl4:
#: ../partman-ext2r0.templates:11001 ../partman-ext2r0.templates:12001
#: ../partman-ext2r0.templates:13001
msgid "Go back to the menu and correct this problem?"
msgstr ""
"Kembali ke menu sebelumnya dan memperbaiki kesalahan-kesalahan tersebut?"

#. Type: boolean
#. Description
#. :sl4:
#: ../partman-ext2r0.templates:11001
msgid ""
"Your boot partition has not been configured with the old ext2 (revision 0) "
"file system.  This is needed by your machine in order to boot.  Please go "
"back and use the old ext2 (revision 0) file system."
msgstr ""
"Partisi boot Anda belum dikonfigurasi untuk menggunakan sistem berkas ext2 "
"(revisi 0) lama. Hal ini diperlukan Anda agar dapat menyalakan komputer "
"Anda. Silakan kembali ke menu sebelumnya dan gunakan sistem berkas ext2 "
"(revisi 0) lama."

#. Type: boolean
#. Description
#. :sl4:
#. Type: boolean
#. Description
#. :sl4:
#. Type: boolean
#. Description
#. :sl4:
#: ../partman-ext2r0.templates:11001 ../partman-ext2r0.templates:12001
#: ../partman-ext2r0.templates:13001
msgid ""
"If you do not go back to the partitioning menu and correct this error, the "
"partition will be used as is.  This means that you may not be able to boot "
"from your hard disk."
msgstr ""
"Jika Anda tidak kembali ke menu partisi dan memperbaiki kesalahan-kesalahan "
"ini, partisi ini akan digunakan menurut kondisi sebelumnya. Hal ini berarti "
"Anda mungkin tidak dapat menyalakan komputer lewat hard disk."

#. Type: boolean
#. Description
#. :sl4:
#: ../partman-ext2r0.templates:12001
msgid ""
"Your boot partition is not located on the first primary partition of your "
"hard disk.  This is needed by your machine in order to boot.  Please go back "
"and use your first primary partition as a boot partition."
msgstr ""
"Partisi boot Anda tidak terletak pada partisi utama (primer) pertama hard "
"disk. Hal ini diperlukan agar dapat menyalakan komputer Anda. Silakan "
"kembali ke menu sebelumnya dan gunakan partisi utama pertama hard disk "
"sebagai partisi boot."

#. Type: boolean
#. Description
#. :sl4:
#: ../partman-ext2r0.templates:13001
msgid ""
"Your root partition is not a primary partition of your hard disk.  This is "
"needed by your machine in order to boot.  Please go back and use a primary "
"partition for your root partition."
msgstr ""
"Partisi root Anda bukan sebagai partisi utama (primer) hard disk. Hal ini "
"diperlukan agar komputer Anda dapat menyala. Silakan kembali ke menu "
"sebelumnya dan gunakan partisi utama bagi partisi root."
