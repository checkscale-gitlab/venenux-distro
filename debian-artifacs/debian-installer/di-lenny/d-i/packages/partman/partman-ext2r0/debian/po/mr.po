# THIS FILE IS GENERATED AUTOMATICALLY FROM THE D-I PO MASTER FILES
# The master files can be found under packages/po/
#
# DO NOT MODIFY THIS FILE DIRECTLY: SUCH CHANGES WILL BE LOST
#
#
#
# Debian Installer master translation file template
# Don't forget to properly fill-in the header of PO files
#
# Debian Installer translators, please read the D-I i18n documentation
# in doc/i18n/i18n.txt
#
#
msgid ""
msgstr ""
"Project-Id-Version: debian-installer\n"
"Report-Msgid-Bugs-To: debian-boot@lists.debian.org\n"
"POT-Creation-Date: 2008-03-26 13:16+0000\n"
"PO-Revision-Date: 2008-07-04 23:50+0530\n"
"Last-Translator: Sampada <sampadanakhare@gmail.com>\n"
"Language-Team: Marathi, janabhaaratii, C-DAC, Mumbai, India "
"<janabhaaratii@cdacmumbai.in>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. Type: text
#. Description
#. :sl4:
#: ../partman-ext2r0.templates:1001
msgid ""
"Checking the ext2 (revision 0) file system in partition #${PARTITION} of "
"${DEVICE}..."
msgstr ""
"${DEVICE} च्या #${PARTITION} या विभाजनावरील ईएक्सटी३ (सुधारणा ०) ही फाइल प्रणाली "
"तपासत आहे..."

#. Type: boolean
#. Description
#. :sl4:
#: ../partman-ext2r0.templates:2001
msgid "Go back to the menu and correct errors?"
msgstr "मेन्यूकडे परत जाऊन त्रूटी सुधारायच्या?"

#. Type: boolean
#. Description
#. :sl4:
#: ../partman-ext2r0.templates:2001
msgid ""
"The test of the file system with type ext2 (revision 0) in partition #"
"${PARTITION} of ${DEVICE} found uncorrected errors."
msgstr ""
"${DEVICE} च्या #${PARTITION} या विभाजनावरील ईएक्सटी२ (सुधारणा ०) या प्रकारासाठी "
"केलेल्या फाइल प्रणालीच्या चाचणीत असुधारीत त्रूटी आढळल्या."

#. Type: boolean
#. Description
#. :sl4:
#: ../partman-ext2r0.templates:2001
msgid ""
"If you do not go back to the partitioning menu and correct these errors, the "
"partition will not be used at all."
msgstr ""
"आपण विभाजनाच्या मेन्यूकडे परत जाऊन या त्रूटी सुधारल्या नाहीत, तर हे विभाजन वापरलेच "
"जाणार नाही."

#. Type: error
#. Description
#. :sl4:
#: ../partman-ext2r0.templates:3001
msgid "Failed to create a file system"
msgstr "फाईल प्रणाली बनवता आली नाही"

#. Type: error
#. Description
#. :sl4:
#: ../partman-ext2r0.templates:3001
msgid ""
"The ext2 (revision 0) file system creation in partition #${PARTITION} of "
"${DEVICE} failed."
msgstr ""
"${DEVICE} च्या #${PARTITION} या विभाजनावर ईएक्सटी२ (सुधारणा ०) ही फाइल प्रणाली "
"बनवणे असफल झाले."

#. Type: boolean
#. Description
#. :sl4:
#: ../partman-ext2r0.templates:4001
msgid "Do you want to return to the partitioning menu?"
msgstr "आपल्याला विभाजनाच्या मेन्यूकडे परत जायचे आहे?"

#. Type: boolean
#. Description
#. :sl4:
#: ../partman-ext2r0.templates:4001
msgid ""
"No mount point is assigned for the ext2 (revision 0) file system in "
"partition #${PARTITION} of ${DEVICE}."
msgstr ""
"${DEVICE} च्या #${PARTITION} या विभाजनावर ईएक्सटी२ (सुधारणा ०) फाइल प्रणालीकरिता "
"कोणताही आरोह बिंदू निश्चित केलेला नाही."

#. Type: boolean
#. Description
#. :sl4:
#: ../partman-ext2r0.templates:4001
msgid ""
"If you do not go back to the partitioning menu and assign a mount point from "
"there, this partition will not be used at all."
msgstr ""
"आपण विभाजनाच्या मेन्यूकडे परत जाऊन तिथून आरोह बिंदू निश्चित केला नाही, तर हे विभाजन "
"वापरलेच जाणार नाही."

#. Type: select
#. Choices
#. :sl4:
#. what's to be entered is a mount point
#: ../partman-ext2r0.templates:5001
msgid "Enter manually"
msgstr "स्वहस्ते भरा"

#. Type: select
#. Choices
#. :sl4:
#. "it" is a partition
#: ../partman-ext2r0.templates:5001
msgid "Do not mount it"
msgstr "हे आरोहित करू नका"

#. Type: select
#. Description
#. Type: string
#. Description
#. :sl4:
#: ../partman-ext2r0.templates:5002 ../partman-ext2r0.templates:6001
msgid "Mount point for this partition:"
msgstr "या विभाजनाकरिता आरोह बिंदू:"

#. Type: error
#. Description
#. :sl4:
#: ../partman-ext2r0.templates:7001
msgid "Invalid mount point"
msgstr "अवैध आरोह बिंदू"

#. Type: error
#. Description
#. :sl4:
#: ../partman-ext2r0.templates:7001
msgid "The mount point you entered is invalid."
msgstr "आपण दिलेला आरोह बिंदू अवैध आहे."

#. Type: error
#. Description
#. :sl4:
#: ../partman-ext2r0.templates:7001
msgid "Mount points must start with \"/\". They cannot contain spaces."
msgstr ""
"आरोह बिंदू  \"/\" पासूनच सुरू झाला पाहिजे. त्यांच्यात रिक्त जागा अंतर्भूत करता येत नाहीत."

#. Type: text
#. Description
#. :sl4:
#. File system name (untranslatable in many languages)
#. Type: text
#. Description
#. :sl4:
#. Short file system name (untranslatable in many languages)
#: ../partman-ext2r0.templates:8001 ../partman-ext2r0.templates:10001
msgid "ext2r0"
msgstr "ext2r0"

#. Type: text
#. Description
#. :sl4:
#. File system name
#: ../partman-ext2r0.templates:9001
msgid "old Ext2 (revision 0) file system"
msgstr "जुनी ईएक्सटी२ (सुधारणा ०) फाइल प्रणाली"

#. Type: boolean
#. Description
#. :sl4:
#. Type: boolean
#. Description
#. :sl4:
#. Type: boolean
#. Description
#. :sl4:
#: ../partman-ext2r0.templates:11001 ../partman-ext2r0.templates:12001
#: ../partman-ext2r0.templates:13001
msgid "Go back to the menu and correct this problem?"
msgstr "मागे मेन्यूकडे जाउन ही समस्या सुधारायची? "

#. Type: boolean
#. Description
#. :sl4:
#: ../partman-ext2r0.templates:11001
msgid ""
"Your boot partition has not been configured with the old ext2 (revision 0) "
"file system.  This is needed by your machine in order to boot.  Please go "
"back and use the old ext2 (revision 0) file system."
msgstr ""
"आपले आरंभ विभाजन जुन्या ईएक्सटी२ (सुधारणा ०) फाइल प्रणालीने संरचित केलेले नाही. आपल्या "
"संगणकाचा आरंभ होण्याकरिता हे आवश्यक आहे. कृपया मागे जा व जुनी ईएक्सटी२ (सुधारणा ०) फाइल "
"प्रणाली वापरा."

#. Type: boolean
#. Description
#. :sl4:
#. Type: boolean
#. Description
#. :sl4:
#. Type: boolean
#. Description
#. :sl4:
#: ../partman-ext2r0.templates:11001 ../partman-ext2r0.templates:12001
#: ../partman-ext2r0.templates:13001
msgid ""
"If you do not go back to the partitioning menu and correct this error, the "
"partition will be used as is.  This means that you may not be able to boot "
"from your hard disk."
msgstr ""
"जर तुम्ही मागील पायरीवर जाउन हा दोष काढून टाकला नाही तर, विभाजनांची सद्य स्थिती "
"कायम धरली जाईल. याचा अर्थ असा की कदाचित तुम्ही तुमच्या हार्डडीस्क वरून संगणक सुरु करू "
"शकणार नाही."

#. Type: boolean
#. Description
#. :sl4:
#: ../partman-ext2r0.templates:12001
msgid ""
"Your boot partition is not located on the first primary partition of your "
"hard disk.  This is needed by your machine in order to boot.  Please go back "
"and use your first primary partition as a boot partition."
msgstr ""
"तुमचे आरंभक विभाजन हार्डडिस्क वरील पहिल्या प्राथमिक विभाजनावर स्थित नाही.संगणक सुरु "
"होण्यासाठी हा क्रम आवश्यक असतो. कृपया मागील पायरीवर जाउन पहिले प्राथमिक विभाजन "
"आरंभक विभाजन म्हणून निवडा."

#. Type: boolean
#. Description
#. :sl4:
#: ../partman-ext2r0.templates:13001
msgid ""
"Your root partition is not a primary partition of your hard disk.  This is "
"needed by your machine in order to boot.  Please go back and use a primary "
"partition for your root partition."
msgstr ""
"तुमचे मुळ विभाजन हे हार्डडिस्क वरील प्राथमिक विभाजन नाही.संगणक सुरु होण्यासाठी हा क्रम "
"आवश्यक असतो. कृपया मागील पायरीवर जाउन प्राथमिक विभाजन मुळ विभाजन म्हणून निवडा."
