# THIS FILE IS GENERATED AUTOMATICALLY FROM THE D-I PO MASTER FILES
# The master files can be found under packages/po/
#
# DO NOT MODIFY THIS FILE DIRECTLY: SUCH CHANGES WILL BE LOST
#
# Bangla translation of Debian-Installer.
# Copyright (C) 2005, 2006, Debian Foundation.
# This file is distributed under the same license as the Debian-Installer package.
#
# Anubadok, the en2bn auto-translator by Golam Mortuza Hossain <golam@imsc.res.in>, 2005.
# Baishampayan Ghose <b.ghose@gnu.org.in>, 2005-2006.
# Quazi Ashfaq-ur Rahman <quazi.ashfaq@gmail.com>, 2005.
# Khandakar Mujahidul Islam <suzan@bengalinux.org>, 2005, 2006.
# Progga <progga@BengaLinux.Org>, 2005, 2006.
# Jamil Ahmed <jamil@bengalinux.org>, 2006-2007.
# Mahay Alam Khan (মাহে আলম খান) <makl10n@yahoo.com>, 2007.
# Tisa Nafisa <tisa_nafisa@yahoo.com>, 2007.
#
msgid ""
msgstr ""
"Project-Id-Version: debian-installer\n"
"Report-Msgid-Bugs-To: cdebconf-terminal@packages.debian.org\n"
"POT-Creation-Date: 2008-07-31 13:04+0200\n"
"PO-Revision-Date: 2008-09-18 15:43+0600\n"
"Last-Translator: Mahay Alam Khan (মাহে আলম খান) <mak@ankur.org.bd>\n"
"Language-Team: Bangla <core@BengaLinux.Org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. Type: text
#. Description
#: ../cdebconf-gtk-terminal.templates:1001
msgid "Resume installation"
msgstr "ইনস্টলেশন পুনরায় আরম্ভ করুন"

#. Type: text
#. Description
#: ../cdebconf-gtk-terminal.templates:2001
msgid ""
"Choose \"Continue\" to really exit the shell and resume the installation; "
"any processes still running in the shell will be aborted."
msgstr ""
"শেল হতে নিশ্চিরূপে প্রস্থান করে ইন্সটলেশন চালিয়ে যেতে \"Continue\" পছন্দ করুন; শেল "
"এ চলন্ত যেকোন প্রসেস বন্ধ হয়ে যাবে।"
