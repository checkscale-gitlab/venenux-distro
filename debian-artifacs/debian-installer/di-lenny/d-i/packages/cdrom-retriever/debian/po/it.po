# THIS FILE IS GENERATED AUTOMATICALLY FROM THE D-I PO MASTER FILES
# The master files can be found under packages/po/
#
# DO NOT MODIFY THIS FILE DIRECTLY: SUCH CHANGES WILL BE LOST
#
# Italian messages for debian-installer.
# Copyright (C) 2003 Software in the Public Interest, Inc.
# This file is distributed under the same license as debian-installer.
# The translation team (for all four levels):
# Cristian Rigamonti <cri@linux.it>
# Danilo Piazzalunga <danilopiazza@libero.it>
# Davide Meloni <davide_meloni@fastwebnet.it>
# Davide Viti <zinosat@tiscali.it>
# Filippo Giunchedi <filippo@esaurito.net>
# Giuseppe Sacco <eppesuig@debian.org>
# Lorenzo 'Maxxer' Milesi 
# Renato Gini
# Ruggero Tonelli
# Samuele Giovanni Tonon <samu@linuxasylum.net>
# Stefano Canepa <sc@linux.it>
# Stefano Melchior <stefano.melchior@openlabs.it>
# Milo Casagrande <milo@ubuntu.com>, 2008
#
msgid ""
msgstr ""
"Project-Id-Version: debian-installer\n"
"Report-Msgid-Bugs-To: debian-boot@lists.debian.org\n"
"POT-Creation-Date: 2008-01-26 07:32+0000\n"
"PO-Revision-Date: 2008-07-05 18:33+0200\n"
"Last-Translator: Milo Casagrande <milo@ubuntu.com>\n"
"Language-Team: Italian <debian-l10n-italian@lists.debian.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. Type: text
#. Description
#. Main menu item
#: ../load-cdrom.templates:1001
msgid "Load installer components from CD"
msgstr "Caricare i componenti del programma di installazione dal CD"

#. Type: boolean
#. Description
#. :sl2:
#: ../cdrom-retriever.templates:1001
msgid "Failed to copy file from CD-ROM. Retry?"
msgstr "Impossibile copiare il file dal CD-ROM. Riprovare?"

#. Type: boolean
#. Description
#. :sl2:
#: ../cdrom-retriever.templates:1001
msgid ""
"There was a problem reading data from the CD-ROM. Please make sure it is in "
"the drive. If retrying does not work, you should check the integrity of your "
"CD-ROM."
msgstr ""
"Si è verificato un problema nella lettura dei dati dal CD-ROM. Assicurarsi "
"che il CD-ROM sia nel lettore. Se il problema persiste, controllare "
"l'integrità del CD-ROM."
