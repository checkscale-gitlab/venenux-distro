# THIS FILE IS GENERATED AUTOMATICALLY FROM THE D-I PO MASTER FILES
# The master files can be found under packages/po/
#
# DO NOT MODIFY THIS FILE DIRECTLY: SUCH CHANGES WILL BE LOST
#
# translation of bg.po to Bulgarian
# Bulgarian messages for debian-installer.
# Copyright (C) 2003 Software in the Public Interest, Inc.
# This file is distributed under the same license as debian-installer.
#
# Ognyan Kulev <ogi@fmi.uni-sofia.bg>, 2004, 2005, 2006.
# Nikola Antonov <nikola@linux-bg.org>, 2004.
# Damyan Ivanov <dam@modsoftsys.com>, 2006, 2007, 2008.
# Damyan Ivanov <dmn@debian.org>, 2008.
msgid ""
msgstr ""
"Project-Id-Version: bg\n"
"Report-Msgid-Bugs-To: debian-boot@lists.debian.org\n"
"POT-Creation-Date: 2008-07-10 22:49+0000\n"
"PO-Revision-Date: 2008-07-11 08:29+0300\n"
"Last-Translator: Damyan Ivanov <dmn@debian.org>\n"
"Language-Team: Bulgarian <dict@fsa-bg.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#. Type: text
#. Description
#. :sl2:
#: ../media-retriever.templates:1001
msgid "Scanning removable media"
msgstr "Преглеждане на сменяем носител"

#. Type: text
#. Description
#. :sl2:
#: ../media-retriever.templates:2001
msgid "Cannot read removable media, or no drivers found."
msgstr "Носителят не може да бъде прочетен или не съдържа драйвери."

#. Type: text
#. Description
#. :sl2:
#: ../media-retriever.templates:2001
msgid ""
"There was a problem reading data from the removable media. Please make sure "
"that the right media is present. If you continue to have trouble, your "
"removable media might be bad."
msgstr ""
"Имаше проблем при четене от сменяемият носител. Проверете дали и поставен "
"правилният носител. Ако продължавате да имате проблеми, може би носителят е "
"повреден."

#. Type: boolean
#. Description
#: ../load-media.templates:1001
msgid "Load drivers from removable media now?"
msgstr "Зареждане на драйвери от сменяем ностел?"

#. Type: boolean
#. Description
#: ../load-media.templates:1001
msgid ""
"You probably need to load drivers from removable media before continuing "
"with the installation. If you know that the install will work without extra "
"drivers, you can skip this step."
msgstr ""
"Може би ще се нуждаете да заредите драйвери от сменяем носител, преди да "
"продължите с инсталацията. Ако сте сигурни, че инсталацията ще продължи да "
"работи и без допълнителни драйвери, можете да пропуснете тази стъпка."

#. Type: boolean
#. Description
#: ../load-media.templates:1001
msgid ""
"If you do need to load drivers, insert the appropriate removable media, such "
"as a driver floppy or USB stick before continuing."
msgstr ""
"Ако се нуждаете от зареждане на драйвери, сложете носителя (например "
"флопидиск или USB носител), преди да продължите."

#. Type: text
#. Description
#. main-menu
#: ../load-media.templates:2001
msgid "Load drivers from removable media"
msgstr "Зареждане на драйвери сменяем ностел"

#. Type: boolean
#. Description
#: ../load-media.templates:3001
msgid "Unknown removable media. Try to load anyway?"
msgstr "Непознат носител. Да бъде ли направен опит за зареждане?"

#. Type: boolean
#. Description
#: ../load-media.templates:3001
msgid ""
"Detected removable media that is not a known driver media. Please make sure "
"that the correct media is inserted. You can still continue if you have an "
"unofficial removable media you want to use."
msgstr ""
"Открит е носител, който не се разпознава като носител с драйвери. Проверете "
"дали е поставен правилният носител. Ако носителят е неофициален, можете да "
"продължите."

#. Type: text
#. Description
#: ../load-media.templates:4001
msgid "Please insert ${DISK_LABEL} ('${DISK_NAME}') first."
msgstr "Моля, първо поставете ${DISK_LABEL} ('${DISK_NAME}')."

#. Type: text
#. Description
#: ../load-media.templates:4001
msgid ""
"Due to dependencies between packages, drivers must be loaded in the correct "
"order."
msgstr ""
"Зависимостите между пакетите изискват драйверите да бъдат зареждане в "
"определен ред."

#. Type: boolean
#. Description
#: ../load-media.templates:5001
msgid "Load drivers from another removable media?"
msgstr "Зареждане на драйвери от друг сменяем ностел?"

#. Type: boolean
#. Description
#: ../load-media.templates:5001
msgid ""
"To load additional drivers from another removable media, please insert the "
"appropriate removable media, such as a driver floppy or USB stick before "
"continuing."
msgstr ""
"За да заредите допълнителни драйвери от друг носител, поставете го преди да "
"продължите."
