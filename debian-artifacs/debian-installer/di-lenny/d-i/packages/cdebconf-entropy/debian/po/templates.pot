# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: debian-boot@lists.debian.org\n"
"POT-Creation-Date: 2008-08-16 22:50+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: 8bit\n"

#. Type: text
#. Description
#. :sl3:
#: ../cdebconf-newt-entropy.templates:1001
#: ../cdebconf-text-entropy.templates:1001
msgid "Enter random characters"
msgstr ""

#. Type: text
#. Description
#. :sl3:
#: ../cdebconf-newt-entropy.templates:2001
#: ../cdebconf-text-entropy.templates:2001
msgid ""
"You can help speed up the process by entering random characters on the "
"keyboard, or just wait until enough key data has been collected (which can "
"take a long time)."
msgstr ""

#. Type: text
#. Description
#. :sl3:
#: ../cdebconf-newt-entropy.templates:3001
#: ../cdebconf-gtk-entropy.templates:3001
#: ../cdebconf-text-entropy.templates:3001
msgid "Key data has been created successfully."
msgstr ""

#. Type: text
#. Description
#. :sl3:
#: ../cdebconf-gtk-entropy.templates:1001
msgid "Enter random characters or make random movements with the mouse"
msgstr ""

#. Type: text
#. Description
#. :sl3:
#: ../cdebconf-gtk-entropy.templates:2001
msgid ""
"You can help speed up the process by entering random characters on the "
"keyboard or by making random movements with the mouse."
msgstr ""
