# THIS FILE IS GENERATED AUTOMATICALLY FROM THE D-I PO MASTER FILES
# The master files can be found under packages/po/
#
# DO NOT MODIFY THIS FILE DIRECTLY: SUCH CHANGES WILL BE LOST
#
# Turkish messages for debian-installer.
# Copyright (C) 2003, 2004 Software in the Public Interest, Inc.
# This file is distributed under the same license as debian-installer.
#
# Mert Dirik <mertdirik@gmail.com>, 2008.
# Recai Oktaş <roktas@omu.edu.tr>, 2004, 2005, 2008.
# Osman Yüksel <yuxel@sonsuzdongu.com>, 2004.
# Özgür Murat Homurlu <ozgurmurat@gmx.net>, 2004.
# Halil Demirezen <halild@bilmuh.ege.edu.tr>, 2004.
# Murat Demirten <murat@debian.org>, 2004.
#
msgid ""
msgstr ""
"Project-Id-Version: debian-installer\n"
"Report-Msgid-Bugs-To: debian-boot@lists.debian.org\n"
"POT-Creation-Date: 2008-07-21 22:51+0000\n"
"PO-Revision-Date: 2008-08-02 23:46+0200\n"
"Last-Translator: Mert Dirik <mertdirik@gmail.com>\n"
"Language-Team: Debian L10n Turkish <debian-l10n-turkish@lists.debian.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"

#. Type: text
#. Description
#. :sl2:
#: ../network-console.templates:1001
msgid "Continue installation remotely using SSH"
msgstr "Kuruluma uzaktan SSH kullanarak devam et"

#. Type: select
#. Choices
#. Note to translators : Please keep your translations of the choices
#. below a 65 columns limit (which means 65 characters
#. in single-byte languages)
#. :sl2:
#: ../network-console.templates:2001
msgid "Start installer"
msgstr "Kurulum programını başlat"

#. Type: select
#. Choices
#. Note to translators : Please keep your translations of the choices
#. below a 65 columns limit (which means 65 characters
#. in single-byte languages)
#. :sl2:
#: ../network-console.templates:2001
msgid "Start installer (expert mode)"
msgstr "Kurulum programını başlat (uzman kipi)"

#. Type: select
#. Choices
#. Note to translators : Please keep your translations of the choices
#. below a 65 columns limit (which means 65 characters
#. in single-byte languages)
#. :sl2:
#: ../network-console.templates:2001
msgid "Start shell"
msgstr "Kabuk başlat"

#. Type: select
#. Description
#. :sl2:
#: ../network-console.templates:2002
msgid "Network console option:"
msgstr "Ağ konsolu seçeneği:"

#. Type: select
#. Description
#. :sl2:
#: ../network-console.templates:2002
msgid ""
"This is the network console for the Debian installer. From here, you may "
"start the Debian installer, or execute an interactive shell."
msgstr ""
"Şu an Debian kurulum programının ağ konsolundasınız. Buradan itibaren, "
"Debian kurulumunu başlatabilir veya etkileşimli bir kabuk "
"çalıştırabilirsiniz."

#. Type: select
#. Description
#. :sl2:
#: ../network-console.templates:2002
msgid "To return to this menu, you will need to log in again."
msgstr "Bu menüye dönmek için tekrar giriş yapmanız gerekecektir."

#. Type: text
#. Description
#. :sl2:
#: ../network-console.templates:3001
msgid "Generating SSH host key"
msgstr "SSH makine anahtarı üretiliyor"

#. Type: password
#. Description
#. :sl2:
#: ../network-console.templates:4001
msgid "Remote installation password:"
msgstr "Uzaktan kurulum parolası:"

#. Type: password
#. Description
#. :sl2:
#: ../network-console.templates:4001
msgid ""
"You need to set a password for remote access to the Debian installer. A "
"malicious or unqualified user with access to the installer can have "
"disastrous results, so you should take care to choose a password that is not "
"easy to guess. It should not be a word found in the dictionary, or a word "
"that could be easily associated with you, like your middle name."
msgstr ""
"Debian kurulum programına uzaktan erişim için bir parola ayarlamanız "
"gerekiyor. Kurulum programına erişen kötü niyetli veya yetkisiz bir "
"kullanıcı çok kötü sonuçlar doğurabilir. Dolayısıyla tahmin edilmesi güç bir "
"parola seçmeye özen gösterin. Bu parola sözlükte bulunabilecek veya ikinci "
"adınız gibi sizinle ilgisi kolaylıkla belirlenebilecek bir sözcük "
"olmamalıdır."

#. Type: password
#. Description
#. :sl2:
#: ../network-console.templates:4001
msgid ""
"This password is used only by the Debian installer, and will be discarded "
"once you finish the installation."
msgstr ""
"Bu parola sadece Debian kurulum programı tarafından kullanılacak ve kurulum "
"tamamlandıktan sonra dikkate alınmayacaktır."

#. Type: password
#. Description
#. :sl2:
#: ../network-console.templates:5001
msgid "Re-enter password to verify:"
msgstr "Doğrulamak için parolayı tekrar girin:"

#. Type: password
#. Description
#. :sl2:
#: ../network-console.templates:5001
msgid ""
"Please enter the same remote installation password again to verify that you "
"have typed it correctly."
msgstr ""
"Lütfen doğru yazıldığından emin olmak için aynı uzaktan kurulum parolasını "
"tekrar girin."

#. Type: error
#. Description
#. :sl2:
#: ../network-console.templates:6001
msgid "Empty password"
msgstr "Boş parola"

#. Type: error
#. Description
#. :sl2:
#: ../network-console.templates:6001
msgid ""
"You entered an empty password, which is not allowed. Please choose a non-"
"empty password."
msgstr ""
"Boş bir parola girdiniz. Buna izin verilmiyor. Lütfen boş olmayan bir parola "
"girin."

#. Type: error
#. Description
#. :sl2:
#: ../network-console.templates:7001
msgid "Password mismatch"
msgstr "Parola uyuşmazlığı"

#. Type: error
#. Description
#. :sl2:
#: ../network-console.templates:7001
msgid ""
"The two passwords you entered were not the same. Please enter a password "
"again."
msgstr "Girdiğiniz iki parola aynı değil. Lütfen tekrar bir parola girin."

#. Type: note
#. Description
#. :sl2:
#: ../network-console.templates:8001
msgid "Start SSH"
msgstr "SSH'ı başlat"

#. Type: note
#. Description
#. :sl2:
#: ../network-console.templates:8001
msgid ""
"To continue the installation, please use an SSH client to connect to the IP "
"address ${ip} and log in as the \"installer\" user. For example:"
msgstr ""
"Kuruluma devam etmek için lütfen bir SSH istemcisi çalıştırarak ${ip} IP "
"adresine bağlanın ve \"installer\" kullanıcısıyla giriş yapın. Örneğin:"

#. Type: note
#. Description
#. :sl2:
#: ../network-console.templates:8001
msgid "The fingerprint of this SSH server's host key is: ${fingerprint}"
msgstr "Bu SSH sunucusuna ait makine anahtarının parmakizi: ${fingerprint}"

#. Type: note
#. Description
#. :sl2:
#: ../network-console.templates:8001
msgid ""
"Please check this carefully against the fingerprint reported by your SSH "
"client."
msgstr ""
"Lütfen bu parmakizini, SSH istemciniz tarafından bildirilen parmakizi ile "
"dikkatlice karşılaştırın."
