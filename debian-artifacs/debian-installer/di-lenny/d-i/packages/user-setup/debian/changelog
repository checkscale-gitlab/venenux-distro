user-setup (1.23) unstable; urgency=low

  [ Updated translations ]
  * Belarusian (be.po) by Pavel Piatruk
  * Bosnian (bs.po) by Armin Besirovic
  * Danish (da.po)
  * Croatian (hr.po) by Josip Rodin
  * Latvian (lv.po) by Peteris Krisjanis
  * Macedonian (mk.po) by Arangel Angov
  * Serbian (sr.po) by Veselin Mijušković

 -- Otavio Salvador <otavio@debian.org>  Sun, 21 Sep 2008 20:52:37 -0300

user-setup (1.22) unstable; urgency=low

  [ Jérémy Bobbio ]
  * Source confmodule in pre-pkgsel.d/10kdesudo.
  * As cdebconf is now fixed, on errors in root password, return to root
    password dialog (and not the one before it).
    Depends: cdebconf-udeb (>= 0.133)

  [ Updated translations ]
  * Arabic (ar.po) by Ossama M. Khayat
  * French (fr.po) by Christian Perrier
  * Kurdish (ku.po) by Erdal Ronahi
  * Portuguese (Brazil) (pt_BR.po) by Felipe Augusto van de Wiel (faw)
  * Turkish (tr.po) by Mert Dirik

 -- Jérémy Bobbio <lunar@debian.org>  Tue, 26 Aug 2008 10:56:42 +0200

user-setup (1.21) unstable; urgency=low

  [ Frans Pop ]
  * user-setup-apply: avoid locale errors from perl when used in D-I.
  * Add pre-pkgsel hook script to setup kdesudo for KDE desktop installs
    without root account (#485655). Thanks to Didier Raboud for bringing up
    the subject and suggesting the solution.

  [ Colin Watson ]
  * Don't exit user-setup-apply if update-gconf-defaults fails.

  [ Updated translations ]
  * Basque (eu.po) by Iñaki Larrañaga Murgoitio
  * Finnish (fi.po) by Esko Arajärvi
  * Italian (it.po) by Milo Casagrande
  * Turkish (tr.po) by Mert Dirik
  * Simplified Chinese (zh_CN.po) by Kov Chai

 -- Frans Pop <fjp@debian.org>  Wed, 16 Jul 2008 13:52:07 +0200

user-setup (1.20) unstable; urgency=low

  [ Updated translations ]
  * Basque (eu.po) by Piarres Beobide
  * Finnish (fi.po) by Esko Arajärvi
  * French (fr.po) by Christian Perrier
  * Gujarati (gu.po) by Kartik Mistry
  * Malayalam (ml.po) by Praveen|പ്രവീണ്‍ A|എ
  * Marathi (mr.po) by Sampada
  * Dutch (nl.po) by Frans Pop
  * Panjabi (pa.po) by Amanpreet Singh Alam

 -- Otavio Salvador <otavio@debian.org>  Thu, 08 May 2008 00:47:15 -0300

user-setup (1.19) unstable; urgency=low

  [ Updated translations ]
  * Finnish (fi.po) by Esko Arajärvi
  * Hindi (hi.po) by Kumar Appaiah
  * Indonesian (id.po) by Arief S Fitrianto
  * Latvian (lv.po) by Viesturs Zarins
  * Panjabi (pa.po) by Amanpreet Singh Alam
  * Turkish (tr.po) by Recai Oktaş
  * Traditional Chinese (zh_TW.po) by Tetralet

 -- Otavio Salvador <otavio@debian.org>  Fri, 15 Feb 2008 09:28:30 -0200

user-setup (1.18) unstable; urgency=low

  * In shadow version 4.1.0, the chpassword -e and -m options are
    mutially exclusive. Avoid sending both options when a pre-encrypted
    password is provided by preseeding. Thanks, Jordi-Pujol. Closes: #462387

 -- Joey Hess <joeyh@debian.org>  Thu, 24 Jan 2008 17:05:00 -0500

user-setup (1.17) unstable; urgency=low

  [ Joey Hess ]
  * Call dh_md5sums.

  [ Otavio Salvador ]
  * Bump Standards-Version to 3.7.2 (no changes needed).

  [ Colin Watson ]
  * Fix comment attached to passwd/user-default-groups.
  * hal was renamed to haldaemon in hal 0.5.7.1-1; Hal is also a reasonably
    common human name. Remove it from reserved-usernames.

  [ Updated translations ]
  * Amharic (am.po) by tegegne tefera
  * Basque (eu.po) by Piarres Beobide
  * Italian (it.po) by Stefano Canepa
  * Korean (ko.po) by Changwoo Ryu
  * Malayalam (ml.po) by Praveen|പരവീണ A|എ
  * Panjabi (pa.po) by A S Alam
  * Romanian (ro.po) by Eddy Petrișor
  * Slovak (sk.po) by Ivan Masár
  * Vietnamese (vi.po) by Clytie Siddall

 -- Christian Perrier <bubulle@debian.org>  Fri, 11 Jan 2008 18:56:46 +0100

user-setup (1.16) unstable; urgency=low

  * Move menu item to come after base-installer, and just before apt-setup.

  [ Updated translations ]
  * Bengali (bn.po) by Jamil Ahmed
  * Punjabi (Gurmukhi) (pa.po) by A S Alam
  * Portuguese (pt.po) by Miguel Figueiredo

 -- Joey Hess <joeyh@debian.org>  Wed, 25 Jul 2007 13:39:40 -0400

user-setup (1.15) unstable; urgency=low

  [ Otavio Salvador ]
  * Add support to control which default groups the initial user will be
    added. Preseed it at passwd/user-default-groups. Closes: #426452

 -- Christian Perrier <bubulle@debian.org>  Fri, 29 Jun 2007 06:33:38 +0200

user-setup (1.14) unstable; urgency=low

  * Additionally, fix chroot call bug in sudo installation code.

 -- Joey Hess <joeyh@debian.org>  Tue, 01 May 2007 17:12:03 -0400

user-setup (1.13) unstable; urgency=low

  [ Colin Watson ]
  * Add mythtv to reserved-usernames (https://launchpad.net/bugs/86358).

  [ Joey Hess ]
  * Fix user-setup-apply to properly set up gksu alternatives for sudo mode.
    The chroot calls were broken. Closes: #421323

 -- Joey Hess <joeyh@debian.org>  Tue, 01 May 2007 17:10:31 -0400

user-setup (1.12) unstable; urgency=low

  * Multiply menu-item-numbers by 100

  [ Updated translations ]
  * Esperanto (eo.po) by Serge Leblanc
  * Basque (eu.po) by Piarres Beobide
  * Norwegian Bokmål (nb.po) by Bjørn Steensrud

 -- Joey Hess <joeyh@debian.org>  Tue, 10 Apr 2007 14:34:34 -0400

user-setup (1.11) unstable; urgency=low

  [ Updated translations ]
  * German (de.po) by Jens Seidel
  * Basque (eu.po) by Piarres Beobide
  * Hebrew (he.po) by Lior Kaplan
  * Malayalam (ml.po) by Praveen A
  * Dutch (nl.po) by Bart Cornelis

 -- Frans Pop <fjp@debian.org>  Tue, 27 Feb 2007 17:01:40 +0100

user-setup (1.10) unstable; urgency=low

  [ Updated translations ]
  * Esperanto (eo.po) by Serge Leblanc
  * Romanian (ro.po) by Eddy Petrișor

 -- Frans Pop <fjp@debian.org>  Wed, 31 Jan 2007 11:57:11 +0100

user-setup (1.9) unstable; urgency=low

  [ Colin Watson ]
  * Refuse to apply an empty root password. I don't think this can happen,
    but it's a useful sanity check.

  [ Frans Pop ]
  * Revert the change from 1.5 and return to STATE 0 again on errors in root
    password. Doing the logical thing triggers a bug in cdebconf (#407577).

  [ Updated translations ]
  * Arabic (ar.po) by Ossama M. Khayat
  * Danish (da.po) by Claus Hindsgaul
  * Spanish (es.po) by Javier Fernández-Sanguino Peña
  * Hebrew (he.po) by Lior Kaplan
  * Kurdish (ku.po) by Amed Çeko Jiyan
  * Latvian (lv.po) by Aigars Mahinovs
  * Panjabi (pa.po) by A S Alam
  * Portuguese (Brazil) (pt_BR.po) by Felipe Augusto van de Wiel (faw)
  * Slovak (sk.po) by Peter Mann
  * Slovenian (sl.po) by Matej Kovačič

 -- Frans Pop <fjp@debian.org>  Mon, 29 Jan 2007 21:34:39 +0100

user-setup (1.8) unstable; urgency=low

  [ Joey Hess ]
  * Fix backing up after having chosen to not enable root logins, and changing
    it to enable root logins. Since the crypted password was set to a locked
    password in the first pass, the second pass failed to ask for a root
    password. Closes: #400766, #388003.

  [ Frans Pop ]
  * Add myself to uploaders.

  [ Updated translations ]
  * Bulgarian (bg.po) by Damyan Ivanov
  * Bosnian (bs.po) by Safir Secerovic
  * Catalan (ca.po) by Jordi Mallach
  * Esperanto (eo.po) by Serge Leblanc
  * Spanish (es.po) by Javier Fernández-Sanguino Peña
  * Georgian (ka.po) by Aiet Kolkhi
  * Kurdish (ku.po) by Rêzan Tovjîn
  * Norwegian Bokmål (nb.po) by Bjørn Steensrud
  * Norwegian Nynorsk (nn.po) by Håvard Korsvoll
  * Panjabi (pa.po) by A S Alam
  * Slovenian (sl.po) by Matej Kovačič
  * Swedish (sv.po) by Daniel Nylander

 -- Frans Pop <fjp@debian.org>  Thu, 30 Nov 2006 15:38:33 +0100

user-setup (1.7) unstable; urgency=low

  [ Updated translations ]
  * Belarusian (be.po) by Andrei Darashenka
  * Esperanto (eo.po) by Serge Leblanc
  * Spanish (es.po) by Javier Fernández-Sanguino Peña
  * Indonesian (id.po) by Arief S Fitrianto
  * Korean (ko.po) by Sunjae park
  * Kurdish (ku.po) by Erdal Ronahi
  * Macedonian (mk.po) by Georgi Stanojevski
  * Norwegian Bokmål (nb.po) by Bjørn Steensrud
  * Nepali (ne.po) by Shiva Prasad Pokharel
  * Dutch (nl.po) by Bart Cornelis
  * Romanian (ro.po) by Eddy Petrișor
  * Albanian (sq.po) by Elian Myftiu
  * Swedish (sv.po) by Daniel Nylander
  * Tamil (ta.po) by Damodharan Rajalingam
  * Vietnamese (vi.po) by Clytie Siddall
  * Wolof (wo.po) by Mouhamadou Mamoune Mbacke
  * Simplified Chinese (zh_CN.po) by Ming Hua

 -- Frans Pop <fjp@debian.org>  Tue, 24 Oct 2006 16:31:47 +0200

user-setup (1.6) unstable; urgency=low

  [ Colin Watson ]
  * Detect and error out on reserved usernames, i.e. those that are already
    used by some parts of the system. Unfortunately there's no particularly
    straightforward way to identify a reserved username, and the only way I
    can think of is to maintain a blacklist; so I've collated this from the
    base-passwd master files and those user and group names I found on some
    of my systems. Feel free to extend this as the need arises.

  [ Christian Perrier ]
  * Spell "username" consistently.

  [ Updated translations ]
  * Bengali (bn.po) by Mahay Alam Khan (মাহে আলম খান)
  * Catalan (ca.po) by Jordi Mallach
  * Czech (cs.po) by Miroslav Kure
  * Danish (da.po) by Claus Hindsgaul
  * German (de.po) by Jens Seidel
  * Dzongkha (dz.po) by Jurmey Rabgay
  * Greek (el.po) by quad-nrg.net
  * Esperanto (eo.po) by Serge Leblanc
  * Spanish (es.po) by Javier Fernández-Sanguino Peña
  * Estonian (et.po) by Siim Põder
  * Basque (eu.po) by Piarres Beobide
  * Finnish (fi.po) by Tapio Lehtonen
  * French (fr.po) by Christian Perrier
  * Galician (gl.po) by Jacobo Tarrio
  * Hebrew (he.po) by Lior Kaplan
  * Hindi (hi.po) by Nishant Sharma
  * Croatian (hr.po) by Josip Rodin
  * Hungarian (hu.po) by SZERVÁC Attila
  * Italian (it.po) by Giuseppe Sacco
  * Japanese (ja.po) by Kenshi Muto
  * Khmer (km.po) by Khoem Sokhem
  * Korean (ko.po) by Sunjae park
  * Kurdish (ku.po) by Erdal Ronahi
  * Lithuanian (lt.po) by Kęstutis Biliūnas
  * Latvian (lv.po) by Aigars Mahinovs
  * Macedonian (mk.po) by Georgi Stanojevski
  * Norwegian Bokmal (nb.po) by Bjørn Steensrud
  * Dutch (nl.po) by Bart Cornelis
  * Polish (pl.po) by Bartosz Fenski
  * Portuguese (Brazil) (pt_BR.po) by André Luís Lopes
  * Portuguese (pt.po) by Miguel Figueiredo
  * Romanian (ro.po) by Eddy Petrişor
  * Russian (ru.po) by Yuri Kozlov
  * Slovak (sk.po) by Peter Mann
  * Slovenian (sl.po) by Jure Čuhalev
  * Albanian (sq.po) by Elian Myftiu
  * Swedish (sv.po) by Daniel Nylander
  * Thai (th.po) by Theppitak Karoonboonyanan
  * Tagalog (tl.po) by Eric Pareja
  * Turkish (tr.po) by Recai Oktaş
  * Ukrainian (uk.po) by Eugeniy Meshcheryakov
  * Vietnamese (vi.po) by Clytie Siddall
  * Traditional Chinese (zh_TW.po) by Tetralet

 -- Frans Pop <fjp@debian.org>  Fri,  6 Oct 2006 03:17:43 +0200

user-setup (1.5) unstable; urgency=low

  [ Frans Pop ]
  * On errors in root password, return to root password dialog (and not the
    one before it).
  * As the password and password confirmation are now asked at the same
    priority, remove code in password check to support differing priorities.

  [ Joey Hess ]
  * Configure gksu to use sudo, via an alternative, if libgksu2-0 version
    1.9.8-2 is installed, and sudo is being used. Closes: #382670

  [ Updated translations ]
  * Catalan (ca.po) by Jordi Mallach
  * German (de.po) by Jens Seidel
  * Greek, Modern (1453-) (el.po) by quad-nrg.net
  * Spanish (es.po) by Javier Fernández-Sanguino Peña
  * Hebrew (he.po) by Lior Kaplan
  * Indonesian (id.po) by Arief S Fitrianto
  * Russian (ru.po) by Yuri Kozlov
  * Northern Sami (se.po) by Børre Gaup
  * Tagalog (tl.po) by Eric Pareja
  * Traditional Chinese (zh_TW.po) by Tetralet

 -- Joey Hess <joeyh@debian.org>  Wed,  6 Sep 2006 15:13:22 -0400

user-setup (1.4) unstable; urgency=low

  [ Colin Watson ]
  * Fix non-POSIX shell use that confuses busybox if the root password is
    currently set to "!".

 -- Christian Perrier <bubulle@debian.org>  Mon, 24 Jul 2006 18:32:00 +0200

user-setup (1.3) unstable; urgency=low

  * Add first user to two additional groups: netdev and powerdev
    Note that this assumes that something creates these groups during
    the task installation, as hal does with powerdev. The first user is added
    to the groups in finish-install so the groups need not be static.
    Closes: #352713

  [ Updated translations ]
  * Estonian (et.po) by Siim Põder
  * Dutch (nl.po) by Bart Cornelis
  * Portuguese (pt.po) by Miguel Figueiredo
  * Swedish (sv.po) by Daniel Nylander

 -- Joey Hess <joeyh@debian.org>  Mon, 10 Jul 2006 18:23:44 -0400

user-setup (1.2) unstable; urgency=low

  [ Christian Perrier ]
  * Space-indentation cleanup in user-setup-ask.
  * Switch from prebaseconfig.d to finish-install.d

  [ Joey Hess ]
  * Comment out STATE debugging message.
  * Check for the passwd file before grepping it.

  [ Colin Watson ]
  * Be more paranoid about clearing passwords from the cdebconf database.
  * Fix up permissions of user's home directory in case a mount point was
    created underneath it by partman (closes:
    https://launchpad.net/bugs/16640).

  [ Joey Hess ]
  * Rename finish-install template.

  [ Updated translations ]
  * Arabic (ar.po) by Ossama M. Khayat
  * Catalan (ca.po) by Jordi Mallach
  * Danish (da.po) by Claus Hindsgaul
  * German (de.po) by Jens Seidel
  * Dzongkha (dz.po)
  * Spanish (es.po) by Javier Fernández-Sanguino Peña
  * Italian (it.po) by Giuseppe Sacco
  * Georgian (ka.po) by Aiet Kolkhi
  * Kurdish (ku.po) by Erdal Ronahi
  * Lithuanian (lt.po) by Kęstutis Biliūnas
  * Malagasy (mg.po) by Jaonary Rabarisoa
  * Macedonian (mk.po) by Georgi Stanojevski
  * Bokmål, Norwegian (nb.po) by Bjørn Steensrud
  * Nepali (ne.po) by Shiva Pokharel
  * Norwegian Nynorsk (nn.po) by Håvard Korsvoll
  * Portuguese (pt.po) by Miguel Figueiredo
  * Northern Sami (se.po) by Børre Gaup
  * Slovenian (sl.po) by Jure Čuhalev
  * Albanian (sq.po) by Elian Myftiu
  * Thai (th.po) by Theppitak Karoonboonyanan
  * Turkish (tr.po) by Recai Oktaş
  * Ukrainian (uk.po) by Eugeniy Meshcheryakov
  * Vietnamese (vi.po) by Clytie Siddall
  * Simplified Chinese (zh_CN.po) by Carlos Z.F. Liu

 -- Joey Hess <joeyh@debian.org>  Wed,  7 Jun 2006 22:13:44 -0400

user-setup (1.1) unstable; urgency=low

  [ Tollef Fog Heen ]
  * Don't use perl to pass parameters to chpasswd.

  [ Joey Hess ]
  * Add passwd/root-login question (asked at medium priority), currently
    defaulting to true.
  * If root-login is false, skip root password prompts, lock the root account,
    always make a user account, install sudo, and configure /etc/sudoers to
    allow the user to become root. Closes: #344873

  [ Updated translations ]
  * Arabic (ar.po) by Ossama M. Khayat
  * Bulgarian (bg.po) by Ognyan Kulev
  * Bengali (bn.po) by Baishampayan Ghose
  * Bosnian (bs.po) by Safir Secerovic
  * Catalan (ca.po) by Jordi Mallach
  * Czech (cs.po) by Miroslav Kure
  * Welsh (cy.po) by Dafydd Harries
  * Danish (da.po) by Claus Hindsgaul
  * German (de.po) by Jens Seidel
  * Dzongkha (dz.po) by Sonam Rinchen
  * Greek, Modern (1453-) (el.po) by quad-nrg.net
  * Esperanto (eo.po) by Serge Leblanc
  * Spanish (es.po) by Javier Fernández-Sanguino Peña
  * Basque (eu.po) by Piarres Beobide
  * Finnish (fi.po) by Tapio Lehtonen
  * French (fr.po) by Christian Perrier
  * Irish (ga.po) by Kevin Patrick Scannell
  * Galician (gl.po) by Jacobo Tarrio
  * Hindi (hi.po) by Nishant Sharma
  * Hungarian (hu.po) by SZERVÑC Attila
  * Indonesian (id.po) by Parlin Imanuel Toh
  * Italian (it.po) by Stefano Canepa
  * Japanese (ja.po) by Kenshi Muto
  * Khmer (km.po) by Leang Chumsoben
  * Korean (ko.po) by Sunjae park
  * Lithuanian (lt.po) by Kęstutis Biliūnas
  * Latvian (lv.po) by Aigars Mahinovs
  * Macedonian (mk.po) by Georgi Stanojevski
  * Bokmål, Norwegian (nb.po) by Bjørn Steensrud
  * Dutch (nl.po) by Bart Cornelis
  * Punjabi (pa.po) by Amanpreet Singh Alam
  * Polish (pl.po) by Bartosz Fenski
  * Portuguese (Brazil) (pt_BR.po) by André Luís Lopes
  * Portuguese (pt.po) by Miguel Figueiredo
  * Romanian (ro.po) by Eddy Petrişor
  * Russian (ru.po) by Yuri Kozlov
  * Northern Sami (se.po) by Børre Gaup
  * Slovak (sk.po) by Peter Mann
  * Slovenian (sl.po) by Jure Cuhalev
  * Albanian (sq.po) by Elian Myftiu
  * Swedish (sv.po) by Daniel Nylander
  * Tamil (ta.po) by Damodharan Rajalingam
  * Tagalog (tl.po) by Eric Pareja
  * Turkish (tr.po) by Recai Oktaş
  * Ukrainian (uk.po) by Eugeniy Meshcheryakov
  * Vietnamese (vi.po) by Clytie Siddall
  * Wolof (wo.po) by Mouhamadou Mamoune Mbacke
  * Simplified Chinese (zh_CN.po) by Carlos Z.F. Liu

 -- Christian Perrier <bubulle@debian.org>  Mon, 17 Apr 2006 18:42:25 +0200

user-setup (1.0) unstable; urgency=low

  [ Updated translations ]
  * Basque (eu.po) by Piarres Beobide
  * Hindi (hi.po) by Nishant Sharma
  * Latvian (lv.po) by Aigars Mahinovs
  * Malagasy (mg.po) by Jaonary Rabarisoa
  * Punjabi (Gurmukhi) (pa_IN.po) by Amanpreet Singh Alam
  * Romanian (ro.po) by Eddy Petrişor
  * Slovenian (sl.po) by Jure Cuhalev
  * Albanian (sq.po) by Elian Myftiu
  * Vietnamese (vi.po) by Clytie Siddall
  * Wolof (wo.po) by Mouhamadou Mamoune Mbacke

  [ Christian Perrier ]
  * Bump to 1.0 as is fairly stable now

 -- Christian Perrier <bubulle@debian.org>  Tue, 24 Jan 2006 22:14:22 +0100

user-setup (0.05) unstable; urgency=low

  [ Colin Watson ]
  * Change the types of error questions from "note" to "error".

  [ Updated translations ]
  * Bulgarian (bg.po) by Ognyan Kulev
  * Hebrew (he.po) by Lior Kaplan
  * Traditional Chinese (zh_TW.po) by Tetralet

  [ Christian Perrier ]
  * Have the udeb depend on cdebconf-udeb and use a better dependency for
    the deb

 -- Christian Perrier <bubulle@debian.org>  Wed, 11 Jan 2006 07:58:27 +0100

user-setup (0.04) unstable; urgency=low

  [ Colin Watson ]
  * Add /usr/share/common-licenses/GPL reference to debian/copyright.
  * Add a primitive user-setup.deb, to replace the old 'dpkg-reconfigure
    passwd.config'; may be of some use in live CD-style environments.
  * Rename user-setup to user-setup-ask to make room for a user-setup
    wrapper script in the .deb.
  * Add myself to Uploaders.

  [ Christian Perrier ]
  * Call commands using [a-z] intervals under a C locale
    Closes: #343636

  [ Joey Hess ]
  * Exit 10 on backup out of the program, to work properly with main-menu.
  * Use -m flag of chpasswd rather than doing the md5 generation by hand.

  [ Updated translations ]
  * Catalan (ca.po) by Guillem Jover
  * Greek, Modern (1453-) (el.po) by quad-nrg.net
  * Finnish (fi.po) by Tapio Lehtonen
  * Galician (gl.po) by Jacobo Tarrio
  * Indonesian (id.po) by Parlin Imanuel Toh
  * Lithuanian (lt.po) by Kęstutis Biliūnas
  * Malagasy (mg.po) by Jaonary Rabarisoa
  * Macedonian (mk.po) by Georgi Stanojevski
  * Dutch (nl.po) by Frans Pop
  * Polish (pl.po) by Bartosz Fenski
  * Russian (ru.po) by Yuri Kozlov
  * Slovak (sk.po) by Peter Mann
  * Slovenian (sl.po) by Jure Čuhalev
  * Swedish (sv.po) by Daniel Nylander
  * Turkish (tr.po) by Recai Oktaş
  * Ukrainian (uk.po) by Eugeniy Meshcheryakov
  * Vietnamese (vi.po) by Clytie Siddall

 -- Colin Watson <cjwatson@debian.org>  Mon,  2 Jan 2006 14:05:32 +0000

user-setup (0.03) unstable; urgency=low

  [ Christian Perrier ]
  * Add the first created user to useful groups (code coming
    from base-config)
  * log all calls to adduser

  [ Colin Watson ]
  * Fix setpassword: perl is already run in the chroot, so there's no need
    to try to chroot again.

  [ Joey Hess ]
  * Add po/output file to ensure utf-8 encoding.

 -- Christian Perrier <bubulle@debian.org>  Sun,  4 Dec 2005 07:11:30 +0100

user-setup (0.02) unstable; urgency=low

  [ Colin Watson ]
  * Fix clearing of encrypted user passwords from the debconf database.
  * Remove a few bits of dead code.
  * Exit with code 10 to back up to main menu, not 30.
  * Use a prebaseconfig script and prompt the user earlier
  * Remove some unnecessary calls to chroot.
  * Simplify state machine so that one fewer line has to be changed when
    adding or removing states.

  [ Christian Perrier ]
  * Add a main menu title
  * Add a template for the prebaseconfig progress bar
  * Removed useless and harmful db_settitle in user-setup

 -- Christian Perrier <bubulle@debian.org>  Sun, 27 Nov 2005 19:59:52 +0100

user-setup (0.01) unstable; urgency=low

  [ Christian Perrier ]
  * First release as a udeb broken out from shadow (and base-config)
    and rewritten.

 -- Christian Perrier <bubulle@debian.org>  Sun, 13 Nov 2005 20:07:32 +0100
