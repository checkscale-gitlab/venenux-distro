# THIS FILE IS GENERATED AUTOMATICALLY FROM THE D-I PO MASTER FILES
# The master files can be found under packages/po/
#
# DO NOT MODIFY THIS FILE DIRECTLY: SUCH CHANGES WILL BE LOST
#
# translation of Debian Installer Level 1 - sublevel 1 to malayalam
# Copyright (c)  2006-2008 Praveen|പ്രവീണ്‍ A|എ <pravi.a@gmail.com>, Santhosh Thottingal <santhosh00@gmail.com>, Sreejith :: ശ്രീജിത്ത് കെ <sreejithk2000@gmail.com> and Debian Project
# Credits:  V Sasi Kumar, Sreejith N, Seena N, Anivar Aravind, Hiran Venugopalan and Suresh P
#
# Debian Installer master translation file template
# Don't forget to properly fill-in the header of PO files
# Debian Installer translators, please read the D-I i18n documentation
# in doc/i18n/i18n.txt#
#
msgid ""
msgstr ""
"Project-Id-Version: Debian Installer Level 1\n"
"Report-Msgid-Bugs-To: debian-boot@lists.debian.org\n"
"POT-Creation-Date: 2008-07-31 22:50+0000\n"
"PO-Revision-Date: 2008-09-01 10:21-0500\n"
"Last-Translator: പ്രവീണ്‍ അരിമ്പ്രത്തൊടിയില്‍ <pravi.a@gmail.com>\n"
"Language-Team: Swathanthra|സ്വതന്ത്ര Malayalam|മലയാളം Computing|കമ്പ്യൂട്ടിങ്ങ് <smc-"
"discuss@googlegroups.com>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. Type: title
#. Description
#. Info message displayed when running in rescue mode
#. :sl2:
#: ../rescue-check.templates:2001
msgid "Rescue mode"
msgstr "രക്ഷാ മോഡ്"

#. Type: text
#. Description
#. :sl1:
#: ../rescue-mode.templates:1001
msgid "Enter rescue mode"
msgstr "രക്ഷാ മോഡില്‍ പ്രവേശിക്കുക"

#. Type: text
#. Description
#. :sl3:
#: ../rescue-mode.templates:2001
msgid "No partitions found"
msgstr "ഭാഗങ്ങളൊന്നും കണ്ടില്ല"

#. Type: text
#. Description
#. :sl3:
#: ../rescue-mode.templates:2001
msgid ""
"The installer could not find any partitions, so you will not be able to "
"mount a root file system. This may be caused by the kernel failing to detect "
"your hard disk drive or failing to read the partition table, or the disk may "
"be unpartitioned. If you wish, you may investigate this from a shell in the "
"installer environment."
msgstr ""
"ഇന്‍സ്റ്റാളറിനു് ഒരു ഭാഗങ്ങളും കണ്ടുപിടിയ്ക്കാന്‍ സാധിച്ചില്ല, അതുകൊണ്ടു് തന്നെ ഒരു റൂട്ട് ഫയല്‍ സിസ്റ്റം "
"മൌണ്ട് ചെയ്യുവാന്‍ നിങ്ങള്‍ക്കു് സാധ്യമല്ല. ഇതിനുള്ള കാരണം കെര്‍ണല്‍ നിങ്ങളുടെ ഹാര്‍ഡ് ഡിസ്ക് ഡ്രൈവ് "
"കണ്ടുപിടിയ്ക്കുന്നതിലോ ഭാഗങ്ങളുടെ പട്ടിക വായിയ്ക്കുന്നതിലോ പരാ‍യപ്പെട്ടതോ അല്ലെങ്കില്‍ ഡിസ്ക് "
"വിഭജിയ്ക്കാത്തതാണെന്നോ ആകാം. നിങ്ങള്‍ക്കു് വേണമെങ്കില്‍ ഇതു് ഇന്‍സ്റ്റാളറിന്റെ പരിസരത്തിലുള്ള ഒരു "
"ഷെല്‍ ഉപയോഗിച്ചു് കൂടുതല്‍ അന്വേഷണം നടത്താവുന്നതാണു്."

#. Type: select
#. Description
#. :sl2:
#: ../rescue-mode.templates:3001
msgid "Device to use as root file system:"
msgstr "റൂട്ട് ഫയല്‍ സിസ്റ്റത്തിനായി ഉപയോഗിക്കേണ്ട ഉപകരണം:"

#. Type: select
#. Description
#. :sl2:
#: ../rescue-mode.templates:3001
msgid ""
"Enter a device you wish to use as your root file system. You will be able to "
"choose among various rescue operations to perform on this file system."
msgstr ""
"നിങ്ങള്‍ റൂട്ട് ഫയല്‍ സിസ്റ്റത്തിനായി ഉപയോഗിക്കാനാഗ്രഹിക്കുന്ന ഉപകരണം നല്‍കിയുക. നിങ്ങള്‍ക്കു് ഈ ഫയല്‍ "
"സിസ്റ്റത്തില്‍ ചെയ്യുന്നതിനായി പല വിധത്തിലുള്ള രക്ഷാപ്രവര്‍ത്തനങ്ങളില്‍ നിന്നും തെരഞ്ഞെടുക്കാന്‍ കഴിയും."

#. Type: error
#. Description
#. :sl3:
#: ../rescue-mode.templates:4001
msgid "No such device"
msgstr "അങ്ങനെയൊരുപകരണമില്ല"

#. Type: error
#. Description
#. :sl3:
#: ../rescue-mode.templates:4001
msgid ""
"The device you entered for your root file system (${DEVICE}) does not exist. "
"Please try again."
msgstr ""
"നിങ്ങള്‍ റൂട്ട് ഫയല്‍ സിസ്റ്റത്തിനായി നല്‍കിയ ഉപകരണം (${DEVICE}) നിലവിലില്ല. ദയവായി വീണ്ടും "
"ശ്രമിയ്ക്കുക."

#. Type: error
#. Description
#. :sl3:
#: ../rescue-mode.templates:5001
msgid "Mount failed"
msgstr "മൌണ്ട് പരാജയപ്പെട്ടു"

#. Type: error
#. Description
#. :sl3:
#: ../rescue-mode.templates:5001
msgid ""
"An error occurred while mounting the device you entered for your root file "
"system (${DEVICE}) on /target."
msgstr ""
"നിങ്ങള്‍ റൂട്ട് ഫയല്‍ സിസ്റ്റത്തിനായി നല്‍കിയ ഉപകരണം (${DEVICE}) /target ല്‍ മൌണ്ട് ചെയ്യുന്നതില്‍ "
"ഒരു തെറ്റ് സംഭവിച്ചു."

#. Type: error
#. Description
#. :sl3:
#: ../rescue-mode.templates:5001
msgid "Please check the syslog for more information."
msgstr "ദയവായി കൂടുതല്‍ വിവരങ്ങള്‍ക്കായി syslog പരിശോധിക്കുക."

#. Type: select
#. Description
#: ../rescue-mode.templates:6001
msgid "Rescue operations"
msgstr "രക്ഷാ ദൌത്യങ്ങള്‍"

#. Type: error
#. Description
#. :sl3:
#: ../rescue-mode.templates:7001
msgid "Rescue operation failed"
msgstr "രക്ഷാ ദൌത്യം പരാജയപ്പെട്ടു"

#. Type: error
#. Description
#. :sl3:
#: ../rescue-mode.templates:7001
msgid "The rescue operation '${OPERATION}' failed with exit code ${CODE}."
msgstr "'${OPERATION}' എന്ന രക്ഷാ ദൌത്യം ${CODE} എന്ന എക്സിറ്റ് കോഡോടുകൂടി പരാജയപ്പെട്ടു."

#. Type: text
#. Description
#. :sl2:
#: ../rescue-mode.templates:8001
msgid "Execute a shell in ${DEVICE}"
msgstr "${DEVICE} ല്‍ ഒരു ഷെല്‍ പ്രവര്‍ത്തിപ്പിയ്ക്കുക"

#. Type: text
#. Description
#. :sl2:
#: ../rescue-mode.templates:9001
msgid "Execute a shell in the installer environment"
msgstr "ഇന്‍സ്റ്റാളറിനു്റെ പരിസ്ഥിതിയില്‍ ഒരു ഷെല്‍ പ്രവര്‍ത്തിപ്പിയ്ക്കുക"

#. Type: text
#. Description
#. :sl2:
#: ../rescue-mode.templates:10001
msgid "Choose a different root file system"
msgstr "വ്യത്യസ്തമായ ഒരു റൂട്ട് ഫയല്‍ സിസ്റ്റം തെരഞ്ഞെടുക്കുക"

#. Type: text
#. Description
#. :sl2:
#: ../rescue-mode.templates:11001
msgid "Reboot the system"
msgstr "സിസ്റ്റം റീബൂട്ട് ചെയ്യുക"

#. Type: text
#. Description
#. :sl2:
#. Type: text
#. Description
#. :sl2:
#. Type: text
#. Description
#. :sl2:
#: ../rescue-mode.templates:12001 ../rescue-mode.templates:16001
#: ../rescue-mode.templates:17001
msgid "Executing a shell"
msgstr "ഒരു ഷെല്‍ പ്രവര്‍ത്തിപ്പിച്ചുകൊണ്ടിരിയ്ക്കുന്നു"

#. Type: text
#. Description
#. :sl2:
#: ../rescue-mode.templates:12001
msgid ""
"After this message, you will be given a shell with ${DEVICE} mounted on \"/"
"\". If you need any other file systems (such as a separate \"/usr\"), you "
"will have to mount those yourself."
msgstr ""
"ഈ സന്ദേശത്തിനു ശേഷം ${DEVICE} \"/\" ല്‍ മൌണ്ട് ചെയ്തിട്ടുള്ള ഒരു ഷെല്‍ നിങ്ങള്‍ക്കു് "
"കിട്ടുന്നതായിരിയ്ക്കും. (വേറിട്ട \"/usr\" പോലെയുള്ള) വേറെ ഏതെങ്കിലും ഫയല്‍ സിസ്റ്റങ്ങള്‍ "
"നിങ്ങള്‍ക്കാവശ്യമുണ്ടെങ്കില്‍ അവ നിങ്ങള്‍ സ്വയം മൌണ്ട് ചെയ്യേണ്ടതായി വരും."

#. Type: error
#. Description
#. :sl3:
#: ../rescue-mode.templates:13001
msgid "Error running shell in /target"
msgstr "/target ല്‍ ഷെല്‍ പ്രവര്‍ത്തിപ്പിക്കുന്നതില്‍ തെറ്റ്"

#. Type: error
#. Description
#. :sl3:
#: ../rescue-mode.templates:13001
msgid ""
"A shell (${SHELL}) was found on your root file system (${DEVICE}), but an "
"error occurred while running it."
msgstr ""
"റൂട്ട് ഫയല്‍ സിസ്റ്റത്തില്‍ (${DEVICE}) ഒരു ഷെല്‍(${SHELL}) കണ്ടിട്ടുണ്ടായിരുന്നു, പക്ഷേ അതു് "
"പ്രവര്‍ത്തിപ്പിക്കുന്ന സമയത്തു് ഒരു തെറ്റ് പറ്റി."

#. Type: error
#. Description
#. :sl3:
#: ../rescue-mode.templates:14001
msgid "No shell found in /target"
msgstr "/target ല്‍ ഷെല്ലൊന്നും കണ്ടില്ല"

#. Type: error
#. Description
#. :sl3:
#: ../rescue-mode.templates:14001
msgid "No usable shell was found on your root file system (${DEVICE})."
msgstr "റൂട്ട് ഫയല്‍ സിസ്റ്റത്തില്‍ (${DEVICE}) ഉപയോഗസാധുവായ ഷെല്ലൊന്നും കണ്ടില്ല."

#. Type: text
#. Description
#. :sl2:
#: ../rescue-mode.templates:15001
msgid "Interactive shell on ${DEVICE}"
msgstr "${DEVICE} ല്‍ ഒരു സംവദിയ്ക്കാവുന്ന ഷെല്‍ പ്രവര്‍ത്തിപ്പിയ്ക്കുക"

#. Type: text
#. Description
#. :sl2:
#: ../rescue-mode.templates:16001
msgid ""
"After this message, you will be given a shell with ${DEVICE} mounted on \"/"
"target\". You may work on it using the tools available in the installer "
"environment. If you want to make it your root file system temporarily, run "
"\"chroot /target\". If you need any other file systems (such as a separate "
"\"/usr\"), you will have to mount those yourself."
msgstr ""
"ഈ സന്ദേശത്തിനു ശേഷം ${DEVICE} \"/target\" ല്‍ മൌണ്ട് ചെയ്തിട്ടുള്ള ഒരു ഷെല്‍ നിങ്ങള്‍ക്കു് "
"കിട്ടുന്നതായിരിയ്ക്കും. ഇന്‍സ്റ്റോളര്‍ പരിസ്ഥിതിയില്‍ ലഭ്യമായിട്ടുള്ള ടൂളുകള്‍ ഉപയോഗിച്ചു് നിങ്ങള്‍ക്കു് "
"പണിയെടുക്കാവുന്നതാണു്. നിങ്ങള്‍ക്കു് താത്കാലികമായി ഇതിനെ റൂട്ട് ഫയല്‍ സിസ്റ്റമാക്കാന്‍ \"chroot /"
"target\" എന്നു് പ്രവര്‍ത്തിപ്പിക്കാം. (വേറിട്ട \"/usr\" പോലെയുള്ള) വേറെ ഏതെങ്കിലും ഫയല്‍ "
"സിസ്റ്റങ്ങള്‍ നിങ്ങള്‍ക്കാവശ്യമുണ്ടെങ്കില്‍ അവ നിങ്ങള്‍ സ്വയം മൌണ്ട് ചെയ്യേണ്ടതായി വരും."

#. Type: text
#. Description
#. :sl2:
#: ../rescue-mode.templates:17001
msgid ""
"After this message, you will be given a shell in the installer environment. "
"Since the installer could not find any partitions, no file systems have been "
"mounted for you."
msgstr ""
"ഈ സന്ദേശത്തിനു ശേഷം ഇന്‍സ്റ്റോളറിന്റെ പരിസരമുള്ള ഒരു ഷെല്‍ നിങ്ങള്‍ക്കു് കിട്ടുന്നതായിരിയ്ക്കും. "
"ഇന്‍സ്റ്റോളറിനു് ഭാഗങ്ങളൊന്നു് കാണാനാവാത്തതു് കൊണ്ടു് നിങ്ങള്‍ക്കായി ഫയല്‍ സിസ്റ്റങ്ങളൊന്നും ചേര്‍ത്തിട്ടില്ല"

#. Type: text
#. Description
#. :sl2:
#: ../rescue-mode.templates:18001
msgid "Interactive shell in the installer environment"
msgstr "ഇന്‍സ്റ്റാളറിന്റെ പരിസരമുള്ള ഒരു ഷെല്‍ പ്രവര്‍ത്തിപ്പിയ്ക്കുക"

#. Type: password
#. Description
#. :sl2:
#: ../rescue-mode.templates:19001
msgid "Passphrase for ${DEVICE}:"
msgstr "${DEVICE} നായുള്ള അടയാളവാക്കു്:"

#. Type: password
#. Description
#. :sl2:
#: ../rescue-mode.templates:19001
msgid "Please enter the passphrase for the encrypted volume ${DEVICE}."
msgstr "ദയവായി ${DEVICE} എന്ന എന്‍ക്രിപ്റ്റ് ചെയ്ത വാള്യത്തിനുള്ള അടയാള വാക്യം നല്‍കുക."

#. Type: password
#. Description
#. :sl2:
#: ../rescue-mode.templates:19001
msgid ""
"If you don't enter anything, the volume will not be available during rescue "
"operations."
msgstr "നിങ്ങളൊന്നും നല്‍കിയില്ലെങ്കില്‍ രക്ഷാപ്രവര്‍ത്തനത്തില്‍ വാള്യം ലഭ്യമായിരിയ്ക്കുകയില്ല."
