# THIS FILE IS GENERATED AUTOMATICALLY FROM THE D-I PO MASTER FILES
# The master files can be found under packages/po/
#
# DO NOT MODIFY THIS FILE DIRECTLY: SUCH CHANGES WILL BE LOST
#
# Simplified Chinese translation for Debian Installer.
#
# Copyright (C) 2003-2008 Software in the Public Interest, Inc.
# This file is distributed under the same license as debian-installer.
#
# Translated by Yijun Yuan (2004), Carlos Z.F. Liu (2004,2005,2006),
# Ming Hua (2005,2006,2007,2008) and Xiyue Deng (2008).
#
msgid ""
msgstr ""
"Project-Id-Version: debian-installer\n"
"Report-Msgid-Bugs-To: debian-boot@lists.debian.org\n"
"POT-Creation-Date: 2008-01-26 07:32+0000\n"
"PO-Revision-Date: 2008-06-21 00:37+0800\n"
"Last-Translator: Kov Chai <tchaikov@gmail.com>\n"
"Language-Team: Simplified Chinese <debian-chinese-gb@lists.debian.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. Type: select
#. Description
#: ../elilo-installer.templates:1001
msgid "Partition for boot loader installation:"
msgstr "可供安装引导管理器的分区："

#. Type: select
#. Description
#: ../elilo-installer.templates:1001
msgid ""
"Partitions currently available in your system are listed. Please choose the "
"one you want elilo to use to boot your new system."
msgstr ""
"以下是您的系统中可用的分区。请选择其中之一用来安装 elilo 以引导新系统。"

#. Type: error
#. Description
#: ../elilo-installer.templates:2001
msgid "No boot partitions detected"
msgstr "没有探测到启动分区"

#. Type: error
#. Description
#: ../elilo-installer.templates:2001
msgid ""
"There were no suitable partitions found for elilo to use.  Elilo needs a "
"partition with a FAT file system, and the boot flag set."
msgstr ""
"没有找到 elilo 适用的分区。Elilo 需要一个使用 FAT 文件系统的分区，并且要设有 "
"boot 标志。"

#. Type: text
#. Description
#. Main menu item
#: ../elilo-installer.templates:3001
msgid "Install the elilo boot loader on a hard disk"
msgstr "安装 elilo 启动引导器至硬盘"

#. Type: text
#. Description
#: ../elilo-installer.templates:4001
msgid "Installing the ELILO package"
msgstr "正在安装 ELILO 软件包"

#. Type: text
#. Description
#: ../elilo-installer.templates:5001
msgid "Running ELILO for ${bootdev}"
msgstr "为 ${bootdev} 运行 ELILO"

#. Type: boolean
#. Description
#: ../elilo-installer.templates:6001
msgid "ELILO installation failed.  Continue anyway?"
msgstr "安装 ELILO 失败。仍然继续安装？"

#. Type: boolean
#. Description
#: ../elilo-installer.templates:6001
msgid ""
"The elilo package failed to install into /target/.  Installing ELILO as a "
"boot loader is a required step.  The install problem might however be "
"unrelated to ELILO, so continuing the installation may be possible."
msgstr ""
"试图把 elilo 软件包安装到 /target/ 失败。安装 ELILO 作为启动程序是一个必需步"
"骤。但是这个安装中的问题可能和 ELILO 并不相关，所以继续安装还是可能的。"

#. Type: error
#. Description
#: ../elilo-installer.templates:7001
msgid "ELILO installation failed"
msgstr "安装 ELILO 失败"

#. Type: error
#. Description
#: ../elilo-installer.templates:7001
msgid "Running \"/usr/sbin/elilo\" failed with error code \"${ERRCODE}\"."
msgstr "运行“/usr/sbin/elilo”失败，返回错误代码“${ERRCODE}”。"
