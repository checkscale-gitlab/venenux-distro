<!-- original version: 56428 -->

 <chapter id="d-i-intro"><title>Het Debian installatiesysteem gebruiken</title>

 <sect1><title>Hoe het installatiesysteem werkt</title>
<para>

Het Debian installatiesysteem bestaat uit een aantal modules die
zijn ontwikkeld om een specifieke taak uit te voeren tijdens de installatie.
Elke module voert zijn taak uit, waarbij vragen kunnen worden gesteld
aan de gebruiker als dat voor die taak nodig is. Aan de vragen zelf is
een prioriteit toegekend, en de prioriteit van de vragen die zullen worden
gesteld wordt ingesteld bij het opstarten van het installatiesysteem.

</para><para>

Als een standaard installatie wordt uitgevoerd, zullen alleen essentiële
vragen (met prioriteit HIGH) worden gesteld. Het resultaat is een in hoge
mate geautomatiseerd installatieproces met weinig interactie met de
gebruiker. Modules worden vanzelf in de juiste volgorde uitgevoerd;
welke modules worden uitgevoerd wordt hoofdzakelijk bepaald door de
gekozen installatiemethode en door uw apparatuur. Het installatiesysteem
zal standaard antwoorden gebruiken voor vragen die niet worden gesteld.

</para><para>

Als er een probleem optreedt, wordt een scherm met een foutboodschap getoond
en kan het menu van het installatiesysteem worden getoond zodat de gebruiker
een alternatieve actie kan selecteren. Als er geen problemen zijn, zal de
gebruiker het menu van het installatiesysteem nooit zien, maar eenvoudig de
vragen voor iedere opeenvolgende module kunnen beantwoorden. Meldingen van
kritische fouten hebben een prioriteit CRITICAL waardoor de gebruiker altijd
geïnformeerd zal worden.
<!-- FJP Prioriteiten in caps (gebeurt ook later in tekst) //-->

</para><para>

Enkele van de standaard waarden die het installatiesysteem gebruikt, kunnen
worden gestuurd door opstartparameters mee te geven bij het starten van &d-i;.
Als u bijvoorbeeld statische netwerkconfiguratie wilt forceren (standaard wordt
DHCP gebruikt indien dit beschikbaar is), kunt u de opstartparameter
<userinput>netcfg/disable_dhcp=true</userinput> toevoegen. Zie
<xref linkend="installer-args"/> voor een overzicht van beschikbare opties.

</para><para>

Het is mogelijk dat gevorderde gebruikers zich meer thuis voelen met een
menugestuurde interface, waarbij niet het installatiesysteem automatisch
elke stap in volgorde uitvoert maar elke stap door de gebruiker zelf
wordt bepaald. Om het installatiesysteem op een handmatige, menugestuurde
manier uit te voeren, moet de opstartparameter
<userinput>priority=medium</userinput> worden toegevoegd.

</para><para>

Als het voor uw apparatuur noodzakelijk is om parameters mee te geven bij de
installatie van kernelmodules, dan zal u het installatiesysteem moeten starten
in de <quote>expert</quote> modus. Dit is mogelijk door voor het opstarten
van het installatiesysteem ofwel het commando <command>expert</command> te
gebruiken ofwel de opstartparameter <userinput>priority=low</userinput>
toe te voegen. De expert modus geeft u volledige controle over &d-i;.

</para><para condition="gtk">

Voor dit platform ondersteunt &d-i; twee verschillende gebruikersinterfaces:
een tekstuele en een graphische. Tenzij u in het initiële opstartmenu kiest
voor <quote>Graphical install</quote> zal de tekstuele interface worden
gebruikt. Zie <xref linkend="graphical"/> voor nadere informatie over de
grafische versie van het installatiesysteem.

</para><para condition="not-gtk">

Voor dit platform gebruikt het installatiesysteem een tekstuele
gebruikersinterface. Een grafische gebruikersinterface is momenteel niet
beschikbaar.

</para><para>

Voor de tekstuele gebruikersinterface wordt gebruik van een muis niet
ondersteund. De volgende toetsen kunnen worden gebruikt om te navigeren binnen
de diverse configuratieschermen. Om <quote>vooruit</quote> of
<quote>terug</quote> te gaan tussen getoonde knoppen of selecties, gebruikt ui
de toetsen <keycap>Tab</keycap> of pijl <keycap>rechts</keycap>, respectievelijk
de <keycombo> <keycap>Shift</keycap> <keycap>Tab</keycap> </keycombo> of pijl
<keycap>links</keycap>. Met de pijlen
<keycap>omhoog</keycap> en <keycap>omlaag</keycap> selecteert u de verschillende
regels in een schuifbare lijst en verschuift u ook de lijst zelf. Daarnaast
kunt u, bij lange lijsten, een letter intoetsen waardoor meteen het deel van de
lijst zal worden getoond waarvan de regels met die letter beginnen. Ook kunt
u met <keycap>Pagina&nbsp;omhoog</keycap> en <keycap>Pagina&nbsp;omlaag</keycap>
de lijst per pagina verschuiven. Met de <keycap>spatiebalk</keycap> selecteert u
opties, bijvoorbeeld bij een keuzevak. Gebruik &enterkey; om keuzes te activeren.

</para><para arch="s390">

S/390 ondersteunt geen virtuele consoles. U kunt echter een tweede en derde
ssh sessie openen om de hieronder beschreven logboeken te bekijken.

</para><para>

Foutmeldingen worden omgeleid naar de vierde console.
U kunt toegang krijgen tot deze console door
op <keycombo><keycap>linker Alt</keycap><keycap>F4</keycap></keycombo>
te drukken (houd de linker <keycap>Alt</keycap> toets vast terwijl u de
<keycap>F4</keycap> functietoets indrukt); u keert terug naar het hoofd
installatiescherm met
<keycombo><keycap>linker Alt</keycap><keycap>F1</keycap></keycombo>.

</para><para>

Deze meldingen kunnen ook worden teruggevonden in
<filename>/var/log/syslog</filename>. Na de installatie wordt dit logbestand
gekopieerd naar <filename>/var/log/installer/syslog</filename> op uw
nieuwe systeem. Andere installatiemeldingen kunnen tijdens de installatie worden
gevonden in <filename>/var/log/</filename> en, nadat de computer opnieuw is
opgestart met het nieuwe systeem, in <filename>/var/log/installer/</filename>.

</para>
 </sect1>


 <sect1 id="modules-list"><title>Introductie van de modules</title>
<para>

Hieronder vindt u een lijst van de modules van het installatiesysteem
met een korte beschrijving van het doel van elke module. Details over
het gebruik van een specifieke module staan in <xref linkend="module-details"/>.
<!-- FJP Toevoegen dat welke modules verplicht/optioneel zijn? //-->

</para>

<variablelist>
<varlistentry>

<term>main-menu</term><listitem><para>
De module 'main-menu' (hoofdmenu) toont tijdens de installatie de
lijst met modules aan de gebruiker en start een module wanneer deze
geselecteerd wordt. De vragen van deze module hebben prioriteit MEDIUM;
dit betekent dat u het menu niet zult zien als u de prioriteit voor
de installatie heeft ingesteld op HIGH of CRITICAL (HIGH is de
standaardwaarde). Indien er echter een fout optreedt waarvoor uw
interventie nodig is, kan de prioriteit tijdelijk worden verlaagd om
u in de gelegenheid te stellen het probleem op te lossen en in dat
geval kan dus het menu alsnog verschijnen.

</para><para>

U kunt het hoofdmenu van het installatiesysteem bereiken door herhaaldelijk
de &BTN-GOBACK; knop te selecteren, waardoor u stapsgewijs teruggaat
uit de op dat moment actieve module.

</para></listitem>
</varlistentry>
<varlistentry>

<term>localechooser</term><listitem><para>

Stelt de gebruiker in staat om de lokalisatie te bepalen voor zowel
de installatie als het geïnstalleerde systeem: taal, locatie en
<quote>locale</quote>. Tijdens de installatie zullen boodschappen in de
gekozen taal worden getoond, tenzij de vertaling voor die taal niet
compleet is. Als een vertaling niet compleet is, worden de boodschappen
in het Engels getoond.

</para></listitem>
</varlistentry>
<varlistentry>

<term>kbd-chooser</term><listitem><para>

De module 'kbd-chooser' (toetsenbordkiezer) toont een lijst van toetsenborden
waaruit de gebruiker het model kan kiezen dat overeenkomt met zijn toetsenbord.

</para></listitem>
</varlistentry>
<varlistentry>

<term>hw-detect</term><listitem><para>

Detecteert automatisch de meeste hardware in het systeem, inclusief
netwerkkaarten, harde schijven en PCMCIA.

</para></listitem>
</varlistentry>
<varlistentry>

<term>cdrom-detect</term><listitem><para>

Zoekt naar en koppelt een Debian installatie-CD.

</para></listitem>
</varlistentry>
<varlistentry>

<term>netcfg</term><listitem><para>

Configureert de netwerkverbindingen van de computer zodat deze verbinding kan maken
met het Internet.

</para></listitem>
</varlistentry>
<varlistentry>

<term>iso-scan</term><listitem><para>

Zoekt op harde schijven naar ISO-images (<filename>.iso</filename> bestanden).

</para></listitem>
</varlistentry>
<varlistentry>

<term>choose-mirror</term><listitem><para>

Deze module toont een overzicht van spiegelservers met het Debian Archief.
De gebruiker kan hiermee de bron voor de installatie van Debian pakketten kiezen.

</para></listitem>
</varlistentry>
<varlistentry>

<term>cdrom-checker</term><listitem><para>

Controleert de integriteit van een CD. Op die manier kan een gebruiker
zich ervan verzekeren dat de installatie-CD niet beschadigd is.

</para></listitem>
</varlistentry>
<varlistentry>

<term>lowmem</term><listitem><para>

De module 'lowmem' probeert te signaleren wanneer een systeem slechts over
beperkt intern geheugen beschikt en doet vervolgens verschillende trucs om
niet strict noodzakelijke onderdelen van &d-i; te verwijderen (ten koste van
enige functionaliteit).

</para></listitem>
</varlistentry>
<varlistentry>

<term>anna</term><listitem><para>

Het acroniem 'anna' staat voor "Anna's Not Nearly APT". Deze module installeert
pakketten die zijn opgehaald vanaf de gekozen mirror of CD.
<!-- FJP Niet alle pakketten; alleen het installatiesysteem? //-->

</para></listitem>
</varlistentry>
<varlistentry>

<term>clock-setup</term><listitem><para>

Stelt de interne systeem clock in en bepaalt of de deze is ingesteld op
UTC of niet.

</para></listitem>
</varlistentry>
<varlistentry>

<term>tzsetup</term><listitem><para>

Selecteert de tijdzone op basis van de eerder geselecteerde locatie.

</para></listitem>
</varlistentry>
<varlistentry>

<term>partman</term><listitem><para>

De module 'partman' (partitie-manager) stelt de gebruiker in staat om
harde schijven die met het systeem verbonden zijn, in te delen, bestandssystemen
te creëren op geselecteerde partities en deze aan te sluiten op koppelpunten.
Daarnaast kent 'partman' interessante functies als het volledig geautomatiseerd
indelen van een harde schijf en ondersteuning van LVM. Voor Debian geniet 'partman'
de voorkeur als hulpmiddel voor het indelen van harde schijven.
<!-- FJP Ook op andere media dan harde schijven (b.v. USB memory sticks)? //-->

</para></listitem>
</varlistentry>
<varlistentry>

<term>partitioner</term><listitem><para>

Maakt het mogelijk om harde schijven in te delen die zijn aangesloten op het
systeem. Er wordt een schijfindelingsprogramma gekozen dat past bij het platform
waartoe uw computer behoort.

</para></listitem>
</varlistentry>
<varlistentry>

<term>partconf</term><listitem><para>

Toont een overzicht van partities en creëert bestandssystemen op de
geselecteerde partities volgens de instructies van de gebruiker.

</para></listitem>
</varlistentry>
<varlistentry>

<term>lvmcfg</term><listitem><para>

Helpt de gebruiker met het configureren van de <firstterm>LVM</firstterm>
('Logical Volume Manager').

</para></listitem>
</varlistentry>
<varlistentry>

<term>mdcfg</term><listitem><para>

Stelt de gebruiker in staat om een Software <firstterm>RAID</firstterm>
('Redundant Array of Inexpensive Disks') te configureren. Een softwarematige RAID
is veelal beter dan de goedkope (semi-hardwarematige) IDE-stuurapparaten voor RAID
die op sommige nieuwere moederborden voorkomen.
<!-- FJP: Origineel: superb is volgens mij niet juist in deze context //-->

</para></listitem>
</varlistentry>
<varlistentry>

<term>base-installer</term><listitem><para>

Installeert een basisset van pakketten die het mogelijk maken om de
computer, nadat deze is herstart, onder Linux te laten functioneren.

</para></listitem>
</varlistentry>
<varlistentry>

<term>user-setup</term><listitem><para>

Stelt het wachtwoord voor het <quote>root</quote> account in en voegt een
account voor een normale gebruiker toe.

</para></listitem>
</varlistentry>
<varlistentry>

<term>apt-setup</term><listitem><para>

Verzorgt de configuratie van apt. Dit gebeurt grotendeels automatisch op
basis van de gebruikte installatiemethode.

</para></listitem>
</varlistentry>
<varlistentry>

<term>pkgsel</term><listitem><para>

Maakt gebruik van <classname>tasksel</classname> voor de selecte en
installatie van aanvullende programmatuur.

</para></listitem>
</varlistentry>
<varlistentry>

<term>os-prober</term><listitem><para>

Zoekt naar reeds op de computer aanwezige besturingssystemen en geeft
deze informatie door aan de module 'bootloader-installer'. Afhankelijk
van de gebruikte opstartlader, kan deze de gevonden besturingssystemen
vervolgens toevoegen aan het menu van de opstartlader. Op deze manier
kan de gebruiker tijdens het opstarten van de computer op een eenvoudige
manier kiezen welk besturingssysteem moet worden gestart.

</para></listitem>
</varlistentry>
<varlistentry>

<term>bootloader-installer</term><listitem><para>

Installeert een opstartlader (boot loader) programma op de harde schijf. Dit
is noodzakelijk om de computer onder Linux te laten opstarten zonder een
diskette of CD te gebruiken. Veel opstartladers kennen de mogelijkheid om
de gebruiker &mdash; elke keer dat de computer wordt opgestart &mdash; te
laten kiezen uit verschillende besturingssystemen.
De installatieprogramma's voor de diverse opstartladers installeren alle een
opstartlader op de harde schijf. Een opstartlader is noodzakelijk om de
computer onder Linux te laten opstarten zonder een diskette of CD te gebruiken.
Veel opstartladers kennen de mogelijkheid om de gebruiker &mdash; elke keer
dat de computer wordt opgestart &mdash; te laten kiezen uit alternatieve
besturingssystemen.

</para></listitem>
</varlistentry>
<varlistentry>

<term>shell</term><listitem><para>

Stelt de gebruiker in staat vanuit het menu of in de tweede console
een shell te starten.
<!-- FJP Vertaling 'shell'? Zie ook 'what-is-linux.xml' //-->
<!-- FJP Welke shell is beschikbaar? //-->

</para></listitem>
</varlistentry>
<varlistentry>

<term>save-logs</term><listitem><para>

Stelt de gebruiker in staat om, wanneer problemen worden tegengekomen,
informatie op een diskette, via het netwerk, op harde schijf of op een
ander medium te bewaren.
Hiermee kan later een nauwkeurig verslag van problemen met het
installatiesysteem worden gestuurd aan de Debian ontwikkelaars.

</para></listitem>
</varlistentry>

</variablelist>

 </sect1>

&using-d-i-components.xml;
&loading-firmware.xml;

</chapter>
