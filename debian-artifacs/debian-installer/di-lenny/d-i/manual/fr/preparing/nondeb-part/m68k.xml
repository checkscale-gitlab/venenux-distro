<!-- original version:  43576 -->


  <sect2 arch="m68k"><title>Partitionnement sur AmigaOS</title>
<para>

Si vous utilisez AmigaOS, vous pouvez vous servir du programme
<command>HDToolBox</command> pour partitionner votre disque avant
l'installation.

</para>
  </sect2>

  <sect2 arch="m68k"><title>Partitionnement d'un Atari TOS</title>
<para>

Les identificateurs de partition pour Atari sont formés de trois
caractères ASCII. Utilisez <quote>LNX</quote> pour les données et <quote>SWP</quote> pour les
partitions d'échange. Si vous utilisez une méthode d'installation
pour machine avec peu de mémoire, une petite partition Minix est aussi
nécessaire (environ 2&nbsp;Mo), pour laquelle l'identificateur est <quote>MNX</quote>.
Une erreur dans la configuration des identificateurs des partitions empêche
non seulement l'installateur de Debian de reconnaître les
partitions, mais fait aussi que le système d'exploitation
essaye d'utiliser les partitions Linux, ce qui perturbe le contrôleur
de disque et rend tout le disque inutilisable.

</para><para>

Il y a de nombreux outils de partitionnement disponibles (les outils
<command>harddisk</command> propres à Atari ne permettent pas de changer les
identificateurs de partitions)&nbsp;; ce manuel ne peut pas donner de
description détaillée pour chacun. La description ci-dessous ne concerne
que <command>SCSITool</command> (de Hard+Soft GmBH).

<orderedlist>
<listitem><para>

Démarrez <command>SCSITool</command> et choisissez le disque que vous voulez
partitionner (menu <guimenu>Disk</guimenu>, 
choix <guimenuitem>select</guimenuitem>)&nbsp;;

</para></listitem>
<listitem><para>

À partir du menu <guimenu>Partition</guimenu>, choisissez soit
<guimenuitem>New</guimenuitem> pour ajouter de nouvelles partitions ou 
changer la taille des partitions existantes, soit 
<guimenuitem>Change</guimenuitem> pour changer
une partition spécifique. Sauf si vous avez déjà ajouté de nouvelles
partitions avec la bonne taille et voulez seulement changer les
identificateurs, <guimenuitem>New</guimenuitem> est certainement le meilleur 
choix&nbsp;;

</para></listitem>
<listitem><para>

Pour l'option <guimenuitem>New</guimenuitem>, choisissez
<guilabel>existing</guilabel> dans la boîte de dialogue qui demande les
paramètres initiaux. La fenêtre suivante donne des informations
détaillées sur l'emplacement des partitions existantes ; la première colonne
de cette liste indique le type de la partition. Vous pouvez le modifier en cliquant
sur le champ du texte. Sauvez les changements en quittant la fenêtre avec le
bouton <guibutton>OK</guibutton>&nbsp;;

</para></listitem>
<listitem><para>

Pour l'option <guimenuitem>Change</guimenuitem>, choisissez la partition à 
modifier dans la liste de sélection, et choisissez 
<guilabel>other systems</guilabel> dans la boîte de dialogue. La fenêtre 
suivante donne des informations
détaillées sur l'emplacement de cette partition, et permet d'en changer
l'identificateur. Sauvez les changements en quittant la fenêtre avec le
bouton <guibutton>OK</guibutton>&nbsp;;

</para></listitem>
<listitem><para>

Inscrivez les noms Linux pour chacune de ces partitions qui seront
utilisées avec Linux &mdash; voir <xref linkend="device-names"/>&nbsp;;

</para></listitem>
<listitem><para>

Quittez <command>SCSITool</command> avec le choix 
<guimenuitem>Quit</guimenuitem> du menu <guimenu>File</guimenu>. 
L'ordinateur va redémarrer pour s'assurer que la
nouvelle table de partitions sera prise en compte par le système
d'exploitation. Si vous avez modifié des partitions pour le système
d'exploitation (TOS) ou l'environnement graphique (GEM), elles seront
rendues inopérantes et devront être réinitialisées&nbsp;; nous vous
avions prévenus qu'il fallait faire une sauvegarde, n'est-ce-pas&nbsp;?

</para></listitem>
</orderedlist>

</para><para>

Il y a un outil de partitionnement pour Linux/m68k appelé
<command>atari-fdisk</command> dans le système d'installation,
mais pour l'instant nous vous recommandons de partitionner votre disque en
utilisant un éditeur de partition TOS ou tout autre utilitaire disque. Si
votre éditeur de partition ne dispose pas d'option pour changer le
type de partition, vous pourrez faire cette étape cruciale à un moment
différent (à partir du RAMdisk au démarrage de l'installation).
<command>SCSITool</command> n'est qu'un des éditeurs de partition qui
supportent la sélection de type quelconque de partitions. Il y en a
d'autres, choisissez celui qui vous convient.

</para>
</sect2>

  <sect2 arch="m68k"><title>Partitionnement sous MacOS</title>
<para>

Les outils de partitionnement pour Macintosh que nous avons testés sont
<command>pdisk</command>, <command>HD SC Setup</command> 7.3.5 (Apple),
<command>HDT</command> 1.8 (FWB), <command>SilverLining</command> (LaCie) et
<command>DiskTool</command> (Tim Endres, GPL).
Des versions complètes sont nécessaires pour <command>HDT</command> et
<command>SilverLining</command>. L'utilitaire d'Apple nécessite un correctif 
pour reconnaître des disques étrangers (un descriptif expliquant comment
corriger <command>HD SC Setup</command> avec <command>ResEdit</command> peut 
être trouvé sur 
<ulink url="http://www.euronet.nl/users/ernstoud/patch.html"></ulink>).

</para><para>

Pour des Macs IDE, vous devez utiliser l'application
<command>Apple Drive Setup</command> pour libérer de la place pour les
partitions Linux, puis poursuivez le partitionnement sous Linux,
ou utilisez la version MacOS de pdisk disponible sur
<ulink url="http://homepage.mac.com/alk/downloads/pdisk.sit.hqx">Alsoft</ulink>.

</para>
</sect2>
