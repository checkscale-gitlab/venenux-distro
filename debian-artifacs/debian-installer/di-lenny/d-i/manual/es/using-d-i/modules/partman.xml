<?xml version="1.0" encoding="ISO-8859-1"?>
<!-- original version: 56425 -->
<!-- Revisado por Steve Langasek -->
<!-- Revisado por Rudy Godoy, 22 feb. 2005 -->
<!-- Revisado por Igor Tamara, enero 2007 -->

<para>

Si no se siente a gusto particionando, o simplemente quiere conocer m�s
detalles, lea el <xref linkend="partitioning"/>.

</para>
<warning arch="sparc"><para>
<!-- BTS: #384653 -->

El instalador puede no ser capaz de detectar el tama�o de la unidad
correctamente si el disco ya se ha utilizado previamente con
Solaris. No se arregla este problema creando una nueva tabla de
particiones. Lo que s� ayuda es poner a <quote>cero</quote> los
primeros sectores de la unidad:

<informalexample><screen>
# dd if=/dev/zero of=/dev/hd<replaceable>X</replaceable> bs=512 count=2; sync
</screen></informalexample>

Tenga en cuenta que hacer esto har� que los datos que existan en el
disco no vuelvan a ser accesibles.

</para></warning>
<para>

Primero se le dar� la oportunidad de particionar autom�ticamente todo
el disco o el espacio libre disponible en el disco. A esto tambi�n se
le llama particionado <quote>guiado</quote>. Si no quiere
autoparticionar, elija <guimenuitem>Manual</guimenuitem> en el men�.

</para>

  <sect3 id="partman-auto">
   <title>Particionado guiado</title>
<para>

Si elije el particionado guiado, puede tener tres opciones: crear las
particiones directamente en el disco duro (el m�todo cl�sico), utilizar el
Gestor de Vol�menes L�gicos (�Logical Volumen Manager�, LVM), o
crearlas utilizando un LVM cifrado<footnote>

<para>
El instalador cifrar� el grupo de vol�menes LVM con una clave AES de
256 bit y hace uso del soporte de <quote>dm-crypt</quote> en el n�cleo.
</para>

</footnote>.

</para>
<note><para>

La opci�n de usar LVM (cifrado) puede no estar disponible en todas las
arquitecturas.

</para></note>
<para>

Cuando utilice LVM o LVM cifrado el instalador crear� la mayor�a de
las particiones dentro de una partici�n si escoge la segunda opci�n,
la ventaja de este m�todo es que las particiones dentro de una
partici�n m�s grande pueden cambiarse de tama�o m�s adelante
relativamente f�cil. 
En el caso de LVM cifrado la partici�n m�s grande no ser� legible sin 
el conocimiento de una contrase�a especial, lo que da un seguridad
adicional en su datos (personales).
 
</para><para>

El instalador borra autom�ticamente el disco escribiendo datos
aleatorios en �l antes de utilizar LVM cifrado. Esto mejora la
seguridad (ya que hace imposible determinar qu� partes del disco se
est�n utilizando y tambi�n se asegura de que cualquier traza de
instalaci�n previa se borra). Sin embargo, esto puede llevar un tiempo
dependiendo del tama�o de su disco.

</para>
<note><para>

Si elige el particionado guiado con LVM o LVM cifrado, es posible que
algunos cambios tengan que escribirse en el disco seleccionado
mientras se termina la configuraci�n de LVM. Estos cambios borran de 
forma permanente todos los datos que existan en el disco que haya 
seleccionado y no podr�n deshacerse m�s adelante. El instalador, sin 
embargo, le pedir� que confirme estos cambios antes de escribirlos en 
disco.

</para></note>
<para>

Una vez haya elegido el particionado guiado, tanto en el caso del 
m�todo cl�sico como en el caso de LVM, se le pedir� primero que
seleccione el disco que quiere utilizar. Compruebe que todos sus discos
est�n en la lista y, si tiene m�s de un disco, aseg�rese de escoger el
disco correcto. Tenga en cuenta que el orden en el que est�n listados
los discos puede ser distinto del que est� acostumbrado. El tama�o de
los discos puede ayudarle a identificarlos.

</para><para>

Se borrar� cualquier dato en el disco que haya seleccionado, pero
siempre se le pedir� que confirme los cambios antes de escribirlos en
el disco. Si ha seleccionado el m�todo cl�sico de particionado podr�
deshacer los cambios hasta el final. Esto no es posible cuando utiliza
LVM (cifrado).

</para><para>

Posteriormente podr� escoger de algunos de los esquemas listados en la
tabla mostrada a continuaci�n.
Todos los esquemas tienen sus pros y sus
contras, algunos de �stos se discuten en <xref linkend="partitioning"/>. Si
no est� seguro, escoja el primero. Tenga en mente, que el particionado guiado
necesita un cierto espacio libre m�nimo para operar. Si no le asigna al menos
un 1 GB de espacio (depende del esquema seleccionado), el particionado guiado
fallar�.

</para>

<informaltable>
<tgroup cols="3">
<thead>
<row>
  <entry>Esquema de particionado</entry>
  <entry>Espacio m�nimo</entry>
  <entry>Particiones creadas</entry>
</row>
</thead>

<tbody>
<row>
  <entry>Todos los ficheros en una partici�n</entry>
  <entry>600 MB</entry>
  <entry><filename>/</filename>, intercambio</entry>
</row><row>
  <entry>Partici�n /home separada</entry>
  <entry>500 MB</entry>
  <entry>
    <filename>/</filename>, <filename>/home</filename>, intercambio
  </entry>
</row><row>
  <entry>Particiones /home, /usr, /var y /tmp separadas</entry>
  <entry>1 GB</entry>
  <entry>
    <filename>/</filename>, <filename>/home</filename>,
    <filename>/usr</filename>, <filename>/var</filename>,
    <filename>/tmp</filename>, intercambio
  </entry>
</row>

</tbody></tgroup></informaltable>

<para>
El instalador crear� una partici�n separada para <filename>/boot</filename> si
escoge el guiado particionado con LVM (cifrado). Todas las dem�s particiones,
incluyendo la partici�n de intercambio, se crear�n dentro de la partici�n LVM.

</para><para arch="ia64">

Si escoge el particionado autom�tico para su sistema IA-64, habr� una
partici�n adicional, con un sistema de ficheros FAT16 arrancable, para el
gestor de arranque EFI. Adem�s hay una opci�n adicional en el men� que le
permite dar formato a los discos para establecer manualmente una partici�n
como una partici�n de arranque EFI.

</para><para arch="alpha">

Si escoge el particionado autom�tico para su sistema Alpha, una
partici�n adicional, sin sistema de ficheros, ser� reservada al
principio del disco como espacio para el gestor de arranque aboot.

</para><para>

Despu�s de seleccionar un esquema, la siguiente pantalla le mostrar� la nueva
tabla de particiones, incluyendo a qu� particiones se dar� formato, c�mo, y
d�nde se montar�n.

</para><para>

La lista de particiones podr�a ser como la siguiente:

<informalexample><screen>
  IDE1 master (hda) - 6.4 GB WDC AC36400L
        #1 primary   16.4 MB  B f ext2       /boot
        #2 primary  551.0 MB      swap       swap
        #3 primary    5.8 GB      ntfs
           pri/log    8.2 MB      ESPACIO LIBRE

  IDE1 slave (hdb) - 80.0 GB ST380021A
        #1 primary   15.9 MB      ext3
        #2 primary  996.0 MB      fat16
        #3 primary    3.9 GB      xfs        /home
        #5 logical    6.0 GB    f ext3       /
        #6 logical    1.0 GB    f ext3       /var
        #7 logical  498.8 GB      ext3
        #8 logical  551.5 GB      swap       swap
        #9 logical   65.8 GB      ext2
</screen></informalexample>

Este ejemplo muestra dos discos duros IDE divididos en varias particiones, el
primer disco tiene algo de espacio libre. Cada l�nea de partici�n est�
conformada por el n�mero de partici�n, su tipo, tama�o, banderas opcionales,
sistema de ficheros y punto de montaje (si fuese el caso).
Nota: esta configuraci�n particular no puede crearse con el particionado guiado pero s� muestra una variaci�n que puede conseguirse utilizando el particionado manual.


</para><para>

Esto finaliza con el particionado guiado. Si est� satisfecho con la tabla de
particiones generada, puede elegir <guimenuitem>Finalizar el particionado y
escribir los cambios en el disco</guimenuitem> desde el men� para implementar
la nueva tabla de particiones (como se describe al final de esta secci�n). Si
no le gusta, puede elegir <guimenuitem>Deshacer los cambios realizados a las
particiones</guimenuitem>, para ejecutar nuevamente el particionado guiado o
modificar los cambios propuestos de forma manual tal y como se describe a
continuaci�n.

</para>
   </sect3>

   <sect3 id="partman-manual">
   <title>Particionado manual</title>
<para>

Una pantalla similar a la mostrada anteriormente se mostrar� si elige
particionar manualmente excepto que se mostrar� su partici�n actual sin los
puntos de montaje. Al final de esta secci�n se explica c�mo configurar 
manualmente sus particiones y el uso de �stas en su sistema Debian nuevo.

</para><para>

Si elige un disco nuevo que no tiene ni particiones o espacio libre en
�l, se le podr�a ofrecer la creaci�n de una nueva tabla de particiones (esto
es necesario para que pueda crear nuevas particiones). Despu�s de esto
una nueva l�nea titulada <quote>ESPACIO LIBRE</quote> deber� aparecer
bajo el disco seleccionado.

</para><para>

Si elije el espacio libre, tendr� la oportunidad crear nuevas particiones. Tendr�
que responder un conjunto de preguntas r�pidas sobre su tama�o,
tipo (primaria o l�gica) y ubicaci�n (al inicio o final del espacio libre).
Despu�s de esto, se le presentar� una perspectiva detallada sobre su
nueva partici�n. El valor principal es <guimenuitem>Utilizar
como:</guimenuitem>, que determina si la partici�n tendr� un sistema
de ficheros o se utilizar� como intercambio, RAID por software, LVM, un
sistema de ficheros cifrado, o no se utilizar�. Otras opciones
incluyen el punto de montaje, opciones de
montaje, bandera arrancable o tipo de uso. Las opciones que se
muestren dependen de c�mo se va a utilizar la partici�n.
Si no le gustan las opciones
predeterminadas, no dude en cambiarlas a su gusto. Por ejemplo,
si selecciona
la opci�n <guimenuitem>Usar como:</guimenuitem>, puede elegir un sistema
de ficheros distinto para esta partici�n, incluyendo opciones para
usar la partici�n como intercambio, RAID por software, LVM, o simplemente
no usarla. Otra caracter�stica interesante es la posibilidad de copiar
datos desde una partici�n existente a �sta.
Cuando est� satisfecho con su nueva partici�n, elija
<guimenuitem>Se ha terminado de definir la partici�n</guimenuitem> y regresar� a la
pantalla principal de <command>partman</command>.

</para><para>

Si decide que desea cambiar algo en su partici�n, simplemente elija
la partici�n, lo cual le conducir� al men� de configuraci�n de la
partici�n. Esta es la misma pantalla que cuando crea la
partici�n, as� que puede cambiar el mismo conjunto de opciones. Algo
que podr�a no ser muy obvio a primera vista, es que puede
redimensionar el tama�o de la partici�n seleccionando el elemento
que muestra el tama�o de �sta. Los sistemas de ficheros que
se conoce que funcionan con esta opci�n son por lo menos fat16, fat32,
ext2, ext3 y �swap�. Este men� tambi�n le permite eliminar una partici�n.

</para><para>

Aseg�rese de crear al menos dos particiones: una para el sistema de ficheros
<emphasis>ra�z</emphasis> (que debe montarse en <filename>/</filename>) y otra
para el <emphasis>espacio de intercambio</emphasis>. Si
olvida montar el sistema de ficheros ra�z, <command>partman</command> no le
dejar� continuar hasta que corrija esto.

</para><para arch="ia64">

Si olvida seleccionar y dar formato a una partici�n de arranque EFI
<command>partman</command> lo detectar� y no le dejar� continuar hasta que
habilite una.

</para><para>

Se pueden extender las capacidades de <command>partman</command> con m�dulos
para el instalador, pero dependen de la arquitectura de su sistema. As� que si
no est�n disponibles todas las funcionalidades que esperaba, compruebe que ha
cargado todos los m�dulos necesarios (p. ej. <filename>partman-ext3</filename>,
<filename>partman-xfs</filename>, o <filename>partman-lvm</filename>).

</para><para>

Cuando est� satisfecho con el particionado, seleccione <guimenuitem>Finalizar
el particionado y escribir los cambios en el disco</guimenuitem> del men� de
particionado. Se le presentar� un resumen de los cambios realizados en los
discos y se le pedir� confirmaci�n para crear los sistemas de ficheros
solicitados.

</para>

</sect3>
