<!-- retain these comments for translator revision tracking -->
<!-- original version: 43576 -->


  <sect2 arch="m68k"><title>Fer particions a l'AmigaOS</title>
<para>

Si esteu executant AmigaOS, podeu usar el programa <command>HDToolBox</command>
per ajustar les particions natives abans de la instal·lació.

</para>
  </sect2>

  <sect2 arch="m68k"><title>Fer particions a l'Atari TOS</title>
<para>

Els IDs de partició d'Atari son tres caràcters ASCII, usa <quote>LNX</quote>
per dades i <quote>SWP</quote> per particions swap. Si useu el mètode
d'instal·lació en memòria baixa, també necessitareu una petita partició
Minix (sobre 2 MiB), per la qual l'ID de partició serà <quote>MNX</quote>.
Un error al definir l'ID de partició apropiat no només impedeix que el
procés d'instal·lació de Debian reconeixi les particions, sinó que a
resultes de que TOS intenti usar les particions Linux, confon al controlador
del disc dur i el fa tot inaccessible.

</para><para>

Hi ha multitud d'eines de partició de tercers disponibles (la
utilitat <command>harddisk</command> d'Atari no permet canviar l'ID
de partició); aquest manual no dona descripcions detallades de totes
elles. La següent descripció cobreix <command>SCSITool</command> (de
Hard+Soft GmBH).

<orderedlist>
<listitem><para>

Inicieu <command>SCSITool</command> i seleccioneu el disc que voleu
partir (menú <guimenu>Disk</guimenu>, ítem
<guimenuitem>selecciona</guimenuitem>).

</para></listitem>
<listitem><para>

Des de el menú <guimenu>Partició</guimenu>, seleccioneu o bé
<guimenuitem>Nou</guimenuitem> per afegir noves particions o canviar la
mida de les particions existents, o <guimenuitem>Canvia</guimenuitem>
per canviar una partició específica. A menys que hàgiu creat particions
amb la mida correcte i només vulgueu canviar l'ID de la partició,
<guimenuitem>Nou</guimenuitem> és probablement, la millor opció.

</para></listitem>
<listitem><para>

Per la opció <guimenuitem>Nou</guimenuitem>, seleccioneu
<guilabel>existent</guilabel> en el quadre de diàleg sol·licitant els
paràmetres inicials. La següent finestra ens mostrarà una llista de
particions existents les quals podreu ajustar usant els botons de
desplaçament, o fent un clic a la barra. La primera columna a la llista
de particions és el tipus de partició; fes un clic al quadre de text
per editar-lo. Quan hàgiu acabat de canviar els paràmetres de la
partició ,graveu els canvis tancant la finestra amb el botó
<guibutton>OK</guibutton>.

</para></listitem>
<listitem><para>

Per la opció <guimenuitem>Canvia</guimenuitem>, seleccioneu la
partició a canviar a la llista de selecció, i seleccioneu
<guilabel>altres sistemes</guilabel> al quadre de diàleg. La següent
finestra mostra informació detallada sobre la localització d'aquesta
partició, i us permet canviar l'ID de partició. Graveu els canvis
tancant la finestra amb el botó <guibutton>OK</guibutton>.

</para></listitem>
<listitem><para>

Escriviu els noms Linux per cadascuna de les particions que heu creat o
canviat per usar-les amb Linux &mdash; veieu <xref linkend="device-names"/>.

</para></listitem>
<listitem><para>

Sortiu de <command>SCSITool</command> usant la opció
<guimenuitem>Surt</guimenuitem> del menú <guimenu>Arxiu</guimenu>.
L'ordinador es reiniciarà per fer efectius els canvis a la taula de
particions usada per TOS. Si heu canviat algunes particions TOS/GEM,
seran invalidades i hauran de ser re-inicialitzades (aconsellem fer
una copia de tot el que tingueu al disc, o no?).

</para></listitem>
</orderedlist>

</para><para>

Hi ha una eina per fer particions per Linux/m68k anomenada
<command>atari-fdisk</command> al sistema d'instal·lació, però per
ara recomanem que particioneu el disc usant l'editor de particions de
TOS o alguna eina de disc. Si el vostre editor de particions no té una
opció per editar el tipus de partició, podeu fer aquest pas crucial en
un pas posterior (des del disc RAM temporal d'instal·lació).
<command>SCSITool</command> és només un dels editors que coneixem que
suporten selecció arbitrària de tipus de partició. Poden haver-n'hi
d'altres; seleccioneu l'eina que satisfaci les vostres necessitats.

</para>
</sect2>

  <sect2 arch="m68k"><title>Fer particions a MacOS</title>
<para>

Les eines per fer particions per Macintosh provades inclouen
<command>pdisk</command>, <command>HD SC Setup</command> 7.3.5 (Apple),
<command>HDT</command> 1.8 (FWB), <command>SilverLining</command> (LaCie) i
<command>DiskTool</command> (Tim Endres, GPL). Versions completes són
requerides per <command>HDT</command> i <command>SilverLining</command>.
L'eina d'Apple requereix un pedaç per reconèixer discs d'altres fabricants
(una descripció de com apedaçar <command>HD SC Setup</command> usant
<command>ResEdit</command> el podeu trobar a
<ulink url="http://www.euronet.nl/users/ernstoud/patch.html"></ulink>).

</para><para>

Per Macs amb bus IDE, necessitareu usar <command>Apple Drive Setup</command>
per crear espai per les particions Linux i finalitzar el particionament
Linux, o useu la versió de MacOS de pdisk disponibles per descarregar a
<ulink url="http://homepage.mac.com/alk/downloads/pdisk.sit.hqx">Alsoft</ulink>.

</para>
</sect2>

