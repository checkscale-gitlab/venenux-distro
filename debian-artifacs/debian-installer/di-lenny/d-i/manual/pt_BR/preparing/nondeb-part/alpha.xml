<!-- retain these comments for translator revision tracking -->
<!-- original version: 43576 -->
<!-- revised by Felipe Augusto van de Wiel (faw) 2005.12.31 -->
<!-- updated 28997:43576 by Felipe Augusto van de Wiel (faw) 2007.01.14 -->


  <sect2 arch="alpha"><title>Particionamento no UNIX Tru64</title>
<para>

O UNIX Tru64, formalmente conhecido como Digital UNIX, que é
conhecido como OSF/1, usa um esquema de particionamento parecido com o
<quote>volume de discos</quote> do BSD, que lhe permite até ter oito partições
por unidade de disco. As partições são numeradas de <quote>1</quote> a
<quote>8</quote> no Linux e classificadas com as letras de <quote>a</quote> até
<quote>h</quote> no UNIX. Nos kernels do Linux 2.2 e superiores, a
correspondência é sempre <quote>1</quote> para <quote>a</quote>,
<quote>2</quote> para <quote>b</quote> e assim por diante. Por exemplo,
<filename>rz0e</filename> no UNIX tru64 será chamado de
<filename>sda5</filename> no Linux.

</para><para>

As partições em um volume de disco Tru64 podem se sobrescrever.
No entanto, caso este disco seja usado pelo Tru64, a partição
<quote>c</quote> é requerida para sobrescrever todo o disco (assim,
sobrescrevendo todas as outras partições não vazias). Sob o Linux, isto torna
<filename>sda3</filename> idêntico a <filename>sda</filename>
(<filename>sdb3</filename> a
<filename>sdb</filename>, se presente, e assim por diante). No entanto,
a ferramenta de particionamento partman, usada pelo &d-i;, atualmente
não trabalha com partições que se sobrepõem. Como resultado, não é
recomendado compartilhar um disco entre o Tru64 e a Debian. As partições
em discos Tru64 podem ser montadas sob a Debian após a instalação ser
completada.

</para><para>

Outro requerimento controverso é da partição <quote>a</quote> iniciar a partir
do início do disco, assim ela sempre incluirá o bloco de partida
com o volume de disco. Se tiver a intenção de iniciar a Debian a
partir deste disco, você precisará de um tamanho de pelo menos 2MB
para o aboot e talvez um kernel.
Note que essa partição é somente requerida por compatibilidade; você não
deverá colocar lá um sistema de arquivos, ou terá perda de dados.

</para><para>

É possível e razoável compartilhar uma partição swap entre o UNIX e
Linux. Neste caso, será preciso executar um <command>mkswap</command>
na partição cada vez que os sistema for reiniciado do UNIX no
Linux, pois o UNIX danificará a assinatura da partição swap. Talvez
você queira executar o <command>mkswap</command> a partir dos scripts de
inicialização do Linux antes de adicionar o espaço para a partição swap com
o comando <command>swapon -a</command>.

</para><para>

Se desejar montar partições UNIX sob o Linux, note que o Digital UNIX
pode usar dois tipos de sistemas de arquivos diferentes, UFS e AdvFS no
qual o Linux somente entende o formal.

</para>
  </sect2>

  <sect2 arch="alpha"><title>Particionamento sob o Windows NT</title>

<para>

O Windows NT utiliza a tabela de partições do estilo PC. Se estiver
manipulando partições FAT ou NTFS existentes, é recomendado que use as
ferramentas nativas do Windows NT (ou mais convenientemente, você
também poderá reparticionar seu disco através do menu de
configuração do AlphaBIOS). Caso contrário, não será realmente
necessário particionar através do Windows; as ferramentas de
particionamento do Linux geralmente farão um bom trabalho. Note que
quando você executar o NT, o Administrador de Discos pode se oferecer
para gravar uma <quote>assinatura inofensiva</quote> nos discos não Windows se
tiver algum.
<emphasis>Nunca</emphasis> o permita fazer isto, pois esta assinatura poderá
destruir informações da partição.

</para><para>

Se planeja inicializar o Linux através de uma console
ARC/AlphaBIOS/ARCSBIOS, você precisará de uma partição
(pequena) para o MILO. 5MB é o suficiente. Caso o Windows NT seja
instalado, sua partição de inicialização de 6MB deverá servir este
propósito. A Debian &releasename; não suporta a instalação do MILO.
Se já possui o MILO instalado em seu sistema ou instalou o
MILO através de outra mídia, a Debian ainda pode ser inicializada
através do ARC.

</para>
  </sect2>
