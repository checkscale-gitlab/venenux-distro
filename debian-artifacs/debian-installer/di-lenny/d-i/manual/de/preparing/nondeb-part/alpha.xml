<!-- retain these comments for translator revision tracking -->
<!-- original version: 43576 -->


  <sect2 arch="alpha"><title>Partitionieren in Tru64 UNIX</title>
<para>

Tru64 UNIX, ehemals bekannt als Digital UNIX, welches wiederum vorher
als OSF/1 bekannt war, verwendet ein Partitionsschema, das den
BSD <quote>disk labels</quote> ähnlich ist, welches bis zu acht Partitionen
pro Laufwerk erlaubt. Die Partitionen werden in Linux von <quote>1</quote>
bis <quote>8</quote> durchnummeriert und in UNIX von <quote>a</quote> bis
<quote>h</quote> <quote>durchbuchstabiert</quote>. Im Linux-Kernel ab Version
2.2 gilt: <quote>1</quote> entspricht <quote>a</quote>, <quote>2</quote>
entspricht <quote>b</quote> und so weiter. Zum Beispiel wird
<filename>rz0e</filename> aus Tru64 UNIX unter Linux wahrscheinlich mit
<filename>sda5</filename> benannt.

</para><para>

Partitionen in einem Tru64-Disk-Label dürfen überlappen. Wenn Tru64 diese
Platte benutzt, ist es sogar nötig, dass die <quote>c</quote>-Partition die
ganze Platte umfasst (also alle nicht-leeren Partitionen enthält). 
Unter Linux macht das <filename>sda3</filename> identisch zu
<filename>sda</filename> (sowie <filename>sdb3</filename> zu
<filename>sdb</filename>, wenn vorhanden, und so weiter). Allerdings
kann partman, das Partitionierungswerkzeug des &d-i;, derzeit
nicht mit überlappenden Partitionen umgehen. Deswegen ist es im Moment
nicht empfehlenswert, Festplatten gleichzeitig unter Tru64 und Debian zu
nutzen. Partitionen auf Tru64-Platten können aber unter Debian ins Dateisystem
eingebunden werden, wenn die Installation abgeschlossen ist.

</para><para>

Eine andere übliche Anforderung ist, dass die <quote>a</quote>-Partition am
Anfang der Festplatte beginnt, so dass Sie immer den Boot-Block mit dem
Disk-Label enthält. Wenn Sie vorhaben, Debian von dieser Festplatte zu booten,
müssen Sie mindestens 2MB hierfür einplanen, damit mindestens aboot und
gegebenenfalls ein Kernel darauf passen. Beachten Sie jedoch, dass diese
Partition nur aus Kompatibilitätsgründen vorhanden ist; legen Sie darauf kein
Dateisystem an, um Daten zu speichern, sonst riskieren Sie Datenverlust.

</para><para>

Es ist möglich und sogar sinnvoll, eine Swap-Partition unter UNIX und Linux
gemeinsam zu benutzen. In diesem Fall muss jedoch <command>mkswap</command> bei
jedem Start von Linux ausgeführt werden, wenn UNIX vorher gelaufen hat, da UNIX
die Signatur der Swap-Partition beschädigt. Sie können <command>mkswap</command>
über die Linux-Start-Up-Skripte ausführen, bevor Sie den Swap mit
<command>swapon -a</command> aktivieren.

</para><para>

Wenn Sie UNIX-Partitionen unter Linux einbinden möchten, müssen Sie beachten,
dass Digital UNIX zwei verschiedene Dateisysteme, UFS und AdvFS, benutzen
kann, von denen Linux jedoch nur UFS versteht.

</para>
  </sect2>

  <sect2 arch="alpha"><title>Partitionierung unter Windows NT</title>

<para>

Windows NT benutzt eine Partitionstabelle nach dem PC-Stil.
Wenn Sie eine existierende FAT- oder NTFS-Partition verändern wollen, wird
empfohlen, die nativen Windows NT-Werkzeuge (oder konventioneller das AlphaBIOS
Setup-Menü) zu verwenden. Ansonsten ist es nicht wirklich notwendig,
von Windows aus zu partitionieren; die Linux-Partitionsprogramme führen
diese Arbeit im Normalfall besser durch. Beachten Sie: wenn Sie Windows NT
starten, wird der Disk-Administrator möglicherweise anbieten, eine
<quote>harmlose Signatur</quote> auf nicht-Windows-Festplatten zu schreiben,
falls Sie welche haben. Lassen Sie dies <emphasis>niemals</emphasis> zu, da
diese Signatur die Partitionsinformationen zerstören wird!

</para><para>

Wenn Sie vorhaben, Linux von einer ARC/AlphaBios/ARCSBIOS-Konsole aus
zu starten, benötigen Sie eine (kleine) FAT-Partition für MILO. 5MB
reichen hierfür aus. Wenn Windows NT installiert ist, kann dessen 6MB große
Bootstrap-Partition hierfür verwendet werden. Debian &releasename-cap;
unterstützt jedoch nicht die Installation von MILO. Wenn MILO bei Ihnen bereits
installiert ist oder Sie MILO von einem anderen Medium installieren, kann
Debian aber von ARC gebootet werden.

</para>
  </sect2>
