<!-- retain these comments for translator revision tracking -->
<!-- $Id: tzsetup.xml 50620 2007-12-25 09:22:03Z fjp $ -->

<!-- As tzsetup is invoked from inside clock-setup, this is not a
     separate section -->

<para>

Depending on the location selected earlier in the installation process,
you may be shown a list of timezones relevant for that location.
If your location has only one time zone, you will not be asked anything and
the system will assume that time zone.

</para><para>

If for some reason you wish to set a time zone for the installed system
that does <emphasis>not</emphasis> match the selected location, there are
two options.

</para>

<orderedlist>
<listitem>

<para>

The simplest option is to just select a different timezone after the
installation has been completed and you've booted into the new system.
The command to do this is:

<informalexample><screen>
# dpkg-reconfigure tzdata
</screen></informalexample>

</para>

</listitem><listitem>

<para>

Alternatively, the time zone can be set at the very start of the
installation by passing the parameter
<userinput>time/zone=<replaceable>value</replaceable></userinput>
when you boot the installation system. The value should of course be a
valid time zone, for example <userinput>Europe/London</userinput> or
<userinput>UTC</userinput>.

</para>

</listitem>
</orderedlist>

<para>

For automated installations the time zone can also be set using preseeding.

</para>
