# THIS FILE IS GENERATED AUTOMATICALLY FROM THE D-I PO MASTER FILES
# The master files can be found under packages/po/
#
# DO NOT MODIFY THIS FILE DIRECTLY: SUCH CHANGES WILL BE LOST
#
# Spanish messages for debian-installer.
# Copyright (C) 2003-2007 Software in the Public Interest, Inc.
# This file is distributed under the same license as debian-installer.
#
# Contributors to the translation of debian-installer:
# Teófilo Ruiz Suárez <teo@debian.org>, 2003.
# David Martínez Moreno <ender@debian.org>, 2003, 2005.
# Carlos Alberto Martín Edo <carlos@dat.etsit.upm.es>, 2003
# Carlos Valdivia Yagüe <valyag@dat.etsit.upm.es>, 2003
# Rudy Godoy <rudy@kernel-panik.org>, 2003-2006
# Steve Langasek <vorlon@debian.org>, 2004
# Enrique Matias Sanchez (aka Quique) <cronopios@gmail.com>, 2005
# Rubén Porras Campo <nahoo@inicia.es>, 2005
# Javier Fernández-Sanguino <jfs@debian.org>, 2003-2007
#
# Equipo de traducción al español, por favor lean antes de traducir
# los siguientes documentos:
#
# - El proyecto de traducción de Debian al español
#   http://www.debian.org/intl/spanish/
#   especialmente las notas de traducción en
#   http://www.debian.org/intl/spanish/notas
#
# - La guía de traducción de po's de debconf:
#   /usr/share/doc/po-debconf/README-trans
#   o http://www.debian.org/intl/l10n/po-debconf/README-trans
#
# Si tiene dudas o consultas sobre esta traducción consulte con el último
# traductor (campo Last-Translator) y ponga en copia a la lista de
# traducción de Debian al español (debian-l10n-spanish@lists.debian.org)
#
# NOTAS: 
#
# - Se ha traducido en este fichero 'boot loader' de forma homogénea por
# 'cargador de arranque' aunque en el manual se utiliza éste término y
# también 'gestor de arranque'
#
# - 'array' no está traducido aún. La traducción como 'arreglo' suena
# fatal (y es poco conocida)
#
#  
msgid ""
msgstr ""
"Project-Id-Version: debian-installer\n"
"Report-Msgid-Bugs-To: debian-boot@lists.debian.org\n"
"POT-Creation-Date: 2008-07-10 22:49+0000\n"
"PO-Revision-Date: 2008-09-06 22:55+0200\n"
"Last-Translator: Javier Fernández-Sanguino Peña <jfs@debian.org>\n"
"Language-Team:  Debian Spanish <debian-l10n-spanish@lists.debian.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. Type: text
#. Description
#. :sl2:
#: ../media-retriever.templates:1001
msgid "Scanning removable media"
msgstr "Analizando el medio extraíble"

#. Type: text
#. Description
#. :sl2:
#: ../media-retriever.templates:2001
msgid "Cannot read removable media, or no drivers found."
msgstr ""
"No se pudo leer el medio extraíble o no se encontró ningún controlador."

#. Type: text
#. Description
#. :sl2:
#: ../media-retriever.templates:2001
msgid ""
"There was a problem reading data from the removable media. Please make sure "
"that the right media is present. If you continue to have trouble, your "
"removable media might be bad."
msgstr ""
"Se produjo un error al leer los datos del disco extraíble. Por favor, "
"asegúrese de que ha insertado el medio correcto. Si continúa teniendo "
"problemas puede tratarse de un dispositivo defectuoso."

#. Type: boolean
#. Description
#: ../load-media.templates:1001
msgid "Load drivers from removable media now?"
msgstr "¿Desea cargar ahora los controladores de un medio extraíble?"

#. Type: boolean
#. Description
#: ../load-media.templates:1001
msgid ""
"You probably need to load drivers from removable media before continuing "
"with the installation. If you know that the install will work without extra "
"drivers, you can skip this step."
msgstr ""
"Probablemente necesite cargar controladores desde un medio extraíble antes "
"de continuar con la instalación. Puede saltarse este paso si está seguro de "
"que el proceso funcionará sin ningún controlador adicional."

#. Type: boolean
#. Description
#: ../load-media.templates:1001
msgid ""
"If you do need to load drivers, insert the appropriate removable media, such "
"as a driver floppy or USB stick before continuing."
msgstr ""
"Inserte el medio extraíble apropiado (un disquete o un dispositivo USB) "
"antes de continuar si desea cargar controladores."

#. Type: text
#. Description
#. main-menu
#: ../load-media.templates:2001
msgid "Load drivers from removable media"
msgstr "Cargando controladores de un medio extraíble"

#. Type: boolean
#. Description
#: ../load-media.templates:3001
msgid "Unknown removable media. Try to load anyway?"
msgstr "Medio extraíble desconocido. ¿Desea cargar de todas formas?"

#. Type: boolean
#. Description
#: ../load-media.templates:3001
msgid ""
"Detected removable media that is not a known driver media. Please make sure "
"that the correct media is inserted. You can still continue if you have an "
"unofficial removable media you want to use."
msgstr ""
"El medio extraíble detectado no es un medio conocido con controladores. Por "
"favor, asegúrese de que ha insertado el dispositivo apropiado correcto. "
"Puede continuar si tiene un medio extraíble no oficial que desea utilizar."

#. Type: text
#. Description
#: ../load-media.templates:4001
msgid "Please insert ${DISK_LABEL} ('${DISK_NAME}') first."
msgstr "Por favor introduzca primero ${DISK_LABEL} ('${DISK_NAME}')."

#. Type: text
#. Description
#: ../load-media.templates:4001
msgid ""
"Due to dependencies between packages, drivers must be loaded in the correct "
"order."
msgstr ""
"Los controladores deben cargarse en el orden apropiado debido a las "
"dependencias entre paquetes."

#. Type: boolean
#. Description
#: ../load-media.templates:5001
msgid "Load drivers from another removable media?"
msgstr "¿Desea cargar los controladores de otro medio extraíble?"

#. Type: boolean
#. Description
#: ../load-media.templates:5001
msgid ""
"To load additional drivers from another removable media, please insert the "
"appropriate removable media, such as a driver floppy or USB stick before "
"continuing."
msgstr ""
"Inserte el medio extraíble correcto (como un disquete o un dispositivo USB) "
"antes de continuar si desea cargar controladores adicionales de otro medio "
"extraíble."
