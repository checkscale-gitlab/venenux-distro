<!-- original version: 56322 -->

 <sect1 id="non-debian-partitioning">
 <title>Partitionnement préalable d'une machine multisystème</title>
<para>

Partitionner votre disque dur est simplement le fait de le diviser
en plusieurs sections. Chaque section est alors indépendante des
autres. C'est en gros équivalent à ajouter des cloisons dans une
maison&nbsp;: ajouter des meubles dans une pièce n'affecte pas les
autres pièces.

</para><para arch="s390">

Dans cette section, vous devez remplacer les occurrences de
<quote>disque</quote> par mini-disque DASD ou VM du monde &arch-title;.
De même, une machine signifie une LPAR ou VM.

</para><para>

Si vous avez déjà un système d'exploitation sur votre machine

<phrase arch="x86">
(Windows 9x, Windows NT/2000/XP, OS/2, MacOS, Solaris, FreeBSD&hellip;)
</phrase>

<phrase arch="alpha">
(Tru64 (Digital UNIX), OpenVMS, Windows NT, FreeBSD&hellip;)
</phrase>

<phrase arch="s390">
(VM, z/OS, OS/390&hellip;)
</phrase>

et si vous désirez placer Linux sur le même disque, vous devrez repartitionner
ce disque. Debian a besoin de partitions spécifiques. Il
ne peut pas être installé sur des partitions Windows ou MacOS. Il peut
partager des partitions avec d'autres systèmes Linux, mais ce point n'est
pas abordé ici. Vous devez attribuer au moins une partition à la
racine du système Debian.

</para><para>

Vous pouvez trouver des informations sur le partitionnement actuel
en utilisant un outil approprié pour votre système d'exploitation<phrase arch="x86">,
tel que fdisk ou PartitionMagic</phrase><phrase arch="powerpc">, 
tel que Drive Setup, HD Toolkit ou MacTools</phrase><phrase arch="m68k">, 
tel que HD SC Setup, HDToolBox ou SCSITool</phrase><phrase arch="s390">, 
tel que le diskmap VM</phrase>. Les outils de partitionnement permettent 
toujours de montrer l'état actuel sans rien modifier.

</para><para>

Modifier une partition contenant déjà un système de fichiers détruit ces fichiers.
Vous devriez donc toujours faire des sauvegardes avant tout partitionnement. En continuant
l'analogie avec la maison, avant d'abattre une cloison, vous déplaceriez certainement
les meubles afin de ne pas les endommager.

</para>

<para arch="hppa" condition="FIXME">

<emphasis>FIXME: write about HP-UX disks?</emphasis>

</para><para>

Si votre ordinateur possède plusieurs disques, vous pouvez en affecter un à Debian.
Ainsi, vous n'aurez pas besoin de partitionner le disque avant de commencer
l'installation&nbsp;: le partitionneur inclus dans l'installateur le fera très bien.

</para><para>

Si votre ordinateur ne possède qu'un seul disque, et si vous désirez
remplacer complètement le système d'exploitation actuel par 
Debian GNU/Linux, vous pouvez aussi attendre d'être dans le processus 
d'installation pour partitionner le disque (voyez la <xref linkend="di-partition"/>). 
Cependant, cela ne fonctionnera que si vous commencez l'installation à partir
d'une bande, d'un cédérom ou de fichiers sur une machine connectée. En effet,
si vous démarrez à partir de fichiers sur le disque dur, puis
partitionnez ce disque dur pendant l'installation, vous effacerez les
fichiers de démarrage&nbsp;: il vaut mieux alors que l'installation se
déroule sans accroc ! Si vous voulez vraiment procéder ainsi, vous devez
vous assurer qu'il existe un moyen de restaurer la machine en cas de
problème, par exemple avec les bandes ou les cédéroms d'installation du système
actuel.

</para><para>

Si votre machine possède déjà plusieurs partitions, et si suffisamment de
place peut être obtenue en effaçant ou remplaçant une ou plusieurs de
ces partitions, alors vous pouvez aussi attendre et utiliser le
programme de partitionnement de l'installateur Debian.
Vous devriez néanmoins continuer de lire les paragraphes suivants, car
il existe certains cas qui obligent à partitionner avant de
démarrer l'installation.

</para><para arch="x86">
Si votre machine possède un système de fichiers FAT ou NTFS, utilisé par DOS 
et Windows, vous pouvez attendre et utiliser le 
partitionneur de l'installateur Debian pour redimensionner le système de 
fichiers FAT.
</para>
<para>

Dans tous les autres cas, vous aurez besoin de partitionner votre disque
dur avant de démarrer l'installation afin de créer de l'espace
disponible sur lequel les partitions pour &debian; seront créées.
Si certaines de ces partitions sont attribuées à un autre système
d'exploitation, il vaut mieux utiliser le programme de partitionnement
de ce système d'exploitation. Dans ce cas, nous vous recommandons de 
<emphasis>ne pas</emphasis> essayer de créer des partitions Linux Debian avec 
ces outils.
Il faut seulement créer les partitions dédiées à l'autre système
d'exploitation.

</para><para>

Si vous installez plusieurs systèmes d'exploitation sur la même
machine, vous devriez commencer par les autres systèmes avant d'installer
Linux. Windows ainsi que d'autres installateurs de système d'exploitation
peuvent vous empêcher de démarrer Linux, ou vous encourager à reformater les
partitions non reconnues.

</para><para>

Vous pouvez annuler ces actions ou les éviter, mais installer en premier 
le système natif vous épargnera des problèmes.

</para><para arch="powerpc">

Pour qu'OpenFirmware puisse démarrer automatiquement &debian;,
les partitions Linux doivent apparaître avant toute autre partition
sur le disque, en particulier avant les partitions d'amorçage de MacOS.
Il faut absolument garder ceci à l'esprit lorsque l'on partitionne le
disque&nbsp;; vous devez créer une place pour les partitions Linux
<emphasis>avant</emphasis> les autres partitions amorçables du disque (les 
petites partitions dédiées aux contrôleurs de disques Apple ne sont pas 
amorçables). Vous pourrez supprimer cet espace libre avec les outils de 
partitionnement Linux durant la phase d'installation, et le remplacer par des 
partitions Linux.

</para><para>

Si vous avez actuellement un seul disque dur avec une seule partition (situation
courante pour les ordinateurs de bureau), et si vous voulez pouvoir
démarrer Debian ainsi que d'autres systèmes d'exploitation, vous aurez
besoin de&nbsp;:

  <orderedlist>
<listitem><para>

sauvegarder tout ce qui se trouve sur l'ordinateur&nbsp;;

</para></listitem>
<listitem><para>

démarrer depuis le support d'installation (du système d'exploitation 
actuellement installé), par exemple un cédérom ou des bandes&nbsp;;

<phrase arch="powerpc">Si vous démarrez depuis un cédérom MacOS, maintenez la
touche <keycap>c</keycap> appuyée pendant le démarrage pour forcer le cédérom 
à devenir le système actif pour MacOS)&nbsp;;</phrase>

</para></listitem>
<listitem><para>

utiliser les outils de partitionnement natifs pour créer soit de nouvelles 
partitions, soit de l'espace libre pour Debian GNU/Linux&nbsp;;

</para></listitem>
<listitem><para>

installer le système d'exploitation natif sur sa nouvelle partition&nbsp;;

</para></listitem>
<listitem><para>

démarrer ce système pour vérifier que tout fonctionne, et pour télécharger 
les fichiers de démarrage pour l'installateur Debian&nbsp;;
</para></listitem>
<listitem><para>

démarrer l'installateur Debian pour commencer à installer Debian.

</para></listitem>
</orderedlist>

</para>

&nondeb-part-alpha.xml;
&nondeb-part-x86.xml;
&nondeb-part-m68k.xml;
&nondeb-part-sparc.xml;
&nondeb-part-powerpc.xml;

 </sect1>
