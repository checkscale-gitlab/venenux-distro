<!-- retain these comments for translator revision tracking -->
<!-- original version: 43696 -->

 <sect1 id="pre-install-bios-setup">
 <title>Hardware- und Betriebssystem-Setup vor der Installation</title>
<para>

Dieses Kapitel wird Sie durch die Hardware-Einstellungen vor der Installation
leiten, die Sie eventuell machen müssen, bevor Sie Debian installieren.
Dies beinhaltet das Überprüfen und eventuell Ändern von Firmware-Einstellungen
für Ihr System. Die <quote>Firmware</quote> ist die von der Hardware genutzte
interne Software; sie ist meistens höchst kritisch in den Boot-Prozess involviert
(direkt nach dem Einschalten). Bekannte Hardware-Probleme, die die
Betriebssicherheit von &debian; auf Ihrem System beeinträchtigen könnten,
werden hier ebenfalls behandelt.

</para>

&bios-setup-i386.xml; 
&bios-setup-m68k.xml; 
&bios-setup-powerpc.xml; 
&bios-setup-sparc.xml; 
&bios-setup-s390.xml; 

  <sect2 arch="m68k;x86;powerpc" id="hardware-issues">
  <title>Hardware-Probleme, auf die Sie achten sollten</title>

<para arch="m68k">

Atari-TT-RAM-Boards sind berühmt-berüchtigt für RAM-Probleme unter Linux;
wenn Ihnen seltsame Symptome unterkommen, versuchen Sie, zumindest den Kernel
im ST-RAM laufen zu lassen. Amiga-Nutzer müssen vielleicht einen Teil
des RAMs ausschließen (benutzen Sie dafür ein <quote>booter memfile</quote>).

<phrase condition="FIXME"><emphasis>

FIXME: more description of this needed.

</emphasis></phrase>

</para>

   <formalpara arch="x86">
   <title>USB-Bios-Support und Tastaturen</title>
<para>

Falls Sie keine AT-Tastatur haben, sondern lediglich ein USB-Modell, müssen Sie
eventuell <quote>Legacy AT keyboard emulation</quote> im BIOS aktivieren. Tun
Sie dies nur, wenn das Installationssystem Ihre Tastatur im USB-Modus nicht
nutzen kann. Umgekehrt kann es auf einige Systemen (speziell Laptops) auch
nötig sein, <quote>Legacy USB Support</quote> zu deaktivieren, wenn die
Tastatur nicht funktioniert. Konsultieren Sie das Handbuch Ihres Mainboards und
schauen Sie im BIOS nach Optionen für <quote>Legacy keyboard emulation</quote>
oder <quote>USB keyboard support</quote>.

</para>
   </formalpara>

   <formalpara arch="powerpc">
   <title>Bildschirmanzeige auf OldWorld Powermac-Systemen</title>

<para>
Bei einigen OldWorld-Powermacs (an erster Stelle sind hier die zu nennen, die
den <quote>control</quote>-Bildschirm-Treiber verwenden) wird unter Linux
möglicherweise keine zuverlässige Farbdarstellung erreicht, wenn das Display
auf mehr als 256 Farben konfiguriert ist.
Falls Sie nach dem Neustart solche Probleme bei Ihrem Display feststellen
(manchmal können Sie noch eine Anzeige auf dem Monitor erkennen, aber in
anderen Fällen sehen Sie vielleicht gar nichts) oder falls der Monitor nach
dem Start des Installers nur ein schwarzes Bild anzeigt statt dem
Benutzerbildschirm, versuchen Sie, unter MacOS die Bildschirmeinstellungen
so einzustellen, dass 256 Farben verwendet werden statt Tausende
(<quote>Thousands</quote>) oder Millionen (<quote>Millions</quote>).

</para>
   </formalpara>
  </sect2>
 </sect1>
