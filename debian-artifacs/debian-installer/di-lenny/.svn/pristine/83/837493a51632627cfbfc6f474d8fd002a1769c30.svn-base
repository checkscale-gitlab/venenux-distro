<?xml version="1.0" encoding="ISO-8859-1"?>
<!-- original version: 45405 -->
<!-- traducido por ikks, mar 2007 -->
<!-- revisado por jfs, mar 2007 -->

 <sect1 id="pppoe" arch="not-s390">
 <title>Instalaci�n de &debian; a trav�s de PPP sobre Ethernet (PPPoE)</title>

<para>

El protocolo PPP sobre Ethernet (PPPoE) para banda ancha (ADSL o cable)
es muy com�n en algunos pa�ses cuando se establecen conexiones con un
proveedor de servicio de internet. Si bien la configuraci�n de la red a trav�s
de PPPoE no se puede hacer en el instalador, puede 
hacerse funcionar de manera muy sencilla. Esta secci�n explica c�mo.

</para><para>

La configuraci�n de la conexi�n PPPoE que se realice durante la instalaci�n
tambi�n estar� disponible al reiniciar en el sistema instalado (consulte
<xref linkend="boot-new"/>).

</para><para>

Necesitar� instalar con una imagen de CD-ROM/DVD para poder contar con la
opci�n de configurar y usar PPPoE durante la instalaci�n. No
est� habilitada en otros m�todos de instalaci�n (p.ej. en el arranque por
red<phrase condition="supports-floppy-boot"> o diskettes</phrase>).

</para><para>

La instalaci�n a trav�s de PPPoE es casi id�ntica que cualquier otra 
instalaci�n. Las diferencias se explican en los pasos descritos a
continuaci�n.

</para>

<itemizedlist>
<listitem><para>

Arranque el instalador con <userinput>modules=ppp-udeb</userinput> como
par�metro de arranque. Esto significa que en el indicador del sistema 
del arranque usted deber�a teclear:

<informalexample><screen>
install modules=ppp-udeb
</screen></informalexample>

o, si prefiere utilizar el instalador gr�fico:

<informalexample><screen>
installgui modules=ppp-udeb
</screen></informalexample>

Esta opci�n har� que se cargue y se ejecute autom�ticamente
componente responsable de la configuraci�n de PPPoE
(<classname>ppp-udeb</classname>).

</para></listitem>
<listitem><para>

Siga los pasos iniciales usuales de instalaci�n: selecci�n de 
idioma, pa�s y mapa de teclado, carga de componentes adicionales
del instalador<footnote>

<para>

En este paso se carga el componente <classname>ppp-udeb</classname> como 
uno de los componentes adicionales. Si quiere instalar utilizando la prioridad
media o baja (modo experto), puede seleccionar manualmente el componente
<classname>ppp-udeb</classname> en lugar de indicar expl�citamente
el par�metro <quote>modules</quote> en el indicador de sistema del
arranque.

</para>

</footnote>).

</para></listitem>
<listitem><para>

El siguiente paso es la detecci�n de hardware de red, que permite identificar
cualquier tarjeta Ethernet presente en su sistema.

</para></listitem>
<listitem><para>

Despu�s de esto comienza la configuraci�n espec�fica de PPPoE.  El
instalador probar� todas las interfaces Ethernet detectadas tratando de
encontrar un concentrador PPPoE (un tipo de servidor que maneja las
conexiones PPPoE).

</para><para>

<!-- jfs: Esta opci�n No aparece en el PO del instalador ? -->
Es probable que el concentrador no se encuentre en el primer intento.
Puede suceder usualmente en redes lentas o sobrecargadas o con servidores
poco fiables. La mayor�a de veces tendr� �xito un segundo intento de detecci�n de
un concentrador. Para volver a intentar, seleccione en el men�
principal del instalador <guimenuitem>Configurar e iniciar 
una conexi�n PPPoE</guimenuitem>.

</para></listitem>
<listitem><para>

Despu�s de haber encontrado el concentrador, se solicitar� al usuario
teclear su informaci�n de autenticaci�n (el usuario y la clave de PPPoE).

</para></listitem>
<listitem><para>

En este punto el instalador usar� la informaci�n proporcionada para establecer
una conexi�n PPPoE. Si se proporcion� la informaci�n correcta, deber�a
configurarse la conexi�n PPPoE y el instalador deber�a ser capaz de usarla
para conectarse a Internet y descargar los paquetes a trav�s de esta (en caso
de que fuera necesario). El instalador se detendr� si la informaci�n de
autenticaci�n no es correcta o si se produce cualquier error,
pero se podr� intentar de nuevo la configuraci�n seleccionando la opci�n
<guimenuitem>Configurar e iniciar una conexi�n PPPoE</guimenuitem> del men�.

</para></listitem>
</itemizedlist>

 </sect1>
