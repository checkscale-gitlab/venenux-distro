<!-- original version: 52960 -->

 <sect1 id="memory-disk-requirements">
<title>Mémoire et espace disque nécessaires</title>

<para>
Vous devez posséder au moins &minimum-memory; de mémoire vive
et &minimum-fs-size; d'espace disque. 
Ce sont vraiment des valeurs minimales. Pour une estimation plus réaliste,
voyez <xref linkend="minimum-hardware-reqts"/>.

</para>
<para>

L'installation sur des systèmes avec moins de mémoire <footnote condition="gtk">

<para>
L'installateur graphique demande plus de mémoire que l'installateur texte. Il ne doit pas
être utilisé avec des systèmes avec une quantité de mémoire inférieure à &minimum-memory;.
Si un choix est proposé, il vaut mieux sélectionner l'installateur texte.
</para>
</footnote>
ou avec moins d'espace disque disponible est encore possible mais cela demande des
utilisateurs expérimentés.
</para>
<para arch="m68k">
Sur l'Amiga, la taille de la FastRAM est en rapport avec la mémoire totale 
nécessaire. De plus, l'utilisation de cartes Zorro avec de la mémoire 16 bits 
n'est pas possible&nbsp;; vous aurez besoin de mémoire 32 bits. Vous pouvez 
utiliser le programme <command>amiboot</command> pour désactiver la mémoire 
16 bits&nbsp;; voir la <ulink url="&url-m68k-faq;">FAQ Linux/m68k</ulink>. 
Les noyaux récents désactivent automatiquement la mémoire 16 bits.
</para>

<para arch="m68k">
Sur l'Atari, aussi bien la ST-RAM que la Fast RAM (TT-RAM) sont utilisées par 
Linux. De nombreux utilisateurs ont signalé des problèmes en faisant tourner 
le noyau en Fast-RAM, aussi le démarrage Atari placera le noyau en ST-RAM. Le 
minimum de mémoire ST-RAM nécessaire est 2&nbsp;Mo. Vous aurez besoin d'au 
moins 12&nbsp;Mo supplémentaires de TT-RAM.
</para>

<para arch="m68k">
Sur Macintosh, il faut faire attention avec les machines qui utilisent la 
RAM-based video (RBV). Le segment de mémoire à l'adresse physique 0 est 
utilisé comme mémoire d'écran, rendant la position de chargement par défaut 
du noyau indisponible. Le segment de mémoire alternatif utilisé par le noyau
et ramdisk doit être d'au moins 4&nbsp;Mo.
</para>

<para arch="m68k">
<emphasis condition="FIXME">FIXME: is this still true?</emphasis> 
</para>

 </sect1>
