# installation-howto.xml
# Kwangwoo Lee <kwlee@dynamicroot.org>, 2005
# Seok-moon Jang <drssay97@gmail.com>, 2006
# Changwoo Ryu <cwryu@debian.org>, 2006, 2007, 2008
#
# 이 번역은 완성 단계로 품질에 많은 신경을 쓰고 있습니다. 반드시 메일링 리스트에
# 번역 사실을 알리고 토의를 하십시오.
#
msgid ""
msgstr ""
"Project-Id-Version: installation-howto.xml\n"
"Report-Msgid-Bugs-To: debian-boot@lists.debian.org\n"
"POT-Creation-Date: 2008-09-28 00:09+0000\n"
"PO-Revision-Date: 2008-10-12 20:19+0900\n"
"Last-Translator: Changwoo Ryu <cwryu@debian.org>\n"
"Language-Team: Korean <debian-l10n-korean@lists.debian.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: application/x-xml2pot; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. Tag: title
#: installation-howto.xml:5
#, no-c-format
msgid "Installation Howto"
msgstr "설치 하우투"

#. Tag: para
#: installation-howto.xml:7
#, no-c-format
msgid ""
"This document describes how to install &debian; &releasename; for the &arch-"
"title; (<quote>&architecture;</quote>) with the new &d-i;. It is a quick "
"walkthrough of the installation process which should contain all the "
"information you will need for most installs. When more information can be "
"useful, we will link to more detailed explanations in other parts of this "
"document."
msgstr ""
"이 문서는 &d-i;를 사용해 &arch-title;용 &debian; &releasename;"
"(<quote>&architecture;</quote>) 배포판을 설치하는 방법을 설명합니다. 간단히 "
"설치 절차만 설명한 문서로, 설치할 때 필요한 정보 대부분이 들어 있습니다. 더 "
"많은 정보가 필요한 경우 더 자세히 설명한 이 문서의 다른 부분으로 링크되어 있"
"습니다."

#. Tag: title
#: installation-howto.xml:19
#, no-c-format
msgid "Preliminaries"
msgstr "들어가기 앞서"

#. Tag: para
#: installation-howto.xml:20
#, no-c-format
msgid ""
"<phrase condition=\"unofficial-build\"> The debian-installer is still in a "
"beta state. </phrase> If you encounter bugs during your install, please "
"refer to <xref linkend=\"submit-bug\"/> for instructions on how to report "
"them. If you have questions which cannot be answered by this document, "
"please direct them to the debian-boot mailing list (&email-debian-boot-"
"list;) or ask on IRC (#debian-boot on the OFTC network)."
msgstr ""
"<phrase condition=\"unofficial-build\">debian-installer는 아직 베타 버전입니"
"다. </phrase>설치할 때 버그를 발견하면, <xref linkend=\"submit-bug\"/> 부분"
"의 방법을 이용해 버그를 알려 주십시오. 이 문서로 알 수 없는 궁금한 사항이 있"
"으면 debian-boot 메일링 리스트에 (&email-debian-boot-list;) 알리거나 IRC에 "
"(OFTC 네트워크의 #debian-boot 채널) 질문해주십시오."

#. Tag: title
#: installation-howto.xml:36
#, no-c-format
msgid "Booting the installer"
msgstr "설치 프로그램 시작하기"

#. Tag: para
#: installation-howto.xml:37
#, no-c-format
msgid ""
"<phrase condition=\"unofficial-build\"> For some quick links to CD images, "
"check out the <ulink url=\"&url-d-i;\"> &d-i; home page</ulink>. </phrase> "
"The debian-cd team provides builds of CD images using &d-i; on the <ulink "
"url=\"&url-debian-cd;\">Debian CD page</ulink>. For more information on "
"where to get CDs, see <xref linkend=\"official-cdrom\"/>."
msgstr ""
"<phrase condition=\"unofficial-build\">CD 이미지의 링크는 <ulink url=\"&url-"
"d-i;\"> &d-i; 홈페이지</ulink>를 보십시오. </phrase>debian-cd 팀에서 만든 CD "
"이미지는 <ulink url=\"&url-debian-cd;\">데비안 CD 페이지</ulink>에 있습니다. "
"CD를 구할 수 있는 곳은, <xref linkend=\"official-cdrom\"/> 부분을 참고하십시"
"오."

#. Tag: para
#: installation-howto.xml:47
#, no-c-format
msgid ""
"Some installation methods require other images than CD images. <phrase "
"condition=\"unofficial-build\"> The <ulink url=\"&url-d-i;\">&d-i; home "
"page</ulink> has links to other images. </phrase> <xref linkend=\"where-files"
"\"/> explains how to find images on Debian mirrors."
msgstr ""
"설치 방법에 따라 CD 이미지 외에 다른 이미지 파일이 필요하기도 합니다. "
"<phrase condition=\"unofficial-build\"> <ulink url=\"&url-d-i;\">&d-i; 홈페이"
"지</ulink>에 다른 이미지의 링크가 있습니다. </phrase> 데비안 미러에서 이미지 "
"파일 찾는 방법은 <xref linkend=\"where-files\"/>에서 설명합니다."

#. Tag: para
#: installation-howto.xml:57
#, no-c-format
msgid ""
"The subsections below will give the details about which images you should "
"get for each possible means of installation."
msgstr ""
"다음 섹션에서는 설치 방법에 따라 어떤 이미지를 받아야 하는 지 자세히 설명합니"
"다."

#. Tag: title
#: installation-howto.xml:65
#, no-c-format
msgid "CDROM"
msgstr "CDROM"

#. Tag: para
#: installation-howto.xml:67
#, no-c-format
msgid ""
"There are two different netinst CD images which can be used to install "
"&releasename; with the &d-i;. These images are intended to boot from CD and "
"install additional packages over a network, hence the name 'netinst'. The "
"difference between the two images is that on the full netinst image the base "
"packages are included, whereas you have to download these from the web if "
"you are using the business card image. If you'd rather, you can get a full "
"size CD image which will not need the network to install. You only need the "
"first CD of the set."
msgstr ""
"&d-i;로 &releasename; 릴리스를 설치하려면 두 가지 netinst CD 이미지가 있습니"
"다. 이 이미지는 CD로 부팅하고 나머지 패키지를 네트워크로 설치하기 때문에 이름"
"이 'netinst'입니다. 이 두 가지 이미지의 다른 점은, 하나는 베이스 패키지가 들"
"어 있는 전체 netinst 이미지이고, 다른 하나는 베이스 패키지도 다운로드해야 하"
"는 business card 이미지라는 점입니다. 원한다면 설치할 때 네트워크가 필요없는 "
"전체 CD 이미지를 받을 수도 있습니다. 설치할 때는 전체 CD 세트에서 첫번째 CD"
"만 필요합니다."

#. Tag: para
#: installation-howto.xml:78
#, no-c-format
msgid ""
"Download whichever type you prefer and burn it to a CD. <phrase arch=\"x86"
"\">To boot the CD, you may need to change your BIOS configuration, as "
"explained in <xref linkend=\"bios-setup\"/>.</phrase> <phrase arch=\"powerpc"
"\"> To boot a PowerMac from CD, press the <keycap>c</keycap> key while "
"booting. See <xref linkend=\"boot-cd\"/> for other ways to boot from CD. </"
"phrase>"
msgstr ""
"마음에 드는 이미지를 다운로드하고 CD를 굽습니다. <phrase arch=\"x86\">CD로 부"
"팅하려면 <xref linkend=\"bios-setup\"/>에 설명한 대로 BIOS 설정을 바꿔야 할 "
"수 있습니다.</phrase> <phrase arch=\"powerpc\"> 부팅할 때 <keycap>c</keycap> "
"키를 누르면 CD에서 PowerMac을 부팅할 수 있습니다. 다른 방법으로 CD에서 부팅하"
"려면 <xref linkend=\"boot-cd\"/> 부분을 보십시오. </phrase>"

#. Tag: title
#: installation-howto.xml:92
#, no-c-format
msgid "Floppy"
msgstr "플로피"

#. Tag: para
#: installation-howto.xml:93
#, no-c-format
msgid ""
"If you can't boot from CD, you can download floppy images to install Debian. "
"You need the <filename>floppy/boot.img</filename>, the <filename>floppy/root."
"img</filename> and one or more of the driver disks."
msgstr ""
"CD로 부팅할 수 없으면, 플로피 이미지를 받아서 플로피로 데비안을 설치할 수 있"
"습니다. 필요한 이미지는 <filename>floppy/boot.img</filename>, "
"<filename>floppy/root.img</filename>이고, 또 드라이버 디스크가 한 개 이상 필"
"요합니다."

#. Tag: para
#: installation-howto.xml:99
#, no-c-format
msgid ""
"The boot floppy is the one with <filename>boot.img</filename> on it. This "
"floppy, when booted, will prompt you to insert a second floppy &mdash; use "
"the one with <filename>root.img</filename> on it."
msgstr ""
"부팅 플로피는 <filename>boot.img</filename> 이미지로 만든 플로피 입니다. 이 "
"플로피로 부팅하면 두 번째 플로피(<filename>root.img</filename> 이미지로 만든 "
"플로피)를 넣으라고 표시합니다."

#. Tag: para
#: installation-howto.xml:105
#, no-c-format
msgid ""
"If you're planning to install over the network, you will usually need the "
"<filename>floppy/net-drivers-1.img</filename>. For PCMCIA or USB networking, "
"and some less common network cards, you will also need a second driver "
"floppy, <filename>floppy/net-drivers-2.img</filename>."
msgstr ""
"네트워크로 설치하려면, 일반적으로 <filename>floppy/net-drivers-1.img</"
"filename> 파일이 필요합니다. PCMCIA 네트워크, USB 네트워크, 많이 사용하지 않"
"는 네트워크 카드의 경우에는, 두 번째 드라이버 플로피로 <filename>floppy/net-"
"drivers-2.img</filename> 파일이 필요합니다."

#. Tag: para
#: installation-howto.xml:112
#, no-c-format
msgid ""
"If you have a CD, but cannot boot from it, then boot from floppies and use "
"<filename>floppy/cd-drivers.img</filename> on a driver disk to complete the "
"install using the CD."
msgstr ""
"CD가 있지만 CD로 부팅할 수 없는 경우, 플로피로 부팅한 뒤에 <filename>floppy/"
"cd-drivers.img</filename> 이미지를 사용하면 CD를 이용해 설치를 마칠 수 있습니"
"다."

#. Tag: para
#: installation-howto.xml:118
#, no-c-format
msgid ""
"Floppy disks are one of the least reliable media around, so be prepared for "
"lots of bad disks (see <xref linkend=\"unreliable-floppies\"/>). Each "
"<filename>.img</filename> file you downloaded goes on a single floppy; you "
"can use the dd command to write it to /dev/fd0 or some other means (see "
"<xref linkend=\"create-floppy\"/> for details). Since you'll have more than "
"one floppy, it's a good idea to label them."
msgstr ""
"플로피 디스크는 가장 신뢰하기 힘든 미디어의 하나이므로, 디스크가 망가진 경우"
"에 대해 대비하십시오. (<xref linkend=\"unreliable-floppies\"/> 참고.) 다운로"
"드한 <filename>.img</filename> 파일은 각각 한 장의 플로피에 기록합니다. /dev/"
"fd0에 기록하려면 dd 명령을 사용하거나 기타 다른 방법을 사용할 수 있습니다. "
"(자세한 내용은 <xref linkend=\"create-floppy\"/> 참고.) 플로피가 여러 장이 되"
"므로 플로피 이름을 기록해 놓아야 좋습니다."

#. Tag: title
#: installation-howto.xml:131
#, no-c-format
msgid "USB memory stick"
msgstr "USB 메모리"

#. Tag: para
#: installation-howto.xml:132
#, no-c-format
msgid ""
"It's also possible to install from removable USB storage devices. For "
"example a USB keychain can make a handy Debian install medium that you can "
"take with you anywhere."
msgstr ""
"이동식 USB 저장 장치로 설치할 수도 있습니다. 예를 들어 USB 열쇠고리를 어디든 "
"가지고 다니면서 간단히 데비안을 설치할 수 있습니다."

#. Tag: para
#: installation-howto.xml:138
#, no-c-format
msgid ""
"The easiest way to prepare your USB memory stick is to download <filename>hd-"
"media/boot.img.gz</filename>, and use gunzip to extract the 256 MB image "
"from that file. Write this image directly to your memory stick, which must "
"be at least 256 mb in size. Of course this will destroy anything already on "
"the memory stick. Then mount the memory stick, which will now have a FAT "
"filesystem on it. Next, download a Debian netinst CD image, and copy that "
"file to the memory stick; any filename is ok as long as it ends in <literal>."
"iso</literal>."
msgstr ""
"USB 메모리를 준비하려면 가장 쉬운 방법으로, <filename>hd-media/boot.img.gz</"
"filename>을 다운로드해서 256 MB 이미지의 압축을 풉니다. 압축을 푼 이미지를 "
"256 MB보다 용량이 큰 USB 메모리에 직접 쓰십시오. 물론 이렇게 하면 USB 메모리"
"에 들어 있는 기존의 데이터를 덮어 씁니다. 이렇게 하고 USB 메모리를 마운트하"
"면 FAT 파일 시스템이 기록된 것을 확인할 수 있습니다. 그리고 데비안 netinst "
"CD 이미지를 다운로드해서 그 파일을 메모리에 복사합니다. <literal>.iso</"
"literal>로 끝나기만하면 어떤 파일 이름이라도 괜찮습니다."

#. Tag: para
#: installation-howto.xml:149
#, no-c-format
msgid ""
"There are other, more flexible ways to set up a memory stick to use the "
"debian-installer, and it's possible to get it to work with smaller memory "
"sticks. For details, see <xref linkend=\"boot-usb-files\"/>."
msgstr ""
"이 방법 외에, debian-installer를 사용하는 USB 메모리를 만드는 방법으로 좀 더 "
"유연한 방법이 있고, 더 작은 용량의 USB 메모리를 사용할 수도 있습니다. 자세한 "
"내용은 <xref linkend=\"boot-usb-files\"/>부분을 참고하십시오."

#. Tag: para
#: installation-howto.xml:155
#, no-c-format
msgid ""
"Some BIOSes can boot USB storage directly, and some cannot. You may need to "
"configure your BIOS to boot from a <quote>removable drive</quote> or even a "
"<quote>USB-ZIP</quote> to get it to boot from the USB device. For helpful "
"hints and details, see <xref linkend=\"usb-boot\"/>."
msgstr ""
"BIOS에 따라 USB 저장 장치에서 직접 부팅할 수도 있지만, 부팅하지 못하는 BIOS"
"도 있습니다. USB 장치로 부팅하려면 <quote>removable drive</quote> 또는 "
"<quote>USB-ZIP</quote>으로 부팅하도록 BIOS 설정을 바꿔야 합니다. 도움이 되는 "
"힌트 및 자세한 설명은 <xref linkend=\"usb-boot\"/> 부분을 참고하십시오."

#. Tag: para
#: installation-howto.xml:162
#, no-c-format
msgid ""
"Booting Macintosh systems from USB storage devices involves manual use of "
"Open Firmware. For directions, see <xref linkend=\"usb-boot\"/>."
msgstr ""
"USB 저장 장치로 매킨토시 시스템에서 부팅하려면 Open Firmware를 수동으로 조작"
"해야 합니다. 방법은 <xref linkend=\"usb-boot\"/> 부분을 보십시오."

#. Tag: title
#: installation-howto.xml:171
#, no-c-format
msgid "Booting from network"
msgstr "네트워크 부팅"

#. Tag: para
#: installation-howto.xml:172
#, no-c-format
msgid ""
"It's also possible to boot &d-i; completely from the net. The various "
"methods to netboot depend on your architecture and netboot setup. The files "
"in <filename>netboot/</filename> can be used to netboot &d-i;."
msgstr ""
"네트워크로 &d-i;를 부팅하는 것도 가능합니다. 아키텍처와 netboot 설정에 따라 "
"다양한 방법의 네트워크 부팅 방법이 있습니다. <filename>netboot/</filename>에 "
"들어 있는 파일을 이용해 &d-i;를 네트워크 부팅합니다."

#. Tag: para
#: installation-howto.xml:178
#, no-c-format
msgid ""
"The easiest thing to set up is probably PXE netbooting. Untar the file "
"<filename>netboot/pxeboot.tar.gz</filename> into <filename>/var/lib/"
"tftpboot</filename> or wherever is appropriate for your tftp server. Set up "
"your DHCP server to pass filename <filename>/pxelinux.0</filename> to "
"clients, and with luck everything will just work. For detailed instructions, "
"see <xref linkend=\"install-tftp\"/>."
msgstr ""
"PXE 네트워크 부팅이 제일 설정하기 쉽습니다. <filename>netboot/pxeboot.tar."
"gz</filename> 파일을 <filename>/var/lib/tftpboot</filename> 또는 tftp 서버가 "
"사용하는 위치에 풀어 놓으십시오. DHCP 서버가 <filename>/pxelinux.0</"
"filename> 파일 이름을 클라이언트에게 넘겨주도록 설정하면, 모두 잘 동작할 것입"
"니다. 자세한 설명은 <xref linkend=\"install-tftp\"/> 부분을 참고하십시오."

#. Tag: title
#: installation-howto.xml:192
#, no-c-format
msgid "Booting from hard disk"
msgstr "하드 디스크 부팅"

#. Tag: para
#: installation-howto.xml:193
#, no-c-format
msgid ""
"It's possible to boot the installer using no removable media, but just an "
"existing hard disk, which can have a different OS on it. Download "
"<filename>hd-media/initrd.gz</filename>, <filename>hd-media/vmlinuz</"
"filename>, and a Debian CD image to the top-level directory of the hard "
"disk. Make sure that the CD image has a filename ending in <literal>.iso</"
"literal>. Now it's just a matter of booting linux with the initrd. <phrase "
"arch=\"x86\"> <xref linkend=\"boot-initrd\"/> explains one way to do it. </"
"phrase>"
msgstr ""
"이동식 미디어가 없이 기존 하드 디스크를 이용해서 설치 프로그램을 부팅할 수 있"
"습니다. 이 하드디스크에는 다른 운영 체제가 들어 있을 수도 있습니다. "
"<filename>hd-media/initrd.gz</filename>, <filename>hd-media/vmlinuz</"
"filename>, 데비안 CD 이미지를 다운로드해서 하드 디스크의 맨 위 디렉토리에 놓"
"으십시오. CD 이미지의 파일 이름이 <literal>.iso</literal>로 끝나도록 하십시"
"오. 이제 initrd로 리눅스를 부팅하는 일만 남았습니다. <phrase arch=\"x86\"> "
"<xref linkend=\"boot-initrd\"/> 부분에서 부팅 방법을 설명합니다. </phrase>"

#. Tag: title
#: installation-howto.xml:210
#, no-c-format
msgid "Installation"
msgstr "설치"

#. Tag: para
#: installation-howto.xml:211
#, no-c-format
msgid ""
"Once the installer starts, you will be greeted with an initial screen. Press "
"&enterkey; to boot, or read the instructions for other boot methods and "
"parameters (see <xref linkend=\"boot-parms\"/>)."
msgstr ""
"설치 프로그램을 시작하면 초기 화면이 나타납니다. &enterkey;를 누를 수도 있"
"고, 아니면 다른 부팅 방법이나 파라미터 설명을 읽어 보십시오. (<xref linkend="
"\"boot-parms\"/> 참고.)"

#. Tag: para
#: installation-howto.xml:217
#, no-c-format
msgid ""
"After a while you will be asked to select your language. Use the arrow keys "
"to pick a language and press &enterkey; to continue. Next you'll be asked to "
"select your country, with the choices including countries where your "
"language is spoken. If it's not on the short list, a list of all the "
"countries in the world is available."
msgstr ""
"잠시 후에 언어를 선택합니다. 화살표 키로 언어를 선택하고 &enterkey;를 눌러 계"
"속 진행합니다. 그 다음에 해당 언어를 사용하는 국가 중에서 자기 국가를 선택하"
"는 부분이 나타납니다. 여기 나오는 짧은 리스트에 자기 국가가 없다면, 세계의 모"
"든 국가 목록에서 선택할 수도 있습니다."

#. Tag: para
#: installation-howto.xml:225
#, no-c-format
msgid ""
"You may be asked to confirm your keyboard layout. Choose the default unless "
"you know better."
msgstr "키보드 설정을 확인합니다. 잘 모르겠으면 기본값을 선택하십시오."

#. Tag: para
#: installation-howto.xml:230
#, no-c-format
msgid ""
"Now sit back while debian-installer detects some of your hardware, and loads "
"the rest of itself from CD, floppy, USB, etc."
msgstr ""
"이제 데비안 설치 프로그램이 하드웨어를 검사하고, CD, 플로피, USB 등에서 나머"
"지 부분을 읽어들이는 동안 기다리십시오."

#. Tag: para
#: installation-howto.xml:235
#, no-c-format
msgid ""
"Next the installer will try to detect your network hardware and set up "
"networking by DHCP. If you are not on a network or do not have DHCP, you "
"will be given the opportunity to configure the network manually."
msgstr ""
"그 다음에 설치 프로그램은 네트워크 하드웨어를 검사하고 DHCP를 통해 네트워크 "
"설정을 합니다. 네트워크에 연결하지 않았거나 DHCP가 없다면, 네트워크를 수동으"
"로 설정할 수 있습니다."

#. Tag: para
#: installation-howto.xml:241
#, no-c-format
msgid ""
"The next step is setting up your clock and time zone. The installer will try "
"to contact a time server on the Internet to ensure the clock is set "
"correctly. The time zone is based on the country selected earlier and the "
"installer will only ask to select one if a country has multiple zones."
msgstr ""
"다음 단계는 시계 및 시간대 설정입니다. 설치 프로그램이 인터넷의 타임 서버에 "
"연결해서 시계를 올바르게 맞춥니다. 시간대는 앞에서 선택한 국가에 따라 결정합"
"니다. 한 국가에 여러 개의 시간대가 있는 경우에만 시간대를 물어봅니다."

#. Tag: para
#: installation-howto.xml:248
#, no-c-format
msgid ""
"Now it is time to partition your disks. First you will be given the "
"opportunity to automatically partition either an entire drive, or available "
"free space on a drive (see <xref linkend=\"partman-auto\"/>). This is "
"recommended for new users or anyone in a hurry. If you do not want to "
"autopartition, choose <guimenuitem>Manual</guimenuitem> from the menu."
msgstr ""
"디스크를 파티션할 차례입니다. 먼저 전체 디스크나 디스크의 남은 공간을 자동으"
"로 파티션 나누기 할 수 있습니다. (<xref linkend=\"partman-auto\"/> 참고.) 이 "
"방법은 처음 설치하는 사용자나 급한 사용자에게 추천합니다. 자동 파티션을 사용"
"하지 않으려면 메뉴에서 <guimenuitem>수동으로</guimenuitem>를 선택하십시오."

#. Tag: para
#: installation-howto.xml:256
#, no-c-format
msgid ""
"If you have an existing DOS or Windows partition that you want to preserve, "
"be very careful with automatic partitioning. If you choose manual "
"partitioning, you can use the installer to resize existing FAT or NTFS "
"partitions to create room for the Debian install: simply select the "
"partition and specify its new size."
msgstr ""
"유지하려는 DOS나 윈도우즈 파티션이 있다면, 자동 파티션을 할 때 주의 하십시"
"오. 수동 파티션을 이용하면 설치 프로그램을 사용해 기존의 FAT 또는 NTFS 파티션"
"의 크기를 조정할 수 있고, 그렇게 만든 빈 공간에 데비안을 설치할 수 있습니다."

#. Tag: para
#: installation-howto.xml:263
#, no-c-format
msgid ""
"On the next screen you will see your partition table, how the partitions "
"will be formatted, and where they will be mounted. Select a partition to "
"modify or delete it. If you did automatic partitioning, you should just be "
"able to choose <guimenuitem>Finish partitioning and write changes to disk</"
"guimenuitem> from the menu to use what it set up. Remember to assign at "
"least one partition for swap space and to mount a partition on <filename>/</"
"filename>. For more detailed information on how to use the partitioner, "
"please refer to <xref linkend=\"di-partition\"/>; the appendix <xref linkend="
"\"partitioning\"/> has more general information about partitioning."
msgstr ""
"다음 화면에 파티션 테이블, 파티션의 포맷, 마운트 위치가 나타납니다. 바꾸거나 "
"지울 파티션을 선택하십시오. 자동 파티션을 할 경우, 메뉴에서 <guimenuitem>파티"
"션 나누기를 마치고 바뀐 사항을 디스크에 쓰기</guimenuitem>를 선택하면 있는 그"
"대로 사용합니다. 반드시 파티션 한 개를 스왑 공간으로 배정하고, 또 한 파티션"
"을 <filename>/</filename>에 마운트하십시오. 파티션 나누기 사용하는 방법을 보"
"려면 <xref linkend=\"di-partition\"/> 부분을 보십시오. <xref linkend="
"\"partitioning\"/>에서 파티션 나누기에 대해 더 자세히 설명합니다."

#. Tag: para
#: installation-howto.xml:276
#, no-c-format
msgid ""
"Now &d-i; formats your partitions and starts to install the base system, "
"which can take a while. That is followed by installing a kernel."
msgstr ""
"파티션을 포맷하고 베이스 시스템 설치를 시작합니다. 약간의 시간이 걸릴 수 있습"
"니다. 다 끝나면 커널을 설치합니다."

#. Tag: para
#: installation-howto.xml:281
#, no-c-format
msgid ""
"The base system that was installed earlier is a working, but very minimal "
"installation. To make the system more functional the next step allows you to "
"install additional packages by selecting tasks. Before packages can be "
"installed <classname>apt</classname> needs to be configured as that defines "
"from where the packages will be retrieved. The <quote>Standard system</"
"quote> task will be selected by default and should normally be installed. "
"Select the <quote>Desktop environment</quote> task if you would like to have "
"a graphical desktop after the installation. See <xref linkend=\"pkgsel\"/> "
"for additional information about this step."
msgstr ""
"앞에서 설치한 베이스 시스템은 완전히 동작하지만 최소한의 시스템입니다. 이 시"
"스템을 좀 더 쓸모 있게 만드려면 다음 단계에서 태스크를 선택해 패키지를 추가"
"로 설치합니다. 패키지를 설치하기 전에 <classname>apt</classname>를 설정해서 "
"어디서 패키지를 가져올 지 지정합니다. <quote>표준 시스템</quote> 태스크가 기"
"본으로 선택되어 있고 보통 이 태스크는 설치해야 합니다. 그래픽 데스크탑을 사용"
"하려면 <quote>데스크탑 환경</quote> 태스크를 선택하십시오. 이 단계에 관해 "
"<xref linkend=\"pkgsel\"/> 부분을 참고하십시오."

#. Tag: para
#: installation-howto.xml:293
#, no-c-format
msgid ""
"Installation of the base system is followed by setting up user accounts. By "
"default you will need to provide a password for the <quote>root</quote> "
"(administrator) account and information necessary to create one regular user "
"account."
msgstr ""
"베이스 시스템 설치 다음에는 사용자 계정을 설정합니다. 기본값으로 먼저 <quote>"
"루트</quote>(root, 관리자) 계정의 암호를 입력하고, 그 다음 일반 사용자 계정"
"에 필요한 정보를 입력합니다."

#. Tag: para
#: installation-howto.xml:300
#, no-c-format
msgid ""
"The last step is to install a boot loader. If the installer detects other "
"operating systems on your computer, it will add them to the boot menu and "
"let you know. <phrase arch=\"x86\">By default GRUB will be installed to the "
"master boot record of the first harddrive, which is generally a good choice. "
"You'll be given the opportunity to override that choice and install it "
"elsewhere. </phrase>"
msgstr ""
"마지막 단계는 부트로더 설치입니다. 컴퓨터에서 다른 운영체제를 찾으면, 그 운영"
"체제를 부팅 메뉴에 추가하고 알려줍니다. <phrase arch=\"x86\">첫번째 하드디스"
"크의 마스터 부트 레코드에 (대부분 이 위치에 설치하면 맞습니다) GRUB을 설치합"
"니다. 기본 설정을 바꾸거나 다른 위치에 설치할 수도 있습니다. </phrase>"

#. Tag: para
#: installation-howto.xml:310
#, no-c-format
msgid ""
"&d-i; will now tell you that the installation has finished. Remove the cdrom "
"or other boot media and hit &enterkey; to reboot your machine. It should "
"boot up into the newly installed system and allow you to log in. This is "
"explained in <xref linkend=\"boot-new\"/>."
msgstr ""
"이제 &d-i;에서 설치 과정이 끝났다고 표시합니다. CD-ROM 또는 기타 부팅 미디어"
"를 꺼내고 &enterkey;를 눌러 다시 부팅하십시오. 그러면 새로 설치한 시스템으로 "
"로그인할 수 있습니다. <xref linkend=\"boot-new\"/>에 설명되어 있습니다."

#. Tag: para
#: installation-howto.xml:317
#, no-c-format
msgid ""
"If you need more information on the install process, see <xref linkend=\"d-i-"
"intro\"/>."
msgstr ""
"설치 과정에 대해 더 알고 싶으시면, <xref linkend=\"d-i-intro\"/> 부분을 보십"
"시오."

#. Tag: title
#: installation-howto.xml:326
#, no-c-format
msgid "Send us an installation report"
msgstr "설치 보고서를 보내주십시오"

#. Tag: para
#: installation-howto.xml:327
#, no-c-format
msgid ""
"If you successfully managed an installation with &d-i;, please take time to "
"provide us with a report. The simplest way to do so is to install the "
"reportbug package (<command>aptitude install reportbug</command>), configure "
"<classname>reportbug</classname> as explained in <xref linkend=\"mail-"
"outgoing\"/>, and run <command>reportbug installation-reports</command>."
msgstr ""
"&d-i;를 이용해 설치에 성공했다면, 시간을 내서 설치 보고서를 보내 주십시오. 보"
"고서를 제출하려면 가장 간단한 방법으로, reportbug 패키지를 설치하시고 "
"(<command>aptitude install reportbug</command>), <classname>reportbug</"
"classname>를 <xref linkend=\"mail-outgoing\"/>에 설명한 대로 설정하고, "
"<command>report installation-reports</command>를 실행하십시오."

#. Tag: para
#: installation-howto.xml:337
#, no-c-format
msgid ""
"If you did not complete the install, you probably found a bug in debian-"
"installer. To improve the installer it is necessary that we know about them, "
"so please take the time to report them. You can use an installation report "
"to report problems; if the install completely fails, see <xref linkend="
"\"problem-report\"/>."
msgstr ""
"설치를 마치지 못했다면 데비안 설치 프로그램의 버그때문일 것입니다. 설치 프로"
"그램을 개선하려면 개발자들에게 알려야 하므로, 시간을 내서 알려 주십시오. 문제"
"를 보고할 때 설치 보고서를 사용할 수 있습니다. 설치가 완전히 실패한다면 "
"<xref linkend=\"problem-report\"/> 부분을 참고하십시오."

#. Tag: title
#: installation-howto.xml:349
#, no-c-format
msgid "And finally&hellip;"
msgstr "그리고 마지막으로&hellip;"

#. Tag: para
#: installation-howto.xml:350
#, no-c-format
msgid ""
"We hope that your Debian installation is pleasant and that you find Debian "
"useful. You might want to read <xref linkend=\"post-install\"/>."
msgstr ""
"즐겁게 데비안을 설치하고 데비안의 좋은 점을 느끼셨기 바랍니다. 이제 <xref "
"linkend=\"post-install\"/> 부분을 읽어 보십시오."
