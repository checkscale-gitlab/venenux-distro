Template: partman-md/text/device
Type: text
# :sl3:
_Description: Software RAID device

Template: partman-md/text/configure_md
Type: text
# :sl3:
_Description: Configure software RAID

Template: partman-md/confirm
Type: boolean
Default: false
# :sl3:
#flag:translate!:4
_Description: Write the changes to the storage devices and configure RAID?
 Before RAID can be configured, the changes
 have to be written to the storage devices.  These changes cannot be
 undone.
 .
 When RAID is configured, no additional changes
 to the partitions in the disks containing physical volumes are
 allowed.  Please convince yourself that you are satisfied with the
 current partitioning scheme in these disks.
 .
 ${ITEMS}

Template: partman-md/confirm_nochanges
Type: boolean
Default: false
# :sl3:
_Description: Keep current partition layout and configure RAID?
 When RAID is configured, no additional changes
 to the partitions in the disks containing physical volumes are
 allowed.  Please convince yourself that you are satisfied with the
 current partitioning scheme in these disks.

Template: partman-md/commit_failed
Type: error
# :sl3:
_Description: RAID configuration failure
 An error occurred while writing the changes to the storage devices.
 .
 RAID configuration has been aborted.

Template: partman/method_long/raid
Type: text
# :sl3:
_Description: physical volume for RAID

Template: partman/method_short/raid
Type: text
# :sl3:
_Description: raid

Template: partman-md/device_remove_md
Type: boolean
Default: false
# :sl3:
_Description: Remove existing software RAID partitions?
 The selected device contains partitions used for software RAID devices.
 The following devices and partitions are about to be removed:
 .
 Software RAID devices about to be removed: ${REMOVED_DEVICES}
 .
 Partitions used by these RAID devices: ${REMOVED_PARTITIONS}
 .
 Note that this will also permanently erase any data currently on the
 software RAID devices.
