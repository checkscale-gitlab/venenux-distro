#!/bin/sh

# Prepare to run base-config after reboot
# Set up network-console-config if installation is running over SSH

set -e

. /usr/share/debconf/confmodule

TEMPLATE_ROOT=sarge-support

log() {
	logger -t sarge-support "$@"
}

config_ncc_user () {
	echo 'installer:x:0:0:installer:/:/usr/sbin/base-config-network-console' >> /target/etc/passwd
	grep "^installer:" /etc/shadow >> /target/etc/shadow
}

setup_ssh_install () {
	if [ "$TERM_TYPE" != network ] ; then
		return 1
	fi

	NCC=network-console-config
	# Try installing network-console-config has been installed
	if apt-install $NCC; then
		config_ncc_user
		db_input critical $TEMPLATE_ROOT/nc-finish-install-reminder
		db_go
		return 0
	else
		log "Package $NCC could not be installed"
		log "The system will not be set up to run base configuration over SSH"
		db_subst $TEMPLATE_ROOT/nc-finish-install-failure PACKAGE $NCC
		db_input critical $TEMPLATE_ROOT/nc-finish-install-failure
		db_go
		return 1
	fi
}

config_inittab () {
	# Since this script is running with debconf, 'tty' does
	# not give reliable answers about what sort of terminal
	# we have.  The stdin of /sbin/debian-installer seems
	# to tell the truth.

	# If the installation is run using network-console (over ssh),
	# there can be two debian-installer processes; only use the first.

	inst_pid=$(pidof debian-installer | cut -d" " -f1)
	rawconsole=$(readlink /proc/${inst_pid}/fd/0)
	console=$(mapdevfs "$rawconsole")
	rawconsole=${rawconsole#/dev/}
	console=${console#/dev/}

	if setup_ssh_install ; then
		log "Network installation: not configuring /etc/inittab to run base-config after reboot"
	else
		case "$console" in
		    ttyS*)
			log "Configuring /etc/inittab to run base-config for serial console"
			mv /target/etc/inittab /target/etc/inittab.real
			sed -e "s/^\([23]\):/#\1:/" \
			    -e "s#CONSOLEDEV#/dev/$console#g" \
			    /usr/share/sarge-support/inittab > /target/etc/inittab
			;;
		    *)
			log "Configuring /etc/inittab to run base-config for regular console"
			mv /target/etc/inittab /target/etc/inittab.real
			sed -e "s#CONSOLEDEV#/dev/$console#g" \
			    /usr/share/sarge-support/inittab > /target/etc/inittab
			;;
		esac
	fi
}

# Only run if we're installing Sarge
if db_get mirror/codename && [ "$RET" ]; then
	if [ "$RET" != sarge ]; then
		exit 0
	fi
fi

config_inittab

# The termwrap shipped with Sarge does not set up correctly for UTF-8 for most
# languages, so we replace it with a modified version. Not the way things
# should be done, but as termwrap is part of base-config, not unacceptable
if [ -f /target/usr/sbin/termwrap ]; then
	cp /usr/share/sarge-support/termwrap /target/usr/sbin/
fi

# We need to copy d-i debconf variable passwd/username for use by exim4-config
# Other variables base-config knows how to deal with itself
debconf-copydb -p "^passwd/username$" configdb target_configdb

# Sarge's base-config expects installation logs in /var/log/debian-installer
# Needed to get debconf values from 1st stage
cd /target/var/log
ln -s installer debian-installer

# This is needed to get the base-config/late_command to be propagated into the
# installed sarge system
debconf-copydb -p "^base-config/late_command$" configdb target_configdb
