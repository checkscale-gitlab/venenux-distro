<!-- retain these comments for translator revision tracking -->
<!-- original version: 56425 -->

<!-- This section is heavily outdated. It only really describes
     older BIOSes and not the current situation. Most of it is not
     really relevant for AMD64, but a general description would be.
     File should be renamed to x86.xml if a more general text is
     written. -->

  <sect2 arch="x86" id="bios-setup"><title>Das BIOS-Setup-Menü aufrufen</title>

<para>

Das BIOS bietet Basisfunktionen, die benötigt werden, um den Rechner
zu starten und dem Betriebssystem Zugriff auf die Hardware zu ermöglichen.
Möglicherweise hat Ihr System ein BIOS-Setup-Menü, in dem das BIOS
konfiguriert wird. Vor der Installation von Debian <emphasis>müssen</emphasis>
Sie sicherstellen, dass Ihr BIOS korrekt eingestellt ist; ansonsten könnte das
zu sporadischen Ausfällen führen oder Sie können Debian möglicherweise nicht
installieren.

</para><para>

Der Rest dieses Abschnitts ist bei <ulink url="&url-pc-hw-faq;"></ulink>
entliehen und beantwortet die Frage <quote>Wie gelange ich in das
CMOS-Konfigurationsmenü?</quote> Wie Sie Ihr BIOS-
(oder <quote>CMOS-</quote>) Konfigurationsmenü erreichen, hängt
davon ab, welche Firma Ihr BIOS erstellt hat:

</para>

<!-- From: burnesa@cat.com (Shaun Burnet) -->
<variablelist>

<varlistentry>
 <term>AMI BIOS</term>
 <listitem><para>

<keycap>Entf</keycap>-Taste während des POST (Power on Self Test, Selbsttest
nach dem Einschalten)

</para></listitem>
</varlistentry>

<varlistentry>
 <term>Award BIOS</term>
 <listitem><para>

<keycombo>
  <keycap>Strg</keycap><keycap>Alt</keycap><keycap>Esc</keycap>
</keycombo> oder <keycap>Entf</keycap>-Taste während des POST

</para></listitem>
</varlistentry>

<varlistentry><term>DTK BIOS</term>
 <listitem><para>

<keycap>Esc</keycap>-Taste während des POST

</para></listitem>
</varlistentry>

<varlistentry><term>IBM PS/2 BIOS</term>
 <listitem><para>

<keycombo>
  <keycap>Strg</keycap><keycap>Alt</keycap><keycap>Einfg</keycap>
</keycombo>
nach
<keycombo>
  <keycap>Strg</keycap><keycap>Alt</keycap><keycap>Entf</keycap>
</keycombo>

</para></listitem>
</varlistentry>

<varlistentry>
 <term>Phoenix BIOS</term>
 <listitem><para>

<keycombo>
  <keycap>Strg</keycap><keycap>Alt</keycap><keycap>Esc</keycap>
</keycombo>
oder
<keycombo>
  <keycap>Strg</keycap><keycap>Alt</keycap><keycap>S</keycap>
</keycombo>
oder
<keycap>F1</keycap>

</para></listitem>
</varlistentry>
</variablelist>

<para>

Informationen, wie Sie BIOS-Menüs weiterer Firmen aufrufen, finden Sie unter
<ulink url="&url-invoking-bios-info;"></ulink>.

</para><para>

Einige &arch-title;-Rechner haben kein CMOS-Konfigurationsmenü im BIOS.
Sie erfordern ein CMOS-Setup-Programm. Wenn Sie keine Installations- und/oder
Diagnosedisketten für Ihren Rechner haben, können Sie versuchen, ein 
Shareware/Freeware-Programm zu benutzen. Werfen Sie einen Blick auf
<ulink url="&url-simtel;"></ulink>.

</para>
  </sect2>

  <sect2 arch="x86" id="boot-dev-select"><title>Das Start-Laufwerk auswählen (Boot Device Selection)</title>

<para>

Viele BIOS-Setup-Menüs erlauben die Auswahl der Laufwerke, von dem
das System gestartet wird. Stellen Sie dies so ein, dass wie folgt
nach einem bootfähigen Betriebssystem gesucht wird: 
zuerst <filename>A:</filename> (das erste Diskettenlaufwerk), dann optional
das erste CD-ROM-Laufwerk (dies erscheint möglicherweise als <filename>D:</filename>
oder <filename>E:</filename>) und dann <filename>C:</filename>, die
erste Festplatte. Diese Einstellung erlaubt, entweder von einer Floppy-Disk
oder einer CD-ROM zu booten; dies sind die beiden meist verwendeten
Start-Laufwerke, um Debian zu installieren.

</para><para>

Wenn Sie einen neueren SCSI-Controller besitzen und ein CD-ROM-Gerät
dort angeschlossen haben, können Sie normalerweise von diesem CD-ROM
booten. Sie müssen dazu lediglich das Booten von CD-ROM im
SCSI-BIOS des Controllers aktivieren.

</para><para>

Eine andere beliebte Option ist das Starten von einem USB-Speicher
(manchmal auch USB-Memorystick oder USB-Key genannt). Einige BIOS
können direkt von einem USB-Speicher booten, andere können dies jedoch
nicht. Möglicherweise müssen Sie im BIOS die Boot-Option
<quote>Wechsel-Plattenlaufwerk (Removable drive)</quote> oder sogar
<quote>USB-ZIP</quote> wählen, um zu erreichen, dass der Rechner
vom USB-Speicher startet.

</para><para>

Hier einige Details über das Festlegen der Startreihenfolge.
Denken Sie daran, die Einstellung wieder auf den ursprünglichen Wert
zurückzustellen, nachdem Linux installiert ist, so dass nach einem
Neustart von der Festplatte gebootet wird.

</para>

   <sect3 id="ctbooi">
   <title>Ändern der Startreihenfolge auf Rechnern mit IDE-Laufwerken</title>

<orderedlist>
<listitem><para>

Sobald Ihr Computer startet, drücken Sie die nötigen Tasten, um in das BIOS-Menü
zu gelangen. Oft ist dies die <keycap>Entf</keycap>-Taste;
konsultieren Sie Ihre Hardware-Dokumentation bezüglich der genauen
Tastenkombination.

</para></listitem>
<listitem><para>

Suchen Sie nach der Einstellung für die Boot-Reihenfolge (boot sequence). Der
genaue Ort hängt von Ihrem BIOS ab; suchen Sie nach einem Feld, in dem Laufwerke
aufgelistet sind.

</para><para>

Gebräuchliche Einstellungen für Rechner mit IDE-Laufwerken sind <quote>C, A, CDROM</quote> oder
<quote>A, C, CDROM</quote>.   

</para><para>
   
C ist die (erste) Festplatte und A ist das Diskettenlaufwerk.

</para></listitem>   
<listitem><para>

Ändern Sie die Boot-Reihenfolge, so dass das CD-ROM oder die Floppy-Disk als erstes
steht. Oft können Sie mit <keycap>Bild Auf</keycap> oder <keycap>Bild Ab</keycap>
durch die möglichen Einstellungen wechseln.

</para></listitem>
<listitem><para>

Speichern Sie die Änderungen (Save Changes). Richten Sie sich dabei nach den Instruktionen
auf dem Bildschirm.

</para></listitem>
</orderedlist>
   </sect3>

   <sect3 id="ctboos">
   <title>Ändern der Startreihenfolge auf Rechnern mit SCSI-Laufwerken</title>
<para>

<orderedlist>
<listitem><para>

Während Ihr Rechner startet, drücken Sie die entsprechenden Tasten, um das SCSI-Setup-Programm
zu starten:

</para><para>

Nachdem der Speichercheck und die Nachricht, wie Sie Ihr BIOS-Setup erreichen,
angezeigt wurden, können Sie das SCSI-Setup-Programm starten.

</para><para>

Die Tastenkombination, die Sie drücken müssen, hängt von dem Setup-Programm ab.
Oft ist es <keycombo><keycap>Strg</keycap><keycap>F2</keycap></keycombo>.
Andernfalls konsultieren Sie Ihre Hardware-Dokumentation, um die richtige
Tastenkombination zu erfahren.

</para></listitem>
<listitem><para>

Suchen Sie nach der Einstellung für die Startreihenfolge (boot order).

</para></listitem>
<listitem><para>

Stellen Sie diese so ein, dass die ID des SCSI-CDROM-Laufwerks als erstes
auf der Liste steht.

</para></listitem>
<listitem><para>

Sichern Sie die Änderungen (Save Changes). Richten Sie sich dabei nach den
Instruktionen auf dem Bildschirm. Oft müssen Sie dazu <keycap>F10</keycap>
drücken.

</para></listitem>
</orderedlist>

</para>
   </sect3>
  </sect2>

  <sect2 arch="x86">
  <title>Verschiedene BIOS-Einstellungen</title>

   <sect3 id="cd-settings"><title>CD-ROM-Einstellungen</title>
<para>

Einige BIOS-Systeme (wie z.B. Award BIOS) erlauben es, die Geschwindigkeit
des CD-ROM-Laufwerks automatisch wählen zu lassen. Sie sollten dies
vermeiden und stattdessen die langsamste Geschwindigkeit wählen.
Wenn Sie <userinput>seek failed</userinput>-Fehlermeldungen (Fehler beim
Suchen nach einer Disk) erhalten, könnte dies das Problem sein.

</para>
   </sect3>

   <sect3><title>Extended contra Expanded Memory</title>
<para>

Wenn Ihr System sowohl ex<emphasis>ten</emphasis>ded wie auch
ex<emphasis>pan</emphasis>ded Memory anbietet, stellen Sie es so ein,
dass möglichst viel extended Memory und so wenig wie möglich expanded Memory
vorhanden ist. Linux benötigt extended Memory, kann aber expanded Memory
nicht nutzen.

</para>
   </sect3>

   <sect3><title>Schutz vor Viren</title>
<para>

Deaktivieren Sie alle Viren-Schutz-Funktionen, die Ihr BIOS anbietet.
Wenn Sie ein <quote>virus-protection board</quote> (Virenschutzfunktionen im Motherboard
integriert) haben oder andere spezielle Hardware, so stellen Sie sicher,
dass die Funktionen deaktiviert oder physikalisch vom Board entfernt sind, wenn
Sie GNU/Linux laufen lassen. Sie sind nicht mit GNU/Linux kompatibel; vielmehr
besteht auf einem Linux-System durch die Rechtevergabe im Dateisystem und
den geschützten Speicher des Linux-Kernels fast keine Virengefahr<footnote>
<para>

Nach der Installation können Sie den Virenschutz für den Boot-Sektor
(Boot Sector protection) wieder aktivieren, wenn Sie möchten. Unter Linux
bietet dies zwar keine zusätzliche Sicherheit, wenn Sie aber gleichzeitig noch
Windows laufen lassen, könnte es eine Katastrophe verhindern. Nachdem der
Bootmanager eingerichtet wurde, gibt es keinen Grund, noch mit dem
Master Boot Record (MBR) herum zu hantieren.

</para>
</footnote>.

</para>
   </sect3>

   <sect3><title>Shadow RAM</title>
<para>

Ihr Motherboard bietet unter Umständen <emphasis>Shadow RAM</emphasis>
oder BIOS-Caching an. Entsprechende BIOS-Parameter sind
<quote>Video BIOS Shadow</quote>, <quote>C800-CBFF Shadow</quote>, etc.
<emphasis>Deaktivieren</emphasis> Sie alle Einstellungen für
Shadow RAM. Shadow RAM wird genutzt, um den Zugriff auf die ROM-Speicher
auf dem Motherboard sowie auf einige Controller-Karten zu beschleunigen.
Linux nutzt diese ROMs nicht mehr, sobald es gebootet ist, da es seine
eigene, schnellere 32-Bit-Software hat statt der 16-Bit-Software in
den ROMs. Indem man den Shadow RAM deaktiviert, kann es sein, dass dieser
Speicher für Anwendungen als normaler Arbeitsspeicher verfügbar wird.
Wenn Sie den Shadow RAM aktiviert lassen, könnte dies Linux's Zugriff
auf die Hardware stören.

</para>
   </sect3>

   <sect3><title>Die Speicherlücke (Memory Hole)</title>
<para>

Wenn Ihr BIOS etwas Ähnliches wie <quote>15&ndash;16 MB Memory Hole</quote> 
(eine Speicherlücke im Bereich zwischen 15 und 16MB) anbietet,
deaktivieren Sie das bitte. Linux erwartet, in diesem Bereich nutzbaren
Speicher zu finden (falls Sie so viel RAM haben).

</para><para>

Wir bekamen einen Bericht über ein Intel Endeavor-Motherboard, auf dem
es eine Option namens <quote>LFB</quote> oder <quote>Linear Frame Buffer</quote> gibt.
Dafür gibt es zwei Einstellungen: <quote>Disabled</quote> (deaktiviert) und
<quote>1 Megabyte</quote>. Wählen Sie <quote>1 Megabyte</quote>. Wenn die Option deaktiviert
war, wurde im genannten Fall eine Installationsdiskette nicht korrekt
gelesen und das System stürzte eventuell sogar ab. Zum Zeitpunkt, als
dies geschrieben wurde, haben wir nicht verstanden, was mit diesem
speziellen Gerät geschah &ndash; es war nur so, dass es mit der passenden
Einstellung funktionierte und ohne nicht.

</para>
   </sect3>

<!-- no other platforms other than x86 provide this sort of thing, AFAIK -->

   <sect3><title>Advanced Power Management</title>
<para>

Wenn Ihr Motherboard Advanced Power Management (APM) unterstützt, stellen Sie
das BIOS so ein, dass das Powermanagement von APM kontrolliert wird.
Deaktivieren Sie den Doze-, Standby-, Suspend-, Nap- und Sleepmodus im BIOS
sowie den Power-down-Timer für die Festplatten. Linux kann die Kontrolle
über die verschiedenen Modi übernehmen und überhaupt den Job des Powermanagements
besser erledigen als das BIOS.

</para>
   </sect3>
  </sect2>
