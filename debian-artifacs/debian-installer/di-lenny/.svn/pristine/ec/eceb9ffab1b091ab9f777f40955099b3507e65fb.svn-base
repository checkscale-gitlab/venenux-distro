<!-- retain these comments for translator revision tracking -->
<!-- original version: 35613 -->


  <sect2 arch="mips" id="boot-tftp"><title>Booten per TFTP</title>

   <sect3>
   <title>SGI TFTP-Boot</title>
<para>

Wenn Sie den Kommandomonitor erreicht haben, nutzen Sie

<informalexample><screen>
bootp():
</screen></informalexample>

um Linux auf SGI-Maschinen zu booten und die Debian-Installation zu starten.
Um dies zu ermöglichen, müssen Sie eventuell die
<envar>netaddr</envar>-Umgebungsvariable deaktivieren. Tippen Sie dazu

<informalexample><screen>
unsetenv netaddr
</screen></informalexample>

in den Kommandomonitor ein.

</para>
  </sect3>

   <sect3>
   <title>TFTP-Boot von Broadcom BCM91250A und BCM91480B</title>
<para>

Bei den Broadcom BCM91250A- und BCM91480B-Evaluation-Boards müssen Sie
den SiByl-Bootloader
per TFTP laden, der dann den Debian-Installer lädt und startet. Meistens
werden Sie wohl eine IP-Adresse per DHCP beziehen, es ist aber auch möglich,
eine statische Adresse zu konfigurieren. Um DHCP zu nutzen, können Sie
folgendes Kommando am CFE-Prompt eingeben:

<informalexample><screen>
ifconfig eth0 -auto
</screen></informalexample>

Sobald Sie eine IP-Adresse bekommen haben, können Sie SiByl mit dem folgenden
Befehl laden:

<informalexample><screen>
boot 192.168.1.1:/boot/sibyl
</screen></informalexample>

Ersetzen Sie die IP-Adresse aus dem Beispiel entweder mit dem Namen oder
der IP-Adresse Ihres TFTP-Servers. Sobald Sie dies Kommando ausführen, wird
der Installer automatisch geladen.

</para>
</sect3>
  </sect2>

  <sect2 arch="mips"><title>Boot-Parameter</title>

   <sect3>
   <title>SGI TFTP-Boot</title>
<para>

Bei SGI-Maschinen können Sie Boot-Parameter zum
<command>bootp():</command>-Befehl im Kommandomonitor hinzufügen.

</para><para>

Hinter dem <command>bootp():</command>-Kommando können Sie den Pfad
und den Namen der Datei angegeben, die gebootet werden soll, wenn
nicht explizit eine vom BOOTP/DHCP-Server vorgeben wird. Ein Beispiel:

<informalexample><screen>
bootp():/boot/tftpboot.img
</screen></informalexample>

Weitere Kernelparameter können per <command>append</command>-Befehl
angegeben werden:

<informalexample><screen>
bootp(): append="root=/dev/sda1"
</screen></informalexample>

</para>
  </sect3>

   <sect3>
   <title>TFTP-Boot von Broadcom BCM91250A und BCM91480B</title>
<para>

Am CFE-Prompt direkt können Sie keine Boot-Parameter angeben. Sie müssen
stattdessen in der Datei <filename>/boot/sibyl.conf</filename> auf dem
TFTP-Server Ihre Parameter zur Variable
<replaceable>extra_args</replaceable> hinzufügen.

</para>
  </sect3>

  </sect2>
