<!-- retain these comments for translator revision tracking -->
<!-- original version: 50620 -->

<!-- As tzsetup is invoked from inside clock-setup, this is not a
     separate section -->

<para>

<!--
Depending on the location selected earlier in the installation process,
you may be shown a list of timezones relevant for that location.
If your location has only one time zone, you will not be asked anything and
the system will assume that time zone.
-->

In base al paese scelto all'inizio dell'installazione potrebbe essere
mostrato l'elenco dei soli fusi orari pertinenti a quel paese; se nel
paese è presente un solo fuso orario, non viene chiesto nulla e il
sistema userà quel fuso orario.

</para><para>

<!--
If for some reason you wish to set a time zone for the installed system
that does <emphasis>not</emphasis> match the selected location, there are
two options.
-->

Se per qualsiasi motivo si vuole impostare un fuso orario che
<emphasis>non</emphasis> è legato al paese scelto, si hanno due
possibilità.

</para>

<orderedlist>
<listitem>

<para>

<!--
The simplest option is to just select a different timezone after the
installation has been completed and you've booted into the new system.
The command to do this is:
-->

Il modo più semplice è scegliere un fuso orario diverso una volta finita
l'installazione e avviato il nuovo sistema. Il comando da usare è:

<informalexample><screen>
# dpkg-reconfigure tzdata
</screen></informalexample>

</para>

</listitem><listitem>

<para>

<!--
Alternatively, the time zone can be set at the very start of the
installation by passing the parameter
<userinput>time/zone=<replaceable>value</replaceable></userinput>
when you boot the installation system. The value should of course be a
valid time zone, for example <userinput>Europe/London</userinput> or
<userinput>UTC</userinput>.
-->

In alternativa il fuso orario può essere impostato all'inizio
dell'installazione passando il parametro
<userinput>time/zone=<replaceable>valore</replaceable></userinput>
all'avvio del sistema d'installazione. Ovviamente il valore deve essere
un fuso orario valido, per esempio <userinput>Europe/London</userinput>
o <userinput>UTC</userinput>.

</para>

</listitem>
</orderedlist>

<para>

<!--
For automated installations the time zone can also be set using preseeding.
-->

Nel caso di installazioni automatiche è possibile preconfigurare anche il
fuso orario.

</para>
