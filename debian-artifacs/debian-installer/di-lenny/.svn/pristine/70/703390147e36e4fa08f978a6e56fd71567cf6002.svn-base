nobootloader (1.24) unstable; urgency=high

  * Rebuild with fixed Danish translation

 -- Christian Perrier <bubulle@debian.org>  Fri, 09 Jan 2009 07:44:00 +0100

nobootloader (1.23) unstable; urgency=low

  [ Martin Michlmayr ]
  * Drop the full stop from the package description because this use is
    inconsistent.

  [ Updated translations ]
  * Arabic (ar.po) by Ossama M. Khayat
  * Belarusian (be.po) by Pavel Piatruk
  * Bengali (bn.po) by Mahay Alam Khan (মাহে আলম খান)
  * Bosnian (bs.po) by Armin Besirovic
  * Catalan (ca.po) by Jordi Mallach
  * Welsh (cy.po) by Jonathan Price
  * Danish (da.po)
  * Greek, Modern (1453-) (el.po)
  * Esperanto (eo.po) by Felipe Castro
  * Basque (eu.po) by Iñaki Larrañaga Murgoitio
  * French (fr.po) by Christian Perrier
  * Hebrew (he.po) by Lior Kaplan
  * Hindi (hi.po) by Kumar Appaiah
  * Croatian (hr.po) by Josip Rodin
  * Italian (it.po) by Milo Casagrande
  * Georgian (ka.po) by Aiet Kolkhi
  * Central Khmer (km.po) by KHOEM Sokhem
  * Kurdish (ku.po) by Erdal Ronahi
  * Latvian (lv.po) by Peteris Krisjanis
  * Macedonian (mk.po) by Arangel Angov
  * Nepali (ne.po) by Shiva Prasad Pokharel
  * Portuguese (Brazil) (pt_BR.po) by Felipe Augusto van de Wiel (faw)
  * Slovak (sk.po) by Ivan Masár
  * Serbian (sr.po) by Veselin Mijušković
  * Turkish (tr.po) by Mert Dirik
  * Ukrainian (uk.po) by Євгеній Мещеряков
  * Wolof (wo.po) by Mouhamadou Mamoune Mbacke
  * Traditional Chinese (zh_TW.po) by Tetralet

 -- Otavio Salvador <otavio@debian.org>  Sun, 21 Sep 2008 22:01:19 -0300

nobootloader (1.21) unstable; urgency=low

  [ Frans Pop ]
  * Reduce the size of the postinst by whitespace cleanup and the use of a
    variable for template names; also improves readability.
  * Add support for PA Semi's evaluation systems (#464429). Thanks to
    Olof Johansson for the patch.
  * Remove Matt Kraai and Sven Luther as Uploaders with many thanks for their
    past contributions.

  [ Updated translations ]
  * Amharic (am.po) by tegegne tefera
  * Arabic (ar.po) by Ossama M. Khayat
  * Bulgarian (bg.po) by Damyan Ivanov
  * Czech (cs.po) by Miroslav Kure
  * German (de.po) by Jens Seidel
  * Dzongkha (dz.po) by Jurmey Rabgay(Bongop) (DIT,BHUTAN)
  * Esperanto (eo.po) by Serge Leblanc
  * Spanish (es.po) by Javier Fernández-Sanguino Peña
  * Basque (eu.po) by Piarres Beobide
  * Finnish (fi.po) by Esko Arajärvi
  * French (fr.po) by Christian Perrier
  * Galician (gl.po) by Jacobo Tarrio
  * Hungarian (hu.po) by SZERVÁC Attila
  * Indonesian (id.po) by Arief S Fitrianto
  * Japanese (ja.po) by Kenshi Muto
  * Korean (ko.po) by Changwoo Ryu
  * Kurdish (ku.po) by Erdal Ronahi
  * Lithuanian (lt.po) by Kęstutis Biliūnas
  * Malayalam (ml.po) by Praveen|പ്രവീണ്‍ A|എ
  * Marathi (mr.po) by Sampada
  * Norwegian Bokmål (nb.po) by Hans Fredrik Nordhaug
  * Dutch (nl.po) by Frans Pop
  * Norwegian Nynorsk (nn.po) by Håvard Korsvoll
  * Panjabi (pa.po) by Amanpreet Singh Alam
  * Polish (pl.po) by Bartosz Fenski
  * Portuguese (Brazil) (pt_BR.po) by Felipe Augusto van de Wiel (faw)
  * Portuguese (pt.po) by Miguel Figueiredo
  * Romanian (ro.po) by Eddy Petrișor
  * Russian (ru.po) by Yuri Kozlov
  * Slovak (sk.po) by Ivan Masár
  * Swedish (sv.po) by Daniel Nylander
  * Tamil (ta.po) by Dr.T.Vasudevan
  * Thai (th.po) by Theppitak Karoonboonyanan
  * Vietnamese (vi.po) by Clytie Siddall
  * Simplified Chinese (zh_CN.po) by Ming Hua
  * Traditional Chinese (zh_TW.po) by Tetralet
  
 -- Otavio Salvador <otavio@debian.org>  Thu, 08 May 2008 13:29:49 -0300

nobootloader (1.20) unstable; urgency=low

  [ Colin Watson ]
  * udev 117 merged all udev tools into a single binary called udevadm.
    Check for this and use it instead of udevinfo if available.

  [ Updated translations ]
  * Amharic (am.po) by tegegne tefera
  * Belarusian (be.po) by Hleb Rubanau
  * Bengali (bn.po) by Jamil Ahmed
  * Finnish (fi.po) by Esko Arajärvi
  * Hindi (hi.po) by Kumar Appaiah
  * Indonesian (id.po) by Arief S Fitrianto
  * Italian (it.po) by Stefano Canepa
  * Korean (ko.po) by Changwoo Ryu
  * Latvian (lv.po) by Viesturs Zarins
  * Malayalam (ml.po) by Praveen|പ്രവീണ്‍ A|എ
  * Panjabi (pa.po) by Amanpreet Singh Alam
  * Polish (pl.po) by Bartosz Fenski
  * Portuguese (Brazil) (pt_BR.po) by Felipe Augusto van de Wiel (faw)
  * Romanian (ro.po) by Eddy Petrișor
  * Russian (ru.po) by Yuri Kozlov
  * Slovak (sk.po) by Ivan Masár
  * Turkish (tr.po) by Recai Oktaş
  * Vietnamese (vi.po) by Clytie Siddall

 -- Otavio Salvador <otavio@debian.org>  Fri, 15 Feb 2008 15:27:40 -0200

nobootloader (1.19) unstable; urgency=low

  [ Otavio Salvador ]
  * Do not depends of a specific base-installer version so we can provide
    an alternative module for it.

  [ Updated translations ]
  * Basque (eu.po) by Piarres Beobide
  * Romanian (ro.po) by Eddy Petrișor

 -- Joey Hess <joeyh@debian.org>  Mon, 18 Jun 2007 22:16:52 +0100

nobootloader (1.18) unstable; urgency=low

  [ Colin Watson ]
  * Merge from Ubuntu, with some improvements:
    - Install and run mkvmlinuz on Pegasos if the kernel didn't already do
      that for us.
    - Mount /target/proc for mkvmlinuz.

  [ Joey Hess ]
  * Multiply menu-item-numbers by 100

  [ Updated translations ]
  * Bengali (bn.po) by Mahay Alam Khan (মাহে আলম)
  * Dzongkha (dz.po) by translator
  * Esperanto (eo.po) by Serge Leblanc
  * Spanish (es.po) by Javier Fernández-Sanguino Peña
  * Tagalog (tl.po) by Eric Pareja
  * Traditional Chinese (zh_TW.po) by Tetralet

 -- Joey Hess <joeyh@debian.org>  Tue, 10 Apr 2007 14:35:44 -0400

nobootloader (1.17) unstable; urgency=low

  [ Updated translations ]
  * Malayalam (ml.po) by Praveen A

 -- Frans Pop <fjp@debian.org>  Tue, 27 Feb 2007 16:57:47 +0100

nobootloader (1.16) unstable; urgency=low

  [ Updated translations ]
  * Arabic (ar.po) by Ossama M. Khayat
  * Belarusian (be.po) by Pavel Piatruk
  * Bulgarian (bg.po) by Damyan Ivanov
  * Danish (da.po) by Claus Hindsgaul
  * Spanish (es.po) by Javier Fernández-Sanguino Peña
  * Galician (gl.po) by Jacobo Tarrio
  * Kurdish (ku.po) by Amed Çeko Jiyan
  * Latvian (lv.po) by Aigars Mahinovs
  * Malayalam (ml.po) by Praveen A
  * Panjabi (pa.po) by A S Alam
  * Portuguese (Brazil) (pt_BR.po) by Felipe Augusto van de Wiel (faw)
  * Portuguese (pt.po) by Miguel Figueiredo
  * Romanian (ro.po) by Eddy Petrișor
  * Slovenian (sl.po) by Matej Kovačič
  * Swedish (sv.po) by Daniel Nylander
  * Thai (th.po) by Theppitak Karoonboonyanan
  * Tagalog (tl.po) by Eric Pareja

 -- Frans Pop <fjp@debian.org>  Wed, 31 Jan 2007 11:50:27 +0100

nobootloader (1.15) unstable; urgency=low

  [ Colin Watson ]
  * Fix broken sed invocation reported in #395259 in a better way, avoiding
    devfs path assumptions.

  [ Updated translations ]
  * Bulgarian (bg.po) by Damyan Ivanov
  * Bosnian (bs.po) by Safir Secerovic
  * Esperanto (eo.po) by Serge Leblanc
  * Kurdish (ku.po) by rizoye-xerzi
  * Norwegian Bokmål (nb.po) by Bjørn Steensrud
  * Norwegian Nynorsk (nn.po) by Håvard Korsvoll
  * Polish (pl.po) by Bartosz Fenski
  * Slovenian (sl.po) by Matej Kovačič

 -- Frans Pop <fjp@debian.org>  Wed, 22 Nov 2006 15:12:22 +0100

nobootloader (1.14) unstable; urgency=low

  [ Sven Luther ]
  * Genesi firmware 1.3 will be released without the chrp compliant partition
    numbering finally, so bumped the check to 1.3.99 and up.

 -- Sven Luther <luther@debian.org>  Sun, 29 Oct 2006 09:36:13 +0100

nobootloader (1.13) unstable; urgency=low

  [ Sven Luther ]
  * Fix bad sed invocation, which failed on devfs-style paths.
    Closes: #395259.

 -- Frans Pop <fjp@debian.org>  Thu, 26 Oct 2006 03:46:59 +0200

nobootloader (1.12) unstable; urgency=low

  [ Updated translations ]
  * Arabic (ar.po) by Ossama M. Khayat
  * Belarusian (be.po) by Andrei Darashenka
  * Catalan (ca.po) by Jordi Mallach
  * German (de.po) by Jens Seidel
  * Dzongkha (dz.po) by Jurmey Rabgay
  * Indonesian (id.po) by Arief S Fitrianto
  * Korean (ko.po) by Sunjae park
  * Kurdish (ku.po) by Erdal Ronahi
  * Macedonian (mk.po) by Georgi Stanojevski
  * Norwegian Bokmål (nb.po) by Bjørn Steensrud
  * Nepali (ne.po) by Shiva Prasad Pokharel
  * Romanian (ro.po) by Eddy Petrișor
  * Albanian (sq.po) by Elian Myftiu
  * Tamil (ta.po) by Damodharan Rajalingam
  * Vietnamese (vi.po) by Clytie Siddall
  * Wolof (wo.po) by Mouhamadou Mamoune Mbacke
  * Simplified Chinese (zh_CN.po) by Ming Hua

 -- Frans Pop <fjp@debian.org>  Tue, 24 Oct 2006 15:33:55 +0200

nobootloader (1.11) unstable; urgency=low

  [ Colin Watson ]
  * Mark Open Firmware commands as untranslatable.
  * Patch adapted from one suggested by Sven Luther (closes: #388296):
    - Check the Pegasos firmware for versions older than 1.2.99, and
      subtract one from the partition number since those start counting
      partitions at 0.

  [ Sven Luther ]
  * Update template for Genesi systems. Closes #388591.

  [ Christian Perrier ]
  * Avoid splitting a sentence in two parts which can make translations
    difficult to handle.

  [ Updated translations ]
  * Bengali (bn.po) by Mahay Alam Khan (মাহে আলম)
  * Catalan (ca.po) by Jordi Mallach
  * Czech (cs.po) by Miroslav Kure
  * Danish (da.po) by Claus Hindsgaul
  * German (de.po) by Jens Seidel
  * Dzongkha (dz.po) by Jurmey Rabgay
  * Greek (el.po) by quad-nrg.net
  * Esperanto (eo.po) by Serge Leblanc
  * Spanish (es.po) by Javier Fernández-Sanguino Peña
  * Estonian (et.po) by Siim Põder
  * Basque (eu.po) by Piarres Beobide
  * Finnish (fi.po) by Tapio Lehtonen
  * French (fr.po) by Christian Perrier
  * Galician (gl.po) by Jacobo Tarrio
  * Hebrew (he.po) by Lior Kaplan
  * Hindi (hi.po) by Nishant Sharma
  * Croatian (hr.po) by Josip Rodin
  * Hungarian (hu.po) by SZERVÁC Attila
  * Italian (it.po) by Stefano Canepa
  * Japanese (ja.po) by Kenshi Muto
  * Khmer (km.po) by Khoem Sokhem
  * Korean (ko.po) by Sunjae park
  * Lithuanian (lt.po) by Kęstutis Biliūnas
  * Latvian (lv.po) by Aigars Mahinovs
  * Macedonian (mk.po) by Georgi Stanojevski
  * Norwegian Bokmal (nb.po) by Bjørn Steensrud
  * Dutch (nl.po) by Bart Cornelis
  * Polish (pl.po) by Bartosz Fenski
  * Portuguese (Brazil) (pt_BR.po) by André Luís Lopes
  * Portuguese (pt.po) by Miguel Figueiredo
  * Romanian (ro.po) by Eddy Petrişor
  * Russian (ru.po) by Yuri Kozlov
  * Slovak (sk.po) by Peter Mann
  * Slovenian (sl.po) by Jure Čuhalev
  * Albanian (sq.po) by Elian Myftiu
  * Swedish (sv.po) by Daniel Nylander
  * Thai (th.po) by Theppitak Karoonboonyanan
  * Tagalog (tl.po) by Eric Pareja
  * Turkish (tr.po) by Recai Oktaş
  * Ukrainian (uk.po) by Eugeniy Meshcheryakov
  * Vietnamese (vi.po) by Clytie Siddall
  * Wolof (wo.po) by Mouhamadou Mamoune Mbacke
  * Traditional Chinese (zh_TW.po) by Tetralet

 -- Frans Pop <fjp@debian.org>  Fri,  6 Oct 2006 02:45:56 +0200

nobootloader (1.10) unstable; urgency=low

  * If /sys/block and udevinfo are available, use them instead of devfs
    device name parsing to work out IDE/SCSI Open Firmware paths for
    Pegasos.

  [ Updated translations ]
  * Spanish (es.po) by Javier Fernández-Sanguino Peña
  * Panjabi (pa.po) by A S Alam
  * Northern Sami (se.po) by Børre Gaup
  * Traditional Chinese (zh_TW.po) by Tetralet

 -- Colin Watson <cjwatson@debian.org>  Tue, 29 Aug 2006 11:20:35 +0100

nobootloader (1.09) unstable; urgency=low

  [ Updated translations ]
  * Arabic (ar.po) by Ossama M. Khayat
  * Bosnian (bs.po) by Safir Secerovic
  * Catalan (ca.po) by Jordi Mallach
  * Welsh (cy.po) by Dafydd Harries
  * Danish (da.po) by Claus Hindsgaul
  * German (de.po) by Jens Seidel
  * Dzongkha (dz.po)
  * Esperanto (eo.po) by Serge Leblanc
  * Estonian (et.po) by Siim Põder
  * Basque (eu.po) by Piarres Beobide
  * Irish (ga.po) by Kevin Patrick Scannell
  * Hindi (hi.po) by Nishant Sharma
  * Hungarian (hu.po) by SZERVÑC Attila
  * Italian (it.po) by Giuseppe Sacco
  * Georgian (ka.po) by Aiet Kolkhi
  * Khmer (km.po) by Leang Chumsoben
  * Kurdish (ku.po) by Erdal Ronahi
  * Lithuanian (lt.po) by Kęstutis Biliūnas
  * Nepali (ne.po) by Shiva Pokharel
  * Dutch (nl.po) by Bart Cornelis
  * Norwegian Nynorsk (nn.po) by Håvard Korsvoll
  * Northern Sami (se.po) by Børre Gaup
  * Slovenian (sl.po) by Jure Čuhalev
  * Tamil (ta.po) by Damodharan Rajalingam
  * Thai (th.po) by Theppitak Karoonboonyanan
  * Vietnamese (vi.po) by Clytie Siddall

 -- Frans Pop <fjp@debian.org>  Thu, 13 Jul 2006 17:33:27 +0200

nobootloader (1.08) unstable; urgency=low

  [ Martin Michlmayr ]
  * Show the correct path of the kernel when no separate boot partition has
    been chosen on Netwinder.  Closes: #345320.

  [ Updated translations ]
  * Catalan (ca.po) by Jordi Mallach
  * Hungarian (hu.po) by SZERVÑC Attila
  * Slovenian (sl.po) by Matej Kovačič
  * Swedish (sv.po) by Daniel Nylander

 -- Martin Michlmayr <tbm@cyrius.com>  Sun, 12 Mar 2006 05:11:01 +0000

nobootloader (1.07) unstable; urgency=low

  [ Updated translations ]
  * Arabic (ar.po) by Ossama M. Khayat
  * Bulgarian (bg.po) by Ognyan Kulev
  * Bengali (bn.po) by Baishampayan Ghose
  * German (de.po) by Jens Seidel
  * Greek, Modern (1453-) (el.po) by Konstantinos Margaritis
  * Finnish (fi.po) by Tapio Lehtonen
  * French (fr.po) by Christian Perrier
  * Galician (gl.po) by Jacobo Tarrio
  * Indonesian (id.po) by Parlin Imanuel Toh
  * Icelandic (is.po) by David Steinn Geirsson
  * Korean (ko.po) by Sunjae park
  * Latvian (lv.po) by Aigars Mahinovs
  * Malagasy (mg.po) by Jaonary Rabarisoa
  * Macedonian (mk.po) by Georgi Stanojevski
  * Norwegian Nynorsk (nn.po)
  * Norwegian Nynorsk (pa_IN.po) by Amanpreet Singh Alam
  * Polish (pl.po) by Bartosz Fenski
  * Romanian (ro.po) by Eddy Petrişor
  * Russian (ru.po) by Yuri Kozlov
  * Slovak (sk.po) by Peter Mann
  * Slovenian (sl.po) by Jure Čuhalev
  * Albanian (sq.po) by Elian Myftiu
  * Swedish (sv.po) by Daniel Nylander
  * Tagalog (tl.po) by Eric Pareja
  * Turkish (tr.po) by Recai Oktaş
  * Vietnamese (vi.po) by Clytie Siddall
  * Simplified Chinese (zh_CN.po) by Ming Hua

 -- Frans Pop <fjp@debian.org>  Mon, 23 Jan 2006 20:26:33 +0100

nobootloader (1.06) unstable; urgency=low

  * Add a missing "a" to one of the templates.
  * While I'm at it, remove some Debian branding, especially some that reads
    better without the "Debian".

  [ Updated translations ]
  * Catalan (ca.po) by Guillem Jover
  * Czech (cs.po) by Miroslav Kure
  * Danish (da.po) by Claus Hindsgaul
  * Greek, Modern (1453-) (el.po) by Greek Translation Team
  * Spanish (es.po) by Javier Fernández-Sanguino Peña
  * French (fr.po) by Christian Perrier
  * Galician (gl.po) by Jacobo Tarrio
  * Italian (it.po) by Giuseppe Sacco
  * Japanese (ja.po) by Kenshi Muto
  * Kurdish (ku.po) by Erdal Ronahi
  * Lithuanian (lt.po) by Kęstutis Biliūnas
  * Bokmål, Norwegian (nb.po) by Bjørn Steensrud
  * Dutch (nl.po) by Bart Cornelis
  * Polish (pl.po) by Bartosz Fenski
  * Portuguese (pt.po) by Miguel Figueiredo
  * Portuguese (Brazil) (pt_BR.po) by André Luís Lopes
  * Romanian (ro.po) by Eddy Petrisor
  * Russian (ru.po) by Yuri Kozlov
  * Slovak (sk.po) by Peter Mann
  * Ukrainian (uk.po) by Eugeniy Meshcheryakov
  * Vietnamese (vi.po) by Clytie Siddall
  * Wolof (wo.po) by Mouhamadou Mamoune Mbacke

 -- Joey Hess <joeyh@debian.org>  Mon, 26 Sep 2005 17:16:23 +0200

nobootloader (1.05) unstable; urgency=low

  [ Colin Watson ]
  * Update GPL notices with the FSF's new address.

  [ Joey Hess ]
  * Update for rename of link_in_boot template, depend on new base-installer
    that contains the new one.

  * Updated translations: 
    - German (de.po) by Holger Wansing
    - Lithuanian (lt.po) by Kęstutis Biliūnas
    - Dutch (nl.po) by Bart Cornelis
    - Ukrainian (uk.po) by Eugeniy Meshcheryakov

 -- Joey Hess <joeyh@debian.org>  Tue,  9 Aug 2005 15:39:50 -0400

nobootloader (1.04) unstable; urgency=low

  * Colin Watson
    - Remove useless dependency on parted-udeb.

  * Updated translations: 
    - Arabic (ar.po) by Ossama M. Khayat
    - Belarusian (be.po) by Andrei Darashenka
    - Catalan (ca.po) by Guillem Jover
    - Czech (cs.po) by Miroslav Kure
    - German (de.po) by Dennis Stampfer
    - Greek, Modern (1453-) (el.po) by Greek Translation Team
    - Esperanto (eo.po) by Serge Leblanc
    - Spanish (es.po) by Javier Fernández-Sanguino Peña
    - Estonian (et.po) by Siim Põder
    - Basque (eu.po) by Piarres Beobide
    - Gallegan (gl.po) by Jacobo Tarrio
    - Hebrew (he.po) by Lior Kaplan
    - Italian (it.po) by Giuseppe Sacco
    - Japanese (ja.po) by Kenshi Muto
    - Lithuanian (lt.po) by Kęstutis Biliūnas
    - Malagasy (mg.po) by Jaonary Rabarisoa
    - Macedonian (mk.po) by Georgi Stanojevski
    - Macedonian (pa_IN.po) by Amanpreet Singh Alam
    - Portuguese (pt.po) by Miguel Figueiredo
    - Portuguese (Brazil) (pt_BR.po) by André Luís Lopes
    - Romanian (ro.po) by Eddy Petrişor
    - Russian (ru.po) by Yuri Kozlov
    - Tagalog (tl.po) by Eric Pareja
    - Ukrainian (uk.po) by Eugeniy Meshcheryakov
    - Vietnamese (vi.po) by Clytie Siddall
    - Wolof (wo.po) by Mouhamadou Mamoune Mbacke
    - Xhosa (xh.po) by Canonical Ltd
    - Traditional Chinese (zh_TW.po) by Tetralet

 -- Joey Hess <joeyh@debian.org>  Fri, 15 Jul 2005 17:57:55 +0300

nobootloader (1.03) unstable; urgency=low

  * Note that this includes fix(es) to substitution bugs in translated
    templates.
  * Updated translations: 
    - Bulgarian (bg.po) by Ognyan Kulev
    - Bosnian (bs.po) by Safir Šećerović
    - Greek, Modern (1453-) (el.po) by Greek Translation Team
    - Spanish (Castilian) (es.po) by Javier Fernandez-Sanguino Peña
    - Basque (eu.po) by Piarres Beobide
    - Finnish (fi.po) by Tapio Lehtonen
    - French (fr.po) by French Team
    - Gallegan (gl.po) by Hctor Fenndez Lpez
    - Hebrew (he.po) by Lior Kaplan
    - Hungarian (hu.po) by VEROK Istvan
    - Italian (it.po) by Giuseppe Sacco
    - Lithuanian (lt.po) by Kęstutis Biliūnas
    - Bøkmal, Norwegian (nb.po) by Hans Fredrik Nordhaug
    - Dutch (nl.po) by Bart Cornelis
    - Romanian (ro.po) by Eddy Petrisor
    - Russian (ru.po) by Dmitry Beloglazov
    - Traditional Chinese (zh_TW.po) by Tetralet

 -- Joey Hess <joeyh@debian.org>  Wed,  2 Feb 2005 17:19:42 -0500

nobootloader (1.02) unstable; urgency=low

  * Stephen R. Marenka
    - Added self to uploaders.
    - Added partconf workaround.

 -- Stephen R. Marenka <smarenka@debian.org>  Thu,  7 Oct 2004 13:38:59 -0500

nobootloader (1.01) unstable; urgency=low

  * Updated translations: 
    - Bulgarian (bg.po) by Ognyan Kulev
    - Catalan (ca.po) by Jordi Mallach
    - Czech (cs.po) by Miroslav Kure
    - Welsh (cy.po) by Dafydd Harries
    - Danish (da.po) by Claus Hindsgaul
    - German (de.po) by Dennis Stampfer
    - Greek, Modern (1453-) (el.po) by Greek Translation Team
    - Spanish (Castilian) (es.po) by Javier Fernandez-Sanguino Peña
    - Basque (eu.po) by Piarres Beobide Egaña
    - Finnish (fi.po) by Tapio Lehtonen
    - French (fr.po) by French Team
    - Hebrew (he.po) by Lior Kaplan
    - Croatian (hr.po) by Krunoslav Gernhard
    - Hungarian (hu.po) by VEROK Istvan
    - Indonesian (id.po) by Debian Indonesia Team
    - Italian (it.po) by Giuseppe Sacco
    - Japanese (ja.po) by Kenshi Muto
    - Korean (ko.po) by Changwoo Ryu
    - Lithuanian (lt.po) by Kęstutis Biliūnasn
    - Latvian (lv.po) by Aigars Mahinovs
    - Bøkmal, Norwegian (nb.po) by Bjorn Steensrud
    - Dutch (nl.po) by Bart Cornelis
    - Norwegian Nynorsk (nn.po) by Håvard Korsvoll
    - Polish (pl.po) by Bartosz Fenski
    - Portuguese (Brazil) (pt_BR.po) by André Luís Lopes
    - Romanian (ro.po) by Eddy Petrisor
    - Russian (ru.po) by Russian L10N Team
    - Slovak (sk.po) by Peter KLFMANiK Mann
    - Slovenian (sl.po) by Jure Čuhalev
    - Albanian (sq.po) by Elian Myftiu
    - Swedish (sv.po) by Per Olofsson
    - Turkish (tr.po) by Recai Oktaş
    - Simplified Chinese (zh_CN.po) by Carlos Z.F. Liu
    - Traditional Chinese (zh_TW.po) by Tetralet

 -- Joey Hess <joeyh@debian.org>  Wed,  6 Oct 2004 15:16:03 -0400

nobootloader (1.00) unstable; urgency=low

  * Joey Hess
    - Gratuitious version number bump.
  * Updated translations: 
    - Arabic (ar.po) by Ossama M. Khayat
    - Bulgarian (bg.po) by Ognyan Kulev
    - Catalan (ca.po) by Jordi Mallach
    - Czech (cs.po) by Miroslav Kure
    - Danish (da.po) by Claus Hindsgaul
    - German (de.po) by Dennis Stampfer
    - Greek, Modern (1453-) (el.po) by Greek Translation Team
    - Spanish (Castilian) (es.po) by Javier Fernandez-Sanguino Peña
    - Basque (eu.po) by Piarres Beobide Egaña
    - French (fr.po) by French Team
    - Gallegan (gl.po) by Héctor Fenández López
    - Hebrew (he.po) by Lior Kaplan
    - Croatian (hr.po) by Krunoslav Gernhard
    - Indonesian (id.po) by Parlin Imanuel Toh
    - Japanese (ja.po) by Kenshi Muto
    - Korean (ko.po) by Changwoo Ryu
    - Lithuanian (lt.po) by Kęstutis Biliūnasn
    - Bøkmal, Norwegian (nb.po) by Axel Bojer
    - Dutch (nl.po) by Bart Cornelis
    - Norwegian Nynorsk (nn.po) by Håvard Korsvoll
    - Polish (pl.po) by Bartosz Fenski
    - Portuguese (pt.po) by Miguel Figueiredo
    - Portuguese (Brazil) (pt_BR.po) by André Luís Lopes
    - Russian (ru.po) by Russian L10N Team
    - Slovak (sk.po) by Peter KLFMANiK Mann
    - Slovenian (sl.po) by Jure Čuhalev
    - Swedish (sv.po) by Per Olofsson
    - Turkish (tr.po) by Recai Oktaş
    - Ukrainian (uk.po) by Eugeniy Meshcheryakov
    - Simplified Chinese (zh_CN.po) by Carlos Z.F. Liu

 -- Joey Hess <joeyh@debian.org>  Mon, 27 Sep 2004 21:06:33 -0400

nobootloader (0.0.22) unstable; urgency=low

  * Sven Luther
    - Polishing of the powerpc/pegasos template.

 -- Sven Luther <luther@debian.org>  Mon,  6 Sep 2004 19:58:47 +0200

nobootloader (0.0.21) unstable; urgency=low

  * Sven Luther
    - Fixed missing $ which broke the case where /boot was not on a separate
      partition.
    - Modified template to mirror the nicer arm netwinder template.

 -- Sven Luther <luther@debian.org>  Fri, 27 Aug 2004 07:52:32 +0200

nobootloader (0.0.20) unstable; urgency=low

  * Martin Michlmayr
    - Add instructions for setting the Netwinder NeTTrom firmware
      so it will boot Debian automatically.
    - Simplify the code to detect the boot and root partitions.
    - Use archdetect to detect the architecture.
  * Updated translations:
    - Czech (cs.po) by Miroslav Kure
    - Danish (da.po) by Claus Hindsgaul
    - Greek (el.po) by George Papamichelakis
    - Spanish (es.po) by Javier Fernandez-Sanguino Peña
    - Basque (eu.po) by Piarres Beobide Egaña
    - Finnish (fi.po) by Tapio Lehtonen
    - French (fr.po) by Christian Perrier
    - Hebrew (he.po) by Lior Kaplan
    - Hungarian (hu.po) by VERÓK István
    - Japanese (ja.po) by Kenshi Muto
    - Korean (ko.po) by Changwoo Ryu
    - Portuguese (pt.po) by Miguel Figueiredo
    - Portuguese (Brazil) (pt_BR.po) by André Luís Lopes
    - Romanian (ro.po) by Eddy Petrisor
    - Slovak (sk.po) by Peter KLFMANiK Mann
    - Turkish (tr.po) by Ozgur Murat Homurlu
    - Ukrainian (uk.po) by Eugeniy Meshcheryakov

 -- Martin Michlmayr <tbm@cyrius.com>  Sun, 15 Aug 2004 18:21:54 +0100

nobootloader (0.0.19) unstable; urgency=low

  * Sven Luther
    - Updated generic case string to show the partition as well.
    - Added OF variable setting for the powerpc/chrp_pegasos case.

 -- Sven Luther <luther@debian.org>  Tue,  3 Aug 2004 21:48:43 +0200

nobootloader (0.0.18) unstable; urgency=low

  * Sven Luther
    - Changed postinst to use sed directly instead of /target/bin/sed, which may
      break if we switch libc. (Closes: #262270)
    - Prepared the code change to provide the $BOOT variable for holding the
      partition containing the kernel. This would need a string change to be
      effective though.

 -- Sven Luther <luther@debian.org>  Sat, 31 Jul 2004 12:44:49 +0200

nobootloader (0.0.17) unstable; urgency=low

  * Sven Luther
    - updated tranlations before the freeze in order to allow arch/subarch
      specific boot instructions.
    - added powerpc/chrp_pegasos specific OF boot instructions.
  * Christian Perrier
    - Corrected POTFILES.in with the new name of the templates file

 -- Sven Luther <luther@debian.org>  Wed, 28 Jul 2004 17:28:37 +0200

nobootloader (0.0.16) unstable; urgency=low

  * Updated translations:
    - Norwegian Nynorsk (nn.po) by Håvard Korsvoll

 -- Joey Hess <joeyh@debian.org>  Tue, 25 May 2004 12:17:09 -0300

nobootloader (0.0.15) unstable; urgency=low

  * Updated translations:
    - Bokmal, Norwegian (nb.po) by Bjørn Steensrud
    - Norwegian Nynorsk (nn.po) by Håvard Korsvoll
    - Romanian (ro.po) by Eddy Petrisor
    - Russian (ru.po) by Dmitry Beloglazov

 -- Joey Hess <joeyh@debian.org>  Fri, 23 Apr 2004 12:52:17 -0400

nobootloader (0.0.14) unstable; urgency=low

  * Stephen R. Marenka
    - Integrate 2.2 kernel plus partconf support (/target/).
  * Updated translations:
    - Hebrew (he.po) by Lior Kaplan
    - Indonesian (id.po) by Parlin Imanuel Toh
    - Italian (it.po) by Giuseppe Sacco
    - Russian (ru.po) by Dmitry Beloglazov
    - Turkish (tr.po) by Ozgur Murat Homurlu

 -- Joey Hess <joeyh@debian.org>  Tue, 20 Apr 2004 10:19:19 -0400

nobootloader (0.0.13) unstable; urgency=low

  * Joshua Kwan
    - switch to debhelper's new udeb support.
  * Updated translations:
    - Gallegan (gl.po) by Héctor Fernández López
    - Hebrew (he.po) by Lior Kaplan

 -- Joey Hess <joeyh@debian.org>  Sat, 10 Apr 2004 00:54:19 -0400

nobootloader (0.0.12) unstable; urgency=low

  * Updated translations:
    - Welsh (cy.po) by Dafydd Harries
    - Indonesian (id.po) by Parlin Imanuel Toh
    - Polish (pl.po) by Bartosz Fenski
    - Russian (ru.po) by Nikolai Prokoschenko

 -- Joey Hess <joeyh@debian.org>  Thu,  1 Apr 2004 20:34:25 -0500

nobootloader (0.0.11) unstable; urgency=low

  * Colin Watson
    - Fix "architecure" => "architecture" typo. Translations unfuzzied
      manually.
  * Updated translations:
    - Romanian (ro.po) by Eddy Petrisor
    - Turkish (tr.po) by Ozgur Murat Homurlu

 -- Colin Watson <cjwatson@debian.org>  Thu, 25 Mar 2004 20:17:02 +0000

nobootloader (0.0.10) unstable; urgency=low

  * Translations:
    - Matjaz Horvat: Added Slovenian translation (sl.po)

 -- Joey Hess <joeyh@debian.org>  Sun, 14 Mar 2004 13:26:27 -0500

nobootloader (0.0.9) unstable; urgency=low

  * Translations:
    - Dafydd Harries : Added Welsh translation (cy.po)

 -- Joey Hess <joeyh@debian.org>  Fri,  9 Mar 2004 13:16:36 -0900

nobootloader (0.0.8) unstable; urgency=low

  * Priority standard.

 -- Joey Hess <joeyh@debian.org>  Fri,  5 Mar 2004 08:16:36 -0900

nobootloader (0.0.7) unstable; urgency=low

  * Translations:
    - Changwoo Ryu
      - Added Korean translation (ko.po)
    - Håvard Korsvoll
      - Added Norwegian nynorsk translation (nn.po)
    - Javier Fernandez-Sanguino
      - Added Spanish translation (es.po)
    - Ming Hua
      - Initial Traditional Chinese translation (zh_TW.po), by Tetralet
      - Updated Traditional Chinese translation (zh_TW.po), by Tetralet
    - Håvard Korsvoll
      - Added Norwegian, bokmål translation, (nb.po). From Axel Bojer

 -- Joey Hess <joeyh@debian.org>  Tue,  2 Mar 2004 13:34:30 -0500

nobootloader (0.0.6) unstable; urgency=low

  * Stephen R. Marenka
    - Fix kernel 2.2.x if statement.
  * Matt Kraai
    - Add self to Uploaders.

 -- Matt Kraai <kraai@debian.org>  Tue, 24 Feb 2004 20:13:08 -0800

nobootloader (0.0.5) unstable; urgency=low

  * Joey Hess
    - Add a dependency on di-utils-mapdevfs.

 -- Joey Hess <joeyh@debian.org>  Sun, 22 Feb 2004 22:15:51 -0500

nobootloader (0.0.4) unstable; urgency=low

  * Stephen R. Marenka
    - Add kernel 2.2.x support.
  * Updated translations:
    - Ognyan Kulev
      - Added Bulgarian translation (bg.po).

 -- Joey Hess <joeyh@debian.org>  Fri, 20 Feb 2004 17:20:32 -0500

nobootloader (0.0.3) unstable; urgency=low

  * Updated translations:
    - Carlos Z.F. Liu
      - Add Simplified Chinese (zh_CN) translation
    - Bart Cornelis
      - Initials Dutch (nl.po) translation
    - Giuseppe Sacco
      - Added first italian translation (it.po)
    - Peter Mann
      - Update Slovak translation
    - Kęstutis Biliūnas
      - Initial Lithuanian translation (lt.po).
    - Andre Dahlqvist
      - Initial Swedish translation (sv.po)
    - Miguel Figueiredo
      - Initial Portuguese translation (pt.po)
    - Safir Secerovic
      - Add Bosnian translation (bs.po).
    - h3li0s
      - added albanian translation (sq.po)
    - Jordi Mallach
      - Update Catalan translation (ca.po).
  * Eugen Meshcheryakov : added Ukrainian translation (uk.po)

 -- Christian Perrier <bubulle@debian.org>  Sun,  8 Feb 2004 20:26:42 +0100

nobootloader (0.0.2) unstable; urgency=low

  * Bartosz Fenski
    - Added Polish (pl) translation
  * Miroslav Kure
    - Update Czech translation
  * Konstantinos Margaritis
    - Update Greek translation.
  * Dennis Stampfer
    - Initial German translation de.po
  * Christian Perrier
    - Rewrite templates (no hard-formatting and a few rephrasing)
    - Run debconf-updatepo
  * Konstantinos Margaritis
    - Updated Greek translation
  * Kenshi Muto
    - Add Japanese translation (ja.po)
  * Peter Mann
    - Initial Slovak translation
  * Claus Hindsgaul
    - Initial Danish translation (da.po)
  * André Luís Lopes
    - Added Brazilian Portuguese (pt_BR) translation.

 -- Joey Hess <joeyh@debian.org>  Thu, 22 Jan 2004 20:18:02 -0500

nobootloader (0.0.1) unstable; urgency=low

  * Initial release.

 -- Sven Luther <luther@debian.org>  Tue, 20 Jan 2004 14:42:01 +0100
