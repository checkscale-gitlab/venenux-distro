# Vietnamese translation for Book Info.
# Copyright © 2006 Free Software Foundation, Inc.
# Clytie Siddall <clytie@riverland.net.au>, 2005-2006.
# 
msgid ""
msgstr ""
"Project-Id-Version: bookinfo\n"
"Report-Msgid-Bugs-To: debian-boot@lists.debian.org\n"
"POT-Creation-Date: 2008-05-30 00:09+0000\n"
"PO-Revision-Date: 2006-12-31 19:51+1030\n"
"Last-Translator: Clytie Siddall <clytie@riverland.net.au>\n"
"Language-Team: Vietnamese <vi-VN@googlegroups.com>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: LocFactoryEditor 1.6fc1\n"

#. Tag: title
#: bookinfo.xml:5
#, no-c-format
msgid "&debian; Installation Guide"
msgstr "Hướng Dẫn Cài Đặt &debian;"

#. Tag: para
#: bookinfo.xml:8
#, no-c-format
msgid ""
"This document contains installation instructions for the &debian; &release; "
"system (codename <quote>&releasename;</quote>), for the &arch-title; "
"(<quote>&architecture;</quote>) architecture. It also contains pointers to "
"more information and information on how to make the most of your new Debian "
"system."
msgstr ""
"Tài liệu này chứa hướng dẫn cài đặt hệ thống &debian; &release; (tên mã là "
"<quote>&releasename;</quote>) vào kiến trúc &arch-title; "
"(<quote>&architecture;</quote>). Nó cũng chứa lời trỏ đến thông tin thêm, "
"cũng đến thông tin về cách sử dụng hệ thống Debian mới một cách hữu hiệu "
"nhất."

#. Tag: para
#: bookinfo.xml:17
#, no-c-format
msgid ""
"Because the &arch-title; port is not a release architecture for "
"&releasename;, there is no official version of this manual for &arch-title; "
"for &releasename;. However, because the port is still active and there is "
"hope that &arch-title; may be included again in future official releases, "
"this development version of the Installation Guide is still available."
msgstr ""
"Vì bản chuyển &arch-title; không phải là kiến trúc phân phối cho "
"&releasename;, không có phiên bản chính thức của sổ tay này dành cho &arch-"
"title; cho &releasename;. Tuy nhiên, vì bản chuyển này vẫn còn hoạt động và "
"mong muốn &arch-title; sẽ được gồm trong bản phân phối chính thức sai, vẫn "
"có sẵn phiên bản phát triển này của Sổ Tay Cài Đặt."

#. Tag: para
#: bookinfo.xml:25
#, no-c-format
msgid ""
"Because &arch-title; is not an official architecture, some of the "
"information, and especially some links, in this manual may be incorrect. For "
"additional information, please check the <ulink url=\"&url-ports;"
"\">webpages</ulink> of the port or contact the <ulink url=\"&url-list-"
"subscribe;\">debian-&arch-listname; mailing list</ulink>."
msgstr ""
"Vì &arch-title; không phải kiến túc chính thức, một phần thông tin, đặc biệt "
"liên kết trong sổ tay này có thể không đúng. Để tìm thêm thông tin, xem các "
"<ulink url=\"&url-ports;\">trang Mạng</ulink> của bản chuyển hoặc liên lạc "
"với hộp thư chung <ulink url=\"&url-list-subscribe;\">debian-&arch-listname;"
"</ulink>."

#. Tag: para
#: bookinfo.xml:36
#, no-c-format
msgid ""
"This installation guide is based on an earlier manual written for the old "
"Debian installation system (the <quote>boot-floppies</quote>), and has been "
"updated to document the new Debian installer. However, for &architecture;, "
"the manual has not been fully updated and fact checked for the new "
"installer. There may remain parts of the manual that are incomplete or "
"outdated or that still document the boot-floppies installer. A newer version "
"of this manual, possibly better documenting this architecture, may be found "
"on the Internet at the <ulink url=\"&url-d-i;\">&d-i; home page</ulink>. You "
"may also be able to find additional translations there."
msgstr ""
"Hướng dẫn cài đặt này dựa vào một sổ tay trước được tạo cho hệ thống cài đặt "
"Debian cũ (<quote>đĩa mềm khởi động</quote>), cũng đã được cập nhật để diễn "
"tả trình cài đặt Debian mới. Tuy nhiên, đối với kiến trúc &architecture;, sổ "
"tay này chưa được cập nhật đầy đủ hay, cũng chưa kiểm tra các sự kiện, cho "
"trình cài đặt mới. Có lẽ một số phần sổ tay này chưa hoàn toàn, hay có lỗi "
"thời, hay vẫn còn diễn tả trình cài đặt bằng đĩa mềm khởi động. Có một phiên "
"bản mới hơn của sổ tay này ở <ulink url=\"&url-d-i;\">trang chủ &d-i;</"
"ulink>. Có lẽ bạn cũng tìm thấy bản dịch thêm tại đó."

#. Tag: para
#: bookinfo.xml:49
#, no-c-format
msgid ""
"Although this installation guide for &architecture; is mostly up-to-date, we "
"plan to make some changes and reorganize parts of the manual after the "
"official release of &releasename;. A newer version of this manual may be "
"found on the Internet at the <ulink url=\"&url-d-i;\">&d-i; home page</"
"ulink>. You may also be able to find additional translations there."
msgstr ""
"Dù sổ tay cài đặt này dành cho kiến trúc &architecture; là hậu hết thông tin "
"mới nhất, chúng tôi định sửa đổi và tổ chức lại một số phần sổ tay sau khi "
"sự phát hành chính thức của &releasename;. Một phiên bản mới hơn của sổ tay "
"này có thể được tìm trên Mạng tại <ulink url=\"&url-d-i;\">trang chủ &d-i;</"
"ulink>. Có lẽ bạn cũng tìm thấy bản dịch thêm tại đó."

#. Tag: para
#: bookinfo.xml:58
#, no-c-format
msgid ""
"Translators can use this paragraph to provide some information about the "
"status of the translation, for example if the translation is still being "
"worked on or if review is wanted (don't forget to mention where comments "
"should be sent!). See build/lang-options/README on how to enable this "
"paragraph. Its condition is \"translation-status\"."
msgstr ""
"Trạng thái của bản dịch: hoàn tất. Mời bạn xem lại bản dịch này và gởi sự "
"gợi ý cho <email>vi-VN@googlegroups.com</email>."

#. Tag: holder
#: bookinfo.xml:75
#, no-c-format
msgid "the Debian Installer team"
msgstr "nhóm trình cài đặt Debian"

#. Tag: para
#: bookinfo.xml:79
#, no-c-format
msgid ""
"This manual is free software; you may redistribute it and/or modify it under "
"the terms of the GNU General Public License. Please refer to the license in "
"<xref linkend=\"appendix-gpl\"/>."
msgstr ""
"Sổ tay này là phần mềm tự do; bạn có thể phát hành lại nó và/hay sửa đổi nó "
"với điều kiện của Giấy Phép Công Cộng GNU (GPL). Xem giấy phép trong <xref "
"linkend=\"appendix-gpl\"/>."
