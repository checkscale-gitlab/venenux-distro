# THIS FILE IS GENERATED AUTOMATICALLY FROM THE D-I PO MASTER FILES
# The master files can be found under packages/po/
#
# DO NOT MODIFY THIS FILE DIRECTLY: SUCH CHANGES WILL BE LOST
#
# Italian messages for debian-installer.
# Copyright (C) 2003 Software in the Public Interest, Inc.
# This file is distributed under the same license as debian-installer.
# The translation team (for all four levels):
# Cristian Rigamonti <cri@linux.it>
# Danilo Piazzalunga <danilopiazza@libero.it>
# Davide Meloni <davide_meloni@fastwebnet.it>
# Davide Viti <zinosat@tiscali.it>
# Filippo Giunchedi <filippo@esaurito.net>
# Giuseppe Sacco <eppesuig@debian.org>
# Lorenzo 'Maxxer' Milesi 
# Renato Gini
# Ruggero Tonelli
# Samuele Giovanni Tonon <samu@linuxasylum.net>
# Stefano Canepa <sc@linux.it>
# Stefano Melchior <stefano.melchior@openlabs.it>
# Milo Casagrande <milo@ubuntu.com>, 2008
#
msgid ""
msgstr ""
"Project-Id-Version: debian-installer\n"
"Report-Msgid-Bugs-To: debian-boot@lists.debian.org\n"
"POT-Creation-Date: 2008-07-31 22:50+0000\n"
"PO-Revision-Date: 2008-08-22 11:36+0200\n"
"Last-Translator: Milo Casagrande <milo@ubuntu.com>\n"
"Language-Team: Italian <debian-l10n-italian@lists.debian.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. Type: title
#. Description
#. Info message displayed when running in rescue mode
#. :sl2:
#: ../rescue-check.templates:2001
msgid "Rescue mode"
msgstr "Modo ripristino"

#. Type: text
#. Description
#. :sl1:
#: ../rescue-mode.templates:1001
msgid "Enter rescue mode"
msgstr "Avviare il modo ripristino"

#. Type: text
#. Description
#. :sl3:
#: ../rescue-mode.templates:2001
msgid "No partitions found"
msgstr "Nessuna partizione trovata"

#. Type: text
#. Description
#. :sl3:
#: ../rescue-mode.templates:2001
msgid ""
"The installer could not find any partitions, so you will not be able to "
"mount a root file system. This may be caused by the kernel failing to detect "
"your hard disk drive or failing to read the partition table, or the disk may "
"be unpartitioned. If you wish, you may investigate this from a shell in the "
"installer environment."
msgstr ""
"Il programma di installazione non ha trovato alcuna partizione e non sarà "
"possibile montare un file system di root. Questo problema può essere causato "
"dal kernel che non riesce a trovare il driver del disco fisso, non riesce a "
"leggere la tabella delle partizioni oppure il disco non è partizionato. È "
"possibile raccogliere maggiori informazioni attraverso una shell all'interno "
"dell'ambiente del programma di installazione."

#. Type: select
#. Description
#. :sl2:
#: ../rescue-mode.templates:3001
msgid "Device to use as root file system:"
msgstr "Device da montare come file system di root:"

#. Type: select
#. Description
#. :sl2:
#: ../rescue-mode.templates:3001
msgid ""
"Enter a device you wish to use as your root file system. You will be able to "
"choose among various rescue operations to perform on this file system."
msgstr ""
"Inserire il device che si vuole utilizzare per il file system principale. "
"Sarà possibile selezionare varie operazioni di ripristino da effettuare su "
"questo file system."

#. Type: error
#. Description
#. :sl3:
#: ../rescue-mode.templates:4001
msgid "No such device"
msgstr "Periferica inesistente"

#. Type: error
#. Description
#. :sl3:
#: ../rescue-mode.templates:4001
msgid ""
"The device you entered for your root file system (${DEVICE}) does not exist. "
"Please try again."
msgstr ""
"Il device scelto per il file system di root (${DEVICE}) non esiste. "
"Riprovare."

#. Type: error
#. Description
#. :sl3:
#: ../rescue-mode.templates:5001
msgid "Mount failed"
msgstr "Montaggio non riuscito"

#. Type: error
#. Description
#. :sl3:
#: ../rescue-mode.templates:5001
msgid ""
"An error occurred while mounting the device you entered for your root file "
"system (${DEVICE}) on /target."
msgstr ""
"Errore montando la periferica scelta con file system di root (${DEVICE}) su /"
"target."

#. Type: error
#. Description
#. :sl3:
#: ../rescue-mode.templates:5001
msgid "Please check the syslog for more information."
msgstr "Controllare il syslog per maggiori informazioni."

#. Type: select
#. Description
#: ../rescue-mode.templates:6001
msgid "Rescue operations"
msgstr "Operazioni di ripristino"

#. Type: error
#. Description
#. :sl3:
#: ../rescue-mode.templates:7001
msgid "Rescue operation failed"
msgstr "Operazione di ripristino non riuscita"

#. Type: error
#. Description
#. :sl3:
#: ../rescue-mode.templates:7001
msgid "The rescue operation '${OPERATION}' failed with exit code ${CODE}."
msgstr ""
"L'operazione di ripristino «${OPERATION}» non è riuscita, codice di uscita "
"${CODE}."

#. Type: text
#. Description
#. :sl2:
#: ../rescue-mode.templates:8001
msgid "Execute a shell in ${DEVICE}"
msgstr "Avviare una shell in ${DEVICE}"

#. Type: text
#. Description
#. :sl2:
#: ../rescue-mode.templates:9001
msgid "Execute a shell in the installer environment"
msgstr "Eseguire una shell nell'ambiente del programma di installazione"

#. Type: text
#. Description
#. :sl2:
#: ../rescue-mode.templates:10001
msgid "Choose a different root file system"
msgstr "Selezionare un file system di root diverso"

#. Type: text
#. Description
#. :sl2:
#: ../rescue-mode.templates:11001
msgid "Reboot the system"
msgstr "Riavviare il sistema"

#. Type: text
#. Description
#. :sl2:
#. Type: text
#. Description
#. :sl2:
#. Type: text
#. Description
#. :sl2:
#: ../rescue-mode.templates:12001 ../rescue-mode.templates:16001
#: ../rescue-mode.templates:17001
msgid "Executing a shell"
msgstr "Avviare una shell"

#. Type: text
#. Description
#. :sl2:
#: ../rescue-mode.templates:12001
msgid ""
"After this message, you will be given a shell with ${DEVICE} mounted on \"/"
"\". If you need any other file systems (such as a separate \"/usr\"), you "
"will have to mount those yourself."
msgstr ""
"Dopo questo messaggio, verrà presentata una shell con ${DEVICE} montato come "
"«/». Se si necessita di altri file system (per esempio un «/usr» separato), "
"dovranno essere montati manualmente."

#. Type: error
#. Description
#. :sl3:
#: ../rescue-mode.templates:13001
msgid "Error running shell in /target"
msgstr "Errore eseguendo la shell in /target"

#. Type: error
#. Description
#. :sl3:
#: ../rescue-mode.templates:13001
msgid ""
"A shell (${SHELL}) was found on your root file system (${DEVICE}), but an "
"error occurred while running it."
msgstr ""
"È stata trovata una shell (${SHELL}) nel file system di root (${DEVICE}), ma "
"si è verificato un errore nell'eseguirla."

#. Type: error
#. Description
#. :sl3:
#: ../rescue-mode.templates:14001
msgid "No shell found in /target"
msgstr "Nessuna shell trovata in /target"

#. Type: error
#. Description
#. :sl3:
#: ../rescue-mode.templates:14001
msgid "No usable shell was found on your root file system (${DEVICE})."
msgstr ""
"Non sono state trovate shell utilizzabili nel file system di root "
"(${DEVICE})."

#. Type: text
#. Description
#. :sl2:
#: ../rescue-mode.templates:15001
msgid "Interactive shell on ${DEVICE}"
msgstr "Shell interattiva su ${DEVICE}"

#. Type: text
#. Description
#. :sl2:
#: ../rescue-mode.templates:16001
msgid ""
"After this message, you will be given a shell with ${DEVICE} mounted on \"/"
"target\". You may work on it using the tools available in the installer "
"environment. If you want to make it your root file system temporarily, run "
"\"chroot /target\". If you need any other file systems (such as a separate "
"\"/usr\"), you will have to mount those yourself."
msgstr ""
"Dopo questo messaggio, verrà presentata una shell con ${DEVICE} montato come "
"«/target» e sarà possibile operare con gli strumenti disponibili nel "
"programma di installazione. Per utilizzarlo temporaneamente come file system "
"di root, eseguire «chroot /target». Se si necessita di altri file system "
"(per esempio un «/usr» separato), dovranno essere montati manualmente."

#. Type: text
#. Description
#. :sl2:
#: ../rescue-mode.templates:17001
msgid ""
"After this message, you will be given a shell in the installer environment. "
"Since the installer could not find any partitions, no file systems have been "
"mounted for you."
msgstr ""
"Dopo questo messaggio, verrà presentata una shell nell'ambiente di "
"installazione. Dato che il programma di installazione non ha rilevato alcuna "
"partizione, nessun file system è stato montato."

#. Type: text
#. Description
#. :sl2:
#: ../rescue-mode.templates:18001
msgid "Interactive shell in the installer environment"
msgstr "Shell interattiva nell'ambiente del programma di installazione"

#. Type: password
#. Description
#. :sl2:
#: ../rescue-mode.templates:19001
msgid "Passphrase for ${DEVICE}:"
msgstr "Passphrase per ${DEVICE}:"

#. Type: password
#. Description
#. :sl2:
#: ../rescue-mode.templates:19001
msgid "Please enter the passphrase for the encrypted volume ${DEVICE}."
msgstr "Inserire la passphrase per il volume cifrato ${DEVICE}."

#. Type: password
#. Description
#. :sl2:
#: ../rescue-mode.templates:19001
msgid ""
"If you don't enter anything, the volume will not be available during rescue "
"operations."
msgstr ""
"Se non si imposta nulla, il volume non sarà disponibile durante le "
"operazioni di recupero."
