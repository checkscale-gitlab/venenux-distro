Debian Installation Manual - notes for translation
--------------------------------------------------

This file explains how to translate the manual using .po files.
This is now officially supported.

!!! For general information on translating the manual, please see the
!!! translations.txt file.

Note: if you happen to modify this file, please don't forget to
      update the trailing changelog

Background information
======================
There are currently two toolsets that support translating XML files using .po
files: KDE's poxml and po4a. We have chosen poxml as that gives better context
(it does not split paragraphs) for translation.
The downside of the poxml tools is that they require the .xml files to be
"proper" XML. Because some of the individual files in the English
original are not proper XML, we've had to "integrate" the files per chapter.

This means that instead of translating about 160 small files, you will be
translating about 15 large files.

Current limitations
===================
Building from PO files is currently not supported in the Sarge branch of d-i's
SVN repository. This is however not a real problem as the XML files created
from the PO files are used to synchronise the Sarge branch from trunk.
Therefore translators should not worry about this.

Requirements
============
The following packages need to be installed on your system:
- gettext
- poxml
- gawk

If you also want to be able to build the manual, you also need to install the
build dependencies listed in build/README.

Available tools
===============
You will not be using poxml itself, but rather some scripts that call the
poxml tools.

- ./scripts/merge_xml  : merges the small .xml files into larger files
- ./scripts/update_pot : creates (new) .pot templates based on integrated
                         English .xml files
- ./scripts/revert_pot : used to revert changes in POT files to avoid
                         accidental commits
- ./scripts/update_po  : updates your .po files when there are changes in the
                         English text (the .pot templates)
- ./scripts/create_xml : creates translated .xml files from the merged English
                         .xml files and the translated .po files
- ./scripts/create_po  : used to convert an existing .xml translation to
                         using .po files
- ./scripts/set_untranslated : used for the conversion of existing translations

All scripts should be run from the ./manual directory and take the language as
parameter.


Starting a new translation
==========================
This procedure should only be used if there is no existing translation using
XML files for your language! See the section on conversion below if you want
to switch from translating XML files to using PO files.

The template files for the manual are available in ./po/pot, so all you need
to do is create a new directory for your language (using the official ISO
code for your language) and copy the .pot file you want to translate to a
.po file in that directory.

Example for a Dutch translation:
  - mkdir ./po/nl
  - cp ./po/pot/bookinfo.pot ./po/nl/bookinfo.po
  - <po editor> ./po/nl/bookinfo.po

Use your favorite PO file editor (like kbabel) to translate the .po file.
You don't need to copy all POT files straight away, just copy each one as you
start translating them.

  
Committing your translation
===========================
The translation can be committed to d-i's SVN repository using:
  1. svn ci ./po/<your language> -m "<some comment describing the changes>"

If this is the first time you commit the translation of a file, you will need
to run the 'svn add' command on the new files before you do 'svn ci'.
Tip: use the commands 'svn st ' and 'svn diff' before you commit your changes
to check what you are committing before you run the 'svn ci' command.


Updating your translation
=========================
The PO files for your language and the templates for the manual are updated
regularly when there are changes in the original English XML files.
This is done during the "daily" builds (currently done by Frans Pop); you
should not have to update your PO files yourself.
Changed strings will be marked 'fuzzy' and new strings will be untranslated.

After this your PO files in the ./po directory should be updated and you can
update the translation with your favorite PO file editor. To commit the
changes, follow again the procedure described in the previous section.

To update the POT and PO files manually, use the following commands:
  1. svn up
  2. ./scripts/merge_xml en
  3. ./scripts/update_pot
  4. ./scripts/update_po <your language>

NOTE
Doing this manually may result in SVN conflicts when you next 'svn up' with
the repository. The preferred method is to just wait until your PO files are
updated for you.


Building the manual yourself
============================
If you want to build a HTML version (or other format) of the manual yourself,
you will have to generate the XML for your language before you can run the
buildone.sh script. This can be useful, for example to debug build errors.

Use the following commands:
  1. ./scripts/merge_xml en
  2. ./scripts/update_pot
  3. ./scripts/update_po <your language>
  4. ./scripts/revert_pot
  5. ./scripts/create_xml <your language>

Steps 2 and 3 are necessary because the English XML files may have been
updated since the last daily build of the manual, so the POT files and your
PO files may be out-of-date. Step 4 makes sure you do not accidentally
update the POT files in the repository. They are not needed to create the
XML files or build the manual.

After that you can run the buildone.sh script as documented in the README in
the ./build directory. If you run buildone.sh without first generating the
XML files, you will either get an error message "unknown language" or build
from an outdated version of the XML files.


Converting an existing translation from .xml files to .po files
===============================================================
It is possible to convert an existing translation of the manual (where the
individual XML files have been translated) to using PO files.
There are two important conditions:
- the translation must be up-to-date with the English XML files;
- the translation must have same structure as the English XML files (i.e.
  the translation must have the same chapters, sections, paragraphs, tables,
  examples, etc; you can have no 'extra' paragraphs in your translation).

The first condition does not mean that everything needs to be translated!

The conversion is fairly difficult and some checks need to be done after some
of the steps. Full instructions for converting a translation can be found at:
http://lists.debian.org/debian-boot/2007/07/msg00814.html.

Note that conversions are only done rarely and that the script used may
need updating.

--------------------------------------------------------------------------

If you find any errors in this file, please fix them.

--------------------------------------------------------------------------
Changelog:

2007-07-31 Frans Pop
 - Add link to full conversion instructions

2005-04-19 Frans Pop
 - Finally rewrite this file following official implementation of PO file
   translation over a month ago...
 - Reflect the changed file structure in the ./po directory

2005-02-15 Frans Pop
 - Corrected the location where generated XML files are written

2005-02-14 Frans Pop
 - Corrected an error in the name of one script
 - Clarified the difference between starting a new translation and
   converting an existing one

2005-02-12 Frans Pop
 - First version
