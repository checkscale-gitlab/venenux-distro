# VenenuX contruir facilmente una iso live

Construccion de OS Live: imagen live debian/devuan VENENUX para el uso diario
For english speakers [README-en.md](README-en.md) but take in consideration we dont care english.

Es un sistema operativo listo para usar que incluye el sistema y 
programa para gestion remota como el componente mas critico para asistir los usuarios.

## Introduccion y Descargas

Ejecutando lo que esta en este repositorio **creara un disco o ISO con un sistema operativo, 
listo para usar, SI, LISTO PARA USAR, NO REQUIERE INSTALAR pero puede instalarse**, el archivo 
que produce lo "graba en un CD/DVD en blanco" o "puede grabarlo en un USBdrive" y se usa en la pc, 
sin borrar su sistema operativo instalado, pero con la opcion de sustituirlo si quiere.

**No es necesario todo esto, descargelo ya listo para usar des nuestra pagina
web: http://vegnuli.sourceforge.net/** Donde dice "VenenuX OS"!

[![Descargar VeGNUli ISO](https://a.fsdn.com/con/app/sf-download-button)](https://sourceforge.net/projects/vegnuli/files/VenenuX-1.0/)

## Guia rapida

VenenuX tiene dos sabores, la version server y la version escritorio.

VenenuX se basa en debian pero se orienta a funcionabilidad, y uso optimo de los recursos, 
emplea el escritorio MATE en equipos modernos y LXDE para equipos de bajo rendimiento.

Las versiones de VenenuX son siempre las mismas que Debian, intentan ser lo mas cercanas 
pero mantienen en ciertos paquetes mejoras y modificaciones enfocadas a performance y ajustes, 
para que siguan funcionando. Adicional se ajustan a la region de latinoamerica.

## VenenuX escritorio

Este sistema se creo utilizando los paquetes VenenuX y el sistema Live-build debian, 

Con esta ISO, se graba tanto en USB como en CD/DVD y simplemente se arranca en un computador, 
que es el proceso que los mortales ignorantes conocemos? obviamente si.

Actualmente el repositorio tiene ramas, la rama principal tiene artifacts y documentos 
generales, pero para cada tipo de venenux hay una rama espoecifica, asi que debera saber 
usar git para ello.

## FAQ: preguntas y repuestas frecuentes

#### **P: Como la puedo usar?**

Con esta ISO, se graba tanto en USB como en CD/DVD y simplemente se arranca en un computador, 
para esto es facilisimo utilize Balena Echer Electron y listo, busquelo en google y ya.
Una vez terminado o grabado se arranca desde la computadora y se usa sin instalar (con opcion a ello).

#### **P: Puedo usarlo como sistema habitual?**

Si, tiene una gran cantidad de programas, inclusive tiene un completo entorno de desarrollo, 
para mas info rapida de que tiene vistar [venenux-contenido-livebuild.md](venenux-contenido-livebuild.md)

# Acerca de live build y VenenuX

Se recomienda visite el proyecto http://gitlab.com/venenux/venenux-distro para informacion, 
cada version y tipo de live iso tiene una rama distinta en el git.. debera saber un minimo de git, 
el porque se incluyen y porque no. VenenuX es un sistema operativo derivado de Debian GNU/Linux 
enfocado en multimedia, no solo profesional, sino tambien para el usuario que lo desea ver todo.

#### Guia resumida live-build VenenuX

live build crea una estructura con la cual se puede alterar el resultado de el live cd, 
aqui mostrada, mas abajo la documentacion especifica de esto y como hacer tu propia iso:

```
├── lb-scriptpropio        <--------- script creado con la invocacion a lb config
└── config
    ├── archives           <--------- xxxx.list.yyyy sources.list de repositorios ajenos
    ├── bootloaders
    │   └── isolinux       <--------- contenido COMPLETO base de el cd/img resultante, incluyendi menu.cfg
    ├── hooks
    ├── includes.binary    <--------- archivos extra que terminaran incluidos en el cd o imagen, en raiz
    ├── includes.chroot
    │   ├── etc
    │   │   ├── lightdm    <--------- queremos meter lxdm pero systemd no nos deja, versiones 0.x usan lxdm/slim
    │   │   ├── live       <--------- live user setups scripts
    │   │   └── skel       <--------- skeleton /home directory, plantilla de como home se preconfigura
    │   └── usr
    │       ├── bin       <--------- colocar aqui cualquier programa precompilado o directo
    │       ├── lib       <--------- colocar aqui cualquier precompilada precompilado o directo
    │       ├── local
    │       └── share       <--------- colocar aqui cualquier oro archiovo e ira a share en el live
    ├── includes.installer <--------- installer preseed.cfg file and logo/theme update for the graphical installer
    │   └── usr
    │       └── share       <--------- colocar aqui cualquier oro archiovo e ira a share en el live
    │           └── graphics  <--------- logo/theme update for the graphical installer
    ├── package-lists      <--------- listado de nombre of packages to be installed, uno por linea
    └── packages.chroot    <--------- otros packages to be installed manualmente o por archivo deb
```

# AUTHORS

Este trabajo es **consecuencia de la complejidad que es no solo el instalar, sino el poner a punto 
tener un sistema operativo e instalar** con el mismo nivel de conocimiento requerido.

VenenuX es una creacion de "master vitronic" @vitronic http://gitlab.com/vitronic

VenenuX-ospos es una creacion de su co-creador PICCORO Lenz McKAY http://gitea.com/mckaygerhard

https://vegnuli.sourceforge.io/

# Vea Tambien:

* [CONTRIBUTING.md](CONTRIBUTING.md) : Reglas de conducta (inglesh)
* [CONTRIBUTING.es.md](CONTRIBUTING.es.md) : Reglas de conducta (español)
* [README.packages.md](README.packages.md) : package list inclusion.

